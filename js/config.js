var latlng = [13.110826, 101.889862];
var config = {
    map: L.map('map').setView(latlng, 6),
    layer: {
        tile: {
            options: {
                maxZoom: 24,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
            },
            googleRoad: L.tileLayer('http://{s}.google.com/vt/lyrs=m&hl=th&x={x}&y={y}&z={z}', this.options),
            googleHybrid: L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&hl=th&x={x}&y={y}&z={z}', this.options),
            googleSat: L.tileLayer('http://{s}.google.com/vt/lyrs=s&hl=th&x={x}&y={y}&z={z}', this.options),
            googleTerrain: L.tileLayer('http://{s}.google.com/vt/lyrs=p&hl=th&x={x}&y={y}&z={z}', this.options),
            osm: new L.TileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png')
        },
        options: {
            container_width: "300px",
            group_maxHeight: "80px",
            exclusive: false,
            collapsed: true,
            position: 'topright'
        }
    },
};
config.layer.tile.googleTerrain = L.tileLayer('http://{s}.google.com/vt/lyrs=p&hl=th&x={x}&y={y}&z={z}', config.layer.tile.options).addTo(config.map);

// set moment local 
moment.locale('th');
// prototype round number
Number.prototype.round = function (places) {
    return +(Math.round(this + "e+" + places) + "e-" + places);
}
readAble = function (data, roundLength, noValueText) {
    // null and undefined case
    if( !data && data !== 0 ) {
        return noValueText || 'ไม่มีข้อมูล'
    }
    // number case
    else if( typeof data === "number" ) return data.round( roundLength || 2 );
    else return data
}
// defined jquery htmlReadAble  
$.fn.htmlReadAble = function () {
    return $.fn.html.apply(this, [
        readAble.apply(this, arguments)
    ]);
}

window.config.onDevelopment = window.location.origin === "http://localhost:9999"
window.config.system = {
    "development": {
        "server": "http://localhost:5001",
        "console": true,
        "rainfall": {
            "icon": {
                "rain": 'img/rain.png',
                "sunny": 'img/sunny.png'
            },
            "menu": {
                "groupName": 'ข้อมูลน้ำฝน',
                "sub_menu": {
                    "สสนก": {
                        "source_name": 'rainfall_nhc',
                        "anch_id": 19012
                    },
                    "กรมทรัพยากรน้ำ": {
                        "source_name": 'rainfall_dwr',
                        "anch_id": 9006
                    },
                    "กรมอุตุนิยมวิทยา": {
                        "source_name": 'rainfall_tmd',
                        "anch_id": 11004
                    },
                    "สำนักการระบายน้ำ กรุงเทพมหานคร": {
                        "source_name": 'rainfall_dds',
                        "anch_id": 15009
                    },
                    "คาดการณ์ฝน": {
                        "source_name": 'rainfall_predict'
                    }
                }
            }
        },
        "water_nhc": {
            "icon": ['img/staff_gauge.png', 'img/red-staff-gauge.png', 'img/blue-staff-gauge.png', 'img/green-staff-gauge.png', 'img/yellow-staff-gauge.png', 'img/brown-staff-gauge.png'],            
            "status": ["น้ำล้นตลิ่ง", "น้ำมาก", "น้ำปกติ", "น้ำน้อย", "น้ำน้อยวิกฤติ"],
            "icon_size": [14, 24]
        }
    },
    "production": {
        "server": "http://hydrologydb.rid.go.th:5001",
        "console": false,
        "rainfall": {
            "icon": {
                "rain": 'img/rain.png',
                "sunny": 'img/sunny.png'
            },
            "menu": {
                "groupName": 'ข้อมูลน้ำฝน',
                "sub_menu": {
                    "สสนก": {
                        "source_name": 'rainfall_nhc',
                        "anch_id": 19012
                    },
                    "กรมทรัพยากรน้ำ": {
                        "source_name": 'rainfall_dwr',
                        "anch_id": 9006
                    },
                    "กรมอุตุนิยมวิทยา": {
                        "source_name": 'rainfall_tmd',
                        "anch_id": 11004
                    },
                    "สำนักการระบายน้ำ กรุงเทพมหานคร": {
                        "source_name": 'rainfall_dds',
                        "anch_id": 15009
                    },
                    "คาดการณ์ฝน": {
                        "source_name": 'rainfall_predict'
                    }
                }
            }
        },
        "water_nhc": {
            "icon": ['img/staff_gauge.png', 'img/red-staff-gauge.png', 'img/blue-staff-gauge.png', 'img/green-staff-gauge.png', 'img/yellow-staff-gauge.png', 'img/brown-staff-gauge.png'],
            "status": ["น้ำล้นตลิ่ง", "น้ำมาก", "น้ำปกติ", "น้ำน้อย", "น้ำน้อยวิกฤติ"],
            "icon_size": [14, 24]
        }
    }
}[window.config.onDevelopment ? 'development' : 'production'];

if (window.config.onDevelopment) {
    setTimeout(function () {
        // $('#leaflet-control-accordion-layers-7 input').eq(1).click();
        $('#leaflet-control-accordion-layers-6 input').eq(3).click();
        // setTimeout(function () {
        //     $('img[src="' + window.config.system.water_nhc.icon + '"]')[0].click();
        //     // setTimeout(function() {
        //     //     $('a[href$="tab_graph"]').click();
        //     // }, 100);
        // }, 5000);
    }, 1000);
}

$(function () {

    // check server down
    $.ajax({
        type: "get",
        url: window.config.system.server+"/ping",
        timeout: 1000,
        success: function () { 
            window.config.system.ping = true;
        },
        error: function () {  
            window.config.system.ping = true;
            window.config.system.server = 'http://52.74.248.227';
        }
    });
});