$(document).ready(function() {
    var map = config.map;
    var baseMaps = [{
        groupName: "แผนที่พื้นฐาน",
        expanded: false,
        layers: {
            "Terrain": config.layer.tile.googleTerrain,
            "Road Map": config.layer.tile.googleRoad,
            "Hybrid": config.layer.tile.googleHybrid,
            "OpenStreetMap": config.layer.tile.osm
        }
    }];
    var control = L.Control.styledLayerControl(baseMaps, config.overlays, config.layer.options);
    map.addControl(control);
    var grid = L.gridLayer({
        attribution: 'RID',
    });
    grid.createTile = function(coords) {
        var tile = L.DomUtil.create('div', 'tile-coords');
        tile.innerHTML = [coords.x, coords.y, coords.z].join(', ');
        return tile;
    };
    map.addLayer(grid);
});