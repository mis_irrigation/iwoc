$(document).ready(function() {
    animateSidebar();
    $(window).resize(function() {
        sizeLayerControl();
    });

    $(document).on("click", ".feature-row", function(e) {
        $(document).off("mouseout", ".feature-row", clearHighlight);
        sidebarClick(parseInt($(this).attr("id"), 10));
        $('#panel-button-back').removeClass('hide');
    });

    $("#btn_sidebar_back").click(function() {
        map.setView(latlng, 6);
        $('#panel-button-back').addClass('hide');
    });

    if (!("ontouchstart" in window)) {
        $(document).on("mouseover", ".feature-row", function(e) {});
    }

    $(document).on("mouseout", ".feature-row", clearHighlight);

    $("#about-btn").click(function() {
        $("#aboutModal").modal("show");
        $(".navbar-collapse.in").collapse("hide");
        return false;
    });

    $("#full-extent-btn").click(function() {
        map.fitBounds(boroughs.getBounds());
        $(".navbar-collapse.in").collapse("hide");
        return false;
    });

    $("#legend-btn").click(function() {
        $("#legendModal").modal("show");
        $(".navbar-collapse.in").collapse("hide");
        return false;
    });

    $("#login-btn").click(function() {
        $("#loginModal").modal("show");
        $(".navbar-collapse.in").collapse("hide");
        return false;
    });

    $("#list-btn").click(function() {
        animateSidebar();
        return false;
    });

    $("#nav-btn").click(function() {
        $(".navbar-collapse").collapse("toggle");
        return false;
    });

    $("#sidebar-toggle-btn").click(function() {
        animateSidebar();
        return false;
    });

    $("#sidebar-hide-btn").click(function() {
        animateSidebar();
        return false;
    });
    /* Highlight search box text on click */
    $("#searchbox").click(function() {
        $(this).select();
    });

    /* Prevent hitting enter from refreshing the page */
    $("#searchbox").keypress(function(e) {
        console.log('ssss');
        if (e.which == 13) {
            e.preventDefault();
        }
    });


    //--- On Show Modal --> Resize, Draggable -----------------
    $('#iwocModal').on('shown.bs.modal', function(e) {
        // alert(222);
        $(".modal-content:not('.modal-flowwater, .modal-weather, .modal-waterquality')").resizable({
            //alsoResize: ".modal-dialog",
            // minHeight: 300,
            // minWidth: 300
        });
        $(".modal-dialog:not('.modal-flowwater, .modal-weather')").draggable({
            handle: ".modal-header"
        });

        $(e.currentTarget).find('.modal-body').css({
            'max-height': '100%'
        });
        //--- Replace Button Close Modal ---
        // console.log($(e.currentTarget))
        $(e.currentTarget).find('button.close').replaceWith('<div class="pull-right"> <button id="btn_fullscreen" type="button" class="btn_modal glyphicon glyphicon-fullscreen" title="Full Screen" onclick="fullscreen(this); "></button>    <button id="btn_maximize" type="button" class="btn_modal glyphicon glyphicon-resize-full  " title="Maximize" onclick="maximize(this); "></button>          <button type="button" class="btn_modal glyphicon glyphicon-remove" data-dismiss="modal" aria-hidden="true" title="Close" onclick="$(\'#iwocModal\').fullScreen(false); "></button> </div>');


        //Event Url Param 
        //Url Param Flowwater
        var _basin = findGetParameter("basin");
        if (_basin == "เจ้าพระยา") {
            iframe_flowwater.location.href = "js/layers/flowwater/chaopraya/";
        } else if (_basin == "ชี") {
            iframe_flowwater.location.href = "js/layers/flowwater/chee/";
        } else if (_basin == "มูล") {
            iframe_flowwater.location.href = "js/layers/flowwater/moon/";
        } else if (_basin == "สะแกกรัง") {
            iframe_flowwater.location.href = "js/layers/flowwater/sagaegrung/";
        } else if (_basin == "ตาปี") {
            iframe_flowwater.location.href = "js/layers/flowwater/tapee/";
        }

        //Modal Width UrlParam
        var _viewModal = (findGetParameter("viewModal") || findGetParameter("viewmodal"));
        if (_viewModal == "fullscreen") {
            $("#btn_fullscreen").click();
        } else if (_viewModal == "maximize") {
            $("#btn_maximize").click();
        }

        //ข้อมูลการเพาะปลูกทั้งประเทศ
        if (findGetParameter("click") == "45") {
            //alert(111);
            $('#dlYear').multiselect('select', '2559');
            $('a[href="#featuretab-rio"]').click();
            $("#btShowRIOTable").click();
        }


    });
    //--- END Show Modal --> Resize, Draggable -----------------


}); //ready

function maximize(_this) {
    var _iwocModal = $('#iwocModal');
    if ($(_this).attr('data-maximize') != 1) {
        $(_this).attr('data-maximize', 1);
        _iwocModal.find('.modal-dialog').addClass('modal-fullscreen').removeAttr('style');
        _iwocModal.find('.modal-content').removeAttr('style');
    } else {
        $(_this).attr('data-maximize', 0);
        _iwocModal.find('.modal-dialog').removeClass('modal-fullscreen').removeAttr('style');
        _iwocModal.find('.modal-content').removeAttr('style');
    }
}

function fullscreen(_this) {
    var _iwocModal = $('#iwocModal');
    if ($(_this).attr('data-fullscreen') != 1) { //Show FullScreen
        $(_this).attr('data-fullscreen', 1);
        _iwocModal.find('.modal-dialog').addClass('modal-fullscreen').removeAttr('style');
        _iwocModal.find('.modal-content').removeAttr('style');
        _iwocModal.fullScreen(true);
    } else { //Exit FullScreen
        $(_this).attr('data-fullscreen', 0);
        _iwocModal.find('.modal-dialog').removeClass('modal-fullscreen').removeAttr('style');
        _iwocModal.find('.modal-content').removeAttr('style');
        _iwocModal.fullScreen(false);
    }
}