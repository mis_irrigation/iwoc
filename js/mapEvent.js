var check_overlay_big = 0;
var check_overlay_bigteleamr = 0;
var check_overlay_bigtelethachin = 0;
var check_overlay_middle = 0;
$(document).ready(function() {
    var map = config.map;
    map.on('overlayadd', function(eventLayer) {
        function cb_default(err, res) {
            if (err) return;
            if (document.body.clientWidth <= 767) {
                $("#sidebar").hide();
            } else {
                $("#sidebar").show();
            }
            map.invalidateSize();
            syncSidebar();
        }
        if (eventLayer.name === 'สภาพน้ำในอ่างเก็บน้ำขนาดใหญ่') {
            check_overlay_big = 1;
            source.bigDam.getLayerData(function(res) {
                if (res > 0) {
                    source.bigDam.legend.addTo(map);
                    if (document.body.clientWidth <= 767) {
                        $("#sidebar").hide();
                    } else {
                        $("#sidebar").show();
                    }
                    map.invalidateSize();
                    syncSidebar();
                }
            });
        } else if (eventLayer.name === 'โทรมาตรขนาดใหญ่ลุ่มน้ำเจ้าพระยา') {
            check_overlay_bigteleamr = 1;
            source.bigteleamr.getBigTeleAMRData(function(res) {
                if (res > 0) {
                    source.bigteleamr.legend.addTo(map);
                    if (document.body.clientWidth <= 767) {
                        $("#sidebar").hide();
                    } else {
                        $("#sidebar").show();
                    }
                    map.invalidateSize();
                    syncSidebar();
                }
            });
        } else if (eventLayer.name === 'โทรมาตรขนาดใหญ่ลุ่มน้ำท่าจีน') {
            check_overlay_bigtelethachin = 1;
            source.bigtelethachin.getLayerData(function(res) {
                if (res > 0) {
                    //    source.bigtelethachin.legend.addTo(map);
                    if (document.body.clientWidth <= 767) {
                        $("#sidebar").hide();
                    } else {
                        $("#sidebar").show();
                    }
                    map.invalidateSize();
                    syncSidebar();
                }
            });
        } else if (eventLayer.name === 'ข้อมูลการเพาะปลูกทั้งประเทศ') {
            source.wuse.getLayerData(function(res) {
                if (res > 0) {
                    //    source.bigtelethachin.legend.addTo(map);
                    if (document.body.clientWidth <= 767) {
                        $("#sidebar").hide();
                    } else {
                        $("#sidebar").show();
                    }
                    map.invalidateSize();
                    //syncSidebar();
                }
            });

            var _ele_chk = $("div.menu-item-checkbox").find("span:contains('ข้อมูลการเพาะปลูกทั้งประเทศ')");
            if (_ele_chk.prev("input[type='checkbox']").prop('checked') == true) { //Check checked
                _ele_chk.prev("input[type='checkbox']").trigger("click");
            }
        } else if (eventLayer.name === 'ข้อมูลการเพาะปลูกลุ่มน้ำแม่กลอง') {
            source.wusemaeklong.getLayerData(function(res) {
                if (res > 0) {
                    //    source.bigtelethachin.legend.addTo(map);
                    if (document.body.clientWidth <= 767) {
                        $("#sidebar").hide();
                    } else {
                        $("#sidebar").show();
                    }
                    map.invalidateSize();
                    // syncSidebar();
                }
            });

            var _ele_chk = $("div.menu-item-checkbox").find("span:contains('ข้อมูลการเพาะปลูกลุ่มน้ำแม่กลอง')");
            if (_ele_chk.prev("input[type='checkbox']").prop('checked') == true) { //Check checked
                _ele_chk.prev("input[type='checkbox']").trigger("click");
            }
        } else if (eventLayer.name === 'ข้อมูลการเพาะปลูกลุ่มน้ำเจ้าพระยา') {
            source.wusechaopraya.getLayerData(function(res) {
                if (res > 0) {
                    //    source.bigtelethachin.legend.addTo(map);
                    if (document.body.clientWidth <= 767) {
                        $("#sidebar").hide();
                    } else {
                        $("#sidebar").show();
                    }
                    map.invalidateSize();
                    // syncSidebar();
                }
            });

            var _ele_chk = $("div.menu-item-checkbox").find("span:contains('ข้อมูลการเพาะปลูกลุ่มน้ำเจ้าพระยา')");
            if (_ele_chk.prev("input[type='checkbox']").prop('checked') == true) { //Check checked
                _ele_chk.prev("input[type='checkbox']").trigger("click");
            }
        } else if (eventLayer.name === 'Tele200') {
            getTele200Data(function(res) {
                if (res > 0) {
                    if (document.body.clientWidth <= 767) {
                        $("#sidebar").hide();
                    } else {
                        $("#sidebar").show();
                    }
                    map.invalidateSize();
                    syncSidebar();
                }
            });
        } else if (eventLayer.name === 'Tele127') {
            getTele127Data(function(res) {
                if (res > 0) {
                    if (document.body.clientWidth <= 767) {
                        $("#sidebar").hide();
                    } else {
                        $("#sidebar").show();
                    }
                    map.invalidateSize();
                    syncSidebar();
                }
            });
        }
        else if (eventLayer.name === 'เหนือเขื่อนแควน้อยบำรุงแดน') {
            source.kwainoidam.getkwainoidamLayer(function(res) {
                if (res > 0) {
                    if (document.body.clientWidth <= 767) {
                        $("#sidebar").hide();
                    } else {
                        $("#sidebar").show();
                    }
                    map.invalidateSize();
                    syncSidebar();
                }
            });
        }
        else if (eventLayer.name === 'สภาพน้ำในอ่างเก็บน้ำขนาดกลาง') {
            check_overlay_middle = 1;
            getLayerMiddleData(function(res) {
                if (res > 0) {
                    source.bigDam.legend.addTo(map);
                    if (document.body.clientWidth <= 767) {
                        $("#sidebar").hide();
                    } else {
                        $("#sidebar").show();
                    }
                    map.invalidateSize();
                    syncSidebar();
                }
            });
        } else if (eventLayer.name === 'ประตูระบายน้ำ') {
            getLayerfloodGateData(function(res) {
                if (res > 0) {
                    source.floodGate.legend.addTo(map);
                    if (document.body.clientWidth <= 767) {
                        $("#sidebar").hide();
                    } else {
                        $("#sidebar").show();
                    }
                    map.invalidateSize();
                    syncSidebar();
                }
            });
        } else if (Object.keys(window.config.system.rainfall.menu.sub_menu).indexOf(eventLayer.name) !== -1) {
            var sub_menu = window.config.system.rainfall.menu.sub_menu[eventLayer.name];
            source.rainfall.getData(sub_menu.source_name, sub_menu.anch_id, cb_default);
        } else if (eventLayer.name === 'ข้อมูลระดับน้ำ (สสนก.)') {
            source.water_nhc.getData(cb_default);
            source.water_nhc.legend.addTo(map);
        } else if (eventLayer.name === 'ผังน้ำ') {

            //console.log(eventLayer.name);
            $.get("js/layers/flowwater/modal.html")
                .done(function(data) {
                    $('#iwocModal').html(data);
                    $("#iwocModal").modal("show");
                });
            var _ele_chk = $("div.menu-item-checkbox").find("span:contains('ผังน้ำ')");
            if (_ele_chk.prev("input[type='checkbox']").prop('checked') == true) { //Check checked
                _ele_chk.prev("input[type='checkbox']").trigger("click");
            }

            /*   if (document.body.clientWidth <= 767) {
                 $("#sidebar").hide();
             } else {
                 $("#sidebar").show();
             }
             map.invalidateSize();
             syncSidebar();*/
        } else if (eventLayer.name === 'ข้อมูลอุตุนิยมวิทยา ทุก 3 ชม.') {
            getweather3HoursLayer(function (res) {
                if (res > 0) {
                    source.weather3Hours.legend.addTo(map);
                    if (document.body.clientWidth <= 767) {
                        $("#sidebar").hide();
                    } else {
                        $("#sidebar").show();
                    }
                    map.invalidateSize();
                    syncSidebar();
                }
            });
        } else if (eventLayer.name === 'แผนที่วิเคราะห์เส้นทางพายุ') {
            $.get("js/layers/weather/modalmapstorm.html",
                function(data) {
                    if (data != null) {
                        $('#iwocModal').html(data);
                        $("#featuretab-title").html('ภูมิอากาศ <i class="glyphicon glyphicon-menu-right"></i> แผนที่วิเคราะห์เส้นทางพายุ');
                        $("#iwocModal").modal("show");
                    }
                });

            var _ele_chk = $("div.menu-item-checkbox").find("span:contains('แผนที่วิเคราะห์เส้นทางพายุ')");
            if (_ele_chk.prev("input[type='checkbox']").prop('checked') == true) { //Check checked
                _ele_chk.prev("input[type='checkbox']").trigger("click");
            }

        } else if (eventLayer.name === 'สภาพภูมิอากาศ') {
            $.get("js/layers/weather/modalmapwindspeedrain.html",
                function(data) {
                    if (data != null) {
                        $('#iwocModal').html(data);
                        $("#featuretab-title").html('ภูมิอากาศ <i class="glyphicon glyphicon-menu-right"></i> สภาพภูมิอากาศ');
                        $("#iwocModal").modal("show");
                    }
                });

            var _ele_chk = $("div.menu-item-checkbox").find("span:contains('สภาพภูมิอากาศ')");
            if (_ele_chk.prev("input[type='checkbox']").prop('checked') == true) { //Check checked
                _ele_chk.prev("input[type='checkbox']").trigger("click");
            }
        } else if (eventLayer.name === 'ลุ่มน้ำท่าจีน') {
            source.waterquality.getthachinLayer(function(res) {
                if (res > 0) {
                    if (document.body.clientWidth <= 767) {
                        $("#sidebar").hide();
                    } else {
                        $("#sidebar").show();
                    }
                    map.invalidateSize();
                    syncSidebar();
                }
            });
        } else if (eventLayer.name === 'ลุ่มน้ำบางปะกง') {
            source.waterquality.getbangpakongLayer(function(res) {
                if (res > 0) {
                    if (document.body.clientWidth <= 767) {
                        $("#sidebar").hide();
                    } else {
                        $("#sidebar").show();
                    }
                    map.invalidateSize();
                    syncSidebar();
                }
            });
        } else if (eventLayer.name === 'ลุ่มน้ำแม่กลอง') {
            source.waterquality.getmaeklongLayer(function(res) {
                if (res > 0) {
                    if (document.body.clientWidth <= 767) {
                        $("#sidebar").hide();
                    } else {
                        $("#sidebar").show();
                    }
                    map.invalidateSize();
                    syncSidebar();
                }
            });
        } else if (eventLayer.name === 'เชื่อมโยงกับหน่วยงานกรมควบคุมมลพิษ1') {
            $.get("js/layers/waterquality/modalagencylink_pcd1.html",
                function (data) {
                    if (data != null) {
                        $('#iwocModal').html(data);
                        $("#featuretab-title").html('คุณภาพน้ำ <i class="glyphicon glyphicon-menu-right"></i> เชื่อมโยงกับหน่วยงานกรมควบคุมมลพิษ1');
                        $("#iwocModal").modal("show");
                    }
                });

            var _ele_chk = $("div.menu-item-checkbox").find("span:contains('เชื่อมโยงกับหน่วยงานกรมควบคุมมลพิษ1')");
            if (_ele_chk.prev("input[type='checkbox']").prop('checked') == true) { //Check checked
                _ele_chk.prev("input[type='checkbox']").trigger("click");
            }
        } else if (eventLayer.name === 'เชื่อมโยงกับหน่วยงานกรมควบคุมมลพิษ2') {
            $.get("js/layers/waterquality/modalagencylink_pcd2.html",
                function (data) {
                    if (data != null) {
                        $('#iwocModal').html(data);
                        $("#featuretab-title").html('คุณภาพน้ำ <i class="glyphicon glyphicon-menu-right"></i> เชื่อมโยงกับหน่วยงานกรมควบคุมมลพิษ2');
                        $("#iwocModal").modal("show");
                    }
                });

            var _ele_chk = $("div.menu-item-checkbox").find("span:contains('เชื่อมโยงกับหน่วยงานกรมควบคุมมลพิษ2')");
            if (_ele_chk.prev("input[type='checkbox']").prop('checked') == true) { //Check checked
                _ele_chk.prev("input[type='checkbox']").trigger("click");
            }
        } else if (eventLayer.name === 'เชื่อมโยงกับหน่วยงานการประปานครหลวง') {
            $.get("js/layers/waterquality/modalagencylink_mwa.html",
                function (data) {
                    if (data != null) {
                        $('#iwocModal').html(data);
                        $("#featuretab-title").html('คุณภาพน้ำ <i class="glyphicon glyphicon-menu-right"></i> เชื่อมโยงกับหน่วยงานการประปานครหลวง');
                        $("#iwocModal").modal("show");
                    }
                });

            var _ele_chk = $("div.menu-item-checkbox").find("span:contains('เชื่อมโยงกับหน่วยงานการประปานครหลวง')");
            if (_ele_chk.prev("input[type='checkbox']").prop('checked') == true) { //Check checked
                _ele_chk.prev("input[type='checkbox']").trigger("click");
            }
        } else if (eventLayer.name === 'เขื่อนขนาดใหญ่+ข้อมูลการตรวจวัด') {
            getLayerbigdamBehData(function(res) {
                if (res > 0) {
                    if (document.body.clientWidth <= 767) {
                        $("#sidebar").hide();
                    } else {
                        $("#sidebar").show();
                    }
                    map.invalidateSize();
                    syncSidebar();
                }
            });
        } else if (eventLayer.name === 'เขื่อนขนาดกลาง+ข้อมูลการตรวจวัด') {
            getLayermiddamBehData(function(res) {
                if (res > 0) {
                    if (document.body.clientWidth <= 767) {
                        $("#sidebar").hide();
                    } else {
                        $("#sidebar").show();
                    }
                    map.invalidateSize();
                    syncSidebar();
                }
            });
        } else if (eventLayer.name === 'เขื่อนขนาดเล็ก') {
            getLayersmalldamBehData(function(res) {
                if (res > 0) {
                    source.smalldamBeh.legend.addTo(map);
                    if (document.body.clientWidth <= 767) {
                        $("#sidebar").hide();
                    } else {
                        $("#sidebar").show();
                    }
                    map.invalidateSize();
                    syncSidebar();
                }
            });
        } else if (eventLayer.name === 'CCTV') {
            getLayerCCTVData(function(res) {
                if (res > 0) {

                    if (document.body.clientWidth <= 767) {
                        $("#sidebar").hide();
                    } else {
                        $("#sidebar").show();
                    }
                    map.invalidateSize();
                    syncSidebar();
                }
            });
        } else if (eventLayer.name === 'รายงานสถาณการณ์น้ำเชิงพื้นที่ในช่วงวิกฤติ') {
            getLayerWaterSituationData(function(res) {
                if (res > 0) {

                    if (document.body.clientWidth <= 767) {
                        $("#sidebar").hide();
                    } else {
                        $("#sidebar").show();
                    }
                    map.invalidateSize();
                    syncSidebar();
                }
            });
        } else if (!VBigTelemety[eventLayer.name] !== true) {
            //eventLayer.name === 'ระบบโทรมาตรแม่กลอง') {
            getBigtelemetryData(VBigTelemety[eventLayer.name], function(res) {
                if (res > 0) {
                    if (controllerBigTelemety.result == 0) {
                        controllerBigTelemety.legend.addTo(map);
                    }
                    controllerBigTelemety.result++;
                    if (document.body.clientWidth <= 767) {
                        $("#sidebar").hide();
                    } else {
                        $("#sidebar").show();
                    }
                    map.invalidateSize();
                    syncSidebar();
                }
            });
        } else if (!cst_event[eventLayer.name] !== true) {
            getCustomLayerGeoData(cst_event[eventLayer.name], function(res) {
                map.invalidateSize();
            });
        }
        else if (!eventRoyalRain[eventLayer.name] !== true) {
            getLayerRoyalRain( eventRoyalRain[eventLayer.name], function(res) {
                map.invalidateSize();
            });
            videoOverlay[ eventRoyalRain[eventLayer.name] ].getElement().play();
        }
    });
    map.on('overlayremove', function(eventLayer) {
        if (eventLayer.name === 'สภาพน้ำในอ่างเก็บน้ำขนาดใหญ่') {
            check_overlay_big = 0;
            if (check_overlay_big == 0 && check_overlay_middle == 0) {
                this.removeControl(source.bigDam.legend);
            }
            map.invalidateSize();
            syncSidebar();
        } else if (eventLayer.name === 'โทรมาตรขนาดใหญ่ลุ่มน้ำเจ้าพระยา') {
            check_overlay_bigteleamr = 0;
            if (check_overlay_bigteleamr == 0   && check_overlay_bigtelethachin == 0) {
                this.removeControl(source.bigteleamr.legend);
            }
            map.invalidateSize();
            syncSidebar();
        }
        else if (eventLayer.name === 'โทรมาตรขนาดใหญ่ลุ่มน้ำท่าจีน') {
            check_overlay_bigtelethachin = 0;
            map.invalidateSize();
            syncSidebar();
        } else if (eventLayer.name === 'Tele200') {
            map.invalidateSize();
            syncSidebar();
        } else if (eventLayer.name === 'Tele127') {
            map.invalidateSize();
            syncSidebar();
        } else if (eventLayer.name === 'เหนือเขื่อนแควน้อยบำรุงแดน') {
            map.invalidateSize();
            syncSidebar();
        } else if (eventLayer.name === 'สภาพน้ำในอ่างเก็บน้ำขนาดกลาง') {
            check_overlay_middle = 0;
            if (check_overlay_big == 0 && check_overlay_middle == 0) {
                this.removeControl(source.bigDam.legend);
            }
            map.invalidateSize();
            syncSidebar();
        } else if (eventLayer.name === 'ประตูระบายน้ำ') {
            this.removeControl(source.floodGate.legend);
            map.invalidateSize();
            syncSidebar();
        } else if (eventLayer.name === 'ข้อมูลอุตุนิยมวิทยา ทุก 3 ชม.') {
            this.removeControl(source.weather3Hours.legend);
            map.invalidateSize();
            syncSidebar();
        } else if (eventLayer.name === 'ลุ่มน้ำบางปะกง') {
            map.invalidateSize();
            syncSidebar();
        } else if (eventLayer.name === 'ลุ่มน้ำท่าจีน') {
            map.invalidateSize();
            syncSidebar();
        } else if (eventLayer.name === 'ลุ่มน้ำแม่กลอง') {
            map.invalidateSize();
            syncSidebar();
        } else if (!VBigTelemety[eventLayer.name] !== true) {
            if (controllerBigTelemety.result == 1) {
                this.removeControl(controllerBigTelemety.legend);
            }
            controllerBigTelemety.result--;
            map.invalidateSize();
            syncSidebar();
        } else if (!cst_event[eventLayer.name] !== true) {
            overlayCustomLayerGeoData(cst_event[eventLayer.name], function(res) {
                //callback;
            });
            map.invalidateSize();
        } else if (eventLayer.name === 'เขื่อนขนาดเล็ก') {
            $("#smalldam_dropdown").html("");
            this.removeControl(source.smalldamBeh.legend);
            map.invalidateSize();
            syncSidebar();
        } else if (eventLayer.name === 'ข้อมูลระดับน้ำ (สสนก.)') {
            this.removeControl(source.water_nhc.legend);
        } else if (eventLayer.name === 'รายงานสถาณการณ์น้ำเชิงพื้นที่ในช่วงวิกฤติ') {
            //this.removeControl(source.water_nhc.legend);
			closeassignmentmodal();
            $('#assignmenmodal').hide();
			$('#btn_watersituation').hide();
            WaterSituationURL="http://gisdata.rid.go.th/upload/getWaterSituationGeoJson.php";
        }

        map.invalidateSize();
        syncSidebar();
    });
    /* Filter sidebar feature list to only show features in current map bounds */
    map.on("moveend", function(e) {
        syncSidebar();
        if (map.getZoom() > 6) {
            $('#panel-button-back').removeClass('hide');
        }
    });

    /* Clear feature highlight when map is clicked */
    map.on("click", function(e) {
        // highlight.clearLayers();
    });
});
