var featureList; // not clear with typehead.js
var map = config.map;

function commafy(num, prec, currSign) {
    if (prec == null) prec = 2;
    var str = parseFloat(num).toFixed(prec).toString().split('.');
    if (str[0].length >= 3) {
        str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
    }
    if (str[1] && str[1].length >= 3) {
        str[1] = str[1].replace(/(\d{3})/g, '$1 ');
    }
    return (currSign == null ? "" : currSign + " ") + str.join('.');
}

function showDialog(title, maxWidth, content) {
    var win = L.control.window(map, {
        title: title,
        maxWidth: maxWidth,
        modal: true
    })
        .content(content)
        .prompt({
            callback: function () {
                alert('This is called after OK click!');
            }
        })
        .show();
}

function block(id, msg) {
    msg = (msg != undefined) ? msg : '';
    var z_index99 = 'z_index99';
    if (id == 'body' || id == '' || id == undefined) { //ถ้าไม่ได้ใส่ชื่อ ID ให้แสดง block คลุมทั้งหน้า
        id = 'body';
        z_index99 = '';
    }
    $(id).css('position', 'relative');
    $(id).children('#div_loading').remove();
    $(id).append(' <div id="div_loading" class="spinner_overlay  ' + z_index99 + ' "> <div class="spinner center"> <div class="spinner-blade"></div> <div class="spinner-blade"></div> <div class="spinner-blade"></div> <div class="spinner-blade"></div> <div class="spinner-blade"></div> <div class="spinner-blade"></div> <div class="spinner-blade"></div> <div class="spinner-blade"></div> <div class="spinner-blade"></div> <div class="spinner-blade"></div> <div class="spinner-blade"></div> <div class="spinner-blade"></div> <div class="spinner_text">' + msg + '</div> </div> </div> ');
}

function unblock(id) {  
    observable.notify('openModal');
    observable.notify('weatherMap');
    if (id == 'body' || id == '' || id == undefined) {
        id = 'body';
    }
    $(id).children('#div_loading').remove();
}

function animateSidebar() {
    $("#sidebar").animate({
        width: "toggle"
    }, 350, function () {
        map.invalidateSize();

    });
}

function sizeLayerControl() {
    $(".leaflet-control-layers").css("max-height", $("#map").height() - 50);
}

function clearHighlight() {
    //  highlight.clearLayers();
}

function sidebarClick(id) {
    Object.keys(source).forEach(function (overlayName) {
        var overlay = source[overlayName];
        if (overlay.geoJson !== undefined && overlay.geoJson.getLayer(id) !== undefined) {
            var layer = overlay.geoJson.getLayer(id);
            map.setView([layer.getLatLng().lat, layer.getLatLng().lng], 17);
            layer.fire("click");
        }
    });
    /* Hide sidebar and go to the map on small screens */
    if (document.body.clientWidth <= 767) {
        $("#sidebar").hide();
        map.invalidateSize();
    }

}

function syncSidebar() {
    $("#feature-list tbody").empty();
    if (map.hasLayer(source.bigDam.layer)) {
        source.bigDam.geoJson.eachLayer(function (layer) {
            if (map.getBounds().contains(layer.getLatLng())) {
                var QTotal = layer.feature.properties.QTotal;
                var damimage = "images/dam/dam.png";
                if (QTotal <= 30)
                    damimage = "images/dam/color-dam2.png"; //yellow
                else if (QTotal > 30 && QTotal <= 50)
                    damimage = "images/dam/color-dam6.png"; //green
                else if (QTotal > 50 && QTotal <= 80)
                    damimage = "images/dam/color-dam3.png"; //blue
                else if (QTotal > 80)
                    damimage = "images/dam/color-dam11.png"; //red
                $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) +
                    '" lat="' + layer.getLatLng().lat + '" lng="' +
                    layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="' + damimage + '"></td><td class="feature-name">' +
                    layer.feature.properties.DamName + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
            }
        });
    }
    if (map.hasLayer(source.bigteleamr.layer)) {
        source.bigteleamr.geoJson.eachLayer(function (layer) {
            if (map.getBounds().contains(layer.getLatLng())) {
                var WLTotal = layer.feature.properties.WLTotal;
                var damimage = "images/telemetry/grey-sign.png";
                if (WLTotal <= 30)
                    damimage = "images/telemetry/yellow-sign.png"; //yellow
                else if (WLTotal > 30 && WLTotal <= 50)
                    damimage = "images/telemetry/green-sign.png"; //green
                else if (WLTotal > 50 && WLTotal <= 80)
                    damimage = "images/telemetry/blue-sign.png"; //blue
                else if (WLTotal > 80)
                    damimage = "images/telemetry/red-sign.png"; //red
                else
                    damimage = "images/telemetry/grey-sign.png"; //red


                $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) +
                    '" lat="' + layer.getLatLng().lat + '" lng="' +
                    layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="' + damimage + '"></td><td class="feature-name">' +
                    layer.feature.properties.StationCode + ' ' + layer.feature.properties.StationName + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
            }
        });
    }
    if (map.hasLayer(source.bigtelethachin.layer)) {
        source.bigtelethachin.geoJson.eachLayer(function (layer) {
            if (map.getBounds().contains(layer.getLatLng())) {
                //var QTotal = layer.feature.properties.WLTotal;
                var damimage = "images/telemetry/blue-sign.png";
                //if (QTotal <= 30)
                //    damimage = "images/dam/color-dam2.png"; //yellow
                //else if (QTotal > 30 && QTotal <= 50)
                //    damimage = "images/dam/color-dam6.png"; //green
                //else if (QTotal > 50 && QTotal <= 80)
                //    damimage = "images/dam/color-dam3.png"; //blue
                //else if (QTotal > 80)
                //    damimage = "images/dam/color-dam11.png"; //red
                $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) +
                    '" lat="' + layer.getLatLng().lat + '" lng="' +
                    layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="' + damimage + '"></td><td class="feature-name">' +
                    layer.feature.properties.StationCode + ' ' + layer.feature.properties.StationName + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
            }
        });
    }

    if (map.hasLayer(source.tele127.layer)) {
        source.tele127.geoJson.eachLayer(function (layer) {
            if (map.getBounds().contains(layer.getLatLng())) {
                var markerUSE = greenIconUrl;
                var StationCode = layer.feature.properties.StationCode;
                var name = layer.feature.properties.StationName;

                if (StationCode == null)
                    StationCode = "";


                $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) +
                    '" lat="' + layer.getLatLng().lat + '" lng="' +
                    layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="' + markerUSE + '"></td><td class="feature-name">' +
                    StationCode + " " + name + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');


            }
        });
    }
    if (map.hasLayer(source.kwainoidam.layer)) {
        source.kwainoidam.geoJson.eachLayer(function (layer) {
            if (map.getBounds().contains(layer.getLatLng())) {
                var markerUSE = source.kwainoidam.blueIconUrl;
                var StationCode = layer.feature.properties.StationCode;
                var name = layer.feature.properties.StationName;

                if (StationCode == null)
                    StationCode = "";


                $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) +
                    '" lat="' + layer.getLatLng().lat + '" lng="' +
                    layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="' + markerUSE + '"></td><td class="feature-name">' +
                    StationCode + " " + name + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');


            }
        });
    }
    if (map.hasLayer(source.tele200.layer)) {
        source.tele200.geoJson.eachLayer(function (layer) {
            if (map.getBounds().contains(layer.getLatLng())) {
                var markerUSE;
                var wpriority = 0;
                var rpriority = 0;
                var apriority = 0;
                var priority = 0;
                var zerogage = layer.feature.properties.ZeroGage;

                var wmaxlv = layer.feature.properties.WMaxLevel;
                var wminlv = layer.feature.properties.WMinLevel;
                var rmaxlv = layer.feature.properties.RMaxLevel;
                var rminlv = layer.feature.properties.RMinLevel;
                var amaxlv = layer.feature.properties.AMaxLevel;
                var aminlv = layer.feature.properties.AMinLevel;
                var briverlv = layer.feature.properties.BRiverLevel;

                var wval = layer.feature.properties.WVal;
                var rval = layer.feature.properties.RVal;
                var aval = layer.feature.properties.AVal;
                wval += zerogage;

                if (wmaxlv != -1) {
                    if (wval > briverlv) {
                        wpriority = 3;

                    } else if (wval > wmaxlv) {
                        wpriority = 2;

                    } else if (wval < wminlv) {
                        wpriority = 1;

                    } else {
                        wpriority = 0;

                    }
                }
                if (rmaxlv != -1) {
                    if (rval > rmaxlv) {
                        rpriority = 3;

                    } else if (rval > rminlv) {
                        rpriority = 2;

                    } else {
                        rpriority = 0;
                    }
                }
                if (amaxlv != -1) {
                    if (aval > amaxlv) {
                        apriority = 3;

                    } else if (aval > aminlv) {
                        apriority = 2;

                    } else {
                        apriority = 0;

                    }
                }
                if (wpriority > rpriority) {
                    if (wpriority > apriority) {
                        priority = wpriority;

                    } else {
                        priority = apriority;
                    }
                } else {
                    if (rpriority > apriority) {
                        priority = rpriority;

                    } else {
                        priority = apriority;

                    }
                }
                //   alert("hi-1 p:" + priority);
                if (priority == 0) {
                    // detail = "<font style='color:green;'>" + detail + "<b>" + info + "</b></font>";
                    markerUSE = greenIconUrl;
                } else if (priority == 1) {
                    //detail = "<font style='color:black;'>" + detail + "<b>" + info + "</b></font>";
                    markerUSE = greenIconUrl;// greyIconUrl;
                } else if (priority == 2) {
                    //detail = "<font style='color:blue;'>" + detail + "<b>" + info + "</b></font>";
                    markerUSE = greenIconUrl;// blueIconUrl;
                } else if (priority == 3) {
                    //  detail = "<font style='color:red;'>" + detail + "<b>" + info + "</b></font>";
                    markerUSE = greenIconUrl;//redIconUrl;
                } else {
                    //detail = "<font style='color:red;'>" + detail + "<b>" + info + "</b></font>";
                    markerUSE = greenIconUrl;// redIconUrl;
                }
                var stationid = layer.feature.properties.StationCode;
                var name = layer.feature.properties.StationName;
                $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) +
                    '" lat="' + layer.getLatLng().lat + '" lng="' +
                    layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="' + markerUSE + '"></td><td class="feature-name">' +
                    stationid + " " + name + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
            }
        });
    }
    if (map.hasLayer(source.middleDam.layer)) {
        source.middleDam.geoJson.eachLayer(function (layer) {
            if (map.getBounds().contains(layer.getLatLng())) {
                var percent_rsv = layer.feature.properties.percent_rsv;
                var damimage = "";
                if (!percent_rsv) {
                    damimage = "images/middle/empty-dot.png";//empty
                } else {
                    if (percent_rsv <= 30)
                        damimage = "images/middle/yellow-dot.png"; //yellow
                    else if (percent_rsv > 30 && percent_rsv <= 50)
                        damimage = "images/middle/green-dot.png"; //green
                    else if (percent_rsv > 50 && percent_rsv <= 80)
                        damimage = "images/middle/blue-dot.png"; //blue
                    else if (percent_rsv > 80)
                        damimage = "images/middle/red-dot.png"; //red
                }
                $("#feature-list tbody").append('<tr class="feature-row" data-type="rsvmiddle" data-cresv="' + layer.feature.properties.cresv + '" id="' + L.stamp(layer) +
                    '" lat="' + layer.getLatLng().lat + '" lng="' +
                    layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="18" height="18" src="' + damimage + '"></td><td class="feature-name">' +
                    layer.feature.properties.nresv + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
            }
        });
    }

    if (map.hasLayer(source.floodGate.layer)) {
        source.floodGate.geoJson.eachLayer(function (layer) {
            if (map.getBounds().contains(layer.getLatLng())) {
                var q_data = layer.feature.properties.TMD_Q;
                var fillColor = "";
                if (q_data == null || q_data == 0) {
                    fillColor = "#E34B62"; //red
                } else {
                    fillColor = "#4ACF4E"; //green
                }
                $("#feature-list tbody").append('<tr class="feature-row" data-type="floodgate" data-TMI_ID="' + layer.feature.properties.TMI_ID + '" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"><canvas style="border:solid 1px #000; width: 18px; height: 18px; margin: 0px; padding: 0px; border-radius: 10px; background-color:' + fillColor + '; "></canvas></td><td class="feature-name">' + layer.feature.properties.TMI_Name + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
            }
        });
    }
    if (map.hasLayer(source.bigdamBeh.layer)) {
        source.bigdamBeh.geoJson.eachLayer(function (layer) {
            if (map.getBounds().contains(layer.getLatLng())) {
                var damimage = "bigdamBeh.png";
                var body = "<tr class='feature-row' data-type='floodgate' data-TMI_ID='" + layer.feature.projid + "' " +
                    "id='" + L.stamp(layer) + "'" +
                    " lat='" + layer.getLatLng().lat + "'" +
                    " lng='" + layer.getLatLng().lng + "'" +
                    "><td style='vertical-align: middle;'><img width='18' height='18' " +
                    "src='images/bigdamBeh/" + damimage + "'></td><td class='feature-name'>" + layer.feature.properties.projname + "" +
                    "</td><td style='vertical-align: middle;'><i class='fa fa-chevron-right pull-right'></i></td></tr>";
                $("#feature-list tbody").append(body);
            }
        });
    }
	if (map.hasLayer(source.CCTV.layer)) {
        source.CCTV.geoJson.eachLayer(function (layer) {
            if (map.getBounds().contains(layer.getLatLng())) {
                var damimage = "CCTV.png";
                var body = "<tr class='feature-row' data-type='floodgate' data-TMI_ID='" + layer.feature.projid + "' " +
                    "id='" + L.stamp(layer) + "'" +
                    " lat='" + layer.getLatLng().lat + "'" +
                    " lng='" + layer.getLatLng().lng + "'" +
                    "><td style='vertical-align: middle;'><img width='18' height='18' " +
                    "src='images/CCTV/" + damimage + "'></td><td class='feature-name'>" + layer.feature.properties.station + "" +
                    "</td><td style='vertical-align: middle;'><i class='fa fa-chevron-right pull-right'></i></td></tr>";
                $("#feature-list tbody").append(body);
            }
        });
    }
    if (map.hasLayer(source.middamBeh.layer)) {
        source.middamBeh.geoJson.eachLayer(function (layer) {
            if (map.getBounds().contains(layer.getLatLng())) {
                var damimage = "middamBeh.png";
                var body = "<tr class='feature-row' data-type='floodgate' data-TMI_ID='" + layer.feature.projid + "' " +
                    "id='" + L.stamp(layer) + "'" +
                    " lat='" + layer.getLatLng().lat + "'" +
                    " lng='" + layer.getLatLng().lng + "'" +
                    "><td style='vertical-align: middle;'><img width='18' height='18' " +
                    "src='images/middamBeh/" + damimage + "'></td><td class='feature-name'>" + layer.feature.properties.projname + "" +
                    "</td><td style='vertical-align: middle;'><i class='fa fa-chevron-right pull-right'></i></td></tr>";
                $("#feature-list tbody").append(body);
            }
        });
    }
    if (map.hasLayer(source.smalldamBeh.layer)) {
        source.smalldamBeh.geoJson.eachLayer(function (layer) {
            if (map.getBounds().contains(layer.getLatLng())) {
                var damimage = "";
                if (layer.feature.properties.transfer == 0) {
                    damimage = "nottransfer.png";
                } else {
                    damimage = "transfer.png";
                }
                var body = "<tr class='feature-row' data-type='floodgate' data-TMI_ID='" + layer.feature.projid + "' " +
                    "id='" + L.stamp(layer) + "'" +
                    " lat='" + layer.getLatLng().lat + "'" +
                    " lng='" + layer.getLatLng().lng + "'" +
                    "><td style='vertical-align: middle;'><img width='18' height='18' " +
                    "src='images/smalldamBeh/" + damimage + "'></td><td class='feature-name'>" + layer.feature.properties.projname + "" +
                    "</td><td style='vertical-align: middle;'><i class='fa fa-chevron-right pull-right'></i></td></tr>";
                $("#feature-list tbody").append(body);
            }
        });
    }
	/*if (map.hasLayer(source.WaterSituation.layer)) {
        source.WaterSituation.geoJson.eachLayer(function (layer) {
            if (map.getBounds().contains(layer.getLatLng())) {
				
				var media_container=layer.feature.properties.media_container;
				var media_name=layer.feature.properties.media_name;
				//var photofullsize=URL_photo+"/"+media_container+"/"+media_name;
				var photothumbnail=URL_photo+"/"+media_container+"/thumb/"+media_name;
				if(media_container==''||media_name==''){
					photothumbnail="images/WaterSituation/no-image-icon.png";
				}
                //var damimage = "middamBeh.png";
                var body = "<tr class='feature-row' data-type='floodgate'  " +
                    "id='" + L.stamp(layer) + "'" +
                    " lat='" + layer.getLatLng().lat + "'" +
                    " lng='" + layer.getLatLng().lng + "'" +
                    "><td style='vertical-align: middle;'><img onError=\"this.src='images/WaterSituation/no-image-icon.png'\" width='18' height='18' " +
                    "src='"+ photothumbnail+ "'></td><td class='feature-name'>" + layer.feature.properties.caption + "" +
                    "</td><td style='vertical-align: middle;'><i class='fa fa-chevron-right pull-right'></i></td></tr>";
                $("#feature-list tbody").append(body);
            }
        });
    }*/
    Object.keys(window.config.system.rainfall.menu.sub_menu).forEach(function (key) {
        var source_name = window.config.system.rainfall.menu.sub_menu[key].source_name;
        if (map.hasLayer(source[source_name].layer) && source[source_name].geoJson) {
            source[source_name].geoJson.eachLayer(function (layer) {
                if (map.getBounds().contains(layer.getLatLng())) {
                    $("#feature-list tbody").append(
                        '<tr class="feature-row" data-rainfall="' + layer.feature.properties[source_name] + '" id="' + L.stamp(layer) +
                        '" lat="' + layer.getLatLng().lat + '" lng="' +
                        layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="18" height="18" src="' + layer._icon.src + '"></td><td class="feature-name">' +
                        '<span style="display:none;">'+layer.feature.properties.rainfall_today*100+'</span>'+layer.feature.properties.term + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>'
                    );
                }
            });
        }
    });
    if (map.hasLayer(source.water_nhc.layer)) {
        source.water_nhc.geoJson.eachLayer(function (layer) {
            if (map.getBounds().contains(layer.getLatLng())) {
                $("#feature-list tbody").append('<tr class="feature-row" data-water_nhc="' + layer.feature.properties.wl_msl + '" id="' + L.stamp(layer) +
                    '" lat="' + layer.getLatLng().lat + '" lng="' +
                    layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="18" height="18" src="' + layer._icon.src + '"></td><td class="feature-name">' +
                    layer.feature.properties.term + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
            }
        });
    }
    if (map.hasLayer(source.weather3Hours.layer)) {
        if (source.weather3Hours.geoJson != undefined) {
            source.weather3Hours.geoJson.eachLayer(function (layer) {
                if (map.getBounds().contains(layer.getLatLng())) {
                    // api V1
                    $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) +
                        '" lat="' + layer.getLatLng().lat + '" lng="' +
                        layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="images/weather/' + getMarkerWeather(layer.feature.properties.barometer_temperature[0]) + '"></td><td class="feature-name">' +
                        layer.feature.properties.station_name[0] + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
                    // api V2
                    // $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) +
                    //     '" lat="' + layer.getLatLng().lat + '" lng="' +
                    //     layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="images/weather/' + getMarkerWeather(layer.feature.properties.air_temperature[0]) + '"></td><td class="feature-name">' +
                    //     layer.feature.properties.station_name[0] + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
                }
            });
        }
    }
    if (map.hasLayer(source.bangpakong.layer)) {
        if (source.bangpakong.geoJson != undefined) {
            source.bangpakong.geoJson.eachLayer(function (layer) {
                if (map.getBounds().contains(layer.getLatLng())) {
                    $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) +
                        '" lat="' + layer.getLatLng().lat + '" lng="' +
                        layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="images/waterquality/icon-waterquality.gif"></td><td class="feature-name">' +
                        layer.feature.properties.stn_name + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
                }
            });
        }
    }
    if (map.hasLayer(source.thachin.layer)) {
        if (source.thachin.geoJson != undefined) {
            source.thachin.geoJson.eachLayer(function (layer) {
                if (map.getBounds().contains(layer.getLatLng())) {
                    $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) +
                        '" lat="' + layer.getLatLng().lat + '" lng="' +
                        layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="images/waterquality/icon-waterquality.gif"></td><td class="feature-name">' +
                        layer.feature.properties.stn_name + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
                }
            });
        }
    }
    if (map.hasLayer(source.maeklong.layer)) {
        if (source.maeklong.geoJson != undefined) {
            source.maeklong.geoJson.eachLayer(function (layer) {
                if (map.getBounds().contains(layer.getLatLng())) {
                    $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) +
                        '" lat="' + layer.getLatLng().lat + '" lng="' +
                        layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="images/waterquality/icon-waterquality.gif"></td><td class="feature-name">' +
                        layer.feature.properties.stn_name + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
                }
            });
        }
    }
    /* Update list.js featureList */
    featureList = new List("features", {
        valueNames: ["feature-name"]
    });
    featureList.sort("feature-name", {
        order: "desc"
    });

    //bigtelemetry
    for (var seri in VBigTelemety) {
        if (map.hasLayer(source[VBigTelemety[seri]].layer)) {
            source[VBigTelemety[seri]].geoJson.eachLayer(function (layer) {
                if (map.getBounds().contains(layer.getLatLng())) {
                    var _status = layer.feature.properties.status;
                    var _crash = layer.feature.properties.crash;
                    var _wtl = layer.feature.properties.water_lavel;
                    var _pst = getPercent(_status, _crash, _wtl);

                    var markerUSE = source.bigtelethachin.greyIconUrl;
                    if (_pst <= 30 && _pst >= 0)
                        markerUSE = source.bigtelethachin.yellowIconUrl; //yellow
                    else if (_pst > 30 && _pst <= 50)
                        markerUSE = source.bigtelethachin.greenIconUrl; //green
                    else if (_pst > 50 && _pst <= 80)
                        markerUSE = source.bigtelethachin.blueIconUrl; //blue
                    else if (_pst > 80)
                        markerUSE = source.bigtelethachin.redIconUrl; //red

                    //console.log(layer.feature.properties);
                    content = '<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '">';
                    content += '<td style="vertical-align: middle;"><img width="16" height="16" src="' + markerUSE + '"></td>';
                    content += '<td class="feature-name">' + layer.feature.properties.full_name + '</td>';
                    content += '<td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td>';
                    content += '</tr>';
                    $("#feature-list tbody").append(content);
                }
            });
        }
    }

    if ($("#feature-list tbody tr").length === 0) {
        $("#sidebar").hide();
    }
}

function zoomToFeature(e) {
    map.fitBounds(e.target.getBounds());
}


var isCollapsed = true;
var greenIconUrl = "libraries/leaflet-plugin/leaflet-color-markers/img/marker-icon-green.png";

var blueIconUrl = "libraries/leaflet-plugin/leaflet-color-markers/img/marker-icon-blue.png";
var redIconUrl = "libraries/leaflet-plugin/leaflet-color-markers/img/marker-icon-red.png";
var greyIconUrl = "libraries/leaflet-plugin/leaflet-color-markers/img/marker-icon-grey.png";
var yellowIconUrl = "libraries/leaflet-plugin/leaflet-color-markers/img/marker-icon-yellow.png";
var violetIconUrl = "libraries/leaflet-plugin/leaflet-color-markers/img/marker-icon-violet.png";
var shadowIconUrl = "libraries/leaflet1.0.3/images/marker-shadow.png";
var greenIcon = new L.Icon({
    iconUrl: greenIconUrl,
    shadowUrl: shadowIconUrl,
    iconSize: [10.5, 16.5],
    iconAnchor: [10, 16.5],
    popupAnchor: [0.5, -17],
    shadowSize: [18.5, 18.5]
});

var blueIcon = new L.Icon({
    iconUrl: blueIconUrl,
    shadowUrl: shadowIconUrl,
    iconSize: [10.5, 16.5],
    iconAnchor: [10, 16.5],
    popupAnchor: [0.5, -17],
    shadowSize: [18.5, 18.5]
});
var redIcon = new L.Icon({
    iconUrl: redIconUrl,
    shadowUrl: shadowIconUrl,
    iconSize: [10.5, 16.5],
    iconAnchor: [10, 16.5],
    popupAnchor: [0.5, -17],
    shadowSize: [18.5, 18.5]
});
var greyIcon = new L.Icon({
    iconUrl: greyIconUrl,
    shadowUrl: shadowIconUrl,
    iconSize: [10.5, 16.5],
    iconAnchor: [10, 16.5],
    popupAnchor: [0.5, -17],
    shadowSize: [18.5, 18.5]
});
var yellowIcon = new L.Icon({
    iconUrl: yellowIconUrl,
    shadowUrl: shadowIconUrl,
    iconSize: [10.5, 16.5],
    iconAnchor: [10, 16.5],
    popupAnchor: [0.5, -17],
    shadowSize: [18.5, 18.5]
});

var violetIcon = new L.Icon({
    iconUrl: violetIconUrl,
    shadowUrl: shadowIconUrl,
    iconSize: [10.5, 16.5],
    iconAnchor: [10, 16.5],
    popupAnchor: [0.5, -17],
    shadowSize: [18.5, 18.5]
});
var HLIconTempString = "temporary";
var HLIconPerString = "permanent";
/*Icon - bigtelemetry */
var bigtelemetryIcon = {};
var bigtelemetryUrl = {};
var nameBTLIcon = ["circle-blue", "circle-green", "circle-grey", "circle-orange", "circle-red", "circle-yellow"];
for (var BTLinfo in nameBTLIcon) {
    bigtelemetryUrl[nameBTLIcon[BTLinfo]] = 'images/bigtelemetry/' + nameBTLIcon[BTLinfo] + '.png';
    bigtelemetryIcon[nameBTLIcon[BTLinfo]] = new L.Icon({
        iconUrl: 'images/bigtelemetry/' + nameBTLIcon[BTLinfo] + '.png',
        iconSize: [16, 16]
    });
}
