$(function () {
    var rank = { "rainfall_today": [] }
    Array.prototype.insert_topten = function ( index, item ) {
        this.splice( index, 0, item );
        this.splice(10);
    };
    function rainfallRank(isClear) {
        Object.keys(map._layers).forEach(function (key, index) { 
            try {
                if(map._layers[key].hide) map._layers[key].hide();
                if(isClear){
                    map._layers[key].show();
                    // check layer rainfall
                } else if(map._layers[key].defaultOptions.icon.options.iconUrl === window.config.system.rainfall.icon.rain || map._layers[key].defaultOptions.icon.options.iconUrl === window.config.system.rainfall.icon.sunny) {
                    var prop = map._layers[key].feature.properties;
                    if( prop.rainfall_today !== null && ( !rank.rainfall_today[9] || rank.rainfall_today[9].feature.properties.rainfall_today < prop.rainfall_today ) ){
                            var i;
                            rank.rainfall_today.every(function (rf_item, index) { 
                                i = prop.rainfall_today < rf_item.feature.properties.rainfall_today ? (index+1) : index;
                                return prop.rainfall_today < rf_item.feature.properties.rainfall_today
                            });
                            rank.rainfall_today.insert_topten(i, map._layers[key]);
                    }
                } 
            } catch (error) {
                if(window.config.system.console) console.log(error);
            }
            if(Object.keys(map._layers).length-1 === index){
                rank.rainfall_today.forEach(function (item, index) {
                    if(item._icon) $(item._icon).attr('src', isClear ? 'img/rain.png' : 'img/rain-rang.'+(index+1)+'.png');
                    item.show()
                })
                if(isClear) rank.rainfall_today = [];
            }
        })
    }
    // decision show hide btn-top-rank
    map.overlayAdded = {};
    var ranfall_sub_menu = Object.keys(window.config.system.rainfall.menu.sub_menu);
    map.on('overlayadd overlayremove', function (param) {
        // btn-rainfall-rank click unactive
        if($('.btn-rainfall-rank').hasClass('active')) $('.btn-rainfall-rank').click();
        map.overlayAdded[param.name] = param.type === 'overlayadd';
        $('.btn-rainfall-rank').hide();
        Object.keys(map.overlayAdded).every(function (key) {
            // Add rainfall 
            if(ranfall_sub_menu.indexOf(key) !== -1 && map.overlayAdded[key] === true) {
                $('.btn-rainfall-rank').show();
                return false;
            }
            return true;
        });
    });
    $('.btn-rainfall-rank').click(function (e) {
        e.preventDefault();
        rainfallRank($('.btn-rainfall-rank').hasClass('active'));
        $('.btn-rainfall-rank').toggleClass('active');
    });
});