var urlParams;
(window.onpopstate = function () {
    var match,
        pl     = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
        query  = window.location.search.substring(1);

    urlParams = {};
    while (match = search.exec(query))
       urlParams[decode(match[1]).toLowerCase()] = isNaN(decode(match[2])) ? decode(match[2]) : parseInt(decode(match[2]) );
})();

$(document).ready(function () {
    // Zoom
    if( urlParams.zoom ){
        map.setZoom(urlParams.zoom);
    }
    // Latlong
    if( urlParams.lat || urlParams.lng ){
        map.setView( new L.LatLng( urlParams.lat || map.getCenter().lat, urlParams.lng || map.getCenter().lng ) );
    }
    // point AND pointSelect
    // ex pointSelect=[src*="rain"]&point=1
    if ( urlParams.point || urlParams.pointSelect ) {
        var openModal = observable.on('openModal', function () { 
            // remove observers listener
            openModal();
            setTimeout(function() {
                $( '.leaflet-marker-icon'+ (urlParams.pointSelect || '') )[urlParams.point || 0].click();
            }, 100);
        })
    }
    // select tab on modal
    if( urlParams.tabNumber ){
        $('#iwocModal').on('shown.bs.modal', function(e) {
            $('.menu_tab').eq(urlParams.tabNumber-1).click();
        });
    }
    if ( urlParams.weatherMap ) {
        $('#iwocModal').on('shown.bs.modal', function(e) {
            var weatherMap = observable.on('weatherMap', function () { 
                // remove observers listener
                weatherMap();
                setTimeout(function() {
                    $('#iwocModal [title*="แผนที่อากาศ"]').eq(0).click();
                }, 100);
            })
        });
    }
});