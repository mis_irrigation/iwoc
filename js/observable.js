//This variables should live within the previous line context (unaffected by this)
var observers = {},
    cached = {},
    allcache = false;
// The observable object, variables outside to be independent of this (the observable var)
var observable = {
    on: function(event, listener) {
        // Initialize array on first call to an event type
        if (!observers.hasOwnProperty(event)) {
            observers[event] = [];
        }
        // Push and keep the index for removal
        var idx = observers[event].push(listener) - 1;

        // If event already fired and is cached (event will be called with the original arguments)
        if (cached.hasOwnProperty(event)) {
            //~ console.log(' ->>> Calling fired event: '+ event);
            var args = cached[event];
            listener.apply(this, args);
        }


        return (function(event, idx) {
            // returning an unregister function
            return function() {
                delete(observers[event][idx]);
            };
        }(event, idx));
    },
    notify: function(event) {
        var args = Array.prototype.slice.call(arguments, 1);
        // Cache if event is cacheable
        if (!cached.hasOwnProperty(event) && allcache) {
            cached[event] = [];
        }
        if (cached.hasOwnProperty(event) || allcache) {
            //~ console.log('adding a cached event ', args);
            cached[event] = args;
        }
        // Notify the observers
        if (!observers[event]) return;
        observers[event].forEach(function(obs) {
            obs.apply(this, args);
        });
    },
    cache: function(event) {
        // Add an array to save the event arguments
        if (!cached.hasOwnProperty(event)) {
            cached[event] = [];
        }
    },
    cacheAll: function(cache) {
        // Set a flag to cache all events
        allcache = cache;
    }
};