$(document).ready(function () {
    var isAddNewOverlay = false;
    $("#map-filter").select2({
        theme: "bootstrap",
        placeholder: "กรองข้อมูล",
        width: '100%'
    });
    map.on('overlayadd overlayremove typehead', function (param) {
        isAddNewOverlay = true;
        if ($("#map-filter").val().length !== 0) $("#map-filter").val('').trigger('change');
    })
    $("#map-filter").on('select2:opening', function (e) {
        if (!isAddNewOverlay) return;
        $("#map-filter").html('');
        function show() {
            $(this._icon).show();
            $(this._shadow).show();
         }
        function hide() {
            $(this._icon).hide();
            $(this._shadow).hide();
        }
        var last_index = Object.keys(map._layers).length - 1,
            _tmp = {
                amphoe: {
                    alias: 'อำเภอ',
                    data: {}
                },
                province: {
                    alias: 'จังหวัด',
                    data: {}
                },
                geocode_basin: {
                    alias: 'ลุ่มน้ำ',
                    data: {}
                },
                term: {
                    alias: 'อื่นๆ',
                    data: {}
                }
            }
        Object.keys(map._layers).forEach(function (layer_id, index) {
            try {
                var cur_l = map._layers[layer_id];
                if(cur_l.feature){
                    isAddNewOverlay = false;
                    if (!cur_l.show) cur_l.show = show;
                    if (!cur_l.hide) cur_l.hide = hide;
                    Object.keys(_tmp).forEach(function (key) {
                        var prop = cur_l.feature.properties;
                        if(prop[key]) {
                            if(!_tmp[key].data[prop[key]]) _tmp[key].data[prop[key]] = layer_id;
                            prop[key+'_i'] = key+'_'+_tmp[key].data[prop[key]];
                        }
                    })
                }
                if(last_index === index) {
                    var opt = Object.keys(_tmp).reduce(function (acc, key) {
                            if(Object.keys(_tmp[key].data).length === 0) return acc;
                            else if(key === 'term' && acc === '') {
                                return  Object.keys(_tmp[key].data).map(function (_key) { return '<option value=' +key+'_'+ _tmp[key].data[_key] + '>' + _key + '</option>'}).join(' ');
                            }
                            else {
                                return acc + ' <optgroup label="'+_tmp[key].alias+'"> ' + 
                                    Object.keys(_tmp[key].data).map(function (_key) { return '<option value=' +key+'_'+ _tmp[key].data[_key] + '>' + _key + '</option>'}).join(' ');
                                +' </optgroup> ';
                            }
                        }, '');
                    $("#map-filter").append(opt);
                }
            } catch (error) {
                if(window.config.system.console) console.log(error);
            }
        });
    });
    $("#map-filter").on('change', function (evt) {
        var _val = $("#map-filter").val();
        Object.keys(map._layers).forEach(function (idx) {
            try {
                if (map._layers[idx].feature) {
                    var prop = map._layers[idx].feature.properties;
                    if (_val.length === 0 || _val.indexOf(prop.amphoe_i) !== -1 || _val.indexOf(prop.province_i) !== -1 || _val.indexOf(prop.basin_i) !== -1 || _val.indexOf(prop.term_i) !== -1) {
                        (map._layers[idx]._path !== undefined) ? $(map._layers[idx]._path).show() : map._layers[idx].show(); //If polyline  For show
                    } else {
                        (map._layers[idx]._path !== undefined) ? $(map._layers[idx]._path).hide() : map._layers[idx].hide(); //If polyline  For hide
                    }
                }
            } catch (error) {
                if(window.config.system.console) console.log(error);
            }
        });
    });
});
