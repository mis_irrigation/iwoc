//bigtelemetry
var controllerBigTelemety;
controllerBigTelemety = {'result':0};
var VBigTelemety = {'ระบบโทรมาตรแม่กลอง':'Maeklong', 'ระบบโทรมาตรตาปี':'Tapi', 'ระบบโทรมาตรลุ่มน้ำป่าสัก':'Pasak', 'ระบบโทรมาตรลุ่มน้ำเลย':'Loei'};
for(var sourcei in VBigTelemety){
  source[VBigTelemety[sourcei]] = {};
}
//custom-layerGeoserver
var cst_legend = {};
var cst_config = {};
var cst_event = {};
cst_config['ข้อมูลพื้นฐานชลประทาน'] = {
    'basin_polygon':{
      'type': 'wms',
      'name': 'ลุ่มน้ำหลัก',
      'fields': {'basin_nt':'ชื่อลุ่มน้ำหลัก(ภาษาไทย)','basin_ne':'ชื่อลุ่มน้ำหลัก(ภาษาอังกฤษ)'},
      'geometric': 'geom',
      'public': true,
      'layers': 'ridbasic:basin_polygon',
      'url': 'http://gis.rid.go.th/geoserver/ridbasic/wms/'
    },
    'subbasin_polygon':{
      'type': 'wms',
      'name': 'ลุ่มน้ำย่อย',
      'fields': {'sub_nt':'ชื่อลุ่มน้ำย่อย(ภาษาไทย)','sub_ne':'ชื่อลุ่มน้ำย่อย(ภาษาอังกฤษ)'},
      'geometric': 'geom',
	  'public': true,
      'layers': 'ridbasic:subbasin_polygon',
      'url': 'http://gis.rid.go.th/geoserver/ridbasic/wms/'
    },
	'province_ridoffice_all':{
      'type': 'wms',
      'name': 'ขอบเขตจังหวัด',
      'geometric': 'geom',
      'fields': {'prov_nam_t':'ชื่อจังหวัด(ภาษาไทย)','prov_nam_e':'ชื่อจังหวัด(ภาษาอังกฤษ)','reg_name':'ภาค'},
      'layers': 'ridbasic:province_ridoffice_all_polygon',
      'url': 'http://gis.rid.go.th/geoserver/ridbasic/wms/'
    },
    'subdistrict':{
        'type': 'wms',
        'name': 'ขอบเขตตำบล',
        'geometric': 'geom',
  	    'public': true,
        'display': false,
        'fields': {'prov_namt':'ชื่อจังหวัด(ภาษาไทย)','prov_name':'ชื่อจังหวัด(ภาษาอังกฤษ)','amp_namt':'ชื่ออำเภอ(ภาษาไทย)','amp_name':'ชื่ออำเภอ(ภาษาอังกฤษ)','tam_namt':'ชื่อตำบล(ภาษาไทย)','tam_name':'ชื่อตำบล(ภาษาอังกฤษ)'},
        'layers': 'ridbasic:subdistrict_2012',
        'url': 'http://gis.rid.go.th/geoserver/ridbasic/wms/'
    },
    'ridoffice_all_polygon':{
      'type': 'wms',
      'name': 'ขอบเขตสำนักงานชลประทาน',
      'geometric': 'geom',
      'public': true,
      'fields': {'rid_name':'สำนักชลประทาน','shot_name':'ชื่อย่อ'},
      'layers': 'ridbasic:ridoffice_all_polygon',
      'url': 'http://gis.rid.go.th/geoserver/ridbasic/wms/'
    },
	'virrproject_all':{
      'type': 'json',
      'name': 'ที่ตั้งโครงการชลประทาน',
      'key': 'objectid',
	    'titlename': 'รายละเอียดที่ตั้งโครงการชลประทาน',
      'fields': {'projname':'ชื่อโครงการ :','owner':'สังกัดหน่วยงาน :','ridcode':'สำนักงานชลประทานที่ :','project_type':'ประเภทโครงการ :'},
      'layers': 'ridbasic:virrproject_all',
      'url': 'http://gis.rid.go.th/geoserver/ridbasic/wms/'
    },
	'vprjarea_all':{
      'type': 'wms',
      'name': 'พื้นที่โครงการชลประทาน',
      //'geometric': 'geom',
     // 'legend': 'test',
      'layers': 'ridbasic:vprjarea_all',
      'url': 'http://gis.rid.go.th/geoserver/ridbasic/wms/'
    },
	'virrarea_all':{
      'type': 'wms',
      'name': 'พื้นที่ชลประทาน',
      //'geometric': 'geom',
     // 'legend': 'test',
      'layers': 'ridbasic:virrarea_all',
      'url': 'http://gis.rid.go.th/geoserver/ridbasic/wms/'
    },
    'mainriver_2012':{
      'type': 'wms',
      'name': 'ทางน้ำสายหลัก',
      //'geometric': 'geom',
     // 'legend': 'test',
      'layers': 'ridbasic:mainriver_2012',
      'url': 'http://gis.rid.go.th/geoserver/ridbasic/wms/'
    },
	'subriver':{
      'type': 'wms',
      'name': 'ทางน้ำสายรอง',
      //'geometric': 'geom',
     // 'legend': 'test',
      'layers': 'ridbasic:subriver',
      'url': 'http://gis.rid.go.th/geoserver/ridbasic/wms/'
    },
    'mainroad_2012':{
      'type': 'wms',
      'name': 'ถนน',
      //'geometric': 'geom',
     // 'legend': 'test',
      'layers': 'ridbasic:mainroad_2012',
      'url': 'http://gis.rid.go.th/geoserver/ridbasic/wms/'
    },
	'railway_2012':{
      'type': 'wms',
      'name': 'ทางรถไฟ',
      'layers': 'ridbasic:railway_2012',
      'url': 'http://gis.rid.go.th/geoserver/ridbasic/wms/'
    }
};

cst_config['Agri-Map (ขอบเขตความเหมาะสมการเพาะปลูกพืช)'] = {
  'agri_rice_all_wgs84': {
    'type': 'tileLayer',
    'name': 'ข้าว',
    'legend': 'landsuit',
    'url': 'http://agri-map-service.rid.go.th/geoserver/gwc/service/gmaps?layers=agrimapsuit:rice_all_wgs84'
  },
  'agri_cane_psuit_all_wgs84': {
    'type': 'tileLayer',
    'name': 'อ้อย',
    'legend': 'landsuit',
    'url':'http://agri-map-service.rid.go.th/geoserver/gwc/service/gmaps?layers=agrimapsuit:cane_psuit_all_wgs84'
  },
  'agri_corn_all_wgs84': {
    'type': 'tileLayer',
    'name': 'ข้าวโพด',
    'legend': 'landsuit',
    'url': 'http://agri-map-service.rid.go.th/geoserver/gwc/service/gmaps?layers=agrimapsuit:corn_all_wgs84'
  },
  'agri_casa_psuit_all_wgs84': {
    'type': 'tileLayer',
    'name': 'มันสัมปะหลัง',
    'legend': 'landsuit',
    'url': 'http://agri-map-service.rid.go.th/geoserver/gwc/service/gmaps?layers=agrimapsuit:casa_psuit_all_wgs84'
  },
  'agri_oilpalm_all_wgs84_s': {
    'type': 'tileLayer',
    'name': 'ปาล์มน้ำมัน',
    'legend': 'landsuit',
    'url': 'http://agri-map-service.rid.go.th/geoserver/gwc/service/gmaps?layers=agrimapsuit:oilpalm_all_wgs84_s'
  },
  'agri_orchard_all_wgs84': {
    'type': 'tileLayer',
    'name': 'ทุเรียน/เงาะ/มังคุด',
    'legend': 'landsuit',
    'url': 'http://agri-map-service.rid.go.th/geoserver/gwc/service/gmaps?layers=agrimapsuit:orchard_all_wgs84'
  },
};
cst_config['รายงานการเพาะปลูกพืชของGistda']={

  'agri_rice_GISTDA':{
      'type': 'wms',
      'name': 'พื้นที่เพาะปลูกข้าว(GISTDA)',
	  'layers': 'ricefield:lfg_rice_flood',
      'url': 'http://150.107.31.104:8080/geoserver/ricefield/wms'
   },
   'agri_corn_GISTDA':{
      'type': 'wms',
      'name': 'พื้นที่เพาะปลูกข้าวโพด(GISTDA)',
	  'layers': 'maize:maize_present',
      'url': 'http://150.107.31.104:8080/geoserver/maize/wms'
   },
   'agri_potato_GISTDA':{
      'type': 'wms',
      'name': 'พื้นที่เพาะปลูกมันสำปะหลัง(GISTDA)',
	  'layers': 'cassava:cassava_present',
      'url': 'http://150.107.31.104:8080/geoserver/cassava/wms'
   },
   'agri_sugarcane_GISTDA':{
      'type': 'wms',
      'name': 'พื้นที่เพาะปลูกอ้อย(GISTDA)',
	  'layers': 'sugarcane:sugarcane_present',
      'url': 'http://150.107.31.104:8080/geoserver/sugarcane/wms'
   },
};
cst_config['ข้อมูลสำนักงานชลประทานที่ 12'] = {
  'prjarea_rid12':{
      'type': 'wms',
      'name': 'พื้นที่โครงการทั้งหมด ในสำนักงานชลประทานที่ 12',
      'geometric': 'geom',
      'fields': {'prj_code':'รหัสโครงการ','proj':'ชื่อย่อโครงการ'},
      'layers': 'rio12:prjarea_rio12',
      'setzoom': {'lat':15.3931, 'lon':99.8084, 'zoom': 11.5},
      'url': 'http://gis.rid.go.th/geoserver/rio12/wms/'
  },
  'paddy_w10_rio12':{
      'type': 'wms',
      'name': 'พื้นที่แปลงนาทั้งหมดสำนักงานชลประทานที่12',
      'geometric': 'geom',
      'fields': {'name':'ชื่อ','proj':'โครงการ','zone':'โซน','weekly':'รายสัปดาห์','tambon':'ตำบล','amphoe':'อำเภอ','province':'จังหวัด'},
      'layers': 'rio12:paddy_w10_rio12',
      'setzoom': {'lat':15.3931, 'lon':99.8084, 'zoom': 11.5},
      'url': 'http://gis.rid.go.th/geoserver/rio12/wms/'
  },
  'harvest_rid12':{
      'type': 'wms',
      'name': 'พื้นที่เก็บเกี่ยวแล้ว ในสำนักงานชลประทานที่ 12',
      'setzoom': {'lat':15.3931, 'lon':99.8084, 'zoom': 11.5},
      'layers': 'rio12:harvest_rid12',
      'url': 'http://gis.rid.go.th/geoserver/rio12/wms/'
  },
  'farming_rid12':{
      'type': 'wms',
      'name': 'พื้นที่เพาะปลูก ในสำนักงานชลประทานที่ 12',
      'layers': 'rio12:farming_rio12',
      'url': 'http://gis.rid.go.th/geoserver/rio12/wms/'
  },
  'rio12kraseaw':{
      'type': 'wms',
      'name': 'โครงการส่งน้ำและบำรุงรักษากระเสียว',
      'geometric': 'geom',
      'fields': {'fieldcode':'รหัส','owner':'เจ้าของ','lutype':'ประเภทการใช้ที่ดิน','planttype':'ประเภทพื้นที่','titledeed':'ประเภทการถือครอง'},
      'layers': 'rio12:kraseaw',
      'setzoom': {'lat':14.83601, 'lon':99.92031, 'zoom': 13},
      'url': 'http://gis.rid.go.th/geoserver/rio12/wms/'
  },
};
for( var sourcei_mncst in cst_config){
  for( var sub_mncst in cst_config[sourcei_mncst]){
    source[ sub_mncst ] = {};
  }
}
