$(document).ready(function () {
    config.overlays = [{
            groupName: "ข้อมูลพื้นฐานชลประทาน",
            layers: {}

        },
        {
            groupName: "Agri-Map (ขอบเขตความเหมาะสมการเพาะปลูกพืช)",
            layers: {}
        },
		{
            groupName: "รายงานการเพาะปลูกพืชของGistda",
            layers: {}
        },
        {
            groupName: "ระบบโทรมาตรขนาดใหญ่",
            layers: {
                //"ระบบโทรมาตรแม่กลอง": source.bigtelemetry.layer,
                "โทรมาตรขนาดใหญ่ลุ่มน้ำเจ้าพระยา": source.bigteleamr.layer,
                "โทรมาตรขนาดใหญ่ลุ่มน้ำท่าจีน": source.bigtelethachin.layer,
                "เหนือเขื่อนแควน้อยบำรุงแดน": source.kwainoidam.layer
            }
        },
        {
            groupName: "ข้อมูลน้ำในอ่าง",
            layers: {
                "สภาพน้ำในอ่างเก็บน้ำขนาดใหญ่": source.bigDam.layer,
                "สภาพน้ำในอ่างเก็บน้ำขนาดกลาง": source.middleDam.layer,
                "ประตูระบายน้ำ": source.floodGate.layer
            }
        }, {
            groupName: "โทรมาตรขนาดเล็ก",
            layers: {
                "Tele200": source.tele200.layer,
                "Tele127": source.tele127.layer
            }
        }, {
            // Allocate rainfall
            groupName: window.config.system.rainfall.menu.groupName,
            layers: {}
        }, {
            // Allocate water level
            groupName: "ข้อมูลระดับน้ำ",
            layers: {
                "ข้อมูลระดับน้ำ (สสนก.)": source.water_nhc.layer
            }
        }, {
            groupName: "แผนที่ผังน้ำ",
            expanded: false,
            layers: {
                "ผังน้ำ": source.flowwater.layer,

            }
        }, {
            groupName: "ภูมิอากาศ (กรมอุตุนิยมวิทยา)",
            layers: {
                "ข้อมูลอุตุนิยมวิทยา ทุก 3 ชม.": source.weather3Hours.layer,
                "แผนที่วิเคราะห์เส้นทางพายุ": source.weather3Hours.mapstorm,
                "สภาพภูมิอากาศ": source.weather3Hours.mapwindspeedrain,
                // "วิเคราะห์ปริมาณฝน": '',
            }
        }, {
            groupName: "คุณภาพน้ำ",
            layers: {
                "ลุ่มน้ำท่าจีน": source.thachin.layer,
                "ลุ่มน้ำบางปะกง": source.bangpakong.layer,
                "ลุ่มน้ำแม่กลอง": source.maeklong.layer,
                "เชื่อมโยงกับหน่วยงานกรมควบคุมมลพิษ1": source.waterquality.pcd1,
                "เชื่อมโยงกับหน่วยงานกรมควบคุมมลพิษ2": source.waterquality.pcd2,
                "เชื่อมโยงกับหน่วยงานการประปานครหลวง": source.waterquality.mwa
            }
        },
        {
            groupName: "ข้อมูลการเพาะปลูก",
            layers: {
                "ข้อมูลการเพาะปลูกทั้งประเทศ": source.wuse.layer,

                "ข้อมูลการเพาะปลูกลุ่มน้ำเจ้าพระยา": source.wusechaopraya.layer,
                "ข้อมูลการเพาะปลูกลุ่มน้ำแม่กลอง": source.wusemaeklong.layer
            }
        }



        , {
            groupName: "ข้อมูลสำนักงานชลประทานที่ 12",
            expanded: false,
            layers: {
                /*
                    "แปลงนา": source.wmsMap.Krasaew,
                    "พื้นที่โครงการ": source.wmsMap.KrasaewProjectArea,
                    "อ่างเก็บน้ำ": source.wmsMap.KrasaewReservoir,
                    "คลอง": source.wmsMap.KrasaewCanal,
                    "จุดปากคลอง": source.wmsMap.KrasaewCanalPoint,
                    "จุดปากคลองย่อย": source.wmsMap.KrasaewSmallCanalPoint
                */
            }
        }, {
            groupName: "ข้อมูลเขื่อน/การตรวจวัดพฤติกรรมเขื่อน",
            layers: {
                "เขื่อนขนาดใหญ่+ข้อมูลการตรวจวัด": source.bigdamBeh.layer,
                "เขื่อนขนาดกลาง+ข้อมูลการตรวจวัด": source.middamBeh.layer
            }
        }, {
            groupName: "ข้อมูลการโอนภารกิจ",
            layers: {

                "เขื่อนขนาดเล็ก": source.smalldamBeh.layer
            }
        }, {
            groupName: "สถาณการณ์น้ำ",
            layers: {

                "รายงานสถาณการณ์น้ำเชิงพื้นที่ในช่วงวิกฤติ": source.WaterSituation.layer
            }
        },{
            groupName: "cctv ติดตามสถานการณ์น้ำ",
            layers: {
                "CCTV": source.CCTV.layer,


            }
        },
        {
            groupName: "SCADA จากกรมฝนหลวง",
            layers: {}
        },
    ];

    //bigtelemetry & custom-layerGeoserver
    for (var cfOverlays in config.overlays) {
        if (config.overlays[cfOverlays].groupName == "ระบบโทรมาตรขนาดใหญ่") {
            for (var overi in VBigTelemety) {
                config.overlays[cfOverlays].layers[overi] = source[VBigTelemety[overi]].layer;
            }
        } else if (config.overlays[cfOverlays].groupName == "SCADA จากกรมฝนหลวง") {
          for (var vRoyalrain  in royalrain_config) {
              var vName = royalrain_config[vRoyalrain]['name'];
              config.overlays[cfOverlays].layers[vName] = source[ vRoyalrain ].layer;
          }
        }else if (!cst_config[config.overlays[cfOverlays].groupName] === false) {
            for (var cfOverlays_cst in cst_config[config.overlays[cfOverlays].groupName]) {
                if (cst_config[config.overlays[cfOverlays].groupName][cfOverlays_cst].display != false) {
                    var cf_cst_name = cst_config[config.overlays[cfOverlays].groupName][cfOverlays_cst].name;
                    config.overlays[cfOverlays].layers[cf_cst_name] = source[cfOverlays_cst].layer;
                }
            }
        } else if (config.overlays[cfOverlays].groupName == window.config.system.rainfall.menu.groupName) {
            Object.keys(window.config.system.rainfall.menu.sub_menu).reduce(function (obj, item) {
                var source_name = window.config.system.rainfall.menu.sub_menu[item].source_name;
                source[source_name] = {
                    "layer": new L.LayerGroup()
                };
                obj[item] = source[source_name].layer;
                return obj;
            }, config.overlays[cfOverlays].layers);
        }
    }
});
