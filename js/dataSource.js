var source = {
    bigDam: {},
    middleDam: {},
    floodGate: {},
    bigteleamr: {},
    bigtelethachin: {},
    tele200: {},
    tele127: {},
    flowwater: {},
    weather3Hours: {},
    thachin: {},
    bangpakong: {},
    maeklong: {},
    wuse: {},
    wusechaopraya: {},
    wusemaeklong: {},
    rainfall: {},
    rainfall_predict: {},
    water_nhc: {},
    irrproject: {},
    prjarea: {},
    irrarea: {},
    bigdamBeh: {},
    middamBeh: {},
    smalldamBeh: {},
    WaterSituation: {},
	CCTV: {},
    kwainoidam: {},
    agrimap: {
        rice_all: L.tileLayer.betterWms('http://agri-map-service.rid.go.th/geoserver/forimport/wms', {
            layers: 'forimport:Rice_ALL_wgs84',
            transparent: true,
            format: 'image/png'
        })
    },
    ridoffice: {
        ridoffice_all: L.tileLayer.betterWms('http://gis.rid.go.th/geoserver/ridbasic/wms', {
            layers: 'ridbasic:ridoffice_all_polygon',
            transparent: true,
            format: 'image/png'
        })
    },
    wmsMap: {
        Basin: L.tileLayer.betterWms('http://app.rid.go.th:8080/geoserver/wuse/wms', {
            layers: 'wuse:basin',
            transparent: true,
            format: 'image/png'
        }),
        Krasaew: L.tileLayer.betterWms('http://app.rid.go.th:8080/geoserver/Kraseaw/wms', {
            layers: 'Kraseaw:KRS',
            transparent: true,
            format: 'image/png'
            //   continuousWorld: true
            // bgcolor: 'FFFFFF'
        }),
        KrasaewProjectArea: L.tileLayer.betterWms('http://app.rid.go.th:8080/geoserver/Kraseaw/wms', {
            layers: 'Kraseaw:Prj_area',
            transparent: true,
            format: 'image/png'
            // bgcolor: 'FFFFFF'
        }),
        KrasaewReservoir: L.tileLayer.betterWms('http://app.rid.go.th:8080/geoserver/Kraseaw/wms', {
            layers: 'Kraseaw:reservoir',
            transparent: true,
            format: 'image/png'
            // bgcolor: 'FFFFFF'
        }),
        KrasaewCanal: L.tileLayer.betterWms('http://app.rid.go.th:8080/geoserver/Kraseaw/wms', {
            layers: 'Kraseaw:Canal',
            transparent: true,
            format: 'image/png'
            // bgcolor: 'FFFFFF'
        }),
        KrasaewCanalPoint: L.tileLayer.betterWms('http://app.rid.go.th:8080/geoserver/Kraseaw/wms', {
            layers: 'Kraseaw:canalpoint',
            transparent: true,
            format: 'image/png'
            // bgcolor: 'FFFFFF'
        }),
        KrasaewSmallCanalPoint: L.tileLayer.betterWms('http://app.rid.go.th:8080/geoserver/Kraseaw/wms', {
            layers: 'Kraseaw:smallcanalpoint',
            transparent: true,
            format: 'image/png'
            // bgcolor: 'FFFFFF'
        })
    }
}