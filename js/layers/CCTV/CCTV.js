$(document).ready(function () {


    source.CCTV.layer = new L.LayerGroup();
	/*$( "#iwocModal" ).resize(function() {
	  console.log($( "#iwocModal"  ).height());
	  $("#detailwindows").css({"max-height": $( "#iwocModal"  ).height()+"px", "max-width": $( "#iwocModal"  ).width()+"px"});
	});*/
	   $("#detailwindows").resizable('disable').removeClass('ui-state-disabled');
	

});//ready

function getLayerCCTVData(callback) {
    if (source.CCTV.geoJson == undefined) {
        block('#map');

        $.getJSON("http://gisdata.rid.go.th/upload/getCCTV.php", function (data) {
            if (data != null) {
                source.CCTV.geoJson = L.geoJSON(data, {

                    style: function (feature) {
                        return feature.properties && feature.properties.style;
                    },
                    onEachFeature: onEachCCTVFeature,
                    pointToLayer: function (feature, latlng) {
                        var smallIcon = L.icon({
                            iconSize: [15, 15],
                            iconAnchor: [13, 27],
                            popupAnchor: [1, -24],
                            iconUrl: 'images/CCTV/CCTV.png'
                        });
                        return L.marker(latlng, { icon: smallIcon });
                    }
                }).bindTooltip(function (e) {
                            return '' + e.feature.properties.station;
                        }, {
                            direction: 'top',
                            offset: L.point(0, -20)
                        }).addTo(source.CCTV.layer);
            }

        })
        .done(function () {
             callback($(source.CCTV.geoJson).length);
        })
        .fail(function (res) {
            alert('CCTV request failed! '+ JSON.stringify(res,null,'\t') );
        })
        .always(function () {
            unblock('#map');
        });

    } else {
        callback($(source.CCTV.geoJson).length);
    }
}

function onEachCCTVFeature(feature, layer) {

    if (feature.properties && feature.properties.station) {
        layer.on({
            //mouseover: highlightFeature,
            //mouseout: resetHighlight,
            click: function (e) {
                //console.warn(feature.properties.cresv);
                //$("#featuretab-info").html('');
                block('#map');
                $.get("js/layers/CCTV/modal.html",
                    function (data) {
                        $('#iwocModal').html(data);
                      
                        $("#featuretab-title").html('<strong>CCTV</strong><span class="glyphicon glyphicon-menu-right"></span> ' + ' <strong>' + feature.properties.station + '</strong>');
                        
						$("#detailwindows").attr('src',feature.properties.url);
                        $("#iwocModal").modal("show");
                        unblock('#map');
                    }
                );



            }
        });

    } 
}
