source.water_nhc.updateTabTable = function(start, end, data, cb) {
    $('#water_nhc #tab_table #content #date_start_text').html(start.format('D  MMMM ') + (parseInt(start.format('YYYY')) + 543));
    $('#water_nhc #tab_table #content #date_end_text').html(end.format('D  MMMM ') + (parseInt(end.format('YYYY')) + 543));
    $('#water_nhc #tab_table tbody').html('');
    if (data === null) return cb();
    data.features.forEach(function(item, idx) {
        var prop = item.properties;
        $('#water_nhc #tab_table tbody').append('<tr>' +
            ' <td align="right">' + (idx + 1) + '</td> ' +
            ' <td style="text-align: center;">' + moment(prop.telestation_datetime).utc().format('D  MMMM ') + (parseInt(start.format('YYYY')) + 543) + '</td> ' +
            ' <td style="text-align: center;">' + moment(prop.telestation_datetime).utc().format('HH:mm:ss') + '</td> ' +
            ' <td style="text-align: right;">' + prop.wl_msl + '</td> ' +
            ' </tr> ');
        if (idx === data.features.length - 1) {
            cb();
        }
    });
}