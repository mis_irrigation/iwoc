$(document).ready(function() {
    source.water_nhc.layer = new L.LayerGroup();
    observable.on('water_nhc_searchDuration', function(_start, _end) {
        block('#water_nhc #tab_table');
        block('#water_nhc #tab_graph');
        var start = _start || moment().add(-1, 'days'),
            end = _end || moment();
        source.water_nhc.loadDataAgo(source.water_nhc.currentPoint.properties.telestation_id, start.format('x'), end.format('x'), function(err, res) {
            observable.notify('water_nhc_setDefaultDate_calendar', start, end);
            if (err) {
                if(window.config.system.console) console.log(err);
                res = null;
            }
            source.water_nhc.updateTabTable(start, end, res, function() {
                unblock('#water_nhc #tab_table');
            });
            source.water_nhc.updateTabGraph(start, end, res, function() {
                unblock('#water_nhc #tab_graph');
            });
        });
    });
    observable.on('water_nhc_setDefaultDate_calendar', function(start, end) {
        $('#water_nhc #tab_table #date_start, #water_nhc #tab_graph #date_start').datepicker("update", start.format('DD/MM/YYYY'));
        $('#water_nhc #tab_table #date_end, #water_nhc #tab_graph #date_end').datepicker("update", end.format('DD/MM/YYYY'));
        // update btn_dayAgo
        var dateDiff = parseInt(moment(end.diff(start)).format('D')) - 1;
        $('#water_nhc .btn_dayAgo').each(function(index, element) {
            parseInt(element.value) === dateDiff ? $(element).addClass('btn-primary').removeClass('btn-default') : $(element).removeClass('btn-primary').addClass('btn-default')
        });
    });

    source.water_nhc.legend = L.control({ position: 'bottomright' });
    source.water_nhc.legend.onAdd = function(map) {
        var div = L.DomUtil.create('div', 'info legend'),
            labels = ["ไม่มีข้อมูล", "<=10", ">10-30", ">30-70", ">70-100", ">100"],
            color = ['white', '#d97f32', '#fbff39', '#44d182', '#619eda', '#fe0010'];
        div.innerHTML = 'ความจุลำน้ำ <br>' + labels.map(function (item, i) { 
            return '<i style="background: '+color[i]+'"></i> ' + item +' &nbsp; ';
        }).join('<br>');
        return div;
    };
});

source.water_nhc.getData = function(callback) {
    if (source.water_nhc.geoJson === undefined) {
        var start = moment().add(-15, 'days').format('x');
        var end = moment().format('x');
        block('#map');
        if(window.config.system.console) console.log(window.config.system.server+"/water/waterLast?time_end=" + end + "&time_start=" + start);
        $.getJSON(window.config.system.server+"/water/waterLast?time_end=" + end + "&time_start=" + start, function(data) {
                if (data !== null) {
                    source.water_nhc.geoJson = L.geoJSON(data, {
                        style: function(feature) {
                            return feature.properties && feature.properties.style;
                        },
                        onEachFeature: source.water_nhc.onEachFeature,
                        pointToLayer: function(feature, latlng) {
                            // matching icon with status
                            var icon_index = window.config.system.water_nhc.status.indexOf(feature.properties.status);

                            var smallIcon = L.icon({
                                iconSize: window.config.system.water_nhc.icon_size,
                                iconAnchor: [13, 27],
                                popupAnchor: [1, -24],
                                iconUrl: window.config.system.water_nhc.icon[ icon_index+1 ] // icon[ 0 ] is default 
                            });
                            return L.marker(latlng, { icon: smallIcon });
                        }
                    }).bindTooltip(function (e) {
                        return 'สถานีวัดน้ำท่า '+e.feature.properties.term;
                     }, {direction: 'top', offset: L.point(0, -20)}).addTo(source.water_nhc.layer);
                    callback();
                }
                unblock('#map');

            })
            .fail(function(err) {
                unblock('#map');
                callback(err);
            });
    } else {
        // Incase already download
        callback(null);
    }
};

source.water_nhc.onEachFeature = function(feature, layer) {
    if (feature.properties) {
        feature.properties.term = feature.properties.name;
        layer.on({
            click: function(e) {
                block('#map');
                source.water_nhc.currentPoint = feature;
                var prop = feature.properties;
                var lat_long = feature.geometry.coordinates;
                $.get("js/layers/water_nhc/modal.html",
                    function(modalTemplate) {
                        $('#iwocModal').htmlReadAble(modalTemplate);
                        $('#water_nhc #featuretab-title').htmlReadAble('<strong> สถานีวัดน้ำท่า  </strong> <span class="glyphicon glyphicon-menu-right"></span> <strong>' + prop.name + '</strong>');
                        $('#water_nhc .telestation_datetime').htmlReadAble(moment(prop.telestation_datetime).clone().add(543, 'years').utc().format('DD MMMM YYYY เวลาประมาณ HH:mm'));
                        $('#water_nhc .district_name').htmlReadAble(prop.district);
                        $('#water_nhc .geocode_basin').htmlReadAble(prop.geocode_basin);
                        $('#water_nhc .ground_level').htmlReadAble(prop.ground_level);
                        $('#water_nhc .left_bank').htmlReadAble(prop.left_bank);
                        $('#water_nhc .old_id').htmlReadAble(prop.old_id);
                        $('#water_nhc .status').htmlReadAble(prop.status);
                        $('#water_nhc .province_name').htmlReadAble(prop.province);
                        $('#water_nhc .amphoe_name').htmlReadAble(prop.amphoe);
                        $('#water_nhc .region_name').htmlReadAble(prop.region_name);
                        $('#water_nhc .right_bank').htmlReadAble(prop.right_bank);
                        $('#water_nhc .telestation_id').htmlReadAble(prop.telestation_id + ' ( ' + prop.old_id.trim() + ' )');
                        $('#water_nhc .telestation_name').htmlReadAble(prop.name);
                        $('#water_nhc .lat_long').htmlReadAble(lat_long[1] + ', ' + lat_long[0]);
                        $('#water_nhc .wl_msl').htmlReadAble(prop.wl_msl);

                        $("#iwocModal").modal("show");
                        $("#iwocModal").on('hidden.bs.modal', function() {
                            if (source.water_nhc.ajaxLoadDataAgo) source.water_nhc.ajaxLoadDataAgo.abort();
                        });

                        // load selected station
                        observable.notify('water_nhc_searchDuration');

                        // ON Modal Shown
                        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                            var target = $(e.target).attr("href");
                            if (target === '#tab_graph' || target === '#tab_table' || target === '#tab_graph1')
                                $('.modal-dialog').removeClass('modal-md').addClass('modal-lg');
                            else
                                $('.modal-dialog').removeClass('modal-lg').addClass('modal-md');
                        });
                        unblock('#map');
                    }
                );
            }
        });

    }
};

source.water_nhc.loadDataAgo = function(telestation_id, date_start, date_end, cb) {
    if(window.config.system.console) console.log(new Date(parseInt(date_start)), new Date(parseInt(date_end)), window.config.system.server+"/water/water?telestation_id=" + telestation_id + "&time_start=" + date_start + "&&time_end=" + date_end);
    source.water_nhc.ajaxLoadDataAgo = $.getJSON(window.config.system.server+"/water/water?telestation_id=" + telestation_id + "&time_start=" + date_start + "&&time_end=" + date_end)
        // $.getJSON("/js/layers/water_nhc/mockDataDateBetween.json")
        .done(function(data) {
            cb(null, data);
        })
        .fail(function(err) {
            cb(err);
        });
};