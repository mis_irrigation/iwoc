source.water_nhc.chartObj = {};
source.water_nhc.updateTabGraph = function(start, end, geoData, cb) {
    $('#water_nhc #tab_graph #content #date_start_text').html(start.format('D  MMMM ') + (parseInt(start.format('YYYY')) + 543));
    $('#water_nhc #tab_graph #content #date_end_text').html(end.format('D  MMMM ') + (parseInt(end.format('YYYY')) + 543));
    if (source.water_nhc.chartObj.elm) source.water_nhc.chartObj.elm.destroy();
    if (geoData === null) return cb();
    source.water_nhc.chartObj.setDatasets(geoData, function(err, chartData) {
        Chart.plugins.register({
            afterEvent: function(chart, e) {
                if (e.chart.canvas.id === 'water_chart_element' && e.type === 'mousemove') {
                    chart.options.customLine.x = e.x;
                }
            },
            afterDraw: function(chart, easing) {
                var ctx = chart.chart.ctx;
                if (ctx.canvas.id !== 'water_chart_element') return;
                var chartArea = chart.chartArea;
                var x = chart.options.customLine.x;

                if (!isNaN(x)) {
                    ctx.save();
                    ctx.strokeStyle = chart.options.customLine.color;
                    ctx.moveTo(chart.options.customLine.x, chartArea.bottom);
                    ctx.lineTo(chart.options.customLine.x, chartArea.top);
                    ctx.stroke();
                    ctx.restore();
                }
            }
        });
        var ctx = $('#water_nhc #tab_graph #water_chart_element')[0];
        source.water_nhc.chartObj.elm = new Chart(ctx, {
            type: 'line',
            data: chartData,
            options: source.water_nhc.chartObj.getOption()
        });
        cb();
    });
};

source.water_nhc.chartObj.getOption = function() {
    return {
        customLine: {
            color: 'black'
        },
        tooltips: {
            intersect: false,
            mode: 'label',
        },
        hover: {
            mode: 'label'
        },
        chartArea: {
            backgroundColor: '#EEFBFC', //สีพื้นหลัง            
        },
        bezierCurve: false,
        datasetFill: false,
        scaleGridLineColor: "#3E434E",
        legendCallback: function(chart) {
            var legendHtml = '';
            legendHtml += '<div class="tbl_legend_chart" align="center">';
            for (var i = 0; i < chart.data.datasets.length; i++) {
                if (chart.data.datasets[i].label) {
                    //---
                    if (i == 2) {
                        legendHtml += '<br>';
                    }
                    legendHtml += '<span class="chart-legend-label-text text-left" onclick="updateDataset(event, ' + '\'' + chart.legend.legendItems[i].datasetIndex + '\'' + ', this)"  > <button type="button" class="legend_color_line" style="border-bottom:solid 4px ' + chart.data.datasets[i].borderColor + '; "></button>  ' + chart.data.datasets[i].label + '</span>';
                }
            } //for
            legendHtml += '</div>';
            return legendHtml;
        },
        scales: {
            xAxes: [{
                type: 'time',
                time: {
                    unit: 'day',
                    displayFormats: {
                        'millisecond': 'DD/MM/YYYY',
                        'second': 'DD/MM/YYYY',
                        'minute': 'DD/MM/YYYY',
                        'hour': 'DD/MM/YYYY',
                        'day': 'DD/MM/YYYY',
                        'week': 'DD/MM/YYYY',
                        'month': 'DD/MM/YYYY',
                        'quarter': 'DD/MM/YYYY',
                        'year': 'DD/MM/YYYY',
                    }
                },
                scaleLabel: {
                    display: true,
                    labelString: 'วันที่'
                },
            }],
            yAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'ระดับน้ำ ( ม.รทก. )'
                },
                ticks: {
                    // beginAtZero: true,
                    //stepSize : y_step,
                    // max: y_max,
                }
            }],
            chartArea: {
                backgroundColor: 'rgba(238, 247, 253, 0.4)'
            },
        }
    };
};

source.water_nhc.chartObj.getLineStyle = function(owner, idx) {
    var colors_pattern = source.water_nhc.colors_pattern;
    return $.extend({
        fill: false,
        lineTension: 0.1,
        backgroundColor: colors_pattern[idx].fillColor,
        borderColor: colors_pattern[idx].strokeColor,
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: colors_pattern[idx].strokeColor,
        pointBackgroundColor: "#fff",
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: colors_pattern[idx].strokeColor,
        pointHoverBorderColor: colors_pattern[idx].pointHighlightStroke,
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        spanGaps: true
    }, owner);
};

source.water_nhc.chartObj.setDatasets = function(geoData, cb) {
    var labels = [];
    var datasets = [];
    var chartData = {
        labels: labels,
        datasets: datasets,
    };
    var wl_msl = [],
        left_bank = [],
        right_bank = [],
        ground_level = [];
    geoData.features.forEach(function(item, idx) {
        var prop = item.properties;
        labels.push(moment(prop.telestation_datetime).utc());
        wl_msl.push(prop.wl_msl);
        left_bank.push(prop.left_bank);
        right_bank.push(prop.right_bank);
        if (idx === geoData.features.length - 1) {
            datasets.push(source.water_nhc.chartObj.getLineStyle({
                label: 'ระดับน้ำ',
                data: wl_msl,
            }, 0));
            datasets.push(source.water_nhc.chartObj.getLineStyle({
                label: 'ระดับตลิ่งด้านซ้าย',
                data: left_bank,
            }, 1));
            datasets.push(source.water_nhc.chartObj.getLineStyle({
                label: 'ระดับตลิ่งด้านขวา',
                data: right_bank,
            }, 2));
            datasets.push(source.water_nhc.chartObj.getLineStyle({
                label: 'ระดับท้องน้ำ',
                data: ground_level,
            }, 3));
            cb(null, chartData);
        }
    });
};