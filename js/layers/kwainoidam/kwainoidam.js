$(document).ready(function () {
    source.kwainoidam.layer = new L.LayerGroup();
});

source.kwainoidam.blueIconUrl = "images/telemetry/blue-sign.png";

source.kwainoidam.blueIcon = new L.Icon({
    iconUrl: source.kwainoidam.blueIconUrl,
    shadowUrl: source.kwainoidam.shadowIconUrl,
    iconSize: [20.5, 20.5],
    iconAnchor: [20, 20.5],
    popupAnchor: [0.5, -17],
    shadowSize: [20.5, 20.5]
});

source.kwainoidam.getkwainoidamLayer = function (callback) {
    // function getkwainoidamLayer(callback) {
    if (source.kwainoidam.geoJson == undefined) {
        block('#map');
        $.getJSON("http://hyd-app.rid.go.th/webservice/wstele127geojson.ashx",
                function (data) {
                    if (data != null) {
                        source.kwainoidam.geoJson = L.geoJSON(data, {
                            style: function (feature) {
                                return feature.properties && feature.properties.style;
                            },
                            onEachFeature: source.kwainoidam.onEachFeature,
                            pointToLayer: source.kwainoidam.pointToLayer
                        }).bindTooltip(function (e) {
                            return '' + e.feature.properties.StationName;
                        }, {
                            direction: 'top',
                            offset: L.point(0, -20)
                        }).addTo(source.kwainoidam.layer);
                    }
                })
            .done(function () {
                callback($(source.kwainoidam.geoJson).length);
            })
            .fail(function () {
                alert('kwainoidam request failed! ');
            })
            .always(function () {
                unblock('#map');
            });
    } else {
        callback($(source.kwainoidam.geoJson).length);
    }
}

source.kwainoidam.onEachFeature = function (feature, layer) {
    if (feature.properties) {
        var winfo = "";
        var rinfo = "";

        if (feature.properties.StationID === 388) {
            var stationcode = feature.properties.StationCode;
            var name = feature.properties.StationName;
            var WaterLevel = feature.properties.WaterLevel;

            var RainFall = feature.properties.RainFall;
            var WLLastTimeString = feature.properties.WLLastTimeString;

            if (WaterLevel == NaN || WaterLevel == undefined || WaterLevel == null)
                winfo = "";
            else
                winfo = source.kwainoidam.tablekwainoidamWaterlevel(WLLastTimeString, WaterLevel);

            if (stationcode == null)
                stationcode = "";
        }
        layer.on({
            click: function (e) {
                $.get("js/layers/kwainoidam/modal.html",
                    function (data) {

                        $('#iwocModal').html(data);
                        $("#featurekwainoidam-title").html(stationcode + " " + name);
                        $("#featurekwainoidam-WL").html(winfo);
                        $("#featurekwainoidam-R").html(rinfo);

                        var tabID = "featurekwainoidam-WL";
                        if (winfo != "")
                            tabID = "featurekwainoidam-WL";
                        else {
                            if (rinfo != "") {
                                tabID = "featurekwainoidam-R";
                            }
                        }

                        $('.nav-tabs a[href="#' + tabID + '"]').tab('show');
                        $("#iwocModal").modal("show");
                    }
                );

            }
        });
    }
}

source.kwainoidam.pointToLayer = function (feature, latlng) {
    if (feature.properties.StationID === 388) {
        /////////////////////////////////////////////////////////////////
        var violetMarker = L.marker(latlng, {

            icon: source.kwainoidam.blueIcon

        });

        ////////////////////////////////////////////////////////////////

        var markerUSE = violetMarker;

        //if (feature.properties.StationID > 0) {


        //    /////////////////////////////////////////
        //    /////////////////////////////////////////

        //    var StationID = feature.properties.StationID;
        //    var StationCode = feature.properties.StationCode;
        //    var StationName = feature.properties.StationName;

        //    var HydroID = feature.properties.HydroID;

        //    var RainFall = feature.properties.RainFall;

        //    var WaterLevel = feature.properties.WaterLevel;
        //    var WLLastTimeString = feature.properties.WLLastTimeString;
        //    var RainfallLastTimeString = feature.properties.RainfallLastTimeString;


        //    //    alert("hi");
        //} 
        /////////////////////////////////////////////////////////////////


        return markerUSE;
    }
}

source.kwainoidam.tablekwainoidamWaterlevel = function (last_date, waterlevel) {
    var html = "";
    html = '<table class="table table-striped table-bordered table-condensed">' +
        '<tr><th colspan="2" class="text-center">วันที่ ' + last_date + '</th></tr>' +
        //'<tr><td class="success">สถานการณ์</td><td class="success">' + winfo + '</td></tr>' +
        '<tr><td>ระดับน้ำ</td><td>' + commafy(waterlevel, 2) + ' ม.</td></tr>' +
        '<table>';
    return html;
}