$(document).ready(function () {
    source.tele127.layer = new L.LayerGroup();




});//ready

function getTele127Data(callback) {
    if (source.tele127.geoJson == undefined) {
        block('#map');
        $.getJSON("http://hyd-app.rid.go.th/webservice/wstele127geojson.ashx", function (data) {
            if (data !== null) {
                source.tele127.geoJson = L.geoJSON(data, {

                    style: function (feature) {
                        return feature.properties && feature.properties.style;
                    },

           
                    onEachFeature: onEachTele127Feature,


                    pointToLayer: function (feature, latlng) {
                        /////////////////////////////////////////////////////////////////
                        var lat;
                        var lng;
                        var latlngNew;

                        switch (feature.properties.StationCode) {
                            case "567":
                                lat = 12.73796;
                                lng = 101.2284;
                                latlngNew = L.latLng(lat, lng);
                                break;
                        
                            default:
                                latlngNew = latlng;
                                break;

                        }

                        //  });//.bindTooltip("My Label", {permanent: true, className: "my-label", offset: [0, 0] });
                        /////////////////////////////////////////////////////////////////
                        var greenMarker = L.marker(latlngNew, {

                            icon: greenIcon
                            //  highlight: HLIconTempString//"permanent","temporary"
                        });
                      
                        ////////////////////////////////////////////////////////////////

                        var markerUSE = greenMarker;

                        //if (feature.properties.StationID > 0) {

              
                        //    /////////////////////////////////////////
                        //    /////////////////////////////////////////

                        //    var StationID = feature.properties.StationID;
                        //    var StationCode = feature.properties.StationCode;
                        //    var StationName = feature.properties.StationName;
                       
                        //    var HydroID = feature.properties.HydroID;
                       
                        //    var RainFall = feature.properties.RainFall;
                          
                        //    var WaterLevel = feature.properties.WaterLevel;
                        //    var WLLastTimeString = feature.properties.WLLastTimeString;
                        //    var RainfallLastTimeString = feature.properties.RainfallLastTimeString;
                     

                        //    //    alert("hi");
                        //} 
                        /////////////////////////////////////////////////////////////////


                        return markerUSE;


                    }

                }).addTo(source.tele127.layer);
            }

        })
        .done(function () {
            callback($(source.tele127.geoJson).length);
        })
        .fail(function () {
            alert('Tele127 request failed! ');
        })
        .always(function () {
            unblock('#map');
        });

    } else {
        callback($(source.tele127.geoJson).length);
    }

}


function tableTele127Waterlevel(last_date, waterlevel) {
    var html = "";
  




        html = '<table class="table table-striped table-bordered table-condensed">' +
            '<tr><th colspan="2" class="text-center">วันที่ ' + last_date + '</th></tr>' +
            //'<tr><td class="success">สถานการณ์</td><td class="success">' + winfo + '</td></tr>' +
            '<tr><td>ระดับน้ำ</td><td>' + commafy(waterlevel, 2) + ' ม.</td></tr>' +
          
            '<table>';    

    return html;

}


function tableTele127Rain(last_date,rainfall) {
    var html = "";

  

        html = '<table class="table table-striped table-bordered table-condensed">' +
            '<tr><th colspan="2" class="text-center">วันที่ ' + last_date + '</th></tr>' +
            //'<tr><td class="success">สถานการณ์</td><td class="success">' + rinfo + '</td></tr>' +
            '<tr><td>ปริมาณน้ำฝน</td><td>' + commafy(rainfall, 2) + ' มม.</td></tr>' +
            //'<tr><td>ระดับวิกฤต</td><td>' + commafy(rmaxlv, 2) + ' มม.</td></tr>' +
            '<table>';
    

    return html;

}



function onEachTele127Feature(feature, layer) {
    if (feature.properties ) {
        var winfo = "";
        var rinfo = "";
     
        if (feature.properties.StationID  > 0) {


            var stationcode = feature.properties.StationCode;
            var name = feature.properties.StationName;
            var WaterLevel = feature.properties.WaterLevel;
 
            var RainFall = feature.properties.RainFall;
            var WLLastTimeString = feature.properties.WLLastTimeString;
            var RainfallLastTimeString = feature.properties.RainfallLastTimeString;


            if (WaterLevel == NaN || WaterLevel == undefined || WaterLevel == null)
                winfo = "";
            else
                winfo = tableTele127Waterlevel(WLLastTimeString, WaterLevel);

            if (RainFall == NaN || RainFall == undefined || RainFall == null)
                rinfo = "";
            else
                rinfo = tableTele127Rain(RainfallLastTimeString, RainFall);


            if (stationcode == null)
                stationcode = "";


         
          
        } 
        layer.on({
            click: function (e) {
                $.get("js/layers/tele127/modal.html",
                    function (data) {

                      

                        $('#iwocModal').html(data);
                        $("#featureTele127-title").html(stationcode + " " + name);
                        $("#featureTele127-WL").html(winfo);
                        $("#featureTele127-R").html(rinfo);
                     
                        var tabID = "featureTele127-WL";
                        if (winfo != "")
                            tabID = "featureTele127-WL";
                        else {
                            if (rinfo != "") {
                                tabID = "featureTele127-R";
                            }
                        }
                        
                     

                        $('.nav-tabs a[href="#' + tabID + '"]').tab('show');
                        $("#iwocModal").modal("show");
                    }
                );

            }
        });
    }
}
  
