$(document).ready(function () {
    source.bigDam.legend = L.control({ position: 'bottomright' });
    source.bigDam.legend.onAdd = function (map) {
        var div = L.DomUtil.create('div', 'info legend'),
            grades = [0, 30, 50, 80],
            labels = [],
            from, to;
        labels.push('ปริมาตรน้ำ(%)');
        for (var i = 0; i < grades.length; i++) {
            from = grades[i];
            to = grades[i + 1];
            labels.push('<i style="background:' + source.bigDam.getColor(from + 1) + '"></i> ' + from + (to ? '&ndash;' + to : '+'));
        }
        div.innerHTML = labels.join('<br>');
        return div;
    };
    source.bigDam.layer = new L.LayerGroup();

  //  alert("hi");



});


source.bigDam.bigDamID = 0;

source.bigDam.greenIconUrl = "images/dam/color-dam6.png";
source.bigDam.blueIconUrl = "images/dam/color-dam3.png";
source.bigDam.redIconUrl = "images/dam/color-dam11.png";
source.bigDam.greyIconUrl = "images/dam/dam.png";
source.bigDam.yellowIconUrl = "images/dam/color-dam2.png";

source.bigDam.shadowIconUrl = "libraries/leaflet1.0.3/images/marker-shadow.png";

source.bigDam.greenIcon = new L.Icon({
    iconUrl: source.bigDam.greenIconUrl,
    shadowUrl: source.bigDam.shadowIconUrl,
    iconSize: [16.5, 20.5],
    iconAnchor: [20, 20.5],
    popupAnchor: [0.5, -17],
    shadowSize: [20.5, 20.5]
});
source.bigDam.blueIcon = new L.Icon({
    iconUrl: source.bigDam.blueIconUrl,
    shadowUrl: source.bigDam.shadowIconUrl,
    iconSize: [16.5, 20.5],
    iconAnchor: [20, 20.5],
    popupAnchor: [0.5, -17],
    shadowSize: [20.5, 20.5]
});
source.bigDam.redIcon = new L.Icon({
    iconUrl: source.bigDam.redIconUrl,
    shadowUrl: source.bigDam.shadowIconUrl,
    iconSize: [16.5, 20.5],
    iconAnchor: [20, 20.5],
    popupAnchor: [0.5, -17],
    shadowSize: [20.5, 20.5]
});
source.bigDam.greyIcon = new L.Icon({
    iconUrl: source.bigDam.greyIconUrl,
    shadowUrl: source.bigDam.shadowIconUrl,
    iconSize: [16.5, 20.5],
    iconAnchor: [20, 20.5],
    popupAnchor: [0.5, -17],
    shadowSize: [20.5, 20.5]
});
source.bigDam.yellowIcon = new L.Icon({
    iconUrl: source.bigDam.yellowIconUrl,
    shadowUrl: source.bigDam.shadowIconUrl,
    iconSize: [16.5, 20.5],
    iconAnchor: [20, 20.5],
    popupAnchor: [0.5, -17],
    shadowSize: [20.5, 20.5]
});

source.bigDam.violetIcon = new L.Icon({
    iconUrl: source.bigDam.violetIconUrl,
    shadowUrl: source.bigDam.shadowIconUrl,
    iconSize: [16.5, 20.5],
    iconAnchor: [20, 20.5],
    popupAnchor: [0.5, -17],
    shadowSize: [20.5, 20.5]
});


source.bigDam.getIcon = function (QTotal) {
    var icon = source.bigDam.greyIcon;
    if (QTotal <= 30)
        icon = source.bigDam.yellowIcon; //yellow
    else if (QTotal > 30 && QTotal <= 50)
        icon = source.bigDam.greenIcon; //green
    else if (QTotal > 50 && QTotal <= 80)
        icon = source.bigDam.blueIcon; //blue
    else if (QTotal > 80)
        icon = source.bigDam.redIcon; //red
    else
        icon = source.bigDam.greyIcon; // no data
    return icon
};

source.bigDam.getLayerData = function (callback) {
    if (source.bigDam.geoJson == undefined) {
        block('#map');

        $.getJSON("http://app.rid.go.th/webservice/getDamDataGeoJson.ashx", function (data) {
            if (data !== null) {
                source.bigDam.geoJson = L.geoJSON(data, {
                    style: function (feature) {
                        return feature.properties && feature.properties.style;
                    },
                    onEachFeature: source.bigDam.onEachFeature,// onEachFeature,
                    pointToLayer: function (feature, latlng) {


                        return L.marker(latlng, {

                            icon: source.bigDam.getIcon(feature.properties.QTotal)

                        }).bindTooltip(function (e) {
                            return '' + e.feature.properties.DamName;
                        }, { direction: 'top', offset: L.point(0, -20) })

                        //return L.circleMarker(latlng, {
                        //    radius: 8,
                        //    fillColor: getColor(feature.properties.QTotal),
                        //    color: "#000",
                        //    weight: 1,
                        //    opacity: 1,
                        //    fillOpacity: 0.8
                        //}) //.bindTooltip("My Label", {permanent: true, className: "my-label", offset: [0, 0] });



                    }
                }).addTo(source.bigDam.layer);
            }
        })
            .done(function () {
                unblock('#map');
            callback($(source.bigDam.geoJson).length);
        })
            .fail(function () {
                unblock('#map');
            alert('bigDam request failed! ');
        })
        .always(function () {
            unblock('#map');
        });

    } else {
        unblock('#map');
        callback($(source.bigDam.geoJson).length);
    }
};

source.bigDam.getColor = function(QTotal) {
    var fillColor = "#FAFAFA";
    if (QTotal <= 30)
        fillColor = "#FFFF00"; //yellow
    else if (QTotal > 30 && QTotal <= 50)
        fillColor = "#00C853"; //green
    else if (QTotal > 50 && QTotal <= 80)
        fillColor = "#1976D2"; //blue
    else if (QTotal > 80)
        fillColor = "#FF4081"; //red
    else
        fillColor = "#FAFAFA"; // no data
    return fillColor
}




source.bigDam.highlightFeature = function(e) {
    var layer = e.target;
    layer.setStyle({
        weight: 5,
        color: '#666',
        dashArray: '',
        fillOpacity: 0.8
    });
    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
    }
}

source.bigDam.resetHighlight = function(e) {
    source.bigDam.geoJson.resetStyle(e.target);
}

source.bigDam.onEachFeature = function (feature, layer) {
    var popupContentReport = "";
    var popupContentBasicData = "";
    if (feature.properties && feature.properties.DamName) {
        popupContentReport = tableStringBigDamReportData(feature.id, feature.properties.DamName,
            feature.properties.Date,
            feature.properties.DUL_Useless,
            feature.properties.QMax,
            feature.properties.QStore,
            feature.properties.QUsage,
            feature.properties.DUL_BeginDate, feature.properties.Q, feature.properties.Inflow, feature.properties.Outflow);
        popupContentBasicData = tableStringBigDamBasicData(feature.id, feature.properties.DamName,
            feature.properties.Date,
            feature.properties.DUL_Useless,
            feature.properties.QMax,
            feature.properties.QStore,
            feature.properties.QUsage,
            feature.properties.DUL_BeginDate, feature.properties.Q, feature.properties.Inflow, feature.properties.Outflow);
        layer.on({
            mouseover: source.bigDam.highlightFeature,
            mouseout: source.bigDam.resetHighlight,
            click: function (e) {

       

                source.bigDam.bigDamID = feature.id;

                $.get("js/layers/bigdam/modal.html",
                    function (data) {


                        $('#iwocModal').html(data);
                        $("#featuretab-title").html(feature.properties.DamName);
                        $("#featuretab-report").html(popupContentReport);
                        $("#featuretab-basicdata").html(popupContentBasicData);
                        $("#iwocModal").modal("show");
                    }
                );
            }
        });
        //source.bigDam.search = [];
        //source.bigDam.search.push({
        //    name: layer.feature.properties.DamName,
        //    qtotal: layer.feature.properties.QTotal,
        //    source: "BigDam",
        //    id: L.stamp(layer),
        //    lat: layer.getLatLng().lat,
        //    lng: layer.getLatLng().lng
        //});
    }
};
//getLayerData();

function tableStringBigDamBasicData(damid, damname, Date, DUL_Useless, QMax, QStore, QUsage, DUL_BeginDate, Q, Inflow, Outflow) {
    var html = '<table class="table table-striped table-bordered table-condensed">' +
        '<tr><th  colspan="2" class="text-center">ข้อมูลวันที่ ' + DUL_BeginDate + '</th></tr>' +
        '<tr><td> ความจุที่ รนส. (ล้านลบ.ม.)</td><td>' + commafy(QMax, 0) + '</td></tr>' +
        '<tr><td> ความจุที่ รนก. (ล้านลบ.ม.)</td><td>' + commafy(QStore, 0) + '</td></tr>' +
        '<tr><td>ปริมาตรเก็บกัก (ล้านลบ.ม.)</td><td>' + commafy(QUsage, 0) + '</td></tr>' +
        '<table>'

    html += "<br/>";
    html += "หมายเหตุ&nbsp;:&nbsp;&nbsp;รนส. หมายถึง ระดับน้ำสูงสุด<br/>";
    html += "<span style='margin-left:5em;'>รนก. หมายถึง ระดับน้ำกักเก็บ</span>";
    
    return html;
};

function tableStringBigDamReportData(damid, damname, Date, DUL_Useless, QMax, QStore, QUsage, DUL_BeginDate, Q, Inflow, Outflow) {
    var x1 = ((Q + DUL_Useless) * 100) / QStore;
    var p1 = commafy(x1, 2);
    var x2 = (Q / QUsage) * 100;
    var p2 = commafy(x2, 2);
    var html = '<table class="table table-striped table-bordered table-condensed">' +
        '<tr><th colspan="2" class="text-center">ข้อมูลวันที่ ' + Date + '</th></tr>' +
        
        '<tr><td>ปริมาตรน้ำในอ่าง (ล้านลบ.ม.)</td><td>' + commafy((Q + DUL_Useless), 2) + ' (' + p1 + '%)</td></tr>' +
        '<tr><td>ปริมาตรใช้การ (ล้านลบ.ม.)</td><td>' + commafy(Q, 2) + ' (' + p2 + '%)</td></tr>' +
        '<tr><td>ปริมาตรน้ำไหลเข้า (ล้านลบ.ม.)</td><td>' + commafy(Inflow, 2) + '</td></tr>' +
        '<tr><td>ปริมาตรน้ำระบาย (ล้านลบ.ม.)</td><td>' + commafy(Outflow, 2) + '</td></tr>' +
        //  '<tr><th>Website</th><td><a href="' + feature.properties.URL + '" target="_blank">' + feature.properties.URL + '</a></td></tr>' +
        '<table>';
    return html;
}
