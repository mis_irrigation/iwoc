// JavaScript Document
var date_start;
var date_end;
var rsvmiddle;
var percent;

var _labels = [];
var _datasets = [];
var _label = '';
var _data = [], _data_cap_resv = [], _data_low_qdisc = [];
var chart;
var arr_color = ['#0000FF', '#F68401', '#009900', '#885502', '#6A0D55', '#0763F6', '#AC698A', '#676515', '#6A0221', '#344E36'];
var year_start, month_start;
var year_end, month_end;
var y_max, arr_y_max = [], y_step;
var i = 0;


//------------------------------------------------------------------------------------                      
$(function () {



    //------------------------------------------------------------------------------------------
    $('#date_start_graph').datepicker({
        language: "th-th",
        autoclose: true
    })
        .on('changeDate', function (e) {
            // date_start=chg_date_en(e.format());
        });
    //---------------------------------------------------------
    $('#date_end_graph').datepicker({
        language: "th-th",
        autoclose: true
    })
        .on('changeDate', function (e) {
            //date_end=chg_date_en(e.format());
        });






}); //ready



function fnc_middleGraph() {

    //$('#show_date_start').html( chg_date_thai(chg_date_en($('#date_start_graph').val())) );
    //$('#show_date_end').html( chg_date_thai(chg_date_en($('#date_end_graph').val())) );

    //load_chart1('2009-01-01', '2009-12-31');
    //console.clear();

    arr_y_max = [];
    y_max = 20;
    y_step = 2;
    _labels = [];
    _data = [];
    _datasets = [];
    i = 0;
    //-------------------------------------------------------

    year_start = chg_date_en($('#date_start_graph').val()).substr(0, 4);
    year_end = chg_date_en($('#date_end_graph').val()).substr(0, 4);

    month_start = chg_date_en($('#date_start_graph').val()).substr(4, 6);
    month_end = chg_date_en($('#date_end_graph').val()).substr(4, 6);

    date_start = year_start + month_start;
    date_end = year_end + month_end;

    if (year_start == year_end) { //ถ้าช่อง date_start และ date_end เป็นปีเดียวกัน 
        load_chart(date_start, date_end);
    } else {
        load_chart(date_start, year_start + '-12-31');
    }





    //====Chart 2=======================================
    arr_y_max2 = [];
    y_max2 = 0.05;
    y_step2 = 0.01;
    _labels2 = [];
    _data2 = [];
    _data_q_info = [];
    _data_q_outfo = [];
    _datasets2 = [];
    q_info_arr = [];
    i2 = 0;
    //-------------------------------------------------------

    year_start2 = chg_date_en($('#date_start_graph').val()).substr(0, 4);
    year_end2 = chg_date_en($('#date_end_graph').val()).substr(0, 4);

    month_start2 = chg_date_en($('#date_start_graph').val()).substr(4, 6);
    month_end2 = chg_date_en($('#date_end_graph').val()).substr(4, 6);

    date_start2 = year_start2 + month_start2;
    date_end2 = year_end2 + month_end2;

    if (year_start2 == year_end2) { //ถ้าช่อง date_start และ date_end เป็นปีเดียวกัน 
        load_chart2(date_start2, date_end2);
    } else {
        load_chart2(date_start2, year_start2 + '-12-31');
    }


} //fnc_middleGraph() 


function load_chart(date_start, date_end) {

    console.info('Chart 1 >>>  date_start=' + chg_date_th(date_start) + ' date_end=' + chg_date_th(date_end));
    block('.chart_body', 'กำลังโหลดปี ' + chg_year_th(date_start));
    var url = base_url + 'api/rsvmiddle';
    $.get(url, {
        date_start: date_start,
        date_end: date_end,
        rsvmiddle: $('#cresv_s').val(),
        percent: percent

    }, function (res) {

        var _date_start = year_start + '-01-01';
        var _date_end = year_start + '-12-31';

        _labels = [];
        _data = [];
        var f = 0;
        var _cap_resv = null, _low_qdisc = null;
        var _data_cap_resv = [], _data_low_qdisc = [];
        var _data_upper = [], _data_lower = [], u = 0; //Upper, Lower 
        for (var _loop_date = moment(_date_start); _loop_date <= moment(_date_end); _loop_date.add(1, 'days')) {
            f++;
            var _date = _loop_date.format('YYYY-MM-DD');
            //var _date=m.format('DD MM');
            //console.log(_date);
            _labels.push(_date); //แสดงแกน x 

            var j = 0, j2 = 0;
            $.each(res.reservoir_data, function (index, value) {
                if (_date == value.date) {
                    if (parseFloat(value.qdisc_curr) > 0) {
                        //console.log(i+' ) '+_date+' = '+value.date+' || '+value.qdisc_curr);                      
                        _data.push(value.qdisc_curr);
                        j++;
                    }
                }
                if (j2 == 0) {
                    //ระดับน้ำสูงสุด - ต่ำสุด 
                    _cap_resv = value.cap_resv;
                    _low_qdisc = value.low_qdisc;
                    j2++;
                }
            }); //$.each 

            if (j == 0) { //ไม่พบปริมาณน้ำ 
                //console.log(_date+' = NULL');
                _data.push(null);
            }

            if (i == 0) {
                //console.log(f + ' ) ' + _date + ' = ' + _cap_resv+' | '+_low_qdisc);
                var arr = ["01", "05", "10", "15", "20", "25", "30", "31"]; //plot จุดตามวันที่นี้ 
                if ($.inArray(_date.substr(8, 2), arr) >= 0) {
                    _data_cap_resv.push(_cap_resv);
                    _data_low_qdisc.push(_low_qdisc);
                } else {
                    _data_cap_resv.push(null);
                    _data_low_qdisc.push(null);
                }
            }

            if (i == 0) {
                //console.warn(i+' | '+_date+' | '+_date.substr(5,2)+' | '+_date.substr(8,2) );
                //console.warn(_date.substr(5,5));
                if (_date.substr(8, 2) == "01" || _date.substr(5, 5) == "12-30") {   //2017-01-20
                    if (_date.substr(5, 5) == "12-30") {
                        u--;
                    }
                    //Push Upper
                    _data_upper.push(res.upper_lower.upper[u]);
                    //console.warn(u+' | Upper : '+ res.upper_lower.upper[u]);

                    //Push Lower
                    _data_lower.push(res.upper_lower.lower[u]);
                    //console.warn(u+' | Lower : '+ res.upper_lower.lower[u] );

                    u++;
                } else {//if 
                    _data_upper.push(null);
                    _data_lower.push(null);
                }

            }//i==0

        }//for 


        if (i == 0) {
            //--- ปริมาตรระดับน้ำกักเก็บปกติ ---            
            _datasets.push(
                {
                    label: 'ปริมาตรระดับน้ำกักเก็บปกติ', //label แสดงข้างบน
                    fill: false,
                    //lineTension: 0.1,
                    borderWidth: 2.5,
                    backgroundColor: "#FFFFFF",
                    borderColor: "#F800A1",
                    borderCapStyle: 'butt',
                    borderDash: [6, 5], //เส้นประ
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "#F800A1",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 0,
                    pointHoverRadius: 3,
                    pointHoverBackgroundColor: "#F800A1",
                    pointHoverBorderColor: "#FFFFFF",
                    pointHoverBorderWidth: 1,
                    pointRadius: 0,
                    pointHitRadius: 10,
                    spanGaps: true,
                    data: _data_cap_resv,
                }
            );

            //--- ปริมาตรระดับน้ำกักต่ำสุด ---          
            _datasets.push(
                {
                    label: 'ปริมาตรระดับน้ำกักต่ำสุด', //label แสดงข้างบน
                    fill: false,
                    // lineTension: 0.1,
                    borderWidth: 2.5,
                    backgroundColor: "#FFFFFF",
                    borderColor: "#FF0000",
                    borderCapStyle: 'butt',
                    borderDash: [6, 5], //เส้นประ
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "#FF0000",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 0,
                    pointHoverRadius: 3,
                    pointHoverBackgroundColor: "#FF0000",
                    pointHoverBorderColor: "#FFFFFF",
                    pointHoverBorderWidth: 1,
                    pointRadius: 0,
                    pointHitRadius: 10,
                    spanGaps: true,
                    data: _data_low_qdisc,
                }
            );


            if (res.upper_lower.upper.length > 0 && res.upper_lower.lower.length > 0) {
                //--- Upper  ---        
                _datasets.push(
                    {
                        label: 'Upper',
                        fill: false,
                        lineTension: 0.1,
                        borderWidth: 1,
                        backgroundColor: "#FFFFFF",
                        borderColor: "#333333",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "#333333",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 3,
                        pointHoverBackgroundColor: "#333333",
                        pointHoverBorderColor: "#FFFFFF",
                        pointHoverBorderWidth: 1,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        spanGaps: true,
                        data: _data_upper,
                    }
                );

                //--- Lower  ---        
                _datasets.push(
                    {
                        label: 'Lower',
                        fill: false,
                        lineTension: 0.1,
                        borderWidth: 1,
                        backgroundColor: "#FFFFFF",
                        borderColor: "#000000",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "#000000",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 3,
                        pointHoverBackgroundColor: "#333333",
                        pointHoverBorderColor: "#FFFFFF",
                        pointHoverBorderWidth: 1,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        spanGaps: true,
                        data: _data_lower,
                    }
                );
            }

        }//i==0

        //Add Line --------------------------------------------------------------
        var color = arr_color[i];
        _datasets.push(
            {
                label: chg_year_th(date_start), //label แสดงข้างบน
                fill: false,
                lineTension: 0.1,
                borderWidth: 3,
                backgroundColor: "#FFFFFF",
                borderColor: color,
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: color,
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 3,
                pointHoverBackgroundColor: color,
                pointHoverBorderColor: "#FFFFFF",
                pointHoverBorderWidth: 1,
                pointRadius: 1,
                pointHitRadius: 10,
                data: _data,
            }
        );


        //------หาค่า Max ของปริมาณน้ำ เพื่อมาแสดง แกน Y--------------------------------------------------------------
        //console.log( _data );
        arr_y_max.push(parseFloat(d3.max(_data)));
        //console.info( arr_y_max );
        var y_max_top = parseFloat(d3.max(arr_y_max));
        //console.log('Y MAX : '+y_max_top);
        if (isNaN(y_max_top) == false) {
            if (y_max_top > 0) {
                y_max = parseFloat(y_max_top * 2.2);
            }
        }
        //console.log('Y MAX >>> '+y_max);

        //-------- Gen Graph ------------------------------------------------------------------
        gen_chart();
        year_start++;
        if (year_start <= year_end) {
            if (year_start == year_end) { //ถ้าเป็นปีเดียวกัน 
                if ((year_end - year_start) + 1 == 0) {
                    load_chart(year_start + month_start, year_end + month_end);
                } else {
                    load_chart(year_start + '-01-01', year_end + month_end);
                }
            } else { //ถ้าไม่ใช่ปีเดียวกัน 
                load_chart(year_start + '-01-01', year_start + '-12-31');
            }
        }

        if (year_start > year_end) {
            unblock('.chart_body');
        }

        i++;
    }); //get 

}


function gen_chart() {


    //--------------------------------------------------------------
    var data = {
        labels: _labels,
        datasets: _datasets,
    };

    var option = {
        chartArea: {
            backgroundColor: '#EEFBFC', //สีพื้นหลัง            
        },

        bezierCurve: false,
        datasetFill: false,
        scaleGridLineColor: "#3E434E",

        legendCallback: function (chart) {
            //console.log(chart);
            var legendHtml = '';
            legendHtml += '<div class="tbl_legend_chart" align="center">';
            var _line, _size;
            for (var i = 0; i < chart.data.datasets.length; i++) {
                if (chart.data.datasets[i].label) {
					var _legend_name=chart.data.datasets[i].label+'';
                    if (_legend_name.indexOf('ปริมาตรระดับน้ำกักเก็บปกติ') != -1 || _legend_name.indexOf('ปริมาตรระดับน้ำกักต่ำสุด') != -1) {
                        _line = 'dashed';
                        _size = '3.4px';
                    }
                    else if (_legend_name.indexOf('Upper') != -1 || _legend_name.indexOf('Lower') != -1) {
                        _line = 'solid';
                        _size = '2.8px';
                    }
                    else {
                        _line = 'solid';
                        _size = '4px';
                    }
                    //---
                    if (i == 2) {
                        legendHtml += '<br>';
                    }
                    legendHtml += '<span class="chart-legend-label-text text-left" onclick="updateDataset(event, ' + '\'' + chart.legend.legendItems[i].datasetIndex + '\'' + ', this)"  > <canvas class="legend_color_line" style="border-bottom:' + _line + ' ' + _size + chart.data.datasets[i].borderColor + '; "></canvas>  ' + chart.data.datasets[i].label + '</span>';
                }
            } //for
            legendHtml += '</div>';
            return legendHtml;
        },

        legend: {
            display: false,

            onClick: function (e, legendItem) {
                // code from https://github.com/chartjs/Chart.js/blob/master/src/core/core.legend.js
                // modified to prevent hiding all legend items

                var index = legendItem.datasetIndex;
                var ci = this.chart;
                var meta = ci.getDatasetMeta(index);

                // See controller.isDatasetVisible comment
                var hidden = meta.hidden === null ? !ci.data.datasets[index].hidden : null;

                if (hidden) {
                    var someShown = _.some(this.legendItems, function (item) {
                        return item !== legendItem && !item.hidden;
                    });
                    if (!someShown)
                        return;
                }

                meta.hidden = hidden;
                // We hid a dataset ... rerender the chart
                ci.update();
            },

            labels: {
                fontColor: '#000000'
            },
        },
        scales: {
            xAxes: [{
                type: 'time',
                time: {
                    //format: "DD MMMM",
                    unit: 'month', //หน่วยการแสดงเป็นเดือน 
                    unitStepSize: 1, //Step การแสดงระยะห่างกัน 
                    displayFormats: { //แสดงแค่ชื่อเดือน 
                        'millisecond': 'MMMM',
                        'second': 'MMMM',
                        'minute': 'MMMM',
                        'hour': 'MMMM',
                        'day': 'MMMM',
                        'week': 'MMMM',
                        'month': 'MMMM',
                        'quarter': 'MMMM',
                        'year': 'MMMM',
                    }
                },
                scaleLabel: {
                    display: false,
                    labelString: 'เดือน'
                },
            }],
            yAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'ปริมาณน้ำในอ่างฯ - ล้าน ลบ.ม.'
                },
                ticks: {
                    beginAtZero: true,
                    //stepSize : y_step,
                    max: y_max,
                }
            }],
            chartArea: {
                backgroundColor: 'rgba(238, 247, 253, 0.4)'
            },
        },
        title: {
            display: false,
            text: "Report Title \n 5555",
            fontFamily: "thaisanslite",
            fontSize: 22,
            fontColor: "#000",
            fontStyle: "bold",
            padding: 10,
        },
        tooltips: {
            enabled: true,
            //mode: 'single',
            //-----
            titleFontSize: 0,
            //-----
            backgroundColor: "#333333",
            bodyFontColor: "#FFFFFF", //#000000
            bodyFontSize: 12,
            bodyFontStyle: "bold",
            bodyFontColor: '#FFFFFF',
            bodyFontFamily: "tahoma",
            //-----
            footerFontSize: 12,
            footerFontFamily: "tahoma",
            custom: function (tooltip) {
                // tooltip will be false if tooltip is not visible or should be hidden
                if (!tooltip) {
                    return;
                }
                //tooltip.body = ['TEST']

            },
            callbacks: {
                label: function (tooltipItem, data) {

                    //console.log( data );
                    //console.log( tooltipItem.index )  
                    var t_i = tooltipItem.datasetIndex;
                    var t_j = tooltipItem.index;
                    var t_arr_date = data.labels;
                    //console.log( arr_date[j] );
                    var t_year = data.datasets[t_i].label;
                    var t_date = chg_day_month(data.labels[t_j]);
                    var t_data = data.datasets[t_i].data[t_j];
                    if (t_year.length > 4) {
                        return " " + t_year;
                    } else {
                        //return "วันที่: "+t_date+" ปริมาณน้ำ: "+t_data+" ล้าน ลบ.ม.";
                        return " วันที่: " + t_date + " " + t_year;
                    }
                },
                footer: function (tooltipItem, data) {
                    //return ['new line', 'another line'];
                    var t_i = tooltipItem[0].datasetIndex;

                    var t_j = tooltipItem[0].index;
                    var t_arr_date = data.labels;
                    //console.log( arr_date[j] );
                    var t_year = data.datasets[t_i].label;
                    var t_date = chg_date_thai(data.labels[t_j]);
                    var t_data = data.datasets[t_i].data[t_j];
                    return "    ปริมาณน้ำ: " + t_data + " ล้าน ลบ.ม. ";

                }

            } //callbacks

        } //tooltips 

    } //options


    // Show/hide chart by click legend
    updateDataset = function (e, datasetIndex, obj) {

        if ($(obj).hasClass('line_hidden') == false) {
            $(obj).addClass('line_hidden');
        } else {
            $(obj).removeClass('line_hidden');
        }
        //console.log(e);
        //console.log(datasetIndex);
        var index = datasetIndex;
        var ci = e.view.chart;
        var meta = ci.getDatasetMeta(index);

        // See controller.isDatasetVisible comment
        //console.log(meta.hidden);
        if (meta.hidden == null) {
            meta.hidden = !ci.data.datasets[index].hidden; //Hidden
        } else {
            meta.hidden = ci.data.datasets[index].hidden; //Show
        }

        // We hid a dataset ... rerender the chart
        ci.update();
    };


    //------------------------------------------------------------------------------------------------
    var ctx = document.getElementById("chart1");
    Chart.plugins.register({
        beforeDraw: function (chart, easing) {
            if (chart.config.options.chartArea && chart.config.options.chartArea.backgroundColor) {
                var helpers = Chart.helpers;
                var ctx = chart.chart.ctx;
                var chartArea = chart.chartArea;

                ctx.save();
                ctx.fillStyle = chart.config.options.chartArea.backgroundColor;
                ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
                ctx.restore();
            }
        }
    });

    if (empty(chart) == false) {
        chart.destroy();
    }
    chart = new Chart(ctx, {
        type: 'line',
        data: data,
        options: option
    });
    document.getElementById('legend').innerHTML = chart.generateLegend();



}




//===============================================================================

// CHART 2

//===============================================================================


var _labels2 = [];
var _datasets2 = [];
var _label2 = '';
var _data2 = [], _data_q_info = [], _data_q_outfo = [];
var chart2;
var arr_color2 = ['#0000FF', '#FB01D2', '#FF0000'];
var year_start2, month_start2;
var year_end2, month_end2;
var y_max2, arr_y_max2 = [], y_step2;
var i2 = 0;
var q_info_arr = [];

function load_chart2(date_start, date_end) {

    console.warn('Chart 2 >>>  date_start=' + chg_date_th(date_start) + ' date_end=' + chg_date_th(date_end));
    //block('.chart_body', 'กำลังโหลดปี '+chg_year_th(date_start) );
    var url = base_url + 'api/rsvmiddle';
    $.get(url, { date_start: date_start, date_end: date_end, rsvmiddle: $('#cresv_s').val() }, function (res) {

        var a = year_start2 + '-01-01';
        var b = year_start2 + '-12-31';

        _labels2 = [];
        _data2 = [];
        var f = 0;
        //q_info_arr=[];
        for (var m = moment(a); m.isBefore(b); m.add(1, 'days')) {
            var _date = m.format('YYYY-MM-DD');
            //var _date=m.format('DD MM');
            //console.log(_date);
            _labels2.push(m.format('YYYY-MM-DD')); //แสดงแกน x 

            if (q_info_arr[f] == undefined) {
                q_info_arr.push([]);
            }
            var j = 0;
            $.each(res.reservoir_data, function (index, value) {
                if (_date == value.date) {

                    //Sum รวม น้ำไหลลง ของทุกปี 
                    q_info_arr[f].push(parseFloat(value.q_info)); //รวมทั้งหมด 

                    if (chg_year_th(_date) == chg_year_th(date_end2)) {  //แสดงเฉพาะปีล่าสุด                          
                        //console.log(' '+ chg_year_th(_date)+' '+chg_year_th(date_end2) );
                        //console.log(i2+' ) '+_date+' = '+value.date+' || q_info= '+value.q_info+' || '+'q_outfo= '+value.q_outfo);  
                        _data_q_info.push(value.q_info);
                        _data_q_outfo.push(value.q_outfo);

                        j++;
                    }
                    //console.warn(i2+' ) '+_date+' = '+value.date+' || q_info= '+value.q_info+' || '+'q_outfo= '+value.q_outfo);                           
                }
            }); //$.each 

            if (chg_year_th(_date) == chg_year_th(date_end2)) {  //แสดงเฉพาะปีล่าสุด      
                if (j == 0) { //ไม่พบปริมาณน้ำ 
                    //console.log(_date+' = NULL');
                    _data_q_info.push(null);
                    _data_q_outfo.push(null);
                }
            }

            var nn = (parseInt(chg_year_th(date_end2)) - parseInt(chg_year_th(date_start2))) + 1; // จำนวนปี นำไปหาค่าเฉลี่ย 
            _data2.push(number_format_num(d3.sum(q_info_arr[f]) / nn, 3));    //// ค่าเฉลี่ย 

            //console.warn(f+' ) '+_date+' = || q_info= '+d3.sum(q_info_arr[f]) );

            f++;
        } //for             


        if (chg_year_th(_date) == chg_year_th(date_end2)) { //แสดงเฉพาะปีล่าสุด                   
            // --AddLine---------------
            var color = arr_color2[0];
            _datasets2.push(
                {
                    label: 'ปริมาณน้ำไหลลงอ่าง ' + chg_year_th(date_start),
                    fill: false,
                    lineTension: 0.1,
                    borderWidth: 3,
                    backgroundColor: "#FFFFFF",
                    borderColor: color,
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: color,
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 3,
                    pointHoverBackgroundColor: color,
                    pointHoverBorderColor: "#FFFFFF",
                    pointHoverBorderWidth: 1,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: _data_q_info,
                }
            );

            var color = arr_color2[1];
            _datasets2.push(
                {
                    label: 'ปริมาณน้ำระบาย ' + chg_year_th(date_start),
                    fill: false,
                    lineTension: 0.1,
                    borderWidth: 3,
                    backgroundColor: "#FFFFFF",
                    borderColor: color,
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: color,
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 3,
                    pointHoverBackgroundColor: color,
                    pointHoverBorderColor: "#FFFFFF",
                    pointHoverBorderWidth: 1,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: _data_q_outfo,
                }
            );

            //Add Line --------------------------------------------------------------
            var color = arr_color2[2];
            _datasets2.push(
                {
                    label: 'ปริมาณน้ำไหลลงอ่างเฉลี่ย ' + chg_year_th(date_start2) + '-' + chg_year_th(date_end2),
                    fill: false,
                    lineTension: 0.1,
                    borderWidth: 3,
                    backgroundColor: "#FFFFFF",
                    borderColor: color,
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: color,
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 3,
                    pointHoverBackgroundColor: color,
                    pointHoverBorderColor: "#FFFFFF",
                    pointHoverBorderWidth: 1,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: _data2,
                }
            );

            //console.log('q_info : '+print_r(_data_q_info).split('<br>').join('') );
            //console.log('q_outfo : ' + print_r(_data_q_outfo).split('<br>').join('') );
            //console.log('น้ำไหลลงอ่างเฉลี่ย : '+print_r(_data2).split('<br>').join('') );
        }


        //------หาค่า Max ของปริมาณน้ำ เพื่อมาแสดง แกน Y--------------------------------------------------------------
        //console.log( _data2 );
        arr_y_max2.push(d3.max(_data2));
        var y_max_top2 = parseFloat(d3.max(arr_y_max2));
        //console.log('2) Y MAX : '+y_max_top2);
        if (isNaN(y_max_top2) == false) {
            if (y_max_top2 > 0) {
                y_max2 = parseFloat(y_max_top2 * 1.2);
                y_step2 = y_max2 / 3.2;
            }
        }

        //-------- Gen Graph ------------------------------------------------------------------
        gen_chart2();
        year_start2++;
        if (year_start2 <= year_end2) {
            if (year_start2 == year_end2) { //ถ้าเป็นปีเดียวกัน 
                if ((year_end2 - year_start2) + 1 == 0) {
                    load_chart2(year_start2 + month_start2, year_end2 + month_end2);
                } else {
                    load_chart2(year_start2 + '-01-01', year_end2 + month_end2);
                }
            } else { //ถ้าไม่ใช่ปีเดียวกัน 
                load_chart2(year_start2 + '-01-01', year_start2 + '-12-31');
            }
        }

        if (year_start2 > year_end2) {
            unblock('.chart_body');
        }


        i2++;
    }); //get 

}


function gen_chart2() {


    //--------------------------------------------------------------
    var data = {
        labels: _labels2,
        datasets: _datasets2,
    };

    var option = {
        chartArea: {
            backgroundColor: '#EEFBFC' //สีพื้นหลัง 
        },

        legendCallback: function (chart2) {
            //console.log(chart2);
            var legendHtml = '';
            legendHtml += '<table class="tbl_legend_chart" align="center">';
            legendHtml += '<tr>';
            for (var i = 0; i < chart2.data.datasets.length; i++) {
                if (chart2.data.datasets[i].label) {
                    legendHtml += '<td class="chart-legend-label-text text-left" onclick="updateDataset2(event, ' + '\'' + chart2.legend.legendItems[i].datasetIndex + '\'' + ', this)"  > <button type="button"  class="legend_color_line" style="border-bottom:solid 4px ' + chart2.data.datasets[i].borderColor + '; "></button>  ' + chart2.data.datasets[i].label + '</td>';
                }
            }
            legendHtml += '</tr>';
            legendHtml += '</table>';
            //return legendHtml.join("");        
            return legendHtml;
        },

        legend: {
            display: false,

            onClick: function (e, legendItem) {
                // code from https://github.com/chartjs/Chart.js/blob/master/src/core/core.legend.js
                // modified to prevent hiding all legend items

                var index = legendItem.datasetIndex;
                var ci = this.chart2;
                var meta = ci.getDatasetMeta(index);

                // See controller.isDatasetVisible comment
                var hidden = meta.hidden === null ? !ci.data.datasets[index].hidden : null;

                if (hidden) {
                    var someShown = _.some(this.legendItems, function (item) {
                        return item !== legendItem && !item.hidden;
                    });
                    if (!someShown)
                        return;
                }

                meta.hidden = hidden;
                // We hid a dataset ... rerender the chart
                ci.update();
            },

            labels: {
                fontColor: '#000000'
            },

        },
        scales: {
            xAxes: [{
                type: 'time',
                time: {
                    //format: "DD MMMM",
                    unit: 'month', //หน่วยการแสดงเป็นเดือน 
                    unitStepSize: 1, //Step การแสดงระยะห่างกัน 
                    displayFormats: { //แสดงแค่ชื่อเดือน 
                        'millisecond': 'MMMM',
                        'second': 'MMMM',
                        'minute': 'MMMM',
                        'hour': 'MMMM',
                        'day': 'MMMM',
                        'week': 'MMMM',
                        'month': 'MMMM',
                        'quarter': 'MMMM',
                        'year': 'MMMM',
                    }
                },
                scaleLabel: {
                    display: false,
                    labelString: 'เดือน'
                },
            }],
            yAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'ปริมาตรน้ำ - ล้าน ลบ.ม.'
                },
                ticks: {
                    beginAtZero: true,
                    stepSize: y_step2,
                    max: y_max2,
                }
            }],
            chartArea: {
                backgroundColor: 'rgba(238, 247, 253, 0.4)'
            },
        },
        title: {
            display: false,
            text: "Report Title \n 5555",
            fontFamily: "thaisanslite",
            fontSize: 22,
            fontColor: "#000",
            fontStyle: "bold",
            padding: 10,
        },
        tooltips: {
            enabled: true,
            //mode: 'single',
            //-----
            titleFontSize: 0,
            //-----
            backgroundColor: "#333333",
            bodyFontColor: "#FFFFFF", //#000000
            bodyFontSize: 12,
            bodyFontStyle: "bold",
            bodyFontColor: '#FFFFFF',
            bodyFontFamily: "tahoma",
            //-----
            footerFontSize: 12,
            footerFontFamily: "tahoma",
            custom: function (tooltip) {
                // tooltip will be false if tooltip is not visible or should be hidden
                if (!tooltip) {
                    return;
                }
                //tooltip.body = ['TEST']

            },
            callbacks: {

                label: function (tooltipItem, data) {

                    //console.log( data );
                    //console.log( tooltipItem.index )  
                    var t_i = tooltipItem.datasetIndex;
                    var t_j = tooltipItem.index;
                    var t_arr_date = data.labels;
                    //console.log( arr_date[j] );
                    var t_year = data.datasets[t_i].label;
                    var t_date = chg_day_month(data.labels[t_j]);
                    var t_data = data.datasets[t_i].data[t_j];

                    if (t_year.length > 4) {
                        return " " + t_year;
                    } else {
                        //return "วันที่: "+t_date+" ปริมาณน้ำ: "+t_data+" ล้าน ลบ.ม.";
                        return " วันที่: " + t_date + " " + t_year;
                    }
                },
                footer: function (tooltipItem, data) {
                    //return ['new line', 'another line'];
                    var t_i = tooltipItem[0].datasetIndex;
                    var t_j = tooltipItem[0].index;
                    var t_arr_date = data.labels;
                    //console.log( arr_date[j] );
                    var t_year = data.datasets[t_i].label;
                    var t_date = chg_date_thai(data.labels[t_j]);
                    var t_data = data.datasets[t_i].data[t_j];
                    return "    ปริมาณน้ำ: " + t_data + " ล้าน ลบ.ม. ";

                }

            } //callbacks

        } //tooltips 

    } //options




    // Show/hide chart by click legend
    updateDataset2 = function (e, datasetIndex, obj) {

        if ($(obj).hasClass('line_hidden') == false) {
            $(obj).addClass('line_hidden');
        } else {
            $(obj).removeClass('line_hidden');
        }
        //console.log(e);
        //console.log(datasetIndex);
        var index = datasetIndex;
        var ci = e.view.chart2;
        var meta = ci.getDatasetMeta(index);

        // See controller.isDatasetVisible comment
        //console.log(meta.hidden);
        if (meta.hidden == null) {
            meta.hidden = !ci.data.datasets[index].hidden; //Hidden
        } else {
            meta.hidden = ci.data.datasets[index].hidden; //Show
        }

        // We hid a dataset ... rerender the chart
        ci.update();
    };

    //------------------------------------------------------------------------------------------------
    var ctx2 = document.getElementById("chart2");

    Chart.plugins.register({
        beforeDraw: function (chart, easing) {
            if (chart.config.options.chartArea && chart.config.options.chartArea.backgroundColor) {
                var helpers = Chart.helpers;
                var ctx2 = chart.chart.ctx;
                var chartArea = chart.chartArea;

                ctx2.save();
                ctx2.fillStyle = chart.config.options.chartArea.backgroundColor;
                ctx2.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
                ctx2.restore();
            }
        }
    });

    if (empty(chart2) == false) {
        chart2.destroy();
    }
    chart2 = new Chart(ctx2, {
        type: 'line',
        data: data,
        options: option
    });
    document.getElementById('legend2').innerHTML = chart2.generateLegend();



}