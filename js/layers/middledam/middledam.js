moment.locale("th");
$(document).ready(function() {
    // source.bigDam.legend = L.control({ position: 'bottomright' });
    // source.bigDam.legend.onAdd = function(map) {
    //     var div = L.DomUtil.create('div', 'info legend'),
    //         grades = [0, 30, 50, 80],
    //         labels = [],
    //         from, to;
    //     labels.push('ปริมาตรน้ำ(ล้านลบ.ม.)');
    //     for (var i = 0; i < grades.length; i++) {
    //         from = grades[i];
    //         to = grades[i + 1];
    //         labels.push('<i style="background:' + getColor(from + 1) + '"></i> ' + from + (to ? '&ndash;' + to : '+'));
    //     }
    //     div.innerHTML = labels.join('<br>');
    //     return div;
    // };

    source.middleDam.layer = new L.LayerGroup();


}); //ready

function getLayerMiddleData(callback) {
    if (source.middleDam.geoJson == undefined) {
        block('#map');
        //--- Add Data To FlowWater   From API ******************************************
        var base_url = "";
        if (window.location.origin === "http://www.ra.com:52") {
            base_url = "http://www.ra.com:52/reservoir/";
        } else {
            base_url = "http://app.rid.go.th:88/reservoir/";
        }

        $.getJSON(base_url + "api/rsvmiddlesGeojson", {
                date_start: current_date(),
                date_end: current_date()
            }, function(data) {
                if (data != null) {
                    source.middleDam.geoJson = L.geoJSON(data, {

                            style: function(feature) {
                                return feature.properties && feature.properties.style;
                            },
                            onEachFeature: onEachMiddleFeature,
                            pointToLayer: function(feature, latlng) {
                                var smallIcon = L.icon({
                                    iconSize: [15, 15],
                                    iconAnchor: [13, 27],
                                    popupAnchor: [1, -24],
                                    iconUrl: 'images/middle/' + getMarkerRsvmiddle(feature.properties.percent_rsv)
                                });
                                return L.marker(latlng, { icon: smallIcon });
                            }
                        })
                        .bindTooltip(function(e) {
                            return '' + e.feature.properties.nresv;
                        }, { direction: 'top', offset: L.point(0, -20) })
                        .addTo(source.middleDam.layer);
                }
            })
            .done(function() {
                callback($(source.middleDam.geoJson).length);
            })
            .fail(function() {
                alert('middleDam request failed! ');
            })
            .always(function() {
                unblock('#map');
            });

    } else {
        callback($(source.middleDam.geoJson).length);
    }
}

function getMarkerRsvmiddle(percent) {
    var marker_image = "";
    if (!percent) {
        marker_image = "empty-dot.png"; //empty
    } else {
        if (percent <= 30)
            marker_image = "yellow-dot.png"; //yellow
        else if (percent > 30 && percent <= 50)
            marker_image = "green-dot.png"; //green
        else if (percent > 50 && percent <= 80)
            marker_image = "blue-dot.png"; //blue
        else if (percent > 80)
            marker_image = "red-dot.png"; //red
    }
    return marker_image;
}

function onEachMiddleFeature(feature, layer) {

    if (feature.properties && feature.properties.nresv) {
        layer.on({
            //mouseover: highlightFeature,
            //mouseout: resetHighlight,
            click: function(e) {
                //console.warn(feature.properties.cresv);
                //$("#featuretab-info").html('');
                block('#map');
                $.get("js/layers/middledam/modal.html",
                    function(data) {
                        $('#iwocModal').html(data);

                        //console.log(feature.properties.cresv);
                        $('#cprov_s').val(feature.properties.cprov); //province_id
                        $('#cresv_s').val(feature.properties.cresv); //reservoir_id
                        // $('#date_start_table').val(chg_date_th(moment(feature.properties.daily_date).add(-1, 'months').format('YYYY-MM-DD')));
                        // $('#date_end_table').val(chg_date_th(feature.properties.daily_date));
                        // $('#date_start_graph').val(chg_date_th(moment(feature.properties.daily_date).add(-3, 'years').format('YYYY-MM-DD')));
                        // $('#date_end_graph').val(chg_date_th(feature.properties.daily_date));
                        $('#date_start_table').val(chg_date_th(moment(current_date()).add(-1, 'months').format('YYYY-MM-DD')));
                        $('#date_end_table').val(chg_date_th(current_date()));
                        $('#date_start_graph').val(chg_date_th(moment(current_date()).add(-3, 'years').format('YYYY-MM-DD')));
                        $('#date_end_graph').val(chg_date_th(current_date()));
                        //---------------------------------------------------------------------------
                        $("#featuretab-title").html('<strong>อ่างขนาดกลาง</strong> <span class="glyphicon glyphicon-menu-right"></span> ' + ' <strong id="show_rsvmiddle_name">' + feature.properties.nresv + '</strong>');

                        // $("#show_date").html(feature.properties.daily_date_th);
                        $("#show_date").html(chg_date_thai(current_date()));

                        if (!feature.properties.daily_date) {
                            $("#cap_resv").html('-');
                            $("#low_qdisc").html('-');
                            $("#water_workable_all").html('-');
                            $("#qdisc").html('-');
                            // $("#percent_rsv").html('-');
                            $("#water_workable").html('-');
                            // $("#percent_water_workable").html('-');
                            $("#q_info").html('-');
                            $("#q_outfo").html('-');
                        } else {
                            $("#cap_resv").html(feature.properties.cap_resv);
                            $("#low_qdisc").html(feature.properties.low_qdisc);
                            var water_workable_all = parseFloat(feature.properties.cap_resv) - parseFloat(feature.properties.low_qdisc);
                            var percent_water_workable = (parseFloat(feature.properties.water_workable) / water_workable_all) * 100;
                            $("#water_workable_all").html(number_format_num(water_workable_all, 3));
                            $("#qdisc").html(feature.properties.qdisc);
                            $("#percent_rsv").html(' (' + feature.properties.percent_rsv + '%)');
                            $("#water_workable").html(feature.properties.water_workable);
                            $("#percent_water_workable").html(' (' + number_format_num(percent_water_workable, 3) + '%)');
                            $("#q_info").html(feature.properties.q_info);
                            $("#q_outfo").html(feature.properties.q_outfo);
                        }
                        $("#iwocModal").modal("show");
                        unblock('#map');
                    }
                );

            }
        });

    } //if 
}
//getLayerMiddleData();


function current_date() {
    var currentDate = new Date();
    var day = currentDate.getDate();
    var month = currentDate.getMonth() + 1;
    var year = currentDate.getFullYear();
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    return year + '-' + month + '-' + day;
}

function chg_date_thai(dd) { //แปลง date = 2017-01-01 || 01/01/2560 --> 1 มกราคม 2560
    if (dd) {
        var date = dd;
        var month_arr = Array("มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
        if (date.substr(4, 1) == "-") {
            var date_array = date.split("-");
            var day = parseInt(date_array[2]) + 0;
            var month_i = date_array[1];
            var month = month_arr[parseInt(month_i) - 1];
            var year = parseInt(date_array[0]) + 543;
            return day + ' ' + month + ' ' + year;
        } else {
            var date_array = date.split("/");
            var day = parseInt(date_array[0]) + 0;
            var month_i = date_array[1];
            var month = month_arr[parseInt(month_i) - 1];
            var year = parseInt(date_array[2]);
            return day + ' ' + month + ' ' + year;
        }
    } else {
        return "";
    }
}