$(document).ready(function () {
	source.smalldamBeh.legend = L.control({ position: 'bottomright' });
    source.smalldamBeh.legend.onAdd = function (map) {
        var div = L.DomUtil.create('div', 'info legend'),
            grades = [],
            labels = [],
            from, to;
        //labels.push('<div><b>สถานการณ์<b></div>');
        let statusBigTele = { "#e5486e": "ถ่ายโอน", "#089ca0": "ไม่ถ่ายโอน" };
        labels.push('<div style="margin-bottom: 2px;">เขื่อนขนาดเล็ก</div>');
        $.each(statusBigTele, function (color, value) {
            labels.push('<div style="margin-bottom: 2px;"><i style="background:' + color + '"></i> ' + value + '</div>');
        });
        div.innerHTML = labels.join('');
        return div;
    };

    source.smalldamBeh.layer = new L.LayerGroup();
	


});//ready
var smalldambehridcode=1;
function getLayersmalldamBehData(callback) {
	/*var droupdownbody="<a id='smalltoolsDrop' href='#' role='button' class='dropdown-toggle' data-toggle='dropdown'><i class='fa fa-globe white'></i>&nbsp;&nbsp;เลือกลุ่มน้ำ  <b class='caret'></b></a>"
                        +"<ul class='dropdown-menu' style='height: 200px; overflow: auto;'>"
                        +"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(1);'>ลุ่มน้ำสาละวิน</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(2);'>ลุ่มน้ำโขง</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(3);'>ลุ่มน้ำกก</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(4);'>ลุ่มน้ำชี</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(5);'>ลุ่มน้ำมูล</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(6);'>ลุ่มน้ำปิง</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(7);'>ลุ่มน้ำวัง</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(8);'>ลุ่มน้ำยม</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(9);'>ลุ่มน้ำน่าน</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(10);'>ลุ่มน้ำเจ้าพระยา</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(11);'>ลุ่มน้ำสะแกกรัง</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(12);'>ลุ่มน้ำป่าสัก</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(13);'>ลุ่มน้ำท่าจีน</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(14);'>ลุ่มน้ำแม่กลอง</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(15);'>ลุ่มน้ำปราจีนบุรี</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(16);'>ลุ่มน้ำบางปะกง</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(17);'>ลุ่มน้ำโตนเลสาป</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(18);'>ลุ่มน้ำชายฝั่งทะเลตะวันออก</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(19);'>ลุ่มน้ำเพชรบุรี</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(20);'>ลุ่มน้ำชายฝั่งทะเลตะวันตก</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(21);'>ลุ่มน้ำภาคใต้ฝั่งตะวันออก</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(22);'>ลุ่มน้ำตาปี</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(23);'>ลุ่มน้ำทะเลสาบสงขลา</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(24);'>ลุ่มน้ำปัตตานี</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(25);'>ลุ่มน้ำภาคใต้ฝั่งตะวันตก</a></li>"
                        +"</ul>";*/
		var droupdownbody="<a id='smalltoolsDrop' href='#' role='button' class='dropdown-toggle' data-toggle='dropdown'><i class='fa fa-globe white'></i>&nbsp;&nbsp;เลือกสำนักชลประทาน  <b class='caret'></b></a>"
                        +"<ul class='dropdown-menu' style='height: 200px; overflow: auto;'>"
                        +"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(1);'>สำนักชลประทานที่ 1</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(2);'>สำนักชลประทานที่ 2</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(3);'>สำนักชลประทานที่ 3</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(4);'>สำนักชลประทานที่ 4</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(5);'>สำนักชลประทานที่ 5</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(6);'>สำนักชลประทานที่ 6</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(7);'>สำนักชลประทานที่ 7</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(8);'>สำนักชลประทานที่ 8</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(9);'>สำนักชลประทานที่ 9</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(10);'>สำนักชลประทานที่ 10</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(11);'>สำนักชลประทานที่ 11</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(12);'>สำนักชลประทานที่ 12</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(13);'>สำนักชลประทานที่ 13</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(14);'>สำนักชลประทานที่ 14</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(15);'>สำนักชลประทานที่ 15</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(16);'>สำนักชลประทานที่ 16</a></li>"
						+"    <li><a href='#' data-toggle='collapse' data-target='.navbar-collapse.in' id='full-extent-btn'onclick='recall(17);'>สำนักชลประทานที่ 17</a></li>"
                        +"</ul>";
		$("#smalldam_dropdown").html(droupdownbody);	
    if (source.smalldamBeh.geoJson == undefined) {
        block('#map');
		var url="http://gis.rid.go.th/damsafety/services/getGeoJson.php?projtype=3&ridcode="+smalldambehridcode;	
		source.smalldamBeh.layer.clearLayers();	

        $.getJSON(url, function (data) {
            if (data != null) {
				var icon="";
				//alert(url);
                source.smalldamBeh.geoJson = L.geoJSON(data, {

                    style: function (feature) {
                        return feature.properties && feature.properties.style;
                    },
                    onEachFeature: onEachsmalldamBehFeature,
                    pointToLayer: function (feature, latlng) {
						if(feature.properties.transfer==0){
							icon="nottransfer.png";
						}else{
							icon="transfer.png";
						}
                        var smallIcon = L.icon({
                            iconSize: [20, 20],
                            iconAnchor: [13, 27],
                            popupAnchor: [1, -24],
                            iconUrl: 'images/smalldamBeh/'+icon
                        });
                        return L.marker(latlng, { icon: smallIcon });
                    }
                }).bindTooltip(function (e) {
                            return '' + e.feature.properties.projname;
                        }, {
                            direction: 'top',
                            offset: L.point(0, -20)
                        }).addTo(source.smalldamBeh.layer);
            }

        })
        .done(function () {
             callback($(source.smalldamBeh.geoJson).length);
        })
        .fail(function (res) {
            alert('smalldamBeh request failed! '+ JSON.stringify(res,null,'\t') );
        })
        .always(function () {
            unblock('#map');
        });

    } else {
		
        callback($(source.smalldamBeh.geoJson).length);
		
    }
}
function recall(data){
smalldambehridcode=data;
map.removeLayer(source.smalldamBeh.geoJson);
//source.smalldamBeh.layer.clearlayer();
source.smalldamBeh.geoJson = undefined;
getLayersmalldamBehData(function(res) {
                if (res > 0) {
                    if (document.body.clientWidth <= 767) {
                        $("#sidebar").hide();
                    } else {
                        $("#sidebar").show();
                    }
                    map.invalidateSize();
                    syncSidebar();
                }
            });



}
function onEachsmalldamBehFeature(feature, layer) {

    if (feature.properties && feature.properties.projname) {
        layer.on({
            //mouseover: highlightFeature,
            //mouseout: resetHighlight,
            click: function (e) {
                //console.warn(feature.properties.cresv);
                //$("#featuretab-info").html('');
                block('#map');
                $.get("js/layers/smalldamBeh/modal.html",
                    function (data) {
                        $('#iwocModal').html(data);
                      //  $('#modal').html(data);

                       // console.log(feature.properties.TMI_ID);
                        // $('#rsvmiddle,#cresv_s').val(feature.properties.cresv);
                        // $('#date_start_table').val('01' + chg_date_th(feature.properties.daily_date).substr(2, 8));
                        // $('#date_end_table').val(chg_date_th(feature.properties.daily_date));
                        // $('#date_start').val(chg_date_th(moment(feature.properties.daily_date).add(-3, 'years').format('YYYY-MM-DD')));
                        // $('#date_end').val(chg_date_th(feature.properties.daily_date));
                        //---------------------------------------------------------------------------
                        $("#featuretab-title").html('<strong>การถ่ายโอน</strong><span class="glyphicon glyphicon-menu-right"></span> ' + ' <strong>' + feature.properties.projname + '</strong>');
                        var address="หมู่ที่ "+feature.properties.moo
                                    +" บ้าน "+feature.properties.village
                                    +" ตำบล "+feature.properties.tambon
                                    +" อำเภอ "+feature.properties.amphoe
                                    +" จังหวัด "+feature.properties.province
                        $("#address").html(address);
                        $("#basin").html(feature.properties.basin);
                        $("#ridcode").html("สำนักชลประทานที่"+feature.properties.ridcode);
                        $("#projcatego").html(feature.properties.projcatego);
                        $("#year_cons").html(feature.properties.year_cons);
                        $("#year_compl").html(feature.properties.year_compl);
						if(feature.properties.transfer==0){
							$("#transfer").html("ไม่ถ่ายโอน");
						}else{
							$("#transfer").html("ถ่ายโอน");
						}
						if(feature.properties.localgov_n){
							$("#localgov_n").html(feature.properties.localgov_n);
						}else{
							$("#localgov_n").html("-");
						}
						
						/*var bodybah="";
						if(feature.properties.owner_project!=null && feature.properties.owner_project.length>0){
							bodybah="<tr><td>โครงการที่รับผิดชอบ</td><td><span >"
										+feature.properties.owner_project+"</span></td></tr>"
										+"<tr><td>ปีที่ติดตั้ง</td><td><span >"
										+feature.properties.setup_year+"</span></td></tr>"
										+"<tr><td>บรษัทติดตั้ง</td><td><span >"
										+feature.properties.company+"</span></td></tr>"
										+"<tr><td>Link</td><td><span ><a target='_blank' href='"
										+feature.properties.link+"'>"
										+feature.properties.link+"</a></span></td></tr>";
						}else{
							bodybah="<tr><td><center>ไม่พบข้อมูล</center></td></tr>";
						}
						$("#body_bah").html(bodybah);*/
                        $("#iwocModal").modal("show");
                       // $("#modal").modal("show");
                        unblock('#map');
                    }
                );



            }
        });

    }//if 
}
//getLayerMiddleData();