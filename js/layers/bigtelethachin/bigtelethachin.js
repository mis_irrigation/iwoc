$(document).ready(function () {
   
    source.bigtelethachin.layer = new L.LayerGroup();

  //  alert("hi");
});


//source.bigtelethachin.bigDamID = 0;

source.bigtelethachin.greenIconUrl = "images/telemetry/green-sign.png";
source.bigtelethachin.blueIconUrl = "images/telemetry/blue-sign.png";
source.bigtelethachin.redIconUrl = "images/telemetry/red-sign.png";
source.bigtelethachin.greyIconUrl = "images/telemetry/gray-sign.png";
source.bigtelethachin.yellowIconUrl = "images/telemetry/yellow-sign.png";

source.bigtelethachin.shadowIconUrl = "libraries/leaflet1.0.3/images/marker-shadow.png";

source.bigtelethachin.greenIcon = new L.Icon({
    iconUrl: source.bigtelethachin.greenIconUrl,
    shadowUrl: source.bigtelethachin.shadowIconUrl,
    iconSize: [20.5, 20.5],
    iconAnchor: [20, 20.5],
    popupAnchor: [0.5, -17],
    shadowSize: [20.5, 20.5]
});
source.bigtelethachin.blueIcon = new L.Icon({
    iconUrl: source.bigtelethachin.blueIconUrl,
    shadowUrl: source.bigtelethachin.shadowIconUrl,
    iconSize: [20.5, 20.5],
    iconAnchor: [20, 20.5],
    popupAnchor: [0.5, -17],
    shadowSize: [20.5, 20.5]
});
source.bigtelethachin.redIcon = new L.Icon({
    iconUrl: source.bigtelethachin.redIconUrl,
    shadowUrl: source.bigtelethachin.shadowIconUrl,
    iconSize: [20.5, 20.5],
    iconAnchor: [20, 20.5],
    popupAnchor: [0.5, -17],
    shadowSize: [20.5, 20.5]
});
source.bigtelethachin.greyIcon = new L.Icon({
    iconUrl: source.bigtelethachin.greyIconUrl,
    shadowUrl: source.bigtelethachin.shadowIconUrl,
    iconSize: [20.5, 20.5],
    iconAnchor: [20, 20.5],
    popupAnchor: [0.5, -17],
    shadowSize: [20.5, 20.5]
});
source.bigtelethachin.yellowIcon = new L.Icon({
    iconUrl: source.bigtelethachin.yellowIconUrl,
    shadowUrl: source.bigtelethachin.shadowIconUrl,
    iconSize: [20.5, 20.5],
    iconAnchor: [20, 20.5],
    popupAnchor: [0.5, -17],
    shadowSize: [20.5, 20.5]
});

source.bigtelethachin.violetIcon = new L.Icon({
    iconUrl: source.bigtelethachin.violetIconUrl,
    shadowUrl: source.bigtelethachin.shadowIconUrl,
    iconSize: [20.5, 20.5],
    iconAnchor: [20, 20.5],
    popupAnchor: [0.5, -17],
    shadowSize: [20.5, 20.5]
});


source.bigtelethachin.getIcon = function (QTotal) {
    var icon = source.bigtelethachin.greyIcon;
    if (QTotal <= 30)
        icon = source.bigtelethachin.yellowIcon; //yellow
    else if (QTotal > 30 && QTotal <= 50)
        icon = source.bigtelethachin.greenIcon; //green
    else if (QTotal > 50 && QTotal <= 80)
        icon = source.bigtelethachin.blueIcon; //blue
    else if (QTotal > 80)
        icon = source.bigtelethachin.redIcon; //red
    else
        icon = source.bigtelethachin.greyIcon; // no data
    return icon
};

source.bigtelethachin.getLayerData = function (callback) {
    if (source.bigtelethachin.geoJson == undefined) {
        block('#map');

        $.getJSON("http://164.115.134.138/webservice/thachingeojson.ashx", function (data) {
            if (data !== null) {
                source.bigtelethachin.geoJson = L.geoJSON(data, {
                    style: function (feature) {
                        return feature.properties && feature.properties.style;
                    },
                    onEachFeature: source.bigtelethachin.onEachFeature,// onEachFeature,
                    pointToLayer: function (feature, latlng) {


                        return L.marker(latlng, {

                            icon: source.bigtelethachin.blueIcon  //getIcon(feature.properties.QTotal)

                        }).bindTooltip(function (e) {
                            return '' + e.feature.properties.StationCode;
                        }, { direction: 'top', offset: L.point(0, -20) })

                        //return L.circleMarker(latlng, {
                        //    radius: 8,
                        //    fillColor: getColor(feature.properties.QTotal),
                        //    color: "#000",
                        //    weight: 1,
                        //    opacity: 1,
                        //    fillOpacity: 0.8
                        //}) //.bindTooltip("My Label", {permanent: true, className: "my-label", offset: [0, 0] });



                    }
                }).addTo(source.bigtelethachin.layer);
            }
        })
            .done(function () {
                unblock('#map');
            callback($(source.bigtelethachin.geoJson).length);
        })
            .fail(function () {
                unblock('#map');
            alert('bigtelethachin request failed! ');
        })
        .always(function () {
            unblock('#map');
        });

    } else {
        unblock('#map');
        callback($(source.bigtelethachin.geoJson).length);
    }
};

//function getColor(QTotal) {
//    var fillColor = "#FAFAFA";
//    if (QTotal <= 30)
//        fillColor = "#FFFF00"; //yellow
//    else if (QTotal > 30 && QTotal <= 50)
//        fillColor = "#00C853"; //green
//    else if (QTotal > 50 && QTotal <= 80)
//        fillColor = "#1976D2"; //blue
//    else if (QTotal > 80)
//        fillColor = "#FF4081"; //red
//    else
//        fillColor = "#FAFAFA"; // no data
//    return fillColor
//}




//function highlightFeature(e) {
//    var layer = e.target;
//    layer.setStyle({
//        weight: 5,
//        color: '#666',
//        dashArray: '',
//        fillOpacity: 0.8
//    });
//    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
//        layer.bringToFront();
//    }
//}

//function resetHighlight(e) {
//    source.bigDam.geoJson.resetStyle(e.target);
//}

source.bigtelethachin.onEachFeature = function (feature, layer) {
    var popupContentReport = "";
  //  var popupContentBasicData = "";
    if (feature.properties && feature.properties.StationCode) {
        popupContentReport = tableStringBigTeleThachinData(feature.id, feature.properties.StationCode, feature.properties.StationName, feature.properties.WaterLevel, feature.properties.WLLastTimeString);
   
        layer.on({
            //  mouseover: highlightFeature,
            //  mouseout: resetHighlight,
            click: function (e) {

       

                source.bigtelethachin.bigtelethachinID = feature.id;

                $.get("js/layers/bigtelethachin/modal.html",
                    function (data) {


                        $('#iwocModal').html(data);
                        $("#featuretab-title").html(feature.properties.StationCode);
                        $("#featuretab-report").html(popupContentReport);

                        $("#iwocModal").modal("show");
                    }
                );
            }
        });
        //source.bigDam.search = [];
        //source.bigDam.search.push({
        //    name: layer.feature.properties.DamName,
        //    qtotal: layer.feature.properties.QTotal,
        //    source: "BigDam",
        //    id: L.stamp(layer),
        //    lat: layer.getLatLng().lat,
        //    lng: layer.getLatLng().lng
        //});
    }
};
//getLayerData();

function tableStringBigTeleThachinData(id, StationCode, StationName, WaterLevel, WLLastTimeString) {

   // alert(WLLastTimeString);
    var html = '<table class="table table-striped table-bordered table-condensed">' +
        '<tr><th  colspan="2" class="text-center">ข้อมูลวันที่ ' + WLLastTimeString + '</th></tr>' +
        '<tr><td>ระดับน้ำ (ม.)</td><td>' + commafy(WaterLevel, 2) + '</td></tr>' +
        '<table>'

    
    return html;
}


