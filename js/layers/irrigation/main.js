﻿$(document).ready(function () {
    source.irrproject.layer = new L.LayerGroup();
    source.prjarea.layer = new L.LayerGroup();
    source.irrarea.layer = new L.LayerGroup();

});


function getIrrprojectData(callback) {
    if (source.irrproject.geoJson == undefined) {
        block('#map');
        L.tileLayer.betterWms('http://gis.rid.go.th/geoserver/riddata/wms', 
        {
            layers: 'riddata:irrproject_all',
            transparent: true,
            format: 'image/png'
        }).addTo(source.irrproject.layer);
		$.ajax({
				url: "http://gis.rid.go.th/geoserver/riddata/ows",
				jsonp: "callback",
				jsonpCallback: "parseResponse",
				dataType: "jsonp",
				data: {
					version: "1.0.0",
					request: "GetFeature",
					typeName: "riddata:irrproject_all",
					styles: "irrproject",  
					service: "WFS",
					outputFormat: "text/javascript",
				},
    			success: function( data ) {
        			if (data !== null) {
 					     					source.irrproject.geoJson = L.geoJSON(data, {
                    							style: function (feature) {
                        							return feature.properties && feature.properties.style; },

                								onEachFeature: onEachIrrprojectFeature,
       
							         			pointToLayer: function (feature, latlng) {
							                        var Marker = L.marker(latlng, {                          
							                            opacity: 0
							                        });
							                        return Marker;
							                	}
 											}).addTo(source.irrproject.layer);
				   							unblock('#map');
         		   }
   				}
		});
    } 
    else 
	    {
	        callback($(source.irrproject.geoJson).length);
	    }
}

function onEachIrrprojectFeature(feature, layer) {
    if (!feature.properties.objectid != true) {
 		let _objectid = feature.properties.objectid;
        layer.on({
            click: function (e) {
                $.ajax({
					url: "http://gis.rid.go.th/geoserver/riddata/ows",
					jsonp: "callback",
					jsonpCallback: "parseResponse",
					dataType: "jsonp",
					data: {
						version: "1.0.0",
						request: "GetFeature",
						typeName: "riddata:irrproject_all",
						styles: "irrproject",
						CQL_FILTER: "objectid="+_objectid,
						service: "WFS",
						outputFormat: "text/javascript",
					},
	    			success: function( data ) {
	    				let info = data.features[0].properties;
	    				let content = "";
	    	
	    				content += '<div class="modal-dialog">';
    					content += '<div class="modal-content">';
	       					content += '<div class="modal-header">';
							content += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
							content += '<h4 class="modal-title" id="featuretab-title">'+info.projcode+'</h4>';
							content += '</div>';
	        				content += '<div class="modal-body" id="featuretab-info">';
		        				content += "<div>"+info.projcode+"</div>";
			    				content += "<div>"+info.projname+"</div>";
			    				content += "<div>"+info.owner+"</div>";
			    				content += "<div>"+info.amphoe+"</div>";
			    				content += "<div>"+info.tambon+"</div>";
			    				content += "<div>"+info.provname+"</div>";
			    				content += "<div>"+info.projarea+"</div>";
			    			content += '</div>';
			    		content += '</div>';
			    		content += '</div>';
	    				$('#iwocModal').html(content).modal("show");
	    			}
    			});  
            }

        });
        
    }
}


function getPrjareaData(callback) {
    if (source.prjarea.geoJson == undefined) {
        block('#map');
        L.tileLayer.betterWms('http://gis.rid.go.th/geoserver/riddata/wms', 
        {
            layers: 'riddata:prjarea_all',
            transparent: true,
            format: 'image/png'
        }).addTo(source.prjarea.layer);
		$.ajax({
				url: "http://gis.rid.go.th/geoserver/riddata/ows",
				jsonp: "callback",
				jsonpCallback: "parseResponse",
				dataType: "jsonp",
				data: {
					version: "1.0.0",
					request: "GetFeature",
					typeName: "riddata:prjarea_all",
					styles: "prjarea", 
					service: "WFS",
					outputFormat: "text/javascript",
				},
    			success: function( data ) {
        			if (data !== null) {
 					     					source.prjarea.geoJson = L.geoJSON(data, {
                    							// style: function (feature) {
                        			// 				return feature.properties && feature.properties.style; },
                								onEachFeature: onEachPrjareaFeature,      
							         			style: function (feature) { // Style option
										            return {
										                'weight': 0,
										                'color': 'transparent',
										                'fillColor': 'transparent'
										            }
        										}
 											}).addTo(source.prjarea.layer);
				   							unblock('#map');
         		   }
   				}
		});
    } 
    else 
	    {
	        callback($(source.prjarea.geoJson).length);
	    }
}

function onEachPrjareaFeature(feature, layer) {
    if (!feature.properties.objectid != true) {
 		let _objectid = feature.properties.objectid;
        layer.on({
            click: function (e) {
                $.ajax({
					url: "http://gis.rid.go.th/geoserver/riddata/ows",
					jsonp: "callback",
					jsonpCallback: "parseResponse",
					dataType: "jsonp",
					data: {
						version: "1.0.0",
						request: "GetFeature",
						typeName: "riddata:prjarea_all",
						styles: "prjarea", 
						CQL_FILTER: "objectid="+_objectid,
						service: "WFS",
						outputFormat: "text/javascript",
					},
	    			success: function( data ) {
	    				let info = data.features[0].properties;
	    				let content = "";	    	
	    				content += '<div class="modal-dialog">';
    					content += '<div class="modal-content">';
	       					content += '<div class="modal-header">';
							content += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
							content += '<h4 class="modal-title" id="featuretab-title">'+info.projcode+'</h4>';
							content += '</div>';
	        				content += '<div class="modal-body" id="featuretab-info">';
		        				content += "<div>"+info.projcode+"</div>";
			    				content += "<div>"+info.projname+"</div>";
			    				content += "<div>"+info.owner+"</div>";
			    				content += "<div>"+info.amphoe+"</div>";
			    				content += "<div>"+info.tambon+"</div>";
			    				content += "<div>"+info.provname+"</div>";
			    				content += "<div>"+info.projarea+"</div>";
			    			content += '</div>';
			    		content += '</div>';
			    		content += '</div>';
	    				$('#iwocModal').html(content).modal("show");
	    			}
    			});  
            }
        });       
    }
}

function getIrrareaData(callback) {
    if (source.irrarea.geoJson == undefined) {
        block('#map');
		
        L.tileLayer.betterWms('http://gis.rid.go.th/geoserver/riddata/wms', 
        {
            layers: 'riddata:irrarea_all',
            transparent: true,
            format: 'image/png'
        }).addTo(source.irrarea.layer);
		unblock('#map');
		
        var ridcode = ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','exit'];
        for (let obj  in ridcode) {
        	if(false){ //ridcode[obj] != "exit"){
				console.log(ridcode[obj]);
				$.ajax({
					url: "http://gis.rid.go.th/geoserver/riddata/ows",
					jsonp: "callback",
					jsonpCallback: "parseResponse",
					dataType: "jsonp",
					data: {
						version: "1.0.0",
						request: "GetFeature",
						typeName: "riddata:irrarea_all",
						styles: "irrarea",  
						CQL_FILTER: "ridcode LIKE '"+ridcode[obj]+"'",
						service: "WFS",
						outputFormat: "text/javascript",
					},
					// Work with the response
					success: function( data ) {
						if (data !== null) {
							source.irrarea.geoJson = L.geoJSON(data, {
							// style: function (feature) {
							// return feature.properties && feature.properties.style; },

							onEachFeature: onEachIrrareaFeature,

							style: function (feature) { // Style option
								return {
									//'weight': 0,
									//'color': 'transparent',
									//'fillColor': 'transparent'
								}
							}
							}).addTo(source.irrarea.layer);
							console.log('success::' + ridcode[obj]);
							if(ridcode[obj] == 17){
								unblock('#map');
							}				
					   }
					}
				});
        	}	
		}
    } 
    else 
	    {
	        callback($(source.irrarea.geoJson).length);
	    }
}

function onEachIrrareaFeature(feature, layer) {
    if (!feature.properties.objectid != true) {
 		let _objectid = feature.properties.objectid;
        layer.on({
            click: function (e) {
                $.ajax({
					url: "http://gis.rid.go.th/geoserver/riddata/ows",
					jsonp: "callback",
					jsonpCallback: "parseResponse",
					dataType: "jsonp",
					data: {
						version: "1.0.0",
						request: "GetFeature",
						typeName: "riddata:irrarea_all",
						styles: "irrarea",  
						CQL_FILTER: "objectid="+_objectid,
						service: "WFS",
						outputFormat: "text/javascript",
					},
	    			success: function( data ) {
	    				let info = data.features[0].properties;
	    				let content = "";
	    	
	    				content += '<div class="modal-dialog">';
    					content += '<div class="modal-content">';
	       					content += '<div class="modal-header">';
							content += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
							content += '<h4 class="modal-title" id="featuretab-title">'+info.projcode+'</h4>';
							content += '</div>';
	        				content += '<div class="modal-body" id="featuretab-info">';
		        				content += "<div>"+info.projcode+"</div>";
			    				content += "<div>"+info.projname+"</div>";
			    				content += "<div>"+info.owner+"</div>";
			    				content += "<div>"+info.amphoe+"</div>";
			    				content += "<div>"+info.tambon+"</div>";
			    				content += "<div>"+info.provname+"</div>";
			    				content += "<div>"+info.projarea+"</div>";
			    			content += '</div>';
			    		content += '</div>';
			    		content += '</div>';
	    				$('#iwocModal').html(content).modal("show");
	    			}
    			});  
            }
        });
    }
}