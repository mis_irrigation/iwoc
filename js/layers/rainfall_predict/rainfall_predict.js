$(document).ready(function() {
    var modalTemplate;
    $.get("js/layers/rainfall_predict/modal.html",
        function (data) {
            modalTemplate = data;
        }
    );
    observable.on('rainfall_predict_toggle', function() {
        $("#iwocModal").modal("show");
        $('#iwocModal').html(modalTemplate);
        $('#rainfall_predict #featuretab-title').html('<strong> ข้อมูลน้ำฝน  </strong> <span class="glyphicon glyphicon-menu-right"></span> <strong> คาดการณ์ฝน </strong>');
        var htmlVideo = '<video controls autoplay style="width: 100%; height: auto;" src="http://live1.haii.or.th/product/latest/wrfroms/v2/ani_d03_large.mp4?c=' + moment().format('YYYYMMDD') + '"></video>'
        $('.modal-dialog.rainfall_predict .col-lg-12 #mapPredict').html(htmlVideo);
        $('.modal-dialog.rainfall_predict .col-lg-12 #mapPredict video').on('ended', function() {
            this.play();
        });
    });
    $('#iwocModal').on('hidden.bs.modal', function (e) {
        observable.notify('rainfall_predict_checked', false)
    })
    $('#iwocModal').on('show.bs.modal', function (e) {
        observable.notify('rainfall_predict_checked', true)
    })
    
});
