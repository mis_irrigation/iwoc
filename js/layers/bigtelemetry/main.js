$(document).ready(function () {
  for(var readyi in VBigTelemety){
    source[VBigTelemety[readyi]].layer = new L.LayerGroup();
  }
  //legend
  controllerBigTelemety.legend = L.control({ position: 'bottomright' });
  controllerBigTelemety.legend.onAdd = function (map) {
      var div = L.DomUtil.create('div', 'info legend'),
          grades = [0, 30, 50, 80],
          labels = [],
          from, to;
      var statusBigTele = { "#FFFF00":"0-30", "#00C853":"30-50", "#1976D2":"50-80", "#FF4081":"80+"};
      labels.push('<div style="margin-bottom: 2px;">ระดับน้ำ(%)</div>');
      $.each(statusBigTele, function( color, value ) {
          labels.push('<div style="margin-bottom: 2px;"><i style="background:' + color + '"></i> ' + value + '</div>');
      });
      div.innerHTML = labels.join('');
      return div;
  };
});//ready
function getBigtelemetryData(node, callback) {
    if (source[node].geoJson == undefined) {
    block('#map');
		$.ajax({
				url: "http://gis.rid.go.th/geoserver/ridtelemetry/ows",
				jsonp: "callback",
				jsonpCallback: "parseResponse",
				dataType: "jsonp",
				data: {
					version: "1.0.0",
					request: "GetFeature",
					typeName: "ridtelemetry:view_telemetry_geometry",
					styles: "pointTelemetry",  //style_from_geoserver
					service: "WFS",
          CQL_FILTER: "river_basin='"+ node+"'",
					outputFormat: "text/javascript",
				},
  			// Work with the response
  			success: function( data ) {
          source[node].responsedata = data;
    			if (source[node].responsedata !== null) {
		     				source[node].geoJson = L.geoJSON(source[node].responsedata, {
            				onEachFeature: onEachBigTeleFeature,
			         			pointToLayer: function (feature, latlng) {
                        var _status = feature.properties.status;
                        var _crash = feature.properties.crash;
                        var _wtl = feature.properties.water_lavel;
                        var _pst = getPercent(_status, _crash, _wtl);
                        return L.marker(latlng, {
                            icon: tmgetColor(_pst)
                        });
                    }
								})
                .bindTooltip(function (e) {
                      return '' + e.feature.properties.full_name;
                  },
                  {
                    direction: 'top',
                    offset: L.point(0, -10)
                }).addTo(source[node].layer);
   						  unblock('#map');
                callback($(source[node].geoJson).length);
     		   }
 				}
		});
    }
    else {
	    callback($(source[node].geoJson).length);
	  }
}
function onEachBigTeleFeature(feature, layer) {
  if (!feature.properties.gid != true) {
    var _id = feature.properties.gid;
    layer.on({
      click: function (e) {
        block('#map');
        $.ajax({
          url: "http://gis.rid.go.th/geoserver/ridtelemetry/ows",
          jsonp: "callback",
          jsonpCallback: "parseResponse",
          dataType: "jsonp",
          data: {
            version: "1.0.0",
            request: "GetFeature",
            typeName: "ridtelemetry:view_telemetry_geometry",
            CQL_FILTER: "gid="+_id,
            service: "WFS",
            outputFormat: "text/javascript",
          },
          // Work with the response
          success: function( data ) {
            var info = data.features[0].properties;
            var content = "";
            var str_status = "";
            var _status = feature.properties.status;
            var _time = moment.tz(info.date_time, "Asia/Bangkok");
            var _nametl = TeleIndexOf(info.river_basin);

            content += '<div class="modal-dialog">';
            content += '<div class="modal-content">';
            content += '<div class="modal-header">';
            content += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
            content += '<h4 class="modal-title" id="featuretab-title">'+info.full_name+'</h4>';
            content += '</div>';
            content += '<div class="modal-body cst-model-geosever" id="featuretab-info ">';
              content += '<ul id="aboutBigTeleAMRTabs" class="nav nav-tabs" style="margin-bottom: 10px;">';
                content += '<li id="mn-main-model" class="active">';
                content += '<a href="#featureBigTeleAMR-WL" data-toggle="tab">';
                content += '<i class="glyphicon glyphicon-time"></i> ระดับน้ำ';
                content += '</a>';
                content += '</li>';
              content += '</ul>';
              content += '<div id="main_body_dialog" class="other-dialog">';
                content += '<table class="table table-striped table-bordered table-condensed">';
                content += '<tbody>';
                  content += '<tr><th colspan="2" class="text-center">วันที่ '+_time.format("DD-MM-YY H:mm:ss")+'</th></tr>';
                  content += "<tr><td>ระบบโทรมาตร</td><td>"+_nametl+"</td></tr>";
                  content += "<tr><td>สถานีโทรมาตร</td><td>"+info.full_name+"</td></tr>";
                  content += "<tr><td>ระดับน้ำปัจจุบัน(ม.รทก.)</td><td>"+info.water_lavel+"</td></tr>";
                  content += "<tr><td>ปริมาณน้ำ(Q)</td><td>"+info.q+"</td></tr>";
                  content += "<tr><td>ปริมาณน้ำฝน (มม.)</td><td>"+info.rain+"</td></tr>";
                content += '</tbody>';
                content += '</table>';
                content += '<table style="margin-top: 10px;" class="table table-striped table-bordered table-condensed">';
                content += '<tbody>';
                  content += '<tr><th colspan="2" class="text-center">ข้อมูลพื้นฐาน</th></tr>';
                  if(info.crash != null){
                    content += "<tr><td>ระดับน้ำวิกฤติ(ม.รทก.)</td><td>"+info.crash+"</td></tr>";
                  }
                  if(info.warning != null){
                    content += "<tr><td>ระดับน้ำเตือนภัย(ม.รทก.)</td><td>"+info.warning+"</td></tr>";
                  }
                  content += "<tr><td>Latitude</td><td>"+info.latitude+"</td></tr>";
                  content += "<tr><td>Longitude</td><td>"+info.longitude+"</td></tr>";
                content += '</tbody>';
                content += '</table>';
              content += '</div>';
              content += '<div id="csinfo_body_dialog" class="other-dialog"></div>';
            content += '</div>';
            content += '</div>';
            content += '</div>';

            content += '<script>';
            content += '$(document).ready(function () {';
              content += '$( "#mn-main-model" ).click(function() {';
                content += '$(\'.other-dialog\').hide();';
                content += '$(\'#main_body_dialog\').show();';
              content += '});';
            content += '});';
            content += '</script>';

            $('#iwocModal').html(content).modal("show");
            unblock("#map");
            getInfoCustomLayerGeoData(info.latitude, info.longitude);
          }
        });
      }
    });
  }

}
function getPercent(status, max, val){
    if(status == 'OFF'){
      return -1;
    }
    if(max>0){
      return (val*100)/max;
    }
    else{
      return 50;
    }
}
function tmgetColor(QTotal) {
    var fillColor = source.bigtelethachin.greyIcon;
    if (QTotal <= 30 && QTotal >= 0)
        fillColor = source.bigtelethachin.yellowIcon; //yellow
    else if (QTotal > 30 && QTotal <= 50)
        fillColor = source.bigtelethachin.greenIcon; //green
    else if (QTotal > 50 && QTotal <= 80)
        fillColor = source.bigtelethachin.blueIcon; //blue
    else if (QTotal > 80)
        fillColor = source.bigtelethachin.redIcon; //red
    else
        fillColor = source.bigtelethachin.greyIcon; // no data
    return fillColor
}
function TeleIndexOf(st){
  for(var vfunc in VBigTelemety){
    if(VBigTelemety[vfunc].trim() == st.trim()){
      return vfunc;
    }
  }
  return '';
}