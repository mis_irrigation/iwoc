

function pad(value, length) { //Add Zero Prevent
    return (value.toString().length < length) ? pad("0"+value, length) : value;
}

function get_date_en(){
	var currentDate = new Date();
	var day = currentDate.getDate();
	var month = currentDate.getMonth()+1;
	var year = currentDate.getFullYear();
	return year+'-'+pad(month,2)+'-'+pad(day,2);
}

function get_date_th(){
	var currentDate = new Date();
	var day = currentDate.getDate();
	var month = currentDate.getMonth()+1;
	var year = currentDate.getFullYear()+543;
	return day+'/'+pad(month,2)+'/'+pad(year,2);
}

function chg_date_en(dd){ //แปลง date English = 31/12/2557 --> 2014-12-31
	if(dd){
		var date =dd;
		var date_array = date.split("/");
		var year = parseInt(date_array[2])-543;
		var month = date_array[1];
		var day = date_array[0];
		return year+'-'+month+'-'+day;
   }else{
		return "";
   }
}

function chg_date_th(dd){ //แปลง date English = 2014-12-31 --> 31/12/2557 
	if(dd){
		var date =dd;
		var date_array = date.split("-");
		var year = parseInt(date_array[0])+543;
		var month = date_array[1];
		var day = date_array[2];
		return day+'/'+month+'/'+year;
   }else{
		return "";
   }
}


function chg_date_thai(dd) { //แปลง date = 2017-01-01 || 01/01/2560 --> 1 มกราคม 2560
	if (dd) {
		var date = dd;
		var month_arr = Array("มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
		if (date.substr(4, 1) == "-") {
			var date_array = date.split("-");
			var day = parseInt(date_array[2]) + 0;
			var month_i = date_array[1];
			var month = month_arr[parseInt(month_i) - 1];
			var year = parseInt(date_array[0]) + 543;
			return day + ' ' + month + ' ' + year;
		} else {                
			var date_array = date.split("/");
			var day = parseInt(date_array[0]) + 0;
			var month_i = date_array[1];
			var month = month_arr[parseInt(month_i) - 1];
			var year = parseInt(date_array[2]);                
			return day + ' ' + month + ' ' + year;
		}
	} else {
		return "";
	}
}

function chg_day_month(dd){ //แปลง date English = 2014-12-31 --> 1 ธ.ค. 
	if(dd){
		var date =dd;
		var date_array = date.split("-");
		//var year = parseInt(date_array[0])+543;
		var month_arr=Array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
		var month = date_array[1];
		var day = parseInt(date_array[2])+0;
		return day+' '+month_arr[parseInt(month)-1];
   }else{
		return "";
   }
}

function chg_year_th(yy){ //แปลง date English = 2017-12-31 --> 2560 
	return parseInt(yy.substr(0,4))+543
}

function getRandomColor() {
 function c() {
    var hex = Math.floor(Math.random()*256).toString(16);
    return ("0"+String(hex)).substr(-2); // pad with zero
  }
  return "#"+c()+c()+c();
 }

function empty(e) {
  switch (e) {
    case "":
    case 0:
    case "0":
    case null:
    case false:	
	case undefined:
    case typeof this == "undefined":
      return true;
    default:
      return false;
  }
}

function print_r(o){
	//return JSON.stringify(o,null,'\t').replace(/\n/g,'<br>').replace(/\t/g,'&nbsp;&nbsp;&nbsp;'); 
	return JSON.stringify(o,null,'\t').replace(/\n/g,'<br>').replace(/\t/g,' '); 
}


// ====================Export Xls====================
var tableToExcel = (function () {
    var uri = 'data:application/vnd.ms-excel;base64,',
       template =
        '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--> <style>                                              table{ border-top:solid 1px #000;	border-right:solid 1px #000; }  table th, table td{ border-left: solid 1px #000; border-bottom: solid 1px #000;  }               td.region{font-size:16px !important;font-weight:bold;background-color:#FFFF99;} .tr_sum td{background-color:#CCFFCC;font-weight:bold;} .tr_min td{background-color:#CCFFCC;} .tr_sum_total td{background-color:#CCFFCC;font-weight:bold;} table th{ background-color:#f1f1f1; }          </style>   </head><body><table>{table}</table></body></html>',
        base64 = function (s) {
            return window.btoa(unescape(encodeURIComponent(s)))
        },
        format = function (s, c) {
            return s.replace(/{(\w+)}/g, function (m, p) {
                return c[p];
            })
        }
    return function (table, tab_name, dl_name) {
        if (!table.nodeType) table = document.getElementById(table)
        var ctx = {
            worksheet: tab_name || 'Worksheet',
            table: table.innerHTML
        }
        //window.location.href = uri + base64(format(template, ctx))
		var link = document.createElement("a");
		  link.href = uri + base64(format(template, ctx));
		  link.download = dl_name || 'Workbook.xls';
		  link.target = '_blank';
		  document.body.appendChild(link);
		  link.click();
		  document.body.removeChild(link);
		  
    }
})();

function exportExcell(tableId, dl_name){
	var htmltable= document.getElementById(tableId);
	var html = htmltable.outerHTML;
	var link = document.createElement("a");
		  link.href = 'data:application/vnd.ms-excel,' + encodeURIComponent(html);
		  link.download = dl_name || 'Workbook.xls';
		  link.target = '_blank';
		  document.body.appendChild(link);
		  link.click();
		  document.body.removeChild(link);
}










