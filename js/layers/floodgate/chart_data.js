// JavaScript Document
var date_start_graph;
var date_end_graph;

var _labels = [];
var _datasets = [];
var _label = '';
var _data = [], _data_up = [], _data_down = [];
var chart;
var arr_color = ['#0000FF', '#F68401', '#009900', '#885502', '#6A0D55', '#0763F6', '#AC698A', '#676515', '#6A0221', '#344E36'];
var year_start, month_start;
var year_end, month_end;
var y_max, arr_y_max = [], y_step;
var i = 0;


//------------------------------------------------------------------------------------              
$(function () {

    //------------------------------------------------------------------------------------------
    $('#date_start_graph').datepicker({
        language: "th-th",
        autoclose: true
    })
        .on('changeDate', function (e) {
            // date_start=chg_date_en(e.format());
        });
    //---------------------------------------------------------
    $('#date_end_graph').datepicker({
        language: "th-th",
        autoclose: true
    })
        .on('changeDate', function (e) {
            //date_end=chg_date_en(e.format());
        });



}); //ready


function fnc_floodgateGraph() {

    arr_y_max = [];
    y_max = 20;
    y_step = 2;
    _labels = [];
    _data = [], _data_up = [], _data_down = [];
    _datasets = [];
    i = 0;
    //-------------------------------------------------------------------------------------
    year_start = chg_date_en($('#date_start_graph').val()).substr(0, 4);
    year_end = chg_date_en($('#date_end_graph').val()).substr(0, 4);

    month_start = chg_date_en($('#date_start_graph').val()).substr(4, 6);
    month_end = chg_date_en($('#date_end_graph').val()).substr(4, 6);

    date_start = year_start + month_start;
    date_end = year_end + month_end;

    if (year_start == year_end) { //ถ้าช่อง date_start และ date_end เป็นปีเดียวกัน 
        load_chart(date_start, date_end);
    } else {
        load_chart(date_start, year_start + '-12-31');
    }


}


function load_chart(date_start, date_end) {

    console.info('Chart 1 >>>  date_start=' + chg_date_th(date_start) + ' date_end=' + chg_date_th(date_end));
    block('#tab_graph');
    $.ajax({
        method: "GET",
        url: base_url + "api/telemetryGeojson?basin=floodgate",
        data: {
            date_start: date_start,
            date_end: date_end,
            station: $('#floodgate_id').val()
        },
        dataType: "json"
    })
        .done(function (data) {

            _labels = [];
            _data = [];
            //console.log('time i == ' + i);

            $.each(data.features, function (index, value) {
                // console.log( value.properties.TMD_DateTime );
                if (value.properties.TMD_DateTime != null) {
                    //console.log(value.properties.TMD_DateTime + ' == ' + value.properties.TMD_Q);
                    _labels.push(value.properties.TMD_DateTime); //แสดงแกน x 
                    _data.push(value.properties.TMD_Q);
                    _data_up.push(value.properties.TMD_UStream);
                    _data_down.push(value.properties.TMD_DStream);
                }
            });

            if (_data.length > 0) {
                var color = arr_color[i];
                _datasets.push(
                    {
                        label: "ปริมาณน้ำ "+chg_year_th(date_start), //label แสดงข้างบน
                        fill: false,
                        lineTension: 0.1,
                        borderWidth: 3,
                        backgroundColor: "#FFFFFF",
                        borderColor: color,
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: color,
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 3,
                        pointHoverBackgroundColor: color,
                        pointHoverBorderColor: "#FFFFFF",
                        pointHoverBorderWidth: 1,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        yAxisID: "y-axis-1",
                        data: _data,
                    }
                );
            }

            if (_data_up.length > 0) {
                //ระดับน้ำเหนือ 
                var color_up = getRandomColorFloodgate(); //"#FF0000";
                _datasets.push(
                    {
                        label: "ระดับน้ำเหนือ " + chg_year_th(date_start), //label แสดงข้างบน
                        fill: false,
                        lineTension: 0.1,
                        borderWidth: 2,
                        backgroundColor: "#FFFFFF",
                        borderColor: color_up,
                        borderCapStyle: 'butt',
                        borderDash: [6, 5], //เส้นประ
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: color_up,
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 3,
                        pointHoverBackgroundColor: color_up,
                        pointHoverBorderColor: "#FFFFFF",
                        pointHoverBorderWidth: 1,
                        pointRadius: 0,
                        pointHitRadius: 10,
                        spanGaps: true,
                        yAxisID: "y-axis-2",
                        data: _data_up,
                    }
                );
            }

            if (_data_down.length > 0) {
                //ระดับน้ำท้าย
                var color_down = getRandomColorFloodgate(); //"#F800A1";
                _datasets.push(
                    {
                        label: "ระดับน้ำท้าย " + chg_year_th(date_start), //label แสดงข้างบน
                        fill: false,
                        lineTension: 0.1,
                        borderWidth: 2,
                        backgroundColor: "#FFFFFF",
                        borderColor: color_down,
                        borderCapStyle: 'butt',
                        borderDash: [6, 5], //เส้นประ
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: color_down,
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 3,
                        pointHoverBackgroundColor: color_down,
                        pointHoverBorderColor: "#FFFFFF",
                        pointHoverBorderWidth: 1,
                        pointRadius: 0,
                        pointHitRadius: 10,
                        spanGaps: true,
                        yAxisID: "y-axis-2",
                        data: _data_down,
                    }
                );
            }

            //------หาค่า Max ของปริมาณน้ำ เพื่อมาแสดง แกน Y--------------------------------------------------------------
            //console.log( _data );
            arr_y_max.push(parseFloat(d3.max(_data)));
            //console.info( arr_y_max );
            var y_max_top = parseFloat(d3.max(arr_y_max));
            //console.log('Y MAX : '+y_max_top);
            if (isNaN(y_max_top) == false) {
                if (y_max_top > 0) {
                    y_max = parseFloat(y_max_top * 2.2);
                }
            }
            console.log('y_max == ' + y_max);

            gen_chart();
            year_start++;
            if (year_start <= year_end) {
                //console.log('amount year == ' + ((year_end - year_start) + 1))
                if (year_start == year_end) { //ถ้าเป็นปีเดียวกัน 
                    if ((year_end - year_start) + 1 == 0) {
                        //console.log('AAA')
                        load_chart(year_start + month_start, year_end + month_end);
                    } else {
                        //console.log('BBB')
                        load_chart(year_start + '-01-01', year_end + month_end);
                    }
                } else { //ถ้าไม่ใช่ปีเดียวกัน 
                    //console.log('CCC')
                    load_chart(year_start + '-01-01', year_start + '-12-31');
                }
            }

            if (year_start > year_end) {
                unblock('#tab_graph');
            }
            i++;
        })
        .fail(function (jqXHR, textStatus, error) {
            unblock('#tab_graph');
            alert("Error : " + textStatus + " " + error);
        });

} //fnc_floodgateGraph() 


function gen_chart() {

    //--------------------------------------------------------------
    var data = {
        labels: _labels,
        datasets: _datasets,
    };

    var option = {
        chartArea: {
            backgroundColor: '#EEFBFC', //สีพื้นหลัง            
        },

        bezierCurve: false,
        datasetFill: false,
        scaleGridLineColor: "#3E434E",
        legendCallback: function (chart) {
            //console.log(chart);
            var legendHtml = '';
            legendHtml += '<div class="tbl_legend_chart" align="center">';
            var _line, _size;
            for (var i = 0; i < chart.data.datasets.length; i++) {
                if (chart.data.datasets[i].label) {
                    var _lg_name = chart.data.datasets[i].label + '';
                    if (_lg_name.indexOf('ระดับน้ำเหนือ') != -1 || _lg_name.indexOf('ระดับน้ำท้าย') != -1) {
                        _line = 'dashed';
                        _size = '3.4px';
                    } else {
                        _line = 'solid';
                        _size = '3.9px';
                    }
                    legendHtml += '<span class="chart-legend-label-text text-left" onclick="updateDataset(event, ' + '\'' + chart.legend.legendItems[i].datasetIndex + '\'' + ', this)"  > <button type="button" class="legend_color_line" style="border-bottom:' + _line + '  ' + _size + chart.data.datasets[i].borderColor + '; "></button>  ' + chart.data.datasets[i].label + '</span>';
                    if (_lg_name.indexOf('ระดับน้ำท้าย') != -1) {
                        legendHtml += '<br>';
                    }
                }
            } //for
            legendHtml += '</div>';
            return legendHtml;
        },

        legend: {
            display: false,

            onClick: function (e, legendItem) {
                // code from https://github.com/chartjs/Chart.js/blob/master/src/core/core.legend.js
                // modified to prevent hiding all legend items

                var index = legendItem.datasetIndex;
                var ci = this.chart;
                var meta = ci.getDatasetMeta(index);

                // See controller.isDatasetVisible comment
                var hidden = meta.hidden === null ? !ci.data.datasets[index].hidden : null;

                if (hidden) {
                    var someShown = _.some(this.legendItems, function (item) {
                        return item !== legendItem && !item.hidden;
                    });
                    if (!someShown)
                        return;
                }

                meta.hidden = hidden;
                // We hid a dataset ... rerender the chart
                ci.update();
            },

            labels: {
                fontColor: '#000000'
            },
        },
        scales: {
            xAxes: [{
                type: 'time',
                time: {
                    //format: "DD MMMM",
                    unit: 'month', //หน่วยการแสดงเป็นเดือน 
                    unitStepSize: 1, //Step การแสดงระยะห่างกัน 
                    displayFormats: { //แสดงแค่ชื่อเดือน 
                        'millisecond': 'MMMM',
                        'second': 'MMMM',
                        'minute': 'MMMM',
                        'hour': 'MMMM',
                        'day': 'MMMM',
                        'week': 'MMMM',
                        'month': 'MMMM',
                        'quarter': 'MMMM',
                        'year': 'MMMM',
                    }
                },
                scaleLabel: {
                    display: true,
                    labelString: 'เดือน',
                    fontColor: '#000000'
                },
                ticks: {
                    fontColor: '#000000'
                },
            }],
            yAxes: [{
                type: "linear",
                display: true,
                position: "left",
                id: "y-axis-1",
                scaleLabel: {
                    display: true,
                    labelString: 'ปริมาณน้ำ (Q)  (ลบ.ม./วิ) ',
                    fontColor: '#000000'
                },
                ticks: {
                    beginAtZero: true,
                    //stepSize : y_step,
                    max: y_max,
                    fontColor: '#000000',
                }
            },
            {
                type: "linear",
                display: true,
                position: "right",
                id: "y-axis-2",
                scaleLabel: {
                    display: true,
                    labelString: 'ระดับน้ำเหนือ - ท้าย  (ม.รทก.) ',
                    fontColor: '#000000'
                },
                ticks: {
                    beginAtZero: true,
                    //stepSize : y_step,
                    //max: y_max,
                    fontColor: '#000000',
                }
            }],
            chartArea: {
                backgroundColor: 'rgba(238, 247, 253, 0.4)'
            },
        },
        title: {
            display: false,
            text: "Report Title \n 5555",
            fontFamily: "thaisanslite",
            fontSize: 22,
            fontColor: "#000",
            fontStyle: "bold",
            padding: 10,
        },
        tooltips: {
            enabled: true,
            //mode: 'single',
            //-----
            titleFontSize: 0,
            //-----
            backgroundColor: "#333333",
            bodyFontColor: "#FFFFFF", //#000000
            bodyFontSize: 12,
            bodyFontStyle: "bold",
            bodyFontColor: '#FFFFFF',
            bodyFontFamily: "tahoma",
            //-----
            footerFontSize: 12,
            footerFontFamily: "tahoma",
            custom: function (tooltip) {
                // tooltip will be false if tooltip is not visible or should be hidden
                if (!tooltip) {
                    return;
                }
                //tooltip.body = ['TEST']

            },
            callbacks: {
                label: function (tooltipItem, data) {
                    //console.log( data );
                    //console.log( tooltipItem.index )  
                    var t_i = tooltipItem.datasetIndex;
                    var t_j = tooltipItem.index;
                    var t_arr_date = data.labels;
                    //console.log( t_arr_date );
                    var t_year = data.datasets[t_i].label;
                    var t_date = chg_day_month(data.labels[t_j]);
                    var t_data = data.datasets[t_i].data[t_j];
                    if (t_year.length > 4) {
                        t_year = t_year.split(' ');
                        return " วันที่: " + t_date + " " + t_year[1];
                    } else {
                        //return "วันที่: "+t_date+" ปริมาณน้ำ: "+t_data+" ล้าน ลบ.ม.";
                        return " วันที่: " + t_date + " " + t_year;
                    }
                },
                footer: function (tooltipItem, data) {
                    //return ['new line', 'another line'];
                    var t_i = tooltipItem[0].datasetIndex;
                    var t_j = tooltipItem[0].index;
                    var t_arr_date = data.labels;
                    //console.log( arr_date[j] );
                    var t_year = data.datasets[t_i].label;
                    var t_date = chg_date_thai(data.labels[t_j]);
                    var t_data = data.datasets[t_i].data[t_j];
                    if (t_year.length > 4) {
                        t_year = t_year.split(' ');
                        return t_year[0]+": " + t_data + " ม.รทก. ";
                    } else {
                        return "  ปริมาณน้ำ: " + t_data + " ลบ.ม./วิ ";
                    }
                }
            } //callbacks

        } //tooltips 

    } //options


    // Show/hide chart by click legend
    updateDataset = function (e, datasetIndex, obj) {

        if ($(obj).hasClass('line_hidden') == false) {
            $(obj).addClass('line_hidden');
        } else {
            $(obj).removeClass('line_hidden');
        }
        //console.log(e);
        //console.log(datasetIndex);
        var index = datasetIndex;
        var ci = e.view.chart;
        var meta = ci.getDatasetMeta(index);

        // See controller.isDatasetVisible comment
        //console.log(meta.hidden);
        if (meta.hidden == null) {
            meta.hidden = !ci.data.datasets[index].hidden; //Hidden
        } else {
            meta.hidden = ci.data.datasets[index].hidden; //Show
        }

        // We hid a dataset ... rerender the chart
        ci.update();
    };


    //------------------------------------------------------------------------------------------------
    var ctx = document.getElementById("chart1");
    Chart.plugins.register({
        beforeDraw: function (chart, easing) {
            if (chart.config.options.chartArea && chart.config.options.chartArea.backgroundColor) {
                var helpers = Chart.helpers;
                var ctx = chart.chart.ctx;
                var chartArea = chart.chartArea;

                ctx.save();
                ctx.fillStyle = chart.config.options.chartArea.backgroundColor;
                ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
                ctx.restore();
            }
        }
    });

    if (empty(chart) == false) {
        chart.destroy();
    }
    chart = new Chart(ctx, {
        type: 'line',
        data: data,
        options: option
    });
    document.getElementById('legend').innerHTML = chart.generateLegend();

}


function getRandomColorFloodgate() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

