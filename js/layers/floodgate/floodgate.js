$(document).ready(function() {

    source.floodGate.legend = L.control({ position: 'bottomright' });
    source.floodGate.legend.onAdd = function(map) {
        var div = L.DomUtil.create('div', 'info legend'),
            grades = [0, 30, 50, 80],
            labels = [],
            from, to;
        //labels.push('<div><b>สถานการณ์<b></div>');
        let statusBigTele = { "#00BD06": "ปกติ", "#FFDB43": "เตือนภัย", "#D90022": "วิกฤติ", "#006DF1": "การสื่อสารขัดข้อง" };
        labels.push('<div style="margin-bottom: 2px;">ประตูระบายน้ำ</div>');
        $.each(statusBigTele, function(color, value) {
            labels.push('<div style="margin-bottom: 2px;"><i style="background:' + color + '"></i> ' + value + '</div>');
        });
        div.innerHTML = labels.join('');
        return div;
    };
    source.floodGate.layer = new L.LayerGroup();

}); //ready

function getLayerfloodGateData(callback) {
    if (source.floodGate.geoJson == undefined) {
        block('#map');

        $.getJSON("http://app.rid.go.th:88/reservoir/telemetry/api/telemetryGeojson?basin=floodgate", function(data) {
                if (data != null) {
                    source.floodGate.geoJson = L.geoJSON(data, {

                            style: function(feature) {
                                return feature.properties && feature.properties.style;
                            },
                            onEachFeature: onEachFloodGateFeature,
                            pointToLayer: function(feature, latlng) {
                                // var smallIcon = L.icon({
                                //     iconSize: [20, 20],
                                //     iconAnchor: [13, 27],
                                //     popupAnchor: [1, -24],
                                //     iconUrl: 'images/floodgate/' + getMarkerFloodGate(feature.properties.TMD_Q)
                                // });
                                // return L.marker(latlng, { icon: smallIcon });
                                //---
                                return L.circleMarker(latlng, {
                                    fillColor: getColorFloodGate(feature.properties.TMD_Q),
                                    color: "#000",
                                    radius: 8,
                                    weight: 1,
                                    opacity: 1,
                                    fillOpacity: 0.8,
                                });
                            }
                        })
                        .bindTooltip(function(e) {
                            return '' + e.feature.properties.TMI_Name;
                        }, { direction: 'top', offset: L.point(0, -20) })
                        .addTo(source.floodGate.layer);
                }
            })
            .done(function() {
                callback($(source.floodGate.geoJson).length);
            })
            .fail(function(res) {
                alert('floodGate request failed! ' + JSON.stringify(res, null, '\t'));
            })
            .always(function() {
                unblock('#map');
            });

    } else {
        callback($(source.floodGate.geoJson).length);
    }
}

// function getMarkerFloodGate(q_data) {
//     var marker_image = "";
//     if (q_data == null || q_data == 0) {
//         marker_image = "m-red.png";
//     }
//     else {
//         marker_image = "m-green.png";
//     }
//     return marker_image;
// }

function getColorFloodGate(q_data) {
    var fillColor = "";
    if (q_data == null || q_data == 0) {
        fillColor = "#E34B62"; //red
    } else {
        fillColor = "#4ACF4E"; //green
    }
    return fillColor;
}

function onEachFloodGateFeature(feature, layer) {

    if (feature.properties && feature.properties.TMI_Name) {
        layer.on({
            //mouseover: highlightFeature,
            //mouseout: resetHighlight,
            click: function(e) {
                //console.warn(feature.properties.cresv);
                //$("#featuretab-info").html('');
                block('#map');
                $.get("js/layers/floodgate/modal.html",
                    function(data) {
                        $('#iwocModal').html(data);

                        console.log(feature.properties.TMI_ID);
                        $('#floodgate_id').val(feature.properties.TMI_ID);
                        $('#date_start_table').val(chg_date_th(moment(feature.properties.TMD_DateTime).add(-30, 'days').format('YYYY-MM-DD')));
                        $('#date_end_table').val(chg_date_th(feature.properties.TMD_DateTime));
                        $('#date_start_graph').val(chg_date_th(moment(feature.properties.TMD_DateTime).add(-30, 'days').format('YYYY-MM-DD')));
                        $('#date_end_graph').val(chg_date_th(feature.properties.TMD_DateTime));
                        //---------------------------------------------------------------------------
                        $("#featuretab-title").html('<strong>ประตูระบายน้ำ</strong> <span class="glyphicon glyphicon-menu-right"></span> ' + ' <strong>' + feature.properties.TMI_Name + '</strong>');
                        $("#show_date").html(feature.properties.TMD_DateTime_th);
                        $("#TMD_ULevel").html(feature.properties.TMD_ULevel);
                        $("#TMD_DLevel").html(feature.properties.TMD_DLevel);
                        $("#TMD_Q").html(feature.properties.TMD_Q);

                        $("#iwocModal").modal("show");

                        unblock('#map');
                    }
                );

                // var today = new Date().toJSON().slice(0,10).replace(/-/g,'-');
                // console.log(today);							
                // $.get("http://ra.com:52/reservoir/iwoc/popup?date_val="+today+"&rsvmiddle_val="+feature.properties.cresv+"", {  }, function(data){
                //     $("#iwocModal").html(data);
                //     $("#iwocModal").modal("show");
                // });

            }
        });

    } //if 
}
//getLayerMiddleData();