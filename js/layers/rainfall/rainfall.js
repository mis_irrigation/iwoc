$(document).ready(function() {
    // observe
    observable.on('rainfall_searchDuration', function(_start, _end) {
        block('#rainfall #tab_table');
        block('#rainfall #tab_graph');
        var start = _start || moment().add(-1, 'd'),
            end = _end || moment();
        source.rainfall.loadDataAgo(source.rainfall.currentPoint.properties.telestation_id, start.format('x'), end.format('x'), function(err, res) {
            observable.notify('rainfall_setDefaultDate_calendar', start, end);
            if (err) {
                if(window.config.system.console) console.log(err);
            } else {
                source.rainfall.updateTabGraph(start, end, res, function() {
                    unblock('#rainfall #tab_graph');
                });
                source.rainfall.updateTabTable(start, end, res, function() {
                    unblock('#rainfall #tab_table');
                });
            }
        });
    });
    observable.on('rainfall_setDefaultDate_calendar', function(start, end) {
        var dateDiff = parseInt(moment(end.diff(start)).format('D')) - 1;
        $('#rainfall #tab_table #date_start, #rainfall #tab_graph #date_start').datepicker("update", start.format('DD/MM/YYYY'));
        $('#rainfall #tab_table #date_end, #rainfall #tab_graph #date_end').datepicker("update", end.format('DD/MM/YYYY'));
        // update btn_dayAgo
        $('#rainfall .btn_dayAgo').each(function(index, element) {
            parseInt(element.value) === dateDiff ? $(element).addClass('btn-primary').removeClass('btn-default') : $(element).removeClass('btn-primary').addClass('btn-default')
        });
    });
});

source.rainfall.getData = function(source_name, anch_id,  callback) {
    if (source[source_name].geoJson === undefined) {
        // var start = moment('2016-12-31 18:00', 'YYYY-MM-DD HH:mm').format('x');
        // var end = moment('2016-12-31 18:59', 'YYYY-MM-DD HH:mm').format('x');
        var start = moment().add(-1, 'days').format('x');
        var end = moment().format('x');
        block('#map');
        if(window.config.system.console) console.log(window.config.system.server+"/rainfall/rainfallLast?anch_id="+anch_id+"&time_end=" + end + "&time_start=" + start);
        $.getJSON(window.config.system.server+"/rainfall/rainfallLast?anch_id="+anch_id+"&time_end=" + end + "&time_start=" + start, function(data) {
                if (data === null) {
                    unblock('#map');
                    callback();
                } else {
                    source[source_name].geoJson = L.geoJSON(data, {
                        style: function(feature) {
                            return feature.properties && feature.properties.style;
                        },
                        onEachFeature: source.rainfall.onEachFeature.bind(this, source_name),
                        pointToLayer: function(feature, latlng) {
                            var smallIcon = L.icon({
                                iconSize: [22, 22],
                                iconAnchor: [13, 27],
                                popupAnchor: [1, -24],
                                iconUrl: feature.properties.rainfall_today === 0 ? window.config.system.rainfall.icon.sunny : window.config.system.rainfall.icon.rain
                            });
                            return L.marker(latlng, { icon: smallIcon });
                        }
                    }).bindTooltip(function (e) {
                        return 'สถานีวัดน้ำฝน '+e.feature.properties.term;
                     }, {direction: 'top', offset: L.point(0, -20)}).addTo(source[source_name].layer);
                    unblock('#map');
                    callback();
                }
            })
            .fail(function(err) {
                callback(err);
            });
    } else {
        // Incase already download
        callback(null);
    }
};

source.rainfall.onEachFeature = function(source_name,feature, layer) {
    layer.show = function () { 
        $(this._icon).show();
        $(this._shadow).show();
     };
    layer.hide = function () { 
        $(this._icon).hide();
        $(this._shadow).hide();
     };
    if (feature.properties) {
        feature.properties.term = feature.properties.name;
        layer.on({
            click: function(e) {
                block('#map');
                source.rainfall.currentPoint = feature;
                var prop = feature.properties;
                var lat_long = feature.geometry.coordinates;
                $.get("js/layers/rainfall/modal.html",
                    function(modalTemplate) {
                        $('#iwocModal').htmlReadAble(modalTemplate);
                        $('#rainfall #featuretab-title').htmlReadAble('<strong> สถานีวัดน้ำฝน  </strong> <span class="glyphicon glyphicon-menu-right"></span> <strong>' + prop.name + '</strong>');
                        $('#rainfall .telestation_datetime').htmlReadAble(moment(prop.telestation_datetime).clone().utc().add(543, 'years').format('D MMMM YYYY เวลาประมาณ HH:mm'));
                        $('#rainfall .district_name').htmlReadAble(prop.district) ;
                        $('#rainfall .geocode_basin').htmlReadAble(prop.geocode_basin);
                        $('#rainfall .ground_level').htmlReadAble(prop.ground_level);
                        $('#rainfall .left_bank').htmlReadAble(prop.left_bank);
                        $('#rainfall .old_id').htmlReadAble(prop.old_id);
                        $('#rainfall .province_name').htmlReadAble(prop.province);
                        $('#rainfall .amphoe_name').htmlReadAble(prop.amphoe);
                        $('#rainfall .region_name').htmlReadAble(prop.region_name);
                        $('#rainfall .right_bank').htmlReadAble(prop.right_bank);
                        $('#rainfall .telestation_id').htmlReadAble(prop.telestation_id + ' ( ' + prop.old_id.trim() + ' )');
                        $('#rainfall .telestation_name').htmlReadAble(prop.name);
                        $('#rainfall .lat_long').htmlReadAble(lat_long[1] + ', ' + lat_long[0]);
                        $('#rainfall .anch').htmlReadAble(prop.anch);
                        $('#rainfall .rainfall_1h').htmlReadAble( prop.rainfall_1h );
                        $('#rainfall .rainfall_today').htmlReadAble( prop.rainfall_today );
                        $('#rainfall .rainfall_24h').htmlReadAble( prop.rainfall_24h );
                        $('#rainfall .rainfall_yesterday').htmlReadAble( prop.rainfall_yesterday );

                        $("#iwocModal").modal("show");
                        // kill ajax
                        $("#iwocModal").on('hidden.bs.modal', function() {
                            if (source.rainfall.ajaxLoadDataAgo) source.rainfall.ajaxLoadDataAgo.abort();
                        });

                        // load table
                        observable.notify('rainfall_searchDuration');

                        // ON Modal Shown
                        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                            var target = $(e.target).attr("href");
                            if (target === '#tab_graph' || target === '#tab_table')
                                $('.modal-dialog').removeClass('modal-md').addClass('modal-lg');
                            else
                                $('.modal-dialog').removeClass('modal-lg').addClass('modal-md');
                        });
                        unblock('#map');
                    }
                );
            }
        });

    }
};

source.rainfall.loadDataAgo = function(telestation_id, date_start, date_end, cb) {
    if(window.config.system.console) console.log(telestation_id, new Date(date_start), new Date(date_end));
    source.rainfall.ajaxLoadDataAgo = $.getJSON(window.config.system.server+"/rainfall/rainfall?telestation_id=" + telestation_id + "&time_start=" + date_start + "&&time_end=" + date_end)
        .done(function(data) {
            if(window.config.system.console) console.log(window.config.system.server+"/rainfall/rainfall?telestation_id=" + telestation_id + "&time_start=" + date_start + "&&time_end=" + date_end);
            cb(null, source.rainfall.rainfallFiller(data));
        })
        .fail(function(err) {
            if(window.config.system.console) console.log("error");
        });
};

/**
 * This function modify data from server with 3 reason
 * 1) I want to add rainfall_1h_bar for bar graph, This is protect graph is overlap
 * 3) On year >= 2017 the data have only rainfall_today, rainfall_acc, I want to calculate rainfall_1h 
 * 2) On year < 2017 the data have only rainfall_1h, I want to add rainfall_acc and rainfall_today with '-'
 */
source.rainfall.rainfallFiller = function (data) {
    if(!data || !data.features || !Array.isArray(data.features)) return data;
    var timeBarBefore, accBarBefore;
    data.features.forEach(function(item, idx) {
        var prop = item.properties;
        var currentTime = moment(prop.telestation_datetime).utc();
        if (parseInt(currentTime.format('YYYY')) < 2017) {
            // chose only barData duration more than 1 hour OR on start 
            if (!timeBarBefore || timeBarBefore.clone().add(1, 'hours').isSameOrBefore(currentTime)) {
                prop.rainfall_1h_bar = prop.rainfall_1h.round(2);
                timeBarBefore = currentTime.clone();

            } else {
                prop.rainfall_1h_bar = 0;
            }
        } else {
            if (!timeBarBefore || timeBarBefore.clone().add(1, 'hours').isSameOrBefore(currentTime)) {
                prop.rainfall_1h = prop.rainfall_1h_bar  = (prop.rainfall_acc - (accBarBefore || 0)).round(2);
                accBarBefore = prop.rainfall_acc;
                timeBarBefore = currentTime.clone();
            } else {
                prop.rainfall_1h = '-';
                prop.rainfall_1h_bar = 0;
            }
        }
        // if undefined should be show '-'
        prop.rainfall_1h = prop.rainfall_1h === undefined ? '-': prop.rainfall_1h;
        prop.rainfall_acc = prop.rainfall_acc === undefined ? '-': prop.rainfall_acc;
        prop.rainfall_today = prop.rainfall_today === undefined ? '-': prop.rainfall_today;
    });
    return data;
}
