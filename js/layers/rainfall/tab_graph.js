source.rainfall.chartObj = {};
source.rainfall.updateTabGraph = function(start, end, geoData, cb) {
    $('#rainfall #tab_graph #content #date_start_text').html(start.format('D  MMMM ') + (parseInt(start.format('YYYY')) + 543) + ' เวลา ' + start.format('HH:mm น.'));
    $('#rainfall #tab_graph #content #date_end_text').html(end.format('D  MMMM ') + (parseInt(end.format('YYYY')) + 543) + ' เวลา ' + end.format('HH:mm น.'));
    // if exits destroy it
    if (source.rainfall.chartObj.elm) source.rainfall.chartObj.elm.destroy();
    if (geoData === null) return cb();
    Chart.plugins.register({
        afterEvent: function(chart, e) {
            if (e.chart.canvas.id === 'rainfall_chart_element' && e.type === 'mousemove') {
                chart.options.customLine.x = e.x;
            }
        },
        afterDraw: function(chart, easing) {
            var ctx = chart.chart.ctx;
            if (ctx.canvas.id !== 'rainfall_chart_element') return;
            var chartArea = chart.chartArea;
            var x = chart.options.customLine.x;
            if (!isNaN(x)) {
                ctx.save();
                ctx.strokeStyle = chart.options.customLine.color;
                ctx.moveTo(chart.options.customLine.x, chartArea.bottom);
                ctx.lineTo(chart.options.customLine.x, chartArea.top);
                ctx.stroke();
                ctx.restore();
            }
        }
    });
    var ctx = $('#rainfall #tab_graph #rainfall_chart_element')[0];
    source.rainfall.chartObj.elm = new Chart(ctx, {
        type: 'bar',
        data: source.rainfall.chartObj.setDataset(geoData.features),
        options: source.rainfall.chartObj.getOption()
    });
    cb();
};

source.rainfall.chartObj.getOption = function() {
    return {
        customLine: {
            color: 'black'
        },
        tooltips: {
            intersect: false,
            mode: 'label',
        },
        hover: {
            mode: 'label'
        },
        chartArea: {
            backgroundColor: '#EEFBFC', //สีพื้นหลัง            
        },
        bezierCurve: false,
        datasetFill: false,
        scaleGridLineColor: "#3E434E",
        legendCallback: function(chart) {
            var legendHtml = '';
            legendHtml += '<div class="tbl_legend_chart" align="center">';
            for (var i = 0; i < chart.data.datasets.length; i++) {
                if (chart.data.datasets[i].label) {
                    //---
                    if (i == 2) {
                        legendHtml += '<br>';
                    }
                    legendHtml += '<span class="chart-legend-label-text text-left" onclick="updateDataset(event, ' + '\'' + chart.legend.legendItems[i].datasetIndex + '\'' + ', this)"  > <button type="button" class="legend_color_line" style="border-bottom:solid 4px ' + chart.data.datasets[i].borderColor + '; "></button>  ' + chart.data.datasets[i].label + '</span>';
                }
            } //for
            legendHtml += '</div>';
            return legendHtml;
        },
        scales: {
            xAxes: [{
                type: 'time',
                time: {
                    unit: 'hour'
                },
                display: false
            }, {
                type: 'time',
                time: {
                    unit: 'day',
                    displayFormats: {
                        'millisecond': 'DD/MM/YYYY',
                        'second': 'DD/MM/YYYY',
                        'minute': 'DD/MM/YYYY',
                        'hour': 'HH:mm',
                        'day': 'DD/MM/YYYY',
                        'week': 'DD/MM/YYYY',
                        'month': 'DD/MM/YYYY',
                        'quarter': 'DD/MM/YYYY',
                        'year': 'DD/MM/YYYY',
                    }
                },
                scaleLabel: {
                    display: true,
                    labelString: 'วันที่'
                }
            }],
            yAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'ปริมาณน้ำฝน ( มม. )'
                },
                ticks: {
                    beginAtZero: true,
                    //stepSize : y_step,
                    // max: y_max,
                }
            }],
            chartArea: {
                backgroundColor: 'rgba(238, 247, 253, 0.4)'
            },
        }
    };
};

source.rainfall.chartObj.getLineStyle = function(owner, idx) {
    var colors_pattern = source.rainfall.colors_pattern;
    return $.extend({
        fill: false,
        lineTension: 0.1,
        backgroundColor: colors_pattern[idx].fillColor,
        borderColor: colors_pattern[idx].strokeColor,
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: colors_pattern[idx].strokeColor,
        pointBackgroundColor: "#fff",
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: colors_pattern[idx].strokeColor,
        pointHoverBorderColor: colors_pattern[idx].pointHighlightStroke,
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        spanGaps: true
    }, owner);
};

source.rainfall.chartObj.setDataset = function(features) {
    var labels = [],
        barData = [],
        lineData = [],
        timeBarBefore,
        maxAccDayBefore,
        accBarBefore;

    features.forEach(function(item, idx) {
        var prop = item.properties;
        var currentTime = moment(prop.telestation_datetime).utc();
        labels.push(currentTime);
        barData.push(prop.rainfall_1h_bar);
        if (parseInt(currentTime.format('YYYY')) >= 2017) lineData.push(prop.rainfall_acc);
    });
    return {
        labels: labels,
        datasets: [source.rainfall.chartObj.getLineStyle({
            label: 'ฝนรายชั่วโมง',
            data: barData,
            borderWidth: 1
        }, 0), source.rainfall.chartObj.getLineStyle({
            label: 'ฝนสะสมทั้งหมด',
            type: 'line',
            data: lineData,
            borderWidth: 1
        }, 1)]
    };
}