source.weather.getDay = function (day) {
    return moment(day, ['DD/MM/YYYY']).format('DD MMMM YYYY');
}

//--------------------------------------------------------------
// tab modal
//--------------------------------------------------------------

source.weather.tab_table = function () {
    $('#span_date_start_table').text(source.weather.getDay($('#table_form_date_start').val()));
    $('#span_date_end_table').text(source.weather.getDay($('#table_form_date_end').val()));

    source.weather.add_tab_table();
}

source.weather.tab_graph = function () {
    $('#span_date_start_graph').text(source.weather.getDay($('#graph_form_date_start').val()));
    $('#span_date_end_graph').text(source.weather.getDay($('#graph_form_date_end').val()));

    source.weather.add_tab_graph();
}

//--------------------------------------------------------------

$(function () {
    source.weather.chkFirstTabTable = 0; // first
    source.weather.chkFirstTabGraph = 0; // first

    // add date current
    var today = moment().format('DD/MM/') + (parseInt(moment().format('YYYY')) + 543);

    $("#table_form_date_start").val(today).text(today);
    $("#table_form_date_end").val(today).text(today);

    $("#graph_form_date_start").val(today).text(today);
    $("#graph_form_date_end").val(today).text(today);

    //----------------------------------------------------------------------------	

    $('.input_date').datepicker({
        language: "th-th",
        autoclose: true,
    });

    //----------------------------------------------------------------------------

    $("a[href='#tab_basic_weather']").click(function (e) {
        e.preventDefault();
        $('#modal-weather').removeClass('modal-lg');
    }); // tab table

    //----------------------------------------------------------------------------	

    $("a[href='#tab_table_weather']").click(function (e) {
        e.preventDefault();
        $('#modal-weather').removeClass('modal-lg').addClass('modal-lg');

        if (source.weather.chkFirstTabTable == 0) {
            // console.info('tab_table_first: ' + source.weather.chkFirstTabTable);
            source.weather.chkFirstTabTable = 1;
            source.weather.tab_table();
        }
    }); // tab table

    //----------------------------------------------------------------------------	

    $("a[href='#tab_graph_weather']").click(function (e) {
        e.preventDefault();
        $('#modal-weather').removeClass('modal-lg').addClass('modal-lg');

        if (source.weather.chkFirstTabGraph == 0) {
            // console.info('tab_graph_first: ' + source.weather.chkFirstTabGraph);
            source.weather.chkFirstTabGraph = 1;
            source.weather.tab_graph();
        }
    }); // tab graph

    //----------------------------------------------------------------------------	

    $("#btn_search_table").click(function (e) {
        e.preventDefault();

        var datestart = $("#table_form_date_start").val();
        var dateend = $("#table_form_date_end").val();
        if (datestart > dateend) {
            alert('เลือกวันที่ไม่ถูกต้อง');
            return false;
        }

        source.weather.tab_table();
    }); // button search table

    //----------------------------------------------------------------------------	

    $("#btn_search_graph").click(function (e) {
        e.preventDefault();

        var datestart = $("#graph_form_date_start").val();
        var dateend = $("#graph_form_date_end").val();
        if (datestart > dateend) {
            alert('เลือกวันที่ไม่ถูกต้อง');
            return false;
        }

        source.weather.tab_graph();
    }); // button search graph
}); //ready

var modal_weather = document.getElementById('weatherModal');
window.onclick = function (event) {
    if (event.target == modal_weather) {
        modal_weather.style.display = "none";
        $('.modal-backdrop').remove();
    }
}