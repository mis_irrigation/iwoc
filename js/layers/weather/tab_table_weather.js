// function add_tab_table () {
source.weather.add_tab_table = function () {
    block('#table_weather');

    // source.weather.cstat = '48583';
    // $('#table_form_date_start').val('2017-05-01');
    // $("#table_form_date_end").val('2017-05-01');

    source.weather.para = "cstat=" + $("#cstat").val(); 
    source.weather.para += "&datestart=" + $('#table_form_date_start').val();
    source.weather.para += "&dateend=" + $("#table_form_date_end").val();

    $.getJSON(source.weather.url + "table/?" + source.weather.para,
            function (data) {
                // console.log(source.weather.cstat, data);
                var date_current = moment().format('DD/MM/') + (parseInt(moment().format("YYYY")) + 543);
                var time_current = moment().format("HH:mm:ss");
                var htmltable = "";
                htmltable += '<table class="table table-bordered">';
                htmltable += '<thead><tr>';
                htmltable += '<th class="center" style="width: 102px;">วันที่</th>';
                htmltable += '<th class="center"">เวลา</th>';
                htmltable += '<th class="center">อุณหภูมิบาโรมิเตอร์ (celcius)</th>';
                htmltable += '<th class="center">ความกดอากาศที่สถานี (mb)</th>';
                htmltable += '<th class="center">ความกดอากาศที่ระดับน้ำทะเล (mb)</th>';
                htmltable += '<th class="center">อุณหภูมิจุดน้ำค้าง (celcius)</th>';
                htmltable += '<th class="center">ความชื้นสัมพัทธ์ (%)</th>';
                htmltable += '<th class="center">ความดันไอ (mb)</th>';
                htmltable += '<th class="center">ทัศนวิสัยทางบก (km)</th>';
                htmltable += '<th class="center">ทิศทางลม (degree)</th>';
                htmltable += '<th class="center">ความเร็วลม (km/h)</th>';
                htmltable += '</tr></thead><tbody>';
                if (data != null) {
                    $(data).each(function (index, value) {
                        // console.log(value);
                        if (index >= $(data).length - 4) {
                            if (index == $(data).length - 4) htmltable += '</tbody><tfoot><tr>'; 
                            else htmltable += '<tr>';
                            htmltable += '<td class="center" colspan="2">' + value.date + '</td>';
                            htmltable += '<td class="right">' + (value.barometer_temperature || '') + '</td>';
                            htmltable += '<td class="right">' + (value.station_pressure || '') + '</td>';
                            htmltable += '<td class="right">' + (value.mean_sea_level_pressure || '') + '</td>';
                            htmltable += '<td class="right">' + (value.dew_point || '') + '</td>';
                            htmltable += '<td class="right">' + (value.relative_Humidity || '') + '</td>';
                            htmltable += '<td class="right">' + (value.vapor_pressure || '') + '</td>';
                            htmltable += '<td class="right">' + (value.land_visibility || '') + '</td>';
                            htmltable += '<td class="right">' + (value.wind_direction || '') + '</td>';
                            htmltable += '<td class="right">' + (value.wind_speed || '') + '</td>';
                            htmltable += '</tr>';
                        } else {
                            htmltable += '<tr>';
                            htmltable += '<td class="center">' + moment(value.date.date, ['YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD hh:mm:ss']).format('DD MMM') + ' ' + (parseInt(moment(value.date.date, ['YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD hh:mm:ss']).format('YYYY')) + 543) + '</td>';
                            htmltable += '<td class="center">' + moment(value.date.date, ['YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD hh:mm:ss']).format('HH:mm:ss') + '</td>';
                            htmltable += '<td class="right">' + (value.barometer_temperature || '') + '</td>';
                            htmltable += '<td class="right">' + (value.station_pressure || '') + '</td>';
                            htmltable += '<td class="right">' + (value.mean_sea_level_pressure || '') + '</td>';
                            htmltable += '<td class="right">' + (value.dew_point || '') + '</td>';
                            htmltable += '<td class="right">' + (value.relative_Humidity || '') + '</td>';
                            htmltable += '<td class="right">' + (value.vapor_pressure || '') + '</td>';
                            htmltable += '<td class="right">' + (value.land_visibility || '') + '</td>';
                            htmltable += '<td class="right">' + (value.wind_direction || '') + '</td>';
                            htmltable += '<td class="right">' + (value.wind_speed || '') + '</td>';
                            htmltable += '</tr>';
                        }
                    });
                    htmltable += '</tfoot></table>';
                } else {
                    htmltable += '<tr>';
                    htmltable += '<td colspan="12" align="center">ไม่พบข้อมูล</td>';
                    htmltable += '</tr>';
                    htmltable += '</tbody></table>';
                }
                $('#table_weather').html(htmltable);
            })
        .always(function () {
            unblock('#table_weather');
        });
} // tab table thachin