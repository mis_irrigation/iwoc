//////////////////////////////////////////////////////////
// ajax load data (chart)
//////////////////////////////////////////////////////////

source.weather.add_tab_graph = function () {
    block('#graph_weather');

    // source.weather.cstat = '48583';
    // $('#table_form_date_start').val('2017-05-01');
    // $("#table_form_date_end").val('2017-05-01');

    source.weather.para = "cstat=" + $("#cstat").val();
    source.weather.para += "&datestart=" + $('#graph_form_date_start').val();
    source.weather.para += "&dateend=" + $("#graph_form_date_end").val();

    $.getJSON(
            source.weather.url + "graph/?" + source.weather.para,
            function (data) {
                // console.log(data);
                var date_station, date_current;
                source.weather.date = [];

                $(data).each(function (id, item) {
                    $(item.datetime).each(function (id, item) {
                        // console.log(item, new Date(moment(item, ['DD/MM/YYYY HH:mm:ss']).format('MM-DD-YYYY HH:mm:ss')).getTime());
                        var datetime = (parseInt(moment(item.date, ['YYYY-MM-DD HH:mm:ss']).format('YYYY')) + 543) + moment(item.date, ['YYYY-MM-DD HH:mm:ss']).format('-MM-DD HH:mm:ss');
                        source.weather.date[id] = moment(datetime).format();
                    });
 
                    source.weather.gen_graph_barometer_temperature(item.barometer_temperature);
                    source.weather.gen_graph_station_pressure(item.station_pressure);
                    source.weather.gen_graph_mean_sea_level_pressure(item.mean_sea_level_pressure);
                    source.weather.gen_graph_dew_point(item.dew_point);
                    source.weather.gen_graph_relative_Humidity(item.relative_humidity);
                    source.weather.gen_graph_vapor_pressure(item.vapor_pressure);
                    source.weather.gen_graph_land_visibility(item.land_visibility);
                    source.weather.gen_graph_wind_direction(item.wind_direction);
                    source.weather.gen_graph_wind_speed(item.wind_speed);
                });
            })
        .always(function () {
            unblock('#graph_weather');
        });
} // end function add_tab_graph
// =======================================================

//////////////////////////////////////////////////////////
// get options and get data (chart)
//////////////////////////////////////////////////////////
// function getOptions(title_name, y_name) {
source.weather.getOptions = function (title_name, y_name, max_data) {
    return {
        hover: {
            // Overrides the global setting
            mode: 'x-axis'
        },
        legend: {
            display: false,
        },
        tooltips: {
            intersect: false,
            mode: 'label',
            callbacks: {
                label: function (tooltipItems, data) {
                    return data.datasets[tooltipItems.datasetIndex].label + ': ' +
                        tooltipItems.yLabel;
                }
            }
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'เวลา'
                },
                type: 'time',
                time: {
                    unit: 'day',
                    displayFormats: {
                        'millisecond': 'D MMM YYYY',
                        'second': 'D MMM YYYY',
                        'minute': 'D MMM YYYY',
                        'hour': 'D MMM YYYY',
                        'day': 'D MMM YYYY',
                        'week': 'D MMM YYYY',
                        'month': 'D MMM YYYY',
                        'quarter': 'D MMM YYYY',
                        'year': 'D MMM YYYY',
                    },
                    tooltipFormat: 'วันที่ D MMM YYYY เวลา HH.mm น.'
                },
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: y_name
                },
                ticks: {
                    max: max_data * 2,
                    // min: 0,
                    // stepSize: step_data
                }
            }]
        },
        title: {
            display: true,
            fontSize: 20,
            text: title_name
        },
        responsive: true,

        // pan: {
        //     enabled: true,
        //     mode: 'x'
        // },
        // zoom: {
        //     enabled: true,
        //     mode: 'x'
        // }
    }
}

// function getData(labeltitle, labelvalue, datavalue) {
source.weather.getData = function (labeltitle, labelvalue, datavalue) {
    return {
        labels: labelvalue, // labelvalue
        datasets: [{
            label: labeltitle,
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: datavalue,
            spanGaps: true,
        }]
    };
}

//////////////////////////////////////////////////////////

// =======================================================

//////////////////////////////////////////////////////////
// gen graph barometer temperature
//////////////////////////////////////////////////////////
source.weather.gen_graph_barometer_temperature = function (data) {
    var ctx = document.getElementById("Chart_barometer_temperature");
    if (source.weather.mychart_barometer_temperature != null) {
        source.weather.mychart_barometer_temperature.destroy();
    }

    source.weather.mychart_barometer_temperature = new Chart(ctx, {
        type: 'line',
        data: source.weather.getData('อุณหภูมิบาโรมิเตอร์ ', source.weather.date, data),
        options: source.weather.getOptions('อุณหภูมิบาโรมิเตอร์ ', 'celcius', data.max())
    });
}
//////////////////////////////////////////////////////////
// gen graph station pressure
//////////////////////////////////////////////////////////
source.weather.gen_graph_station_pressure = function (data) {
    var ctx = document.getElementById("Chart_station_pressure");
    if (source.weather.mychart_station_pressure != null) {
        source.weather.mychart_station_pressure.destroy();
    }

    source.weather.mychart_station_pressure = new Chart(ctx, {
        type: 'line',
        data: source.weather.getData('ความกดอากาศที่สถานี', source.weather.date, data),
        options: source.weather.getOptions('ความกดอากาศที่สถานี', 'mb', data.max())
    });
}
//////////////////////////////////////////////////////////
// gen graph mean sea level pressure
//////////////////////////////////////////////////////////
source.weather.gen_graph_mean_sea_level_pressure = function (data) {
    var ctx = document.getElementById("Chart_mean_sea_level_pressure");
    if (source.weather.mychart_mean_sea_level_pressure != null) {
        source.weather.mychart_mean_sea_level_pressure.destroy();
    }

    source.weather.mychart_mean_sea_level_pressure = new Chart(ctx, {
        type: 'line',
        data: source.weather.getData('ความกดอากาศที่ระดับน้ำทะเล', source.weather.date, data),
        options: source.weather.getOptions('ความกดอากาศที่ระดับน้ำทะเล', 'mb', data.max())
    });
}
//////////////////////////////////////////////////////////
// gen graph dew point
//////////////////////////////////////////////////////////
source.weather.gen_graph_dew_point = function (data) {
    var ctx = document.getElementById("Chart_dew_point");
    if (source.weather.mychart_dew_point != null) {
        source.weather.mychart_dew_point.destroy();
    }

    source.weather.mychart_dew_point = new Chart(ctx, {
        type: 'line',
        data: source.weather.getData('อุณหภูมิจุดน้ำค้าง', source.weather.date, data),
        options: source.weather.getOptions('อุณหภูมิจุดน้ำค้าง', 'celcius', data.max())
    });
}
//////////////////////////////////////////////////////////
// gen graph relative Humidity
//////////////////////////////////////////////////////////
source.weather.gen_graph_relative_Humidity = function (data) {
    var ctx = document.getElementById("Chart_relative_Humidity");
    if (source.weather.mychart_relative_Humidity != null) {
        source.weather.mychart_relative_Humidity.destroy();
    }

    source.weather.mychart_relative_Humidity = new Chart(ctx, {
        type: 'line',
        data: source.weather.getData('ความชื้นสัมพัทธ์', source.weather.date, data),
        options: source.weather.getOptions('ความชื้นสัมพัทธ์', '%', data.max() || 0)
    });
}
//////////////////////////////////////////////////////////
// gen graph vapor pressure
//////////////////////////////////////////////////////////
source.weather.gen_graph_vapor_pressure = function (data) {
    var ctx = document.getElementById("Chart_vapor_pressure");
    if (source.weather.mychart_vapor_pressure != null) {
        source.weather.mychart_vapor_pressure.destroy();
    }

    source.weather.mychart_vapor_pressure = new Chart(ctx, {
        type: 'line',
        data: source.weather.getData('ความดันไอ', source.weather.date, data),
        options: source.weather.getOptions('ความดันไอ', 'mb', data.max())
    });
}
//////////////////////////////////////////////////////////
// gen graph land visibility
//////////////////////////////////////////////////////////
source.weather.gen_graph_land_visibility = function (data) {
    var ctx = document.getElementById("Chart_land_visibility");
    if (source.weather.mychart_land_visibility != null) {
        source.weather.mychart_land_visibility.destroy();
    }

    source.weather.mychart_land_visibility = new Chart(ctx, {
        type: 'line',
        data: source.weather.getData('ทัศนวิสัยทางบก', source.weather.date, data),
        options: source.weather.getOptions('ทัศนวิสัยทางบก', 'km', data.max())
    });
}
//////////////////////////////////////////////////////////
// gen graph wind direction
//////////////////////////////////////////////////////////
source.weather.gen_graph_wind_direction = function (data) {
    var ctx = document.getElementById("Chart_wind_direction");
    if (source.weather.mychart_wind_direction != null) {
        source.weather.mychart_wind_direction.destroy();
    }

    source.weather.mychart_wind_direction = new Chart(ctx, {
        type: 'line',
        data: source.weather.getData('ทิศทางลม', source.weather.date, data),
        options: source.weather.getOptions('ทิศทางลม', 'degree', data.max())
    });
}
//////////////////////////////////////////////////////////
// gen graph wind speed
//////////////////////////////////////////////////////////
// function gen_graph_velocity(data_velocity) {
source.weather.gen_graph_wind_speed = function (data) {
    var ctx = document.getElementById("Chart_wind_speed");
    if (source.weather.mychart_wind_speed != null) {
        source.weather.mychart_wind_speed.destroy();
    }

    source.weather.mychart_wind_speed = new Chart(ctx, {
        type: 'line',
        data: source.weather.getData('ความเร็วลม', source.weather.date, data),
        options: source.weather.getOptions('ความเร็วลม', 'km/h', data.max())
    });
}
// =======================================================

Array.prototype.max = function () {
    return Math.max.apply(null, this);
};