$(document).ready(function () {
    source.weather3Hours.legend = L.control({
        position: 'bottomright'
    });

    source.weather3Hours.legend.onAdd = function (map) {
        var div = L.DomUtil.create('div', 'info legend'),
            grades = [40, 30, 20, 10, 0],
            gradesName = ['40 +', '30 - 39', '20 - 29', '10 - 19', '0 - 9'],
            // from, to,
            labels = [];
        // labels.push('<span>อุณหภูมิ(องศาเซลเซียส)</span>');
        for (var i = 0; i < grades.length; i++) {
            labels.push('<img src="images/weather/' + getMarkerWeather(grades[i] + 1) + '" style="width: 17px; height: 25px; margin-top: 5px;">' + ' ' + gradesName[i]);
        }
        div.innerHTML = '<span>อุณหภูมิ(องศาเซลเซียส)</span><div style="margin-top: 10px; margin-left: 35px;">' + labels.join('<br>') + '</div>';
        return div;
    };

    source.weather3Hours.layer = new L.LayerGroup();
    source.weather3Hours.mapstorm = new L.LayerGroup();
    source.weather3Hours.mapwindspeedrain = new L.LayerGroup();
});

source.weather = {
    // url data
    'url': 'http://hydrologydb.rid.go.th/hydromet/code/weatherAPI/weather3hoursV1/',
    // station id
    'cstat': null,
    // graph data
    'mychart_do': null,
    'mychart_conductivity': null,
    'mychart_ph': null,
    'mychart_ph': null,
    'mychart_temp': null,
    'mychart_salinity': null,
    'mychart_waterlevel': null,
    'mychart_flow': null,
    'mychart_tds': null,
    'mychart_velocity': null,
    'mychart_length': null,
    'date': null
}

/* color Temperature */
function getMarkerWeather(value) {
    if (parseFloat(value) > 39) {
        return 'weather-05.gif';
    } else if (parseFloat(value) > 29) {
        return 'weather-04.gif';
    } else if (parseFloat(value) > 19) {
        return 'weather-03.gif';
    } else if (parseFloat(value) > 9) {
        return 'weather-02.gif';
    } else {
        return 'weather-01.gif';
    }
}

// ====================================================
// weather

function getweather3HoursLayer(callback) {
    if (source.weather3Hours.geoJson == undefined) {
        block('#map');
        // $.getJSON("js/layers/weather/weatherAPI/weather3hoursV1/",
        $.getJSON(source.weather.url,
                function (data) {
                    if (data.features != null) {
                        source.weather3Hours.geoJson = L.geoJSON(data, {

                            style: function (feature) {
                                return feature.properties && feature.properties.style;
                            },

                            onEachFeature: function (feature, layer) {
                                if (feature.properties) {
                                    layer.on({
                                        click: function (e) {
                                            $.get("js/layers/weather/modalV1.html",
                                                function (data) {
                                                    $('#iwocModal').html(data);
                                                    $("#featuretab-title").html('ภูมิอากาศ <i class="glyphicon glyphicon-menu-right"></i> ' + feature.properties.station_name[0]);
                                                    $("#cstat").val(feature.id);
                                                    load_data_weather(feature.properties);

                                                    $("#iwocModal").modal("show");
                                                }
                                            );
                                        }
                                    });
                                }
                            },

                            pointToLayer: function (feature, latlng) {
                                var smallIcon = L.icon({
                                    iconSize: [17, 25],
                                    iconAnchor: [13, 27],
                                    popupAnchor: [1, -24],
                                    iconUrl: 'images/weather/' + getMarkerWeather(feature.properties.barometer_temperature[0])
                                });
                                return L.marker(latlng, {
                                    icon: smallIcon
                                });
                            }
                        }).bindTooltip(function (e) {
                            return '' + e.feature.properties.station_name[0];
                        }, {
                            direction: 'top',
                            offset: L.point(0, -20)
                        }).addTo(source.weather3Hours.layer);
                    } else {
                        alert("กำลังอัพเดตข้อมูลใหม่ ทุก 3 ชม. (1:00, 4:00, 7:00, 10:00, 13:00, 16:00, 19:00, 22:00)");
                    }
                })
            .done(function () {
                callback($(source.weather3Hours.geoJson).length);
            })
            .fail(function () {
                // alert('weather3Hours request failed! ');
                alert('weather3Hours request failed!');
            })
            .always(function () {
                unblock('#map');
            });
    } else {
        callback($(source.weather3Hours.geoJson).length);
    }
}

function load_data_weather(properties) {
    // console.log(properties);
    $("#date_weather").text(moment(properties.date.date, ['YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD hh:mm:ss']).format('DD MMMM') + ' ' + (parseInt(moment(properties.date.date, ['YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD hh:mm:ss']).format('YYYY')) + 543));
    $("#time_weather").text(moment(properties.date.date, ['YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD hh:mm:ss']).format('HH.mm น.'));
    $("#stn_name_weather").text(properties.station_name[0]);
    $("#province_weather").text(properties.province);
    $("#barometer_temperature_weather").text(properties.barometer_temperature[0]);
    $("#station_pressure_weather").text(properties.station_pressure[0]);
    $("#mean_sea_level_pressure_weather").text(properties.mean_sea_level_pressure[0]);
    $("#dew_point_weather").text(properties.dew_point[0]);
    $("#relative_Humidity_weather").text(properties.relative_Humidity[0]);
    $("#vapor_pressure_weather").text(properties.vapor_pressure[0]);
    $("#land_visibility_weather").text(properties.land_visibility[0]);
    $("#wind_direction_weather").text(properties.wind_direction[0]);
    $("#wind_speed_weather").text(properties.wind_speed[0]);
}