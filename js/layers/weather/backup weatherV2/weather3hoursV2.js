$(document).ready(function () {
    source.weather3Hours.legend = L.control({
        position: 'bottomright'
    });

    source.weather3Hours.legend.onAdd = function (map) {
        var div = L.DomUtil.create('div', 'info legend'),
            grades = [40, 30, 20, 10, 0],
            gradesName = ['40 +', '30 - 39', '20 - 29', '10 - 19', '0 - 9'],
            // from, to,
            labels = [];
        // labels.push('<span>อุณหภูมิ(องศาเซลเซียส)</span>');
        for (var i = 0; i < grades.length; i++) {
            labels.push('<img src="images/weather/' + getMarkerWeather(grades[i] + 1) + '" style="width: 17px; height: 25px; margin-top: 5px;">' + ' ' + gradesName[i]);
        }
        div.innerHTML = '<span>อุณหภูมิ(องศาเซลเซียส)</span><div style="margin-top: 10px; margin-left: 35px;">' + labels.join('<br>') + '</div>';
        return div;
    };

    source.weather3Hours.layer = new L.LayerGroup();
    source.weather3Hours.mapstorm = new L.LayerGroup();
    source.weather3Hours.mapwindspeedrain  = new L.LayerGroup();
});

/* color Temperature */
function getMarkerWeather(value) {
    if (parseFloat(value) > 39) {
        return 'weather-05.gif';
    } else if (parseFloat(value) > 29) {
        return 'weather-04.gif';
    } else if (parseFloat(value) > 19) {
        return 'weather-03.gif';
    } else if (parseFloat(value) > 9) {
        return 'weather-02.gif';
    } else {
        return 'weather-01.gif';
    }
}

// ====================================================
// weather

function getweather3HoursLayer(callback) {
    if (source.weather3Hours.geoJson == undefined) {
        block('#map');
        // $.getJSON("js/layers/weather/weatherAPI/getWeather3Hours.php",
        $.getJSON("http://hydrologydb.rid.go.th/hydromet/code/weatherAPI/weather3hoursV2/",
                function (data) {
                    if (data.features != null) {
                        source.weather3Hours.geoJson = L.geoJSON(data, {

                            style: function (feature) {
                                return feature.properties && feature.properties.style;
                            },

                            onEachFeature: function (feature, layer) {
                                if (feature.properties) {
                                    layer.on({
                                        click: function (e) {
                                            $.get("js/layers/weather/modalV2.html",
                                                function (data) {
                                                    $('#iwocModal').html(data);
                                                    $("#featuretab-title").html('ภูมิอากาศ <i class="glyphicon glyphicon-menu-right"></i> ' + feature.properties.station_name[0]);

                                                    load_data_weather(feature.properties);

                                                    $("#iwocModal").modal("show");
                                                }
                                            );
                                        }
                                    });
                                }
                            },

                            pointToLayer: function (feature, latlng) {
                                var smallIcon = L.icon({
                                    iconSize: [17, 25],
                                    iconAnchor: [13, 27],
                                    popupAnchor: [1, -24],
                                    iconUrl: 'images/weather/' + getMarkerWeather(feature.properties.air_temperature[0])
                                });
                                return L.marker(latlng, {
                                    icon: smallIcon
                                });
                            }
                        }).bindTooltip(function (e) {
                            return '' + e.feature.properties.station_name[0];
                        }, {
                            direction: 'top',
                            offset: L.point(0, -20)
                        }).addTo(source.weather3Hours.layer);
                    } else {
                        alert("กำลังอัพเดตข้อมูลใหม่ ทุก 3 ชม. (1:00, 4:00, 7:00, 10:00, 13:00, 16:00, 19:00, 22:00)");
                    }
                })
            .done(function () {
                callback($(source.weather3Hours.geoJson).length);
            })
            .fail(function () {
                // alert('weather3Hours request failed! ');
                alert('weather3Hours request failed!');
            })
            .always(function () {
                unblock('#map');
            });
    } else {
        callback($(source.weather3Hours.geoJson).length);
    }
}

function getDateTime_weather(datetime) {
    var month = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฏาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม']
    var split = datetime.split(" ");
    var dmy = split[0].split("/");
    return 'ข้อมูลเมื่อวันที่ ' + dmy[1] + ' ' + month[parseInt(dmy[0]) - 1] + ' ' + (parseInt(dmy[2]) + 543).toString() + ' เวลา ' + split[1];
}

function load_data_weather(properties) {
    var value = '';
    value += '<div class="row" style="margin-top: 10px;"><div class="col-md-12"><div class="table-responsive">';
    value += '<table class="table table-striped table-bordered table-condensed">';
    value += '<tr><th colspan="2" style="text-align: center;">' + getDateTime_weather(properties.date) + '</th></tr>';
    value += '<tr><th style="text-align: center; width: 55%;">รายการ</th><th style="text-align: center;">ค่าที่ได้</th></tr>';
    value += '<tr><th>สถานี</th><td>' + properties.station_name[0] + '</td></tr>';
    value += '<tr><th>จังหวัด</th><td>' + properties.province + '</td></tr>';
    value += '<tr><th>ความกดอากาศที่ระดับน้ำทะเล (' + properties.mean_sea_level_pressure[1] + ')</th><td>' + (properties.mean_sea_level_pressure[0] || '-') + '</td></tr>';
    value += '<tr><th>อุณหภูมิอากาศ (' + properties.air_temperature[1] + ')</th><td>' + (properties.air_temperature[0] || '-') + '</td></tr>';
    value += '<tr><th>อุณหภูมิจุดน้ำค้าง (' + properties.dew_point[1] + ')</th><td>' + (properties.dew_point[0] || '-') + '</td></tr>';
    value += '<tr><th>ความดันไอ (' + properties.vapor_pressure[1] + ')</th><td>' + (properties.vapor_pressure[0] || '-') + '</td></tr>';
    value += '<tr><th>ทัศนวิสัยทางบก (' + properties.land_visibility[1] + ')</th><td>' + (properties.land_visibility[0] || '-') + '</td></tr>';
    value += '<tr><th>ความชื้นสัมพัทธ์ (' + properties.relative_Humidity[1] + ')</th><td>' + (properties.relative_Humidity[0] || '-') + '</td></tr>';
    value += '<tr><th>ทิศทางลม (' + properties.wind_direction[1] + ')</th><td>' + (properties.wind_direction[0] || '-') + '</td></tr>';
    value += '<tr><th>ความเร็วลม (' + properties.wind_speed[1] + ')</th><td>' + (properties.wind_speed[0] || '-') + '</td></tr>';
    // value += '<tr><th>ปริมาณฝน ทุกๆ 3 ชม. (' + properties.rainfall[1] + ')</th><td>' + properties.rainfall[0] + '</td></tr>';
    // value += '<tr><th>ปริมาณฝน 24 ชม. (' + properties.rainfall_24_hr[1] + ')</th><td>' + properties.rainfall_24_hr[0] + '</td></tr>';
    value += '</table></div></div></div>';
 
    $('#tab_basic_weather').html(value);
}
