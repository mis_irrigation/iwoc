$(document).ready(function () {
    source.irrarea.layer = new L.LayerGroup();

});//ready


function getIrrareaData(callback) {
    if (source.irrarea.geoJson == undefined) {
        block('#map');
        L.tileLayer.betterWms('http://gis.rid.go.th/geoserver/riddata/wms', 
        {
            layers: 'riddata:irrarea_all',
            transparent: true,
            format: 'image/png'
        }).addTo(source.irrarea.layer);

        var ridcode = {'01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17'}
        for (ridcode in obj) {
			$.ajax({
					url: "http://gis.rid.go.th/geoserver/riddata/ows",
					jsonp: "callback",
					jsonpCallback: "parseResponse",
					dataType: "jsonp",
					data: {
						version: "1.0.0",
						request: "GetFeature",
						typeName: "riddata:irrarea_all",
						styles: "irrarea",  //style_from_geoserver
						//maxFeatures: "50",
						CQL_FILTER: "ridcode="+obj,
						service: "WFS",
						outputFormat: "text/javascript",
					},
	    				// Work with the response
	    			success: function( data ) {
	        			if (data !== null) {
	 					     					source.irrarea.geoJson = L.geoJSON(data, {
	                    							// style: function (feature) {
	                        			// 				return feature.properties && feature.properties.style; },

	                								onEachFeature: onEachirrareaFeature,
	       
								         			style: function (feature) { // Style option
											            return {
											                'weight': 0,
											                'color': 'transparent',
											                'fillColor': 'transparent'
											            }
	        										}
	 											}).addTo(source.irrarea.layer);
					   							unblock('#map');
	         		   }
	   				}
			});
		}
    } 
    else 
	    {
	        callback($(source.irrarea.geoJson).length);
	    }
}

function onEachirrareaFeature(feature, layer) {
	console.log(feature.properties.objectid);
    if (!feature.properties.objectid != true) {
 		let _objectid = feature.properties.objectid;
        layer.on({
            click: function (e) {
            	console.log( "Hi" );
                //
                //$("#iwocModal").modal("show");
                $.ajax({
					url: "http://gis.rid.go.th/geoserver/riddata/ows",
					jsonp: "callback",
					jsonpCallback: "parseResponse",
					dataType: "jsonp",
					data: {
						version: "1.0.0",
						request: "GetFeature",
						typeName: "riddata:irrarea_all",
						styles: "irrarea",  //style_from_geoserver
						CQL_FILTER: "objectid="+_objectid,
						service: "WFS",
						outputFormat: "text/javascript",
					},
	    				// Work with the response
	    			success: function( data ) {
	    				let info = data.features[0].properties;
	    				let content = "";
	    	
	    				content += '<div class="modal-dialog">';
    					content += '<div class="modal-content">';
	       					content += '<div class="modal-header">';
							content += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
							content += '<h4 class="modal-title" id="featuretab-title">'+info.projcode+'</h4>';
							content += '</div>';
	        				content += '<div class="modal-body" id="featuretab-info">';
		        				content += "<div>"+info.projcode+"</div>";
			    				content += "<div>"+info.projname+"</div>";
			    				content += "<div>"+info.owner+"</div>";
			    				content += "<div>"+info.amphoe+"</div>";
			    				content += "<div>"+info.tambon+"</div>";
			    				content += "<div>"+info.provname+"</div>";
			    				content += "<div>"+info.projarea+"</div>";
			    			content += '</div>';
			    		content += '</div>';
			    		content += '</div>';
	    				$('#iwocModal').html(content).modal("show");
	    			}
    			});  
            }

        });
        
    }
}