$(document).ready(function () {
    source.bigteleamr.legend = L.control({ position: 'bottomright' });
    source.bigteleamr.legend.onAdd = function (map) {
        var div = L.DomUtil.create('div', 'info legend'),
            grades = [0, 30, 50, 80],
            labels = [],
            from, to;
        labels.push('ระดับน้ำ(%)');
        for (var i = 0; i < grades.length; i++) {
            from = grades[i];
            to = grades[i + 1];
            labels.push('<i style="background:' + source.bigteleamr.getColor(from + 1) + '"></i> ' + from + (to ? '&ndash;' + to : '+'));
        }
        div.innerHTML = labels.join('<br>');
        return div;
    };
    source.bigteleamr.layer = new L.LayerGroup();

});

source.bigteleamr.greenIconUrl = "images/telemetry/green-sign.png";
source.bigteleamr.blueIconUrl = "images/telemetry/blue-sign.png";
source.bigteleamr.redIconUrl = "images/telemetry/red-sign.png";
source.bigteleamr.greyIconUrl = "images/telemetry/gray-sign.png";
source.bigteleamr.yellowIconUrl = "images/telemetry/yellow-sign.png";
source.bigteleamr.shadowIconUrl = "libraries/leaflet1.0.3/images/marker-shadow.png";


source.bigteleamr.greenIcon = new L.Icon({
    iconUrl: source.bigteleamr.greenIconUrl,
    shadowUrl: source.bigteleamr.shadowIconUrl,
    iconSize: [20.5, 20.5],
    iconAnchor: [20, 20.5],
    popupAnchor: [0.5, -17],
    shadowSize: [20.5, 20.5]
});
source.bigteleamr.blueIcon = new L.Icon({
    iconUrl: source.bigteleamr.blueIconUrl,
    shadowUrl: source.bigteleamr.shadowIconUrl,
    iconSize: [20.5, 20.5],
    iconAnchor: [20, 20.5],
    popupAnchor: [0.5, -17],
    shadowSize: [20.5, 20.5]
});
source.bigteleamr.redIcon = new L.Icon({
    iconUrl: source.bigteleamr.redIconUrl,
    shadowUrl: source.bigteleamr.shadowIconUrl,
    iconSize: [20.5, 20.5],
    iconAnchor: [20, 20.5],
    popupAnchor: [0.5, -17],
    shadowSize: [20.5, 20.5]
});
source.bigteleamr.greyIcon = new L.Icon({
    iconUrl: source.bigteleamr.greyIconUrl,
    shadowUrl: source.bigteleamr.shadowIconUrl,
    iconSize: [20.5, 20.5],
    iconAnchor: [20, 20.5],
    popupAnchor: [0.5, -17],
    shadowSize: [20.5, 20.5]
});
source.bigteleamr.yellowIcon = new L.Icon({
    iconUrl: source.bigteleamr.yellowIconUrl,
    shadowUrl: source.bigteleamr.shadowIconUrl,
    iconSize: [20.5, 20.5],
    iconAnchor: [20, 20.5],
    popupAnchor: [0.5, -17],
    shadowSize: [20.5, 20.5]
});

source.bigteleamr.violetIcon = new L.Icon({
    iconUrl: source.bigteleamr.violetIconUrl,
    shadowUrl: source.bigteleamr.shadowIconUrl,
    iconSize: [20.5, 20.5],
    iconAnchor: [20, 20.5],
    popupAnchor: [0.5, -17],
    shadowSize: [20.5, 20.5]
});


source.bigteleamr.getIcon = function (QTotal) {
    var icon = source.bigteleamr.greyIcon;
    if (QTotal <= 30)
        icon = source.bigteleamr.yellowIcon; //yellow
    else if (QTotal > 30 && QTotal <= 50)
        icon = source.bigteleamr.greenIcon; //green
    else if (QTotal > 50 && QTotal <= 80)
        icon = source.bigteleamr.blueIcon; //blue
    else if (QTotal > 80)
        icon = source.bigteleamr.redIcon; //red
    else
        icon = source.bigteleamr.greyIcon; // no data
    return icon
};


function getNum(val) {
    if (isNaN(val)) {
        return "-";
    }
    return val;
}

source.bigteleamr.getBigTeleAMRData = function (callback) {
    
    if (source.bigteleamr.geoJson == undefined) {
        block('#map');
     
        $.ajax({

            type: "GET",
            jsonp: 'jsonp_callback',
            url: "http://203.113.97.30/webservice/wscpygeojson.ashx",

            contentType: "application/json; charset=utf-8",
            dataType: "jsonp",

            success: function (msg) {
                ///////////////////////////////////////////

                var data = msg.Response;
                if (data !== null) {
                    source.bigteleamr.geoJson = L.geoJSON(data, {
                        style: function (feature) {
                            return feature.properties && feature.properties.style;
                        },
                        onEachFeature: source.bigteleamr.onEachFeature,
                    
                        //  onEachFeature: onEachBigTeleAMRFeature,
                        pointToLayer: function (feature, latlng) {
                            //return L.circleMarker(latlng, {
                            //    radius: 8,
                            //    fillColor: getColor(feature.properties.WLTotal),
                            //    color: "#000",
                            //    weight: 1,
                            //    opacity: 1,
                            //    fillOpacity: 0.8
                            //}); //.bindTooltip("My Label", {permanent: true, className: "my-label", offset: [0, 0] });

                            return L.marker(latlng, {

                                icon: source.bigteleamr.getIcon(feature.properties.WLTotal)  //getIcon(feature.properties.QTotal)

                            }).bindTooltip(function (e) {
                                return '' + e.feature.properties.StationCode;
                            }, { direction: 'top', offset: L.point(0, -20) })
                        }
                    }).addTo(source.bigteleamr.layer);
                }

            },
            complete: function () {
                // $('#loadingStation').hide();
                unblock('#map');
                callback($(source.bigteleamr.geoJson).length);

            },
            error: function (xhr, status, error) {
                unblock('#map');
                alert("An error has occurred during processing: " + error);
            }
        });
    }

    else {
        unblock('#map');
        callback($(source.bigteleamr.geoJson).length);
    }
}

source.bigteleamr.getColor = function (QTotal) {
    var fillColor = "#FAFAFA";
    if (QTotal <= 30)
        fillColor = "#FFFF00"; //yellow
    else if (QTotal > 30 && QTotal <= 50)
        fillColor = "#00C853"; //green
    else if (QTotal > 50 && QTotal <= 80)
        fillColor = "#1976D2"; //blue
    else if (QTotal > 80)
        fillColor = "#FF4081"; //red
    else
        fillColor = "#FAFAFA"; // no data
    return fillColor
}

source.bigteleamr.highlightFeature = function (e) {
    var layer = e.target;
    layer.setStyle({
        weight: 5,
        color: '#666',
        dashArray: '',
        fillOpacity: 0.8
    });
    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
    }
}

source.bigteleamr.resetHighlight = function (e) {
    source.bigteleamr.geoJson.resetStyle(e.target);
}

//source.bigteleamr.onEachFeature = function(feature, layer) {
source.bigteleamr.onEachFeature = function (feature, layer) {
  //function  onEachBigTeleAMRFeature  (feature, layer) {

 //   alert("hi");
    var popupContentWL = "";
    var popupContentRF = "";
    if (feature.properties && feature.properties.StationCode) {


        popupContentWL = tableStringBigTeleAMR_WLData(feature.id,
            feature.properties.StationCode,
            feature.properties.StationName,
            feature.properties.WARNING_UP,
            feature.properties.CRITICAL_UP,

            feature.properties.WL_UP,
            feature.properties.WL_UP_MAX,
            feature.properties.WL_UP_MSL,
            feature.properties.WL_UP_MSL_MAX,
              feature.properties.FLOW,
             feature.properties.LAST_UPDATE );
        popupContentRF = tableStringBigTeleAMR_RainData(feature.id,
            feature.properties.StationCode,
            feature.properties.StationName,
            feature.properties.RF_15,
            feature.properties.RF_7TO7,
            feature.properties.RF_7TONOW,
            feature.properties.LAST_UPDATE );

        layer.on({
            mouseover: source.bigteleamr.highlightFeature,
            mouseout: source.bigteleamr.resetHighlight,
            click: function (e) {

                source.bigteleamr.bigteleamrID = feature.id;

                $.get("js/layers/bigteleamr/modal.html",
                    function (data) {

                        $('#iwocModal').html(data);
                        $("#featureBigTeleAMR-title").html(feature.properties.StationCode + " " + feature.properties.StationName);
                        $("#featureBigTeleAMR-WL").html(popupContentWL);
                        $("#featureBigTeleAMR-R").html(popupContentRF);
                        $("#iwocModal").modal("show");
                    }
                );
            }
        });
        //source.bigteleamr.search = [];
        //source.bigteleamr.search.push({
        //    name: layer.feature.properties.DamName,
        //    qtotal: layer.feature.properties.QTotal,
        //    source: "BigDam",
        //    id: L.stamp(layer),
        //    lat: layer.getLatLng().lat,
        //    lng: layer.getLatLng().lng
        //});
    }
}


function tableStringBigTeleAMR_WLData(stationid, StationCode, StationName, WARNING_UP, CRITICAL_UP, WL_UP, WL_UP_MAX, WL_UP_MSL, WL_UP_MSL_MAX, FLOW, LAST_UPDATE) {
    var html = '<table class="table table-striped table-bordered table-condensed">' +
        '<tr><th th colspan="2"  class="text-center">เมื่อวันที่ ' + LAST_UPDATE + '</th></tr>' +
        '<tr><td> ระดับน้ำปัจจุบัน (ม.รทก.)</td><td>' + getNum(commafy(WL_UP_MSL, 2)) + '</td></tr>' +
        '<tr><td> ระดับน้ำสูงสุดวันนี้ (ม.รทก.)</td><td>' + getNum(commafy(WL_UP_MSL_MAX, 2)) + '</td></tr>' +
        '<tr><td> ระดับน้ำปัจจุบัน (ม.รสม.)</td><td>' + getNum(commafy(WL_UP, 2)) + '</td></tr>' +
        '<tr><td> ระดับน้ำสูงสุดวันนี้ (ม.รสม.)</td><td>' + getNum(commafy(WL_UP_MAX, 2)) + '</td></tr>' +

        '<tr><td> อัตราการไหล (Q) (ลบ.ม/วินาที)</td><td>' + getNum(commafy(FLOW, 2)) + ' </td></tr>' +
         '<tr><th  th colspan="2"  class="text-center">ข้อมูลพื้นฐาน</th></tr>' +
         '<tr><td> ระดับน้ำวิกฤติ (ม.รทก.)</td><td>' + getNum(commafy(CRITICAL_UP, 2)) + '</td></tr>' +
        '<tr><td> ระดับน้ำเตือนภัย (ม.รทก.)</td><td>' + getNum(commafy(WARNING_UP, 2)) + '</td></tr>' +
        '<table>'

    return html;
}

function tableStringBigTeleAMR_RainData(stationid, StationCode, StationName, RF_15, RF_7TO7, RF_7TONOW, LAST_UPDATE) {

    var html = '<table class="table table-striped table-bordered table-condensed">' +
        //'<tr><th colspan="2" class="text-center">วันที่ ' + LAST_UPDATE + '</th></tr>' +
        //'<tr><th class="text-center">รายงาน</th><th class="text-center">มม.</th></tr>' +

         '<tr><th th colspan="2"  class="text-center">เมื่อวันที่ ' + LAST_UPDATE + '</th></tr>' +
        '<tr><td>ฝนราย 15 นาที (มม.)</td><td>' + getNum(commafy(RF_15, 2)) + '</td></tr>' +
        '<tr><td>ฝนสะสม 24 ชม. (มม.)</td><td>' + getNum(commafy(RF_7TO7, 2)) + '</td></tr>' +
        '<tr><td>ฝนสะสม 7.00 น. – ปัจจุบัน (มม.)</td><td>' + getNum(commafy(RF_7TONOW, 2)) + '</td></tr>' +

        '<table>';

    return html;
}
