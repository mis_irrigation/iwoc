$(document).ready(function () {
    source.irrproject.layer = new L.LayerGroup();

});//ready


function getIrrprojectData(callback) {
    if (source.irrproject.geoJson == undefined) {
        block('#map');
        L.tileLayer.betterWms('http://gis.rid.go.th/geoserver/ridiwoc/wms', 
        {
            layers: 'ridiwoc:irrproject_all',
            transparent: true,
            format: 'image/png'
        }).addTo(source.irrproject.layer);

		$.ajax({
				url: "http://gis.rid.go.th/geoserver/ridiwoc/ows",
				jsonp: "callback",
				jsonpCallback: "parseResponse",
				dataType: "jsonp",
				data: {
					version: "1.0.0",
					request: "GetFeature",
					typeName: "ridiwoc:irrproject_all",
					styles: "irrproject",  //style_from_geoserver
					//maxFeatures: "50",
					service: "WFS",
					outputFormat: "text/javascript",
				},
    				// Work with the response
    			success: function( data ) {
        			if (data !== null) {
 					     					source.irrproject.geoJson = L.geoJSON(data, {
                    							style: function (feature) {
                        							return feature.properties && feature.properties.style; },

                								onEachFeature: onEachIrrprojectFeature,
       
							         			pointToLayer: function (feature, latlng) {
							                        var Marker = L.marker(latlng, {                          
							                            opacity: 0
							                        });
							                        return Marker;
							                	}
 											}).addTo(source.irrproject.layer);
				   							unblock('#map');
         		   }
   				}
		});
    } 
    else 
	    {
	        callback($(source.irrproject.geoJson).length);
	    }
}

function onEachIrrprojectFeature(feature, layer) {
	console.log(feature.properties.objectid);
    if (!feature.properties.objectid != true) {
 		let _objectid = feature.properties.objectid;
        layer.on({
            click: function (e) {
            	console.log( "Hi" );
                //
                //$("#iwocModal").modal("show");
                $.ajax({
					url: "http://gis.rid.go.th/geoserver/ridiwoc/ows",
					jsonp: "callback",
					jsonpCallback: "parseResponse",
					dataType: "jsonp",
					data: {
						version: "1.0.0",
						request: "GetFeature",
						typeName: "ridiwoc:irrproject_all",
						styles: "irrproject",  //style_from_geoserver
						CQL_FILTER: "objectid="+_objectid,
						service: "WFS",
						outputFormat: "text/javascript",
					},
	    				// Work with the response
	    			success: function( data ) {
	    				let info = data.features[0].properties;
	    				let content = "";
	    	
	    				content += '<div class="modal-dialog">';
    					content += '<div class="modal-content">';
	       					content += '<div class="modal-header">';
							content += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
							content += '<h4 class="modal-title" id="featuretab-title">'+info.projcode+'</h4>';
							content += '</div>';
	        				content += '<div class="modal-body" id="featuretab-info">';
		        				content += "<div>"+info.projcode+"</div>";
			    				content += "<div>"+info.projname+"</div>";
			    				content += "<div>"+info.owner+"</div>";
			    				content += "<div>"+info.amphoe+"</div>";
			    				content += "<div>"+info.tambon+"</div>";
			    				content += "<div>"+info.provname+"</div>";
			    				content += "<div>"+info.projarea+"</div>";
			    			content += '</div>';
			    		content += '</div>';
			    		content += '</div>';
	    				$('#iwocModal').html(content).modal("show");
	    			}
    			});  
            }

        });
        
    }
}