$(document).ready(function () {
    source.bangpakong.layer = new L.LayerGroup();
    source.thachin.layer = new L.LayerGroup();
    source.maeklong.layer = new L.LayerGroup();
    source.waterquality.mwa = new L.LayerGroup();
    source.waterquality.pcd1 = new L.LayerGroup();
    source.waterquality.pcd2 = new L.LayerGroup();
});

source.waterquality = {
    // url data
    'url': 'http://hydrologydb.rid.go.th/hydromet/code/waterqualityAPI/',
    // 'url': 'js/layers/waterquality/',
    // station id
    'cstat': null,
    // graph data
    'mychart_do': null,
    'mychart_conductivity': null,
    'mychart_ph': null,
    'mychart_ph': null,
    'mychart_temp': null,
    'mychart_salinity': null,
    'mychart_waterlevel': null,
    'mychart_flow': null,
    'mychart_tds': null,
    'mychart_velocity': null,
    'mychart_length': null,
    'date': null
}

source.waterquality.getbangpakongLayer = function (callback) {
    // function getbangpakongLayer(callback) {
    if (source.bangpakong.geoJson == undefined) {
        block('#map');
        $.getJSON(source.waterquality.url + "bangpakong/",
                function (data) {
                    if (data != null) {
                        source.waterquality.bangpakong = data;
                        source.bangpakong.geoJson = L.geoJSON(data, {

                            style: function (feature) {
                                return feature.properties && feature.properties.style;
                            },

                            onEachFeature: function (feature, layer) {

                                if (feature.properties) {
                                    layer.on({
                                        click: function (e) {
                                            $.get("js/layers/waterquality/modal.html",
                                                function (data) {
                                                    $('#iwocModal').html(data);
                                                    $("#featuretab-title").html('คุณภาพน้ำ <i class="glyphicon glyphicon-menu-right"></i> ' + feature.properties.stn_name);

                                                    source.waterquality.load_data_waterquality(feature);

                                                    $("#iwocModal").modal("show");
                                                }
                                            );
                                        }
                                    });
                                }
                            },

                            pointToLayer: function (feature, latlng) {
                                var smallIcon = L.icon({
                                    iconSize: [19, 27],
                                    iconAnchor: [13, 27],
                                    popupAnchor: [1, -24],
                                    iconUrl: 'images/waterquality/icon-waterquality.gif'
                                });
                                return L.marker(latlng, {
                                    icon: smallIcon
                                });
                            }
                        }).bindTooltip(function (e) {
                            return '' + e.feature.properties.stn_name;
                        }, {
                            direction: 'top',
                            offset: L.point(0, -20)
                        }).addTo(source.bangpakong.layer);
                    }
                })
            .done(function () {
                callback($(source.bangpakong.geoJson).length);
            })
            .fail(function () {
                alert('bangpakong request failed! ');
            })
            .always(function () {
                unblock('#map');
            });
    } else {
        callback($(source.bangpakong.geoJson).length);
    }
}

source.waterquality.getthachinLayer = function (callback) {
    // function getthachinLayer(callback) {
    if (source.thachin.geoJson == undefined) {
        block('#map');
        $.getJSON(source.waterquality.url + "thachin/",
                // $.getJSON("js/layers/waterquality/thachin/index.json",
                function (data) {
                    if (data != null) {
                        source.waterquality.thachin = data;
                        source.thachin.geoJson = L.geoJSON(data, {

                            style: function (feature) {
                                return feature.properties && feature.properties.style;
                            },

                            onEachFeature: function (feature, layer) {
                                if (feature.properties) {
                                    layer.on({
                                        click: function (e) {
                                            $.get("js/layers/waterquality/modal.html",
                                                function (data) {
                                                    $('#iwocModal').html(data);
                                                    $("#featuretab-title").html('คุณภาพน้ำ <i class="glyphicon glyphicon-menu-right"></i> ' + feature.properties.stn_name);

                                                    source.waterquality.load_data_waterquality(feature);

                                                    $("#iwocModal").modal("show");
                                                }
                                            );
                                        }
                                    });
                                }
                            },

                            pointToLayer: function (feature, latlng) {
                                var smallIcon = L.icon({
                                    iconSize: [19, 27],
                                    iconAnchor: [13, 27],
                                    popupAnchor: [1, -24],
                                    iconUrl: 'images/waterquality/icon-waterquality.gif'
                                });
                                return L.marker(latlng, {
                                    icon: smallIcon
                                });
                            }
                        }).bindTooltip(function (e) {
                            return '' + e.feature.properties.stn_name;
                        }, {
                            direction: 'top',
                            offset: L.point(0, -20)
                        }).addTo(source.thachin.layer);
                    }
                })
            .done(function () {
                callback($(source.thachin.geoJson).length);
            })
            .fail(function () {
                alert('thachin request failed! ');
            })
            .always(function () {
                unblock('#map');
            });
    } else {
        callback($(source.thachin.geoJson).length);
    }
}

source.waterquality.getmaeklongLayer = function (callback) {
    // function getmaeklongLayer(callback) {
    if (source.maeklong.geoJson == undefined) {
        block('#map');
        $.getJSON(source.waterquality.url + "maeklong/",
                function (data) {
                    if (data != null) {
                        source.waterquality.maeklong = data;
                        source.maeklong.geoJson = L.geoJSON(data, {

                            style: function (feature) {
                                return feature.properties && feature.properties.style;
                            },

                            onEachFeature: function (feature, layer) {
                                if (feature.properties) {
                                    layer.on({
                                        click: function (e) {
                                            $.get("js/layers/waterquality/modal.html",
                                                function (data) {
                                                    $('#iwocModal').html(data);
                                                    $("#featuretab-title").html('คุณภาพน้ำ <i class="glyphicon glyphicon-menu-right"></i> ' + feature.properties.stn_name);

                                                    source.waterquality.load_data_waterquality(feature);

                                                    $("#iwocModal").modal("show");
                                                }
                                            );
                                        }
                                    });
                                }
                            },

                            pointToLayer: function (feature, latlng) {
                                var smallIcon = L.icon({
                                    iconSize: [17, 25],
                                    iconAnchor: [13, 27],
                                    popupAnchor: [1, -24],
                                    iconUrl: 'images/waterquality/icon-waterquality.gif'
                                });
                                return L.marker(latlng, {
                                    icon: smallIcon
                                });
                            }
                        }).bindTooltip(function (e) {
                            return '' + e.feature.properties.stn_name;
                        }, {
                            direction: 'top',
                            offset: L.point(0, -20)
                        }).addTo(source.maeklong.layer);
                    }
                })
            .done(function () {
                callback($(source.maeklong.geoJson).length);
            })
            .fail(function () {
                alert('maeklong request failed! ');
            })
            .always(function () {
                unblock('#map');
            });
    } else {
        callback($(source.maeklong.geoJson).length);
    }
}

source.waterquality.load_data_waterquality = function (feature) {
    // function load_data_waterquality(feature) {
    $("#image_waterquality").attr("src", "images/waterquality/" +
        feature.properties.image);
    $("#date_waterquality").text(moment(feature.properties.date, [
        'DD/MM/YYYY HH:mm:ss', 'YYYY-MM-DD HH:mm:ss'
    ]).format('DD MMMM') + ' ' + (parseInt(moment(
        feature.properties.date, ['DD/MM/YYYY HH:mm:ss', 'YYYY-MM-DD HH:mm:ss']).format(
        'YYYY')) + 543));
    $("#time_waterquality").text(moment(feature.properties.date, [
        'DD/MM/YYYY HH:mm:ss', 'YYYY-MM-DD HH:mm:ss'
    ]).format('HH.mm น.'));
    $("#stn_name_waterquality").text(feature.properties.stn_name);
    $("#location_waterquality").text(feature.properties.location);
    $("#do_waterquality").text(feature.properties.do);
    $("#conductivity_waterquality").text(feature.properties.conductivity);
    $("#ph_waterquality").text(feature.properties.ph);
    $("#temp_waterquality").text(feature.properties.temp);
    $("#salinity_waterquality").text(feature.properties.salinity);
    $("#waterlevel_waterquality").text(feature.properties.waterlevel);
    $("#flow_waterquality").text(feature.properties.flow);
}