// function add_tab_table () {
source.waterquality.add_tab_table = function () {
    block('#table_waterquality');

    if (source.waterquality.tstat == "thachin") {
        source.waterquality.para = "sid=" + ($("#table_form_station_id").val() || source.waterquality.cstat); //source.waterquality.cstat;
        source.waterquality.para += "&sModel=" + $('#table_form_display').find(":selected").val();
        source.waterquality.para += "&dtBegin=" + $("#table_form_date_start").val();
        source.waterquality.para += "&dtFinal=" + $("#table_form_date_end").val();
    } else if (source.waterquality.tstat == "maeklong") {
        if ($('#table_form_display').find(":selected").val() == '0') {
            $('#table_form_display').find(":selected").val('1');
        } else if ($('#table_form_display').find(":selected").val() == '1') {
            $('#table_form_display').find(":selected").val('0');
        }

        source.waterquality.para = "sid=" + ($("#table_form_station_id").val() || source.waterquality.cstat); // source.waterquality.cstat;
        source.waterquality.para += "&sModel=" + $('#table_form_display').find(":selected").val();
        source.waterquality.para += "&dtBegin=" + $("#table_form_date_start").val(); // "2017-04-03";
        source.waterquality.para += "&dtFinal=" + $("#table_form_date_end").val(); // "2017-04-03";
    } else {
        if ($('#table_form_display').find(":selected").val() == '1') {
            $('#table_form_display').find(":selected").val('15');
        } else if ($('#table_form_display').find(":selected").val() == '0') {
            $('#table_form_display').find(":selected").val('60');
        }

        source.waterquality.para = "station=" + ($("#table_form_station_id").val() || source.waterquality.cstat); //source.waterquality.cstat;
        source.waterquality.para += "&displaylist=" + $('#table_form_display').find(":selected").val();
        source.waterquality.para += "&fromdate=" + $("#table_form_date_start").val();
        source.waterquality.para += "&todate=" + $("#table_form_date_end").val();
        source.waterquality.para += "&showtype=table";
    }

    $.getJSON(source.waterquality.url + source.waterquality.tstat + "/table/?" + source.waterquality.para,
            function (data) {
                // console.log(source.waterquality.tstat, source.waterquality.tstat == "bangpakong");
                var date_current = moment().format('DD/MM/') + (parseInt(moment().format("YYYY")) + 543);
                var time_current = moment().format("HH:mm:ss");
                var count_time;
                var htmltable = "";
                htmltable += '<table class="table table-bordered">';
                htmltable += '<thead><tr>';
                htmltable += '<th class="center" style="width: 102px;">วันที่</th>';
                htmltable += '<th class="center">เวลา</th>';
                htmltable += '<th class="center">ออกซิเจนในน้ำ (mg/l)</th>';
                htmltable += '<th class="center">ค่าการนำไฟฟ้า (µS/cm)</th>';
                htmltable += '<th class="center">กรด-ด่าง (pH)</th>';
                htmltable += '<th class="center">อุณหภูมิ (°C)</th>';
                htmltable += '<th class="center">ความเค็ม (g/l)</th>';
                htmltable += '<th class="center">สารละลาย (mg/l)</th>';
                htmltable += '<th class="center">ระดับน้ำ m.(MSL.)</th>';
                htmltable += '<th class="center">อัตราการไหล (m3/s)</th>';
                htmltable += '<th class="center">อัตราความเร็ว (m/s)</th>';
                if (source.waterquality.tstat == "bangpakong") htmltable += '<th>ระยะลำน้ำ (m)</th>';
                htmltable += '</tr></thead><tbody>';
                if (data != null && data.length > 5) {
                    $(data).each(function (id, item) {
                        count_time = item['time'];

                        if (id > $(data).length - 5) {
                            (id == $(data).length - 4) ? htmltable += '</tbody><tfoot><tr>': htmltable += '<tr>';
                            htmltable += '<td colspan="2" class="center">' + item['date'] + '</td>';
                            htmltable += '<td class="right">' + (item['do'] || '') + '</td>';
                            htmltable += '<td class="right">' + (item['conductivity'] || '') + '</td>';
                            htmltable += '<td class="right">' + (item['ph'] || '') + '</td>';
                            htmltable += '<td class="right">' + (item['temp'] || '') + '</td>';
                            htmltable += '<td class="right">' + (item['salinity'] || '') + '</td>';
                            htmltable += '<td class="right">' + (item['tds'] || '') + '</td>';
                            htmltable += '<td class="right">' + (item['waterlevel'] || '') + '</td>';
                            htmltable += '<td class="right">' + (item['flow'] || '') + '</td>';
                            htmltable += '<td class="right">' + (item['velocity'] || '') + '</td>';
                            if (source.waterquality.tstat == "bangpakong") htmltable += '<td>' + (item['length'] || '') + '</td>';
                            htmltable += '</tr>';
                        } else {
                            if ((date_current == item['date']) && (count_time > time_current)) {
                                // console.log(date_current, item['date']);
                                // console.log(time_current, item['time']);
                                // id = $(data).length - 5;
                                // return false;
                            } else {
                                htmltable += '<tr>';
                                htmltable += '<td class="center">' + moment(item['date'], 'DD/MM/YYYY').format('DD MMM YYYY') + '</td>';
                                htmltable += '<td class="center">' + item['time'] + '</td>';
                                htmltable += '<td class="right">' + (item['do'] || '') + '</td>';
                                htmltable += '<td class="right">' + (item['conductivity'] || '') + '</td>';
                                htmltable += '<td class="right">' + (item['ph'] || '') + '</td>';
                                htmltable += '<td class="right">' + (item['temp'] || '') + '</td>';
                                htmltable += '<td class="right">' + (item['salinity'] || '') + '</td>';
                                htmltable += '<td class="right">' + (item['tds'] || '') + '</td>';
                                htmltable += '<td class="right">' + (item['waterlevel'] || '') + '</td>';
                                htmltable += '<td class="right">' + (item['flow'] || '') + '</td>';
                                htmltable += '<td class="right">' + (item['velocity'] || '') + '</td>';
                                if (source.waterquality.tstat == "bangpakong") htmltable += '<td>' + (item['length'] || '') + '</td>';
                                htmltable += '</tr>';
                            }
                        }

                    });
                    htmltable += '</tfoot></table>';
                } else {
                    htmltable += '<tr>';
                    htmltable += '<td colspan="12" align="center">ไม่พบข้อมูล</td>';
                    htmltable += '</tr>';
                    htmltable += '</tbody></table>';
                }
                $('#table_waterquality').html(htmltable);
            })
        .always(function () {
            unblock('#table_waterquality');
        });
} // tab table thachin
