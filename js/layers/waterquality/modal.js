source.waterquality.getDay = function (day) {
    return moment(day, ['DD/MM/YYYY']).format('DD MMMM YYYY');
}

//--------------------------------------------------------------
// drop down menu
//--------------------------------------------------------------

source.waterquality.getDropdownStation = function () {
    var tag_cstat = "";
    var tag_selected = "";
    var width_dropdown = '100%';

    // clear data
    $("#table_form_station_id").chosen('destroy');
    $("#graph_form_station_id").chosen('destroy');

    tag_cstat += '<option value=""></option>';

    if (source.waterquality.tstat == "thachin") {
        $.each(source.waterquality.thachin.features, function (index, value) {
            if (source.waterquality.cstat == value.id) {
                tag_selected = 'selected';
            } else {
                tag_selected = '';
            }
            tag_cstat += '<option value="' + value.id + '" ' + tag_selected + '>' + value.properties.stn_name + ' ' + value.properties.location + '</option>';
        });
    } else if (source.waterquality.tstat == "maeklong") {
        $.each(source.waterquality.maeklong.features, function (index, value) {
            if (source.waterquality.cstat == value.id) {
                tag_selected = 'selected';
            } else {
                tag_selected = '';
            }
            tag_cstat += '<option value="' + value.id + '" ' + tag_selected + '>' + value.properties.stn_name + ' ' + value.properties.location + '</option>';
        });
    } else {
        $.each(source.waterquality.bangpakong.features, function (index, value) {
            if (source.waterquality.cstat == value.id) {
                tag_selected = 'selected';
            } else {
                tag_selected = '';
            }
            tag_cstat += '<option value="' + value.id + '" ' + tag_selected + '>' + value.properties.stn_name + ' ' + value.properties.location + '</option>';
        });
    }

    // table add station
    $("#table_form_station_id").html(tag_cstat);
    $('#table_form_station_id').chosen({
        width: width_dropdown,
        no_results_text: "ไม่พบข้อมูล",
        disable_search: true
    });
    $('#table_form_display').chosen({
        width: width_dropdown,
        no_results_text: "ไม่พบข้อมูล",
        disable_search: true
    });

    // graph add station
    $("#graph_form_station_id").html(tag_cstat);
    $('#graph_form_station_id').chosen({
        width: width_dropdown,
        no_results_text: "ไม่พบข้อมูล",
        disable_search: true
    });
    $('#graph_form_display').chosen({
        width: width_dropdown,
        no_results_text: "ไม่พบข้อมูล",
        disable_search: true
    });
}

//--------------------------------------------------------------
// tab modal
//--------------------------------------------------------------

source.waterquality.tab_basic = function () {
    block('#tab_basic_waterquality');
    $.getJSON(source.waterquality.url + source.waterquality.tstat + "/",
            function (data) {
                // console.log(data.features);
                $.each(data.features, function (index, value) {
                    if (source.waterquality.cstat == value.id) {
                        $("#featuretab-title").html('คุณภาพน้ำ <i class="glyphicon glyphicon-menu-right"></i> ' + value.properties.stn_name);

                        source.waterquality.load_data_waterquality(value);
                    }
                });
            })
        .fail(function () {
            alert(source.waterquality.tstat + ' request failed! ');
        })
        .always(function () {
            unblock('#tab_basic_waterquality');
        });
}

source.waterquality.tab_table = function () {
    $('#span_date_start_table').text(source.waterquality.getDay($('#table_form_date_start').val()));
    $('#span_date_end_table').text(source.waterquality.getDay($('#table_form_date_end').val()));

    source.waterquality.add_tab_table();
}

source.waterquality.tab_graph = function () {
    $('#span_date_start_graph').text(source.waterquality.getDay($('#graph_form_date_start').val()));
    $('#span_date_end_graph').text(source.waterquality.getDay($('#graph_form_date_end').val()));

    source.waterquality.add_tab_graph();
}

//--------------------------------------------------------------

$(function () {
    // check first tab
    source.waterquality.chkFirstTabBasic = 1; // other
    source.waterquality.chkFirstTabTable = 0; // first
    source.waterquality.chkFirstTabGraph = 0; // first

    var split = $("#image_waterquality").attr("src").split("/");
    source.waterquality.tstat = split[2];
    source.waterquality.cstat = split[3].substring(0, split[3].toString().length - 4);

    source.waterquality.getDropdownStation();

    if (source.waterquality.tstat === "thachin" || source.waterquality.tstat === "maeklong") {
        $('#Chart_length').attr('style', 'display: none;');
    }

    // add date current
    var today = moment().format('DD/MM/') + (parseInt(moment().format('YYYY')) + 543);

    $("#table_form_date_start").val(today).text(today);
    $("#table_form_date_end").val(today).text(today);

    $("#graph_form_date_start").val(today).text(today);
    $("#graph_form_date_end").val(today).text(today);

    //----------------------------------------------------------------------------	

    $('.input_date').datepicker({
        language: "th-th",
        autoclose: true,
    });

    //----------------------------------------------------------------------------

    $("a[href='#tab_basic_waterquality']").click(function (e) {
        e.preventDefault();
        $('#modal-waterquality').removeClass('modal-lg');
        if (source.waterquality.chkFirstTabBasic == 0) {
            // console.info('tab_basic_first: ' + source.waterquality.chkFirstTabBasic);
            source.waterquality.chkFirstTabBasic = 1;
            source.waterquality.tab_basic();
        }
    }); // tab table

    //----------------------------------------------------------------------------	

    $("a[href='#tab_table_datawaterquality']").click(function (e) {
        e.preventDefault();
        $('#modal-waterquality').removeClass('modal-lg').addClass('modal-lg');
        if (source.waterquality.chkFirstTabTable == 0) {
            // console.info('tab_table_first: ' + source.waterquality.chkFirstTabTable);
            source.waterquality.chkFirstTabTable = 1;
            source.waterquality.tab_table();
        }
    }); // tab table

    //----------------------------------------------------------------------------	

    $("a[href='#tab_graph_datawaterquality']").click(function (e) {
        e.preventDefault();
        $('#modal-waterquality').removeClass('modal-lg').addClass('modal-lg');
        if (source.waterquality.chkFirstTabGraph == 0) {
            // console.info('tab_graph_first: ' + source.waterquality.chkFirstTabGraph);
            source.waterquality.chkFirstTabGraph = 1;
            source.waterquality.tab_graph();
        }
    }); // tab graph

    //----------------------------------------------------------------------------	

    $("#btn_search_table").click(function (e) {
        e.preventDefault();

        if ($('#table_form_date_start').val() > $('#table_form_date_end').val()) {
            alert('เลือกวันที่ไม่ถูกต้อง');
            return false;
        }

        source.waterquality.tab_table();

        if ($("#table_form_station_id").val() != $("#graph_form_station_id").val()) {
            source.waterquality.chkFirstTabBasic = 0;
            source.waterquality.chkFirstTabGraph = 0;
            source.waterquality.cstat = $("#table_form_station_id").val();
            $("#graph_form_station_id").val($("#table_form_station_id").val());
            $('#graph_form_station_id').trigger('chosen:updated');
        }
    }); // button search table

    //----------------------------------------------------------------------------	

    $("#btn_search_graph").click(function (e) {
        e.preventDefault();

        if ($('#graph_form_date_start').val() > $('#graph_form_date_end').val()) {
            alert('เลือกวันที่ไม่ถูกต้อง');
            return false;
        }

        source.waterquality.tab_graph();

        if ($("#table_form_station_id").val() != $("#graph_form_station_id").val()) {
            source.waterquality.chkFirstTabBasic = 0;
            source.waterquality.chkFirstTabTable = 0;
            source.waterquality.cstat = $("#graph_form_station_id").val();
            $("#table_form_station_id").val($("#graph_form_station_id").val());
            $('#table_form_station_id').trigger('chosen:updated');
        }
    }); // button search graph

    // $(".modal-content").resizable({
    $(".modal-waterquality").resizable({
            resize: function (event, ui) {
                console.log($(".container").width());
                if ($(".container").width() > 666) {
                    $(".form-group.station").css({
                        "height": "32px",
                        "margin-top": "25px",
                        "text-align": "center",
                        "display": "inline-flex",
                        "width": "705px"
                    });
                    $(".form-group.date-start").css({
                        "margin": "10px",
                        "display": "inline-block"
                    });
                    $(".form-group.date-end").css({
                        "text-align": "center",
                        "display": "inline-block"
                    });
                    $(".form-group.display-time").css({
                        "display": "inline-flex",
                        "height": "32px",
                        "margin-top": "6px"
                    });
                    $("#btn_search_table").css({
                        "margin-top": "-7px"
                    });

                    $(".form-group.station label").css({
                        "margin": "8px 10px"
                    });
                    $(".form-group.display-time label").css({
                        "margin": "8px 10px 0px 8px"
                    });

                    $(".form-control.input_date").css({
                        "width": "120px"
                    });
                } else {
                    $(".form-group.station").css({
                        "display": "initial"
                    });
                    $(".form-group.date-start").css({
                        "display": "initial"
                    });
                    $(".form-group.date-end").css({
                        "display": "initial"
                    });
                    $(".form-group.display-time").css({
                        "display": "initial"
                    });

                    $(".form-group.station label").css({
                        "margin-top": "15px"
                    });
                    $(".form-group.date-start label").css({
                        "margin-top": "15px"
                    });
                    $(".form-group.date-end label").css({
                        "margin-top": "15px"
                    });
                    $(".form-group.display-time label").css({
                        "margin-top": "15px"
                    });
                    $(".form-group.btn-submit").css({
                        "margin-top": "15px"
                    });

                    $(".form-control.input_date").css({
                        "width": "100%"
                    });
                }
            }
        });
    // $('#iwocModal').on('shown.bs.modal', function (e) {
    //     // console.log($(".container").width());
    //     $("#modal-waterquality .ui-resizable-handle").remove();
        
    // });
}); //ready