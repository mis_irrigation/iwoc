//////////////////////////////////////////////////////////
// ajax load data (chart)
//////////////////////////////////////////////////////////

source.waterquality.add_tab_graph = function () {
    block('#graph_waterquality');

    if (source.waterquality.tstat == "thachin") {
        source.waterquality.para = "sid=" + ($("#graph_form_station_id").val() || source.waterquality.cstat); // source.waterquality.cstat;
        source.waterquality.para += "&sModel=" + $('#graph_form_display').find(":selected").val();
        source.waterquality.para += "&dtBegin=" + $("#graph_form_date_start").val(); // "2017-04-03";
        source.waterquality.para += "&dtFinal=" + $("#graph_form_date_end").val(); // "2017-04-03";
    } else if (source.waterquality.tstat == "maeklong") {
        if ($('#graph_form_display').find(":selected").val() == '0') {
            $('#graph_form_display').find(":selected").val('1');
        } else if ($('#graph_form_display').find(":selected").val() == '1') {
            $('#graph_form_display').find(":selected").val('0');
        }

        source.waterquality.para = "sid=" + ($("#graph_form_station_id").val() || source.waterquality.cstat); // source.waterquality.cstat;
        source.waterquality.para += "&sModel=" + $('#graph_form_display').find(":selected").val();
        source.waterquality.para += "&dtBegin=" + $("#graph_form_date_start").val(); // "2017-04-03";
        source.waterquality.para += "&dtFinal=" + $("#graph_form_date_end").val(); // "2017-04-03";
    } else {
        if ($('#graph_form_display').find(":selected").val() == '1') {
            $('#graph_form_display').find(":selected").val('15');
        } else if ($('#graph_form_display').find(":selected").val() == '0') {
            $('#graph_form_display').find(":selected").val('60');
        }

        source.waterquality.para = "station=" + ($("#graph_form_station_id").val() || source.waterquality.cstat); //source.waterquality.cstat;
        source.waterquality.para += "&displaylist=" + $('#graph_form_display').find(":selected").val();
        source.waterquality.para += "&fromdate=" + $("#graph_form_date_start").val();
        source.waterquality.para += "&todate=" + $("#graph_form_date_end").val();
        source.waterquality.para += "&showtype=table";
    }

    $.getJSON(
            source.waterquality.url + source.waterquality.tstat + "/graph/?" + source.waterquality.para,
            function (data) {
                var date_station, date_current;
                source.waterquality.date = [];

                $(data).each(function (id, item) {
                    $(item.datetime).each(function (id, item) {
                        // console.log(item, new Date(moment(item, ['DD/MM/YYYY HH:mm:ss']).format('MM-DD-YYYY HH:mm:ss')).getTime());
                        if ((source.waterquality.tstat == "thachin") || (source.waterquality.tstat == "maeklong")) {
                            date_station = new Date(moment(item, ['DD/MM/YYYY HH:mm:ss']).format('MM-DD-') + (parseInt(moment(item, ['DD/MM/YYYY HH:mm:ss']).format('YYYY')) - 543) + moment(item, ['DD/MM/YYYY HH:mm:ss']).format(' HH:mm:ss')).getTime();
                            date_current = moment().toDate().getTime();
                            // console.log(new Date(date_station), new Date(date_current));
                            if (date_station > date_current) {

                            } else {
                                source.waterquality.date[id] = moment(item, ['DD/MM/YYYY HH:mm:ss']).format();
                            }
                        } else {
                            source.waterquality.date[id] = moment(item, ['DD/MM/YYYY HH:mm:ss']).format();
                        }
                    });

                    source.waterquality.gen_graph_do(item.do);
                    source.waterquality.gen_graph_conductivity(item.conductivity);
                    source.waterquality.gen_graph_ph(item.ph);
                    source.waterquality.gen_graph_temp(item.temp);
                    source.waterquality.gen_graph_salinity(item.salinity);
                    source.waterquality.gen_graph_waterlevel(item.waterlevel);
                    source.waterquality.gen_graph_flow(item.flow);
                    source.waterquality.gen_graph_tds(item.tds);
                    source.waterquality.gen_graph_velocity(item.velocity);
                    if (source.waterquality.tstat == "bangpakong") source.waterquality.gen_graph_length(item.length);
                    // if ((source.waterquality.tstat != "thachin") || (source.waterquality.tstat != "maeklong")) source.waterquality.gen_graph_length(item.length);
                });
            })
        .always(function () {
            unblock('#graph_waterquality');
        });
} // end function add_tab_graph
// =======================================================

//////////////////////////////////////////////////////////
// get options and get data (chart)
//////////////////////////////////////////////////////////
// function getOptions(title_name, y_name) {
source.waterquality.getOptions = function (title_name, y_name, max_data, step_data) {
    return {
        hover: {
            // Overrides the global setting
            mode: 'x-axis'
        },
        legend: {
            display: false,
        },
        tooltips: {
            intersect: false,
            mode: 'label',
            callbacks: {
                label: function (tooltipItems, data) {
                    return data.datasets[tooltipItems.datasetIndex].label + ': ' +
                        tooltipItems.yLabel;
                }
            }
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'เวลา'
                },
                type: 'time',
                time: {
                    unit: 'day',
                    displayFormats: {
                        'millisecond': 'D MMM YYYY',
                        'second': 'D MMM YYYY',
                        'minute': 'D MMM YYYY',
                        'hour': 'D MMM YYYY',
                        'day': 'D MMM YYYY',
                        'week': 'D MMM YYYY',
                        'month': 'D MMM YYYY',
                        'quarter': 'D MMM YYYY',
                        'year': 'D MMM YYYY',
                    },
                    tooltipFormat: 'วันที่ D MMM YYYY เวลา HH.mm น.'
                },
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: y_name
                },
                ticks: {
                    max: max_data * 2,
                    // min: 0,
                    // stepSize: step_data
                }
            }]
        },
        title: {
            display: true,
            fontSize: 20,
            text: title_name
        },
        responsive: true,

        // pan: {
        //     enabled: true,
        //     mode: 'x'
        // },
        // zoom: {
        //     enabled: true,
        //     mode: 'x'
        // }
    }
}

// function getData(labeltitle, labelvalue, datavalue) {
source.waterquality.getData = function (labeltitle, labelvalue, datavalue) {
    return {
        labels: labelvalue, // labelvalue
        datasets: [{
            label: labeltitle,
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: datavalue,
            spanGaps: true,
        }]
    };
}

//////////////////////////////////////////////////////////

// =======================================================

//////////////////////////////////////////////////////////
// gen graph do
//////////////////////////////////////////////////////////
// function gen_graph_do(data_do) {
source.waterquality.gen_graph_do = function (data_do) {
    var ctx = document.getElementById("Chart_do");
    if (source.waterquality.mychart_do != null) {
        source.waterquality.mychart_do.destroy();
    }

    source.waterquality.mychart_do = new Chart(ctx, {
        type: 'line',
        data: source.waterquality.getData('ออกซิเจนในน้ำ', source.waterquality.date, data_do),
        options: source.waterquality.getOptions('ออกซิเจนในน้ำ', 'mg/l', data_do.max(), null)
    });
}
//////////////////////////////////////////////////////////
// gen graph conductivity
//////////////////////////////////////////////////////////
// function gen_graph_conductivity(data_conductivity) {
source.waterquality.gen_graph_conductivity = function (data_conductivity) {
    var ctx = document.getElementById("Chart_conductivity");
    if (source.waterquality.mychart_conductivity != null) {
        source.waterquality.mychart_conductivity.destroy();
    }

    source.waterquality.mychart_conductivity = new Chart(ctx, {
        type: 'line',
        data: source.waterquality.getData('ค่าการนำไฟฟ้า', source.waterquality.date, data_conductivity),
        options: source.waterquality.getOptions('ค่าการนำไฟฟ้า', 'µS/cm', data_conductivity.max(), null)
    });
}
//////////////////////////////////////////////////////////
// gen graph ph
//////////////////////////////////////////////////////////
// function gen_graph_ph(data_ph) {
source.waterquality.gen_graph_ph = function (data_ph) {
    var ctx = document.getElementById("Chart_ph");
    if (source.waterquality.mychart_ph != null) {
        source.waterquality.mychart_ph.destroy();
    }

    source.waterquality.mychart_ph = new Chart(ctx, {
        type: 'line',
        data: source.waterquality.getData('กรด-ด่าง', source.waterquality.date, data_ph),
        options: source.waterquality.getOptions('กรด-ด่าง', '', data_ph.max(), null)
    });
}
//////////////////////////////////////////////////////////
// gen graph temp
//////////////////////////////////////////////////////////
// function gen_graph_temp(data_temp) {
source.waterquality.gen_graph_temp = function (data_temp) {
    var ctx = document.getElementById("Chart_temp");
    if (source.waterquality.mychart_temp != null) {
        source.waterquality.mychart_temp.destroy();
    }

    source.waterquality.mychart_temp = new Chart(ctx, {
        type: 'line',
        data: source.waterquality.getData('อุณหภูมิ', source.waterquality.date, data_temp),
        options: source.waterquality.getOptions('อุณหภูมิ', '°C', data_temp.max(), null)
    });
}
//////////////////////////////////////////////////////////
// gen graph salinity
//////////////////////////////////////////////////////////
// function gen_graph_salinity(data_salinity) {
source.waterquality.gen_graph_salinity = function (data_salinity) {
    var ctx = document.getElementById("Chart_salinity");
    if (source.waterquality.mychart_salinity != null) {
        source.waterquality.mychart_salinity.destroy();
    }

    source.waterquality.mychart_salinity = new Chart(ctx, {
        type: 'line',
        data: source.waterquality.getData('ความเค็ม', source.waterquality.date, data_salinity),
        options: source.waterquality.getOptions('ความเค็ม', 'g/l', data_salinity.max(), null)
    });
}
//////////////////////////////////////////////////////////
// gen graph waterlevel
//////////////////////////////////////////////////////////
// function gen_graph_waterlevel(data_waterlevel) {
source.waterquality.gen_graph_waterlevel = function (data_waterlevel) {
    var ctx = document.getElementById("Chart_waterlevel");
    if (source.waterquality.mychart_waterlevel != null) {
        source.waterquality.mychart_waterlevel.destroy();
    }

    source.waterquality.mychart_waterlevel = new Chart(ctx, {
        type: 'line',
        data: source.waterquality.getData('ระดับน้ำ', source.waterquality.date, data_waterlevel),
        options: source.waterquality.getOptions('ระดับน้ำ', 'm.(MSL.)', data_waterlevel.max(), null)
    });
}
//////////////////////////////////////////////////////////
// gen graph flow
//////////////////////////////////////////////////////////
// function gen_graph_flow(data_flow) {
source.waterquality.gen_graph_flow = function (data_flow) {
    var ctx = document.getElementById("Chart_flow");
    if (source.waterquality.mychart_flow != null) {
        source.waterquality.mychart_flow.destroy();
    }

    source.waterquality.mychart_flow = new Chart(ctx, {
        type: 'line',
        data: source.waterquality.getData('อัตราการไหล', source.waterquality.date, data_flow),
        options: source.waterquality.getOptions('อัตราการไหล', 'm3/s', data_flow.max(), null)
    });
}
//////////////////////////////////////////////////////////
// gen graph tds
//////////////////////////////////////////////////////////
// function gen_graph_tds(data_tds) {
source.waterquality.gen_graph_tds = function (data_tds) {
    var ctx = document.getElementById("Chart_tds");
    if (source.waterquality.mychart_tds != null) {
        source.waterquality.mychart_tds.destroy();
    }

    source.waterquality.mychart_tds = new Chart(ctx, {
        type: 'line',
        data: source.waterquality.getData('สารละลาย', source.waterquality.date, data_tds),
        options: source.waterquality.getOptions('สารละลาย', 'mg/l', data_tds.max(), null)
    });
}
//////////////////////////////////////////////////////////
// gen graph velocity
//////////////////////////////////////////////////////////
// function gen_graph_velocity(data_velocity) {
source.waterquality.gen_graph_velocity = function (data_velocity) {
    var ctx = document.getElementById("Chart_velocity");
    if (source.waterquality.mychart_velocity != null) {
        source.waterquality.mychart_velocity.destroy();
    }

    source.waterquality.mychart_velocity = new Chart(ctx, {
        type: 'line',
        data: source.waterquality.getData('อัตราความเร็ว', source.waterquality.date, data_velocity),
        options: source.waterquality.getOptions('อัตราความเร็ว', 'm/s', data_velocity.max(), null)
    });
}
//////////////////////////////////////////////////////////
// gen graph length
//////////////////////////////////////////////////////////
// function gen_graph_length(data_length) {
source.waterquality.gen_graph_length = function (data_length) {
    var ctx = document.getElementById("Chart_length");
    if (source.waterquality.mychart_length != null) {
        source.waterquality.mychart_length.destroy();
    }

    source.waterquality.mychart_length = new Chart(ctx, {
        type: 'line',
        data: source.waterquality.getData('ระยะลำน้ำ', source.waterquality.date, data_length),
        options: source.waterquality.getOptions('ระยะลำน้ำ', 'm', data_length.max(), null)
    });
}

// =======================================================

Array.prototype.max = function () {
    return Math.max.apply(null, this);
};