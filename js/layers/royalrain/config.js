var royalrain_config = {
  'royalrain_omkoi':{
    'vedio' : 'http://huahin.royalrain.go.th/RadarData/MP4/omkoi/omkoi.webm',
    'iframe' : 'http://122.154.75.14/RRMThaiGov/RadarApp/RadarCurImage.php?isBack=0&Site=3&type=CAPPI%25',
    'name' : 'อมก๋อย',
    'lu' : [19.970903017879, 95.669648096697],
    'rl' : [15.506569495619, 100.72661044048],
    'opacity' :.8
  },
  'royalrain_pimai':{
    'vedio' : 'http://huahin.royalrain.go.th/RadarData/MP4/pimai/pimai.webm',
    'iframe' : 'http://122.154.75.14/RRMThaiGov/RadarApp/RadarCurImage.php?isBack=0&Site=4&type=CAPPI%25',
    'name' : 'พิมาย',
    'lu' : [ 17.3541305594104, 100.39843967347],
    'rl' : [ 13.0001149203977, 105.666798596675],
    'opacity' :.8
  },
  'royalrain_rongkwang':{
    'vedio' : 'http://huahin.royalrain.go.th/RadarData/MP4/rongkwang/rongkwang.webm',
    'iframe' : 'http://122.154.75.14/RRMThaiGov/RadarApp/RadarCurImage.php?isBack=0&Site=37&type=CAPPI%25',
    'name' : 'ร้องกวาง',
    'lu' : [ 20.4937271511867, 97.6964366623731],
    'rl' : [ 16.058538356608, 102.475352588532],
    'opacity' :.8
  },
  'royalrain_inburi':{
    'vedio' : 'http://huahin.royalrain.go.th/RadarData/MP4/inburi/inburi.webm',
    'iframe' : 'http://122.154.75.14/RRMThaiGov/RadarApp/RadarCurImage.php?isBack=0&Site=36&type=CAPPI%25',
    'name' : 'อินทร์บุรี',
    'lu' : [ 17.1564826380132, 97.7306409303209],
    'rl' : [ 12.7565155928737, 102.471605421459],
    'opacity' :.8
  },
  'royalrain_chatui':{
    'vedio' : 'http://huahin.royalrain.go.th/RadarData/MP4/chatu/chatu.webm',
    'iframe' : 'http://122.154.75.14/RRMThaiGov/RadarApp/RadarCurImage.php?isBack=0&Site=26&type=CAPPI%25',
    'name' : 'จตุร',
    'lu' : [ 17.9334553006936, 100.95977090621],
    'rl' : [ 13.5100122491433, 105.726030794256],
    'opacity' :.8
  },
  'royalrain_sattahip':{
    'vedio' : 'http://huahin.royalrain.go.th/RadarData/MP4/sattahip/sattahip.webm',
    'iframe' : 'http://122.154.75.14/RRMThaiGov/RadarApp/RadarCurImage.php?isBack=0&Site=2&type=CAPPI%25',
    'name' : 'สัตหีบ',
    'lu' : [ 14.8190186216374, 98.790907352678],
    'rl' : [ 10.0761722401545, 104.373292059159],
    'opacity' :.8
  },
  'royalrain_pathio':{
    'vedio' : 'http://huahin.royalrain.go.th/RadarData/MP4/pathio/pathio.webm',
    'iframe' : 'http://122.154.75.14/RRMThaiGov/RadarApp/RadarCurImage.php?isBack=0&Site=31&type=CAPPI%25',
    'name' : 'ปะทิว',
    'lu' : [ 12.8965746542107, 96.7684479540196],
    'rl' : [ 8.48082105656173, 101.526422455486],
    'opacity' :.8
  },
  'royalrain_phanom':{
    'vedio' : 'http://huahin.royalrain.go.th/RadarData/MP4/phanom/phanom.webm',
    'iframe' : 'http://122.154.75.14/RRMThaiGov/RadarApp/RadarCurImage.php?isBack=0&Site=29&type=CAPPI%',
    'name' : 'พนม',
    'lu' : [ 11.0309726614445, 96.6261652724755],
    'rl' : [ 6.66745907284688, 101.906016714679],
    'opacity' :.8
  },
}
