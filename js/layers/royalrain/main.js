/*
var imageOverlay;
function displayRoyalRain(){
	 imageOverlay = L.imageOverlay(
		royalrain_config[node]['img'],
		[ royalrain_config[node]['lu'], royalrain_config[node]['rl'] ],
		{ opacity: royalrain_config[node]['opacity'] },
	);
	map.addLayer(imageOverlay);
}
*/
var eventRoyalRain = {};
var videoOverlay = {};
var bounds = {};
var videoUrls = {};
/** add val source **/
for (var vRoyalrain  in royalrain_config) {
		source[ vRoyalrain ] = {};
		eventRoyalRain[ royalrain_config[vRoyalrain]['name'] ] = vRoyalrain;
}

$(document).ready(function () {
  for (var vRoyalrain  in royalrain_config) {
    source[ vRoyalrain ].layer = new L.LayerGroup();
  }
});

function getLayerRoyalRain(node, callback) {
  if (source[node].videoOverlay == undefined) {
    block('#map');
		source[node].videoOverlay = displayRoyalRain(node);
		unblock('#map');
		callback($(source[node].geoJson).length);
	}
}
function displayRoyalRain(node){
	videoUrls[node] = [ royalrain_config[node]['vedio'] ];
	bounds[node] = L.latLngBounds([royalrain_config[node]['lu'], royalrain_config[node]['rl']]);
	videoOverlay[node] = L.videoOverlay( videoUrls[node], bounds[node], {
      opacity: royalrain_config[node]['opacity']
  }).addTo(source[node].layer);
	$(videoOverlay[node]._image).css("position","inherit");

	$( videoOverlay[node]._image ).click(function() {
	  //alert( "Handler for .click() called." + node);
		displayPopUpRoyalRain(node);
	});

	return videoOverlay[node];
}
function displayPopUpRoyalRain(node){
	var content;
	content += '<div class="modal-dialog">';
	content += '<div class="modal-content">';
	content += '<div class="modal-header">';
	content += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
	content += '<h4 class="modal-title" id="featuretab-title">ภาพเรดาร์(CAPPI) จุดสถานี'+royalrain_config[node]['name']+'</h4>';
	content += '</div>';
	content += '<div class="modal-body cst-model-geosever" id="featuretab-info" style="width: 100%;height: calc( 100% - 60px);">';
		content += '<iframe align="center" style="min-height: 400px; width: 100%;height: 100%;border: none" src="'+royalrain_config[node]['iframe']+'"></iframe>';
	content += '</div>';
	content += '</div>';
	content += '</div>';

	$('#iwocModal').html(content).modal("show");
}
