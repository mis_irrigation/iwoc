﻿
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
    

    
    <meta name="theme-color" content="#ffffff">
    <meta name="description" content="">
    <meta name="author" content="">
    
    
    
    <title>IWOC : ศูนย์ปฏิบัติการน้ำอัจฉริยะ</title>

    <link rel="stylesheet" href="libraries/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="libraries/bootstrap/css/navbar.css">
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.css">
    <!--<link rel="stylesheet" href="libraries/leaflet1.0.3/leaflet.css">-->

	<link rel="stylesheet" href="asset/css/app.css">
    
   <!-- <link rel="stylesheet" href="asset/css/app.css">
    -->
    
    
    <!-- Stylesheets -->
		
        <link rel="stylesheet" type="text/css" href="map3d/css/style.css">
		<link rel="stylesheet" type="text/css" href="map3d/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="map3d/css/map.css">
		<link rel="stylesheet" type="text/css" href="map3d/mapplic/mapplic.css">
        
        
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

  <link rel='stylesheet prefetch' href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css'>

      
        
        <!--<link rel="stylesheet" type="text/css" href="map3d/css/style_breadcrumb.css">-->
    

    <!--<link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon-76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/img/favicon-120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/img/favicon-152.png">-->
    <link rel="icon" sizes="196x196" href="images/logo01.png">
    <link rel="icon" type="image/x-icon" href="images/irrigation.ico">

    <link href="fontsth/thsarabunnew/thsarabunnew.css" rel="stylesheet" type="text/css" media="screen" />

    <style>
       
        
        .info 
        {
             padding: 6px 8px;
                font: 14px/16px 'THSarabunNew', Arial, Helvetica, sans-serif; 
                line-height: 25px; 
                background: white; 
                background: rgba(255,255,255,0.8); 
                box-shadow: 0 0 15px rgba(0,0,0,0.2); 
                border-radius: 5px; 

        } 
 
        .info h4 { margin: 0 0 5px; color: #777; }

        .legend {font-family:'THSarabunNew'; text-align: left; line-height: 18px; color: #555; } 
        .legend i { width: 60px; height: 18px; float: left; margin-right: 8px; opacity: 0.7; }



#water2_test {
  width: 50px;
  height: 50px;
  position: absolute;
  background-color: red;
}



   #water1x {
	   
	
    
    stroke: #000;
    stroke-width: 0.5px;
    stroke-dasharray: 870;
    stroke-dashoffset: 870;
    animation: draw 10s infinite linear;
	
  }
  @keyframes draw {
  to {
	 
	 
	  
	  
    stroke-dashoffset: 0;
  }
  
 
}



 #water1,#water1_2 ,#water1-2{
  animation: animt_b 2.5s infinite linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}


#water3_1{
  animation: animt_b_2 2.5s infinite linear  ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}

#water3_2{
  animation: animt_b_1 2.5s infinite linear  ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}



#water1_4{
  animation: animt_b_1 0.7s infinite linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}






 #water2{
  animation: animt_b2 1.8s infinite linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}




 #water3{
  animation: animt_b2_1 1.8s infinite linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}


 #water4,#water5{
  animation: animt_b2_2 1.4s infinite linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}




#water9x{
  animation: animt_b2 0.8s infinite linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}

 #water2_1,#water2_2,#water2_3,#water2_4,#water2_5,#water2_6,#water2_7,#water2_8,#water2_9,#water2_10{
  animation: animt_b3 1s infinite linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}

/*#m2,#m4,#m6,#m8,#m10,#m12,#m14,#m16,#m18{
	
  animation: scale1  1.5s  linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}


#m1,#m3,#m5,#m6,#m7,#m9,#m11,#m13,#m15,#m17,#m19,#m4_2{
	
  animation: scale1  1.5s  linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}*/





 #water2x{
  
  animation: animationFrames 3s ease infinite;
  transform-origin: 100% 100%;
  
  
 
}

@keyframes animt_b{



0% {
    
    
    opacity: 0;
    filter: alpha(opacity=0);
  }
  10% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
    90% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
  100% {
   
    transform:translate(0px,300px);
    opacity: 0;
    filter: alpha(opacity=0);
  }
  
  
  
  
  
  
  
  
  
 

  


}

@keyframes animt_b_1{



0% {
    
    
    opacity: 0;
    filter: alpha(opacity=0);
  }
  10% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
    90% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
  100% {
   
    transform:translate(0px,60px);
    opacity: 0;
    filter: alpha(opacity=0);
  }
  
  
  
  
  
  
  
  
  
 

  


}

@keyframes animt_b_2{



0% {
    
    
    opacity: 0;
    filter: alpha(opacity=0);
  }
  10% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
    90% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
  100% {
   
    transform:translate(0px,-60px);
    opacity: 0;
    filter: alpha(opacity=0);
  }
  
  
  
  
  
  
  
  
  
 

  


}

@keyframes animt_b_1_old{

  
  
0% {
   
	transform:translate(0px);
	opacity: 0.1; 
	
	
	
  }
  
  50% {
   
	
	opacity: 1; 
	
	
	
  }
  
  100% {
    
	transform:translate(0px,5px);
	
	
  }
  


}


@keyframes animt_b2{

  
  
0% {
   
	transform:translate(0px);
	
	opacity: 0.1; 
	
	
  }
  
  10% {
   
	
	opacity: 1; 
	
	
	
  }
  
   90% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
  
  100% {
  
	transform: translateX(170px)  translateY(0px);
	
	
    opacity: 0;
    filter: alpha(opacity=0);
	
	
  }
  


}


@keyframes animt_b2_1{

  
  
0% {
   
	transform:translate(0px);
	
	opacity: 0.1; 
	
	
  }
  
  10% {
   
	
	opacity: 1; 
	
	
	
  }
  
   90% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
  
  100% {
  
	transform: translateX(120px)  translateY(0px);
	
	
    opacity: 0;
    filter: alpha(opacity=0);
	
	
  }
  


}

@keyframes animt_b2_2{

  
  
0% {
   
	transform:translate(0px);
	
	opacity: 0.1; 
	
	
  }
  
  10% {
   
	
	opacity: 1; 
	
	
	
  }
  
   90% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
  
  100% {
  
	transform: translateX(25px)  translateY(0px);
	
	
    opacity: 0;
    filter: alpha(opacity=0);
	
	
  }
  


}




@keyframes animt_b3{

  
  

  
  
  
  
  0% {
    
    
    opacity: 0;
    filter: alpha(opacity=0);
  }
  50% {
    opacity: 1;
    -ms-filter: none;
    filter: none;
  }
  100% {
   
   transform:translate(-1px,0px);
    opacity: 0;
    filter: alpha(opacity=0);
  }
  
  
  


}

@-moz-keyframes animationFrames{
  0% {
    -moz-transform:  translate(10px) ;
  }
  100% {
    -moz-transform:   translate(100px,100px) ;
	
  }
}

@-webkit-keyframes animationFrames {
  0% {
    -webkit-transform:   translate(10px);
  }
  100% {
    -webkit-transform:    translate(100px,100px);
  }
}

@-o-keyframes animationFrames {
  0% {
    -o-transform:   translate(10px);
  }
  100% {
    -o-transform:   translate(100px,100px) ;
  }
}

@-ms-keyframes animationFrames {
  0% {
    -ms-transform:    translate(10px) ;
  }
  100% {
    -ms-transform:    translate(100px,100px) ;
  }
}


@keyframes scale1{

  

  
  
  
  
 0% {
    
    transform: scale(0.1, 0.1);
    opacity: 0;
    filter: alpha(opacity=0);
  }
  50% {
    opacity: 1;
    -ms-filter: none;
    filter: none;
  }
  100% {
   
    transform: scale(1.2, 1.2);
    opacity: 0;
    filter: alpha(opacity=0);
  }


}




  

    </style>
    
    
  
    
   
</head>

<body>
    <div class="navbar navbar-default " role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <div class="navbar-icon-container">
                    <a href="#" class="navbar-icon pull-right visible-xs" id="nav-btn"><i class="fa fa-bars fa-lg white"></i></a>
                    <a href="#" class="navbar-icon pull-right visible-xs" id="sidebar-toggle-btn"><i class="fa fa-search fa-lg white"></i></a>
                </div>
                <a class="navbar-brand" href="index.html" style="margin-top:-12px"><span ><img    src="images/RID_Logo_E_BackSmall1.png" height="45" /></span><span style="font-size:20px;">&nbsp;&nbsp;IWOC ศูนย์ปฏิบัติการน้ำอัจฉริยะ</span>  </a>
     
           
            </div>
           


            <div class="navbar-collapse collapse">
                <form class="navbar-form navbar-right" role="search">
                    <div class="form-group has-feedback">
                        <input id="searchbox" type="text" placeholder="ค้นหา" class="form-control">
                        <span id="searchicon" class="fa fa-search form-control-feedback"></span>
                    </div>
                </form>
                <ul class="nav navbar-nav">
                    <!--<li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="about-btn"><i class="fa fa-question-circle white"></i>&nbsp;&nbsp;IWOC</a></li>-->
                    <li class="dropdown">
                        <a id="toolsDrop" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-globe white"></i>&nbsp;&nbsp;คลังข้อมูลกลาง  <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="full-extent-btn"><i class="fa fa-cloud"></i>&nbsp;&nbsp;ภูมิอากาศ</a></li>
                            <li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="legend-btn"><i class="fa fa-ship"></i>&nbsp;&nbsp;ฝน/น้ำท่า/อ่างเก็บน้ำ</a></li>
                            <li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="legend-btn"><i class="fa fa-tachometer"></i>&nbsp;&nbsp;คุณภาพน้ำ</a></li>
                            <li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="legend-btn"><i class="fa fa-cogs"></i>&nbsp;&nbsp;การจัดสรรน้ำ</a></li>
                            <li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="legend-btn"><i class="fa fa-tree"></i>&nbsp;&nbsp;ข้อมูลการเพาะปลูก</a></li>
                            <li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="legend-btn"><i class="fa fa-map"></i>&nbsp;&nbsp;Agri Map</a></li>
                            <li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="legend-btn"><i class="fa fa-sitemap"></i>&nbsp;&nbsp;แผนที่ผังน้ำ</a></li>
                            <li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="legend-btn"><i class="fa fa-podcast"></i>&nbsp;&nbsp;รายงานสถานการณ์น้ำ</a></li>
                            <li class="divider hidden-xs"></li>
                            <li><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="login-btn"><i class="fa fa-user"></i>&nbsp;&nbsp;เข้าระบบ</a></li>
                        </ul>
                    </li>
                    <!--<li class="dropdown">
                        <a class="dropdown-toggle" id="downloadDrop" href="#" role="button" data-toggle="dropdown"><i class="fa fa-cloud-download white"></i>&nbsp;&nbsp;Download <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="data/boroughs.geojson" download="boroughs.geojson" target="_blank" data-toggle="collapse" data-target=".navbar-collapse.in"><i class="fa fa-download"></i>&nbsp;&nbsp;Boroughs</a></li>
                            <li><a href="data/subways.geojson" download="subways.geojson" target="_blank" data-toggle="collapse" data-target=".navbar-collapse.in"><i class="fa fa-download"></i>&nbsp;&nbsp;Subway Lines</a></li>
                            <li><a href="data/DOITT_THEATER_01_13SEPT2010.geojson" download="theaters.geojson" target="_blank" data-toggle="collapse" data-target=".navbar-collapse.in"><i class="fa fa-download"></i>&nbsp;&nbsp;Theaters</a></li>
                            <li><a href="data/DOITT_MUSEUM_01_13SEPT2010.geojson" download="museums.geojson" target="_blank" data-toggle="collapse" data-target=".navbar-collapse.in"><i class="fa fa-download"></i>&nbsp;&nbsp;Museums</a></li>
                        </ul>
                    </li>-->
                    <li class="hidden-xs"><a href="#" data-toggle="collapse" data-target=".navbar-collapse.in" id="list-btn"><i class="fa fa-list white"></i></a></li>
                </ul>
            </div><!--/.navbar-collapse -->
        </div>
    </div>
    
    
    


  <div id="wrap">

			<!-- Site header -->
			<header id="header">
				
			</header>

			<!-- Site content -->
			<div id="content">
				
				<section id="map-section" class="inner over">
				
					<!-- Map -->
					<div class="map-container">
						
                        
                        
                        
                        
                        <div id="mapplic">   </div>
                        
                        
                       
                        
                        
						<div id="mapplic2"></div>
                        
                        
                        
                  

<br />



                        
                        
					</div>

					
				</section>

			
			</div>

			<!-- Site footer -->
			
		</div>

    

 <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
<script src='http://oss.maxcdn.com/libs/snapsvg/0.1.0/snap.svg-min.js'></script>



<!--<script type="text/javascript" src="map3d/js/jquery.min.js"></script>
-->
    <!--<script src="libraries/jquery/scripts/jquery-3.1.1.min.js"></script>-->
    <!--google api  -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA7JyQX3nde0OcFkgf6wEtz8JLtNmiWGw8" async defer></script>


    <script src="libraries/bootstrap/js/bootstrap.min.js"></script>




	
		<script type="text/javascript" src="map3d/js/hammer.min.js"></script>
		<script type="text/javascript" src="map3d/js/jquery.easing.js"></script>
		<script type="text/javascript" src="map3d/js/jquery.mousewheel.js"></script>
		<script type="text/javascript" src="map3d/js/smoothscroll.js"></script>
		<script type="text/javascript" src="map3d/mapplic/mapplic.js"></script>
        
        <script type="text/javascript" src="http://ricostacruz.com/jquery.transit/jquery.transit.min.js"></script>
        
        
        
       
        
        <script type="text/javascript">
			$(document).ready(function() {
				
				
				
				
			setInterval($('#mapplic').mapplic({
					source: 'map3d/overall.json',
					height: 800,
					sidebar: true,
					minimap: true,
					markers: true,
					fullscreen: true,
					smartip: false,
					hovertip: true,
					developer: false,
					maxscale: 2,
					zoom: false,
					
				}),2000);	
				
				
			/*	var mapplic = $('#mapplic').mapplic({
					source: 'map3d/overall.json',
					height: 800,
					sidebar: true,
					minimap: true,
					markers: true,
					fullscreen: true,
					smartip: false,
					hovertip: true,
					developer: false,
					maxscale: 2,
					zoom: false,
					
				});*/
	


	//$("#water2").hide();
				/* Landing Page */
			
	
	
	 $('#gohome').click(function() {  
       alert( "Handler for .click() called." );
    });	
		
		
	function autoupdate(para1) {
		var pa1 = para1;
  setTimeout(function(){
     $(pa1).css('display', 'none');
     setTimeout(function(){
        $(pa1).css('display', 'block');
        setTimeout(autoupdate(pa1), 50);
     }, 1000)
  }, 300)
}

$(function(){ //same as $(document).ready(function(){
  setTimeout(autoupdate('#water2_test'), 50);
   
   //$("#water1").transition({ x: 200 });
   
  // setInterval(autoupdate,20);
});
		




		
	//	 animateSVG();
		 
		 	
	//var animateSVG = animateSVG();
	 // ---------
  // SVG 
		
				
			});
			
			
			
	

		</script>
        
        
       
		
        


</body>
</html>
