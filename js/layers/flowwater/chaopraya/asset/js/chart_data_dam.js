// JavaScript Document
var date_start;
var date_end;

var _labels = [];
var _datasets = [];
var _label = '';
var _data = [], _data_max_resv = [], _data_cap_resv = [], _data_low_qdisc = [];
var chart;
var arr_color = ['#0000FF', '#F68401', '#009900', '#885502', '#6A0D55', '#0763F6', '#AC698A', '#676515', '#6A0221', '#344E36'];
var year_start, month_start;
var year_end, month_end;
var y_max, arr_y_max = [], y_step;
var i = 0;


//------------------------------------------------------------------------------------                      
$(function () {








}); //ready



function fnc_graph() {

    //$('#show_date_start').html( chg_date_thai(chg_date_en($('#date_start_graph').val())) );
    //$('#show_date_end').html( chg_date_thai(chg_date_en($('#date_end_graph').val())) );

    //load_chart1('2009-01-01', '2009-12-31');
    //console.clear();

    arr_y_max = [];
    y_max = 20;
    y_step = 2;
    _labels = [];
    _data = [];
    _datasets = [];
    i = 0;
    //-------------------------------------------------------

    year_start = chg_date_en($('#date_start_graph').val()).substr(0, 4);
    year_end = chg_date_en($('#date_end_graph').val()).substr(0, 4);

    month_start = chg_date_en($('#date_start_graph').val()).substr(4, 6);
    month_end = chg_date_en($('#date_end_graph').val()).substr(4, 6);

    date_start = year_start + month_start;
    date_end = year_end + month_end;

    if (year_start == year_end) { //ถ้าช่อง date_start และ date_end เป็นปีเดียวกัน 
        load_chart(date_start, date_end);
    } else {
        load_chart(date_start, year_start + '-12-31');
    }




} //fnc_graph() 


function load_chart(date_start, date_end) {

    console.info('Chart 1 >>>  date_start=' + chg_date_th(date_start) + ' date_end=' + chg_date_th(date_end));
    block('.chart_body', 'กำลังโหลดปี ' + chg_year_th(date_start));
    var url = base_url + 'api/damGeojson?basin=chaophraya';
    $.get(url, {
        date_start: date_start,
        date_end: date_end,
        station: $('#station_id').val()
    }, function (res) {

        var _date_start = year_start + '-01-01';
        var _date_end = year_start + '-12-31';

        _labels = [];
        _data = [];
        var f = 0;
        var _max_resv=null, _cap_resv = null, _low_qdisc = null;
        var _data_max_resv=[], _data_cap_resv = [], _data_low_qdisc = [];
        for (var _loop_date = moment(_date_start); _loop_date <= moment(_date_end); _loop_date.add(1, 'days')) {
            f++;
            var _date = _loop_date.format('YYYY-MM-DD');
            //var _date=m.format('DD MM');
            //console.log(_date);
            _labels.push(_date); //แสดงแกน x 

            var j = 0, j2 = 0;
            $.each(res.features, function (index, value) {
                if (_date == value.properties.DMD_Date) {
                    if (parseFloat(value.properties.DMD_Q) > 0) {
                        //console.log(i+' ) '+_date+' = '+value.date+' || '+value.qdisc_curr);                      
                        _data.push(value.properties.DMD_Q);
                        j++;
                    }
                }
                if (j2 == 0) {
                    //ระดับน้ำสูงสุด - ต่ำสุด 
                    _max_resv = value.properties.DAM_QMax;
                    _cap_resv = value.properties.DAM_QStore;
                    _low_qdisc = value.properties.DAM_QUsage;
                    j2++;
                }
            }); //$.each 

            if (j == 0) { //ไม่พบปริมาณน้ำ 
                //console.log(_date+' = NULL');
                _data.push(null);
            }

            if (i == 0) {
                // console.log(f + ' ) ' + _date + ' = ' + _max_resv + ' |  ' + _cap_resv + ' | ' + _low_qdisc);
                var arr = ["01", "05", "10", "15", "20", "25", "30", "31"]; //plot จุดตามวันที่นี้ 
                if ($.inArray(_date.substr(8, 2), arr) >= 0) {
                    _data_max_resv.push(_max_resv);
                    _data_cap_resv.push(_cap_resv);
                    _data_low_qdisc.push(_low_qdisc);
                } else {
                    _data_max_resv.push(null);
                    _data_cap_resv.push(null);
                    _data_low_qdisc.push(null);
                }
            }

        }//for 


        if (i == 0) {
            //--- ปริมาตรระดับน้ำกักเก็บสูงสุด ---            
            _datasets.push(
                {
                    label: 'ปริมาตรระดับน้ำกักเก็บสูงสุด', //label แสดงข้างบน
                    fill: false,
                    //lineTension: 0.1,
                    borderWidth: 2.5,
                    backgroundColor: "#FFFFFF",
                    borderColor: "#9B07E6",
                    borderCapStyle: 'butt',
                    borderDash: [6, 5], //เส้นประ
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "#9B07E6",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 0,
                    pointHoverRadius: 3,
                    pointHoverBackgroundColor: "#9B07E6",
                    pointHoverBorderColor: "#FFFFFF",
                    pointHoverBorderWidth: 1,
                    pointRadius: 0,
                    pointHitRadius: 10,
                    spanGaps: true,
                    data: _data_max_resv,
                }
            );

            //--- ปริมาตรระดับน้ำกักเก็บปกติ ---            
            _datasets.push(
                {
                    label: 'ปริมาตรระดับน้ำกักเก็บปกติ', //label แสดงข้างบน
                    fill: false,
                    //lineTension: 0.1,
                    borderWidth: 2.5,
                    backgroundColor: "#FFFFFF",
                    borderColor: "#F800A1",
                    borderCapStyle: 'butt',
                    borderDash: [6, 5], //เส้นประ
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "#F800A1",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 0,
                    pointHoverRadius: 3,
                    pointHoverBackgroundColor: "#F800A1",
                    pointHoverBorderColor: "#FFFFFF",
                    pointHoverBorderWidth: 1,
                    pointRadius: 0,
                    pointHitRadius: 10,
                    spanGaps: true,
                    data: _data_cap_resv,
                }
            );

            //--- ปริมาตรระดับน้ำกักต่ำสุด ---          
            _datasets.push(
                {
                    label: 'ปริมาตรระดับน้ำกักต่ำสุด', //label แสดงข้างบน
                    fill: false,
                    // lineTension: 0.1,
                    borderWidth: 2.5,
                    backgroundColor: "#FFFFFF",
                    borderColor: "#FF0000",
                    borderCapStyle: 'butt',
                    borderDash: [6, 5], //เส้นประ
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "#FF0000",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 0,
                    pointHoverRadius: 3,
                    pointHoverBackgroundColor: "#FF0000",
                    pointHoverBorderColor: "#FFFFFF",
                    pointHoverBorderWidth: 1,
                    pointRadius: 0,
                    pointHitRadius: 10,
                    spanGaps: true,
                    data: _data_low_qdisc,
                }
            );



        }//i==0

        //Add Line --------------------------------------------------------------
        var color = arr_color[i];
        _datasets.push(
            {
                label: chg_year_th(date_start), //label แสดงข้างบน
                fill: false,
                lineTension: 0.1,
                borderWidth: 3,
                backgroundColor: "#FFFFFF",
                borderColor: color,
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: color,
                pointBackgroundColor: "#fff",
                pointBorderWidth: 1,
                pointHoverRadius: 3,
                pointHoverBackgroundColor: color,
                pointHoverBorderColor: "#FFFFFF",
                pointHoverBorderWidth: 1,
                pointRadius: 1,
                pointHitRadius: 10,
                data: _data,
            }
        );


        //------หาค่า Max ของปริมาณน้ำ เพื่อมาแสดง แกน Y--------------------------------------------------------------
        //console.log( _data );
        arr_y_max.push(parseFloat(d3.max(_data)));
        //console.info( arr_y_max );
        var y_max_top = parseFloat(d3.max(arr_y_max));
        //console.log('Y MAX : '+y_max_top);
        if (isNaN(y_max_top) == false) {
            if (y_max_top > 0) {
                y_max = parseFloat(y_max_top * 2.2);
            }
        }
        //console.log('Y MAX >>> '+y_max);

        //-------- Gen Graph ------------------------------------------------------------------
        gen_chart();
        year_start++;
        if (year_start <= year_end) {
            if (year_start == year_end) { //ถ้าเป็นปีเดียวกัน 
                if ((year_end - year_start) + 1 == 0) {
                    load_chart(year_start + month_start, year_end + month_end);
                } else {
                    load_chart(year_start + '-01-01', year_end + month_end);
                }
            } else { //ถ้าไม่ใช่ปีเดียวกัน 
                load_chart(year_start + '-01-01', year_start + '-12-31');
            }
        }

        if (year_start > year_end) {
            unblock('.chart_body');
        }

        i++;
    }); //get 

}


function gen_chart() {


    //--------------------------------------------------------------
    var data = {
        labels: _labels,
        datasets: _datasets,
    };

    var option = {
        chartArea: {
            backgroundColor: '#EEFBFC', //สีพื้นหลัง            
        },

        bezierCurve: false,
        datasetFill: false,
        scaleGridLineColor: "#3E434E",

        legendCallback: function (chart) {
            //console.log(chart);
            var legendHtml = '';
            legendHtml += '<div class="tbl_legend_chart" align="center">';
            var _line, _size;
            for (var i = 0; i < chart.data.datasets.length; i++) {
                if (chart.data.datasets[i].label) {
                    if (i == 0 || i == 1 || i == 2) {
                        _line = 'dashed';
                        _size = '3.4px';
                    }
                    else {
                        _line = 'solid';
                        _size = '4px';
                    }
                    //---
                    if (i == 3) {
                        legendHtml += '<br>';
                    }
                    legendHtml += '<span class="chart-legend-label-text text-left" onclick="updateDataset(event, ' + '\'' + chart.legend.legendItems[i].datasetIndex + '\'' + ', this)"  > <button type="button" class="legend_color_line" style="border-bottom:' + _line + ' ' + _size + chart.data.datasets[i].borderColor + '; "></button>  ' + chart.data.datasets[i].label + '</span>';
                }
            } //for
            legendHtml += '</div>';
            return legendHtml;
        },

        legend: {
            display: false,

            onClick: function (e, legendItem) {
                // code from https://github.com/chartjs/Chart.js/blob/master/src/core/core.legend.js
                // modified to prevent hiding all legend items

                var index = legendItem.datasetIndex;
                var ci = this.chart;
                var meta = ci.getDatasetMeta(index);

                // See controller.isDatasetVisible comment
                var hidden = meta.hidden === null ? !ci.data.datasets[index].hidden : null;

                if (hidden) {
                    var someShown = _.some(this.legendItems, function (item) {
                        return item !== legendItem && !item.hidden;
                    });
                    if (!someShown)
                        return;
                }

                meta.hidden = hidden;
                // We hid a dataset ... rerender the chart
                ci.update();
            },

            labels: {
                fontColor: '#000000'
            },
        },
        scales: {
            xAxes: [{
                type: 'time',
                time: {
                    //format: "DD MMMM",
                    unit: 'month', //หน่วยการแสดงเป็นเดือน 
                    unitStepSize: 1, //Step การแสดงระยะห่างกัน 
                    displayFormats: { //แสดงแค่ชื่อเดือน 
                        'millisecond': 'MMMM',
                        'second': 'MMMM',
                        'minute': 'MMMM',
                        'hour': 'MMMM',
                        'day': 'MMMM',
                        'week': 'MMMM',
                        'month': 'MMMM',
                        'quarter': 'MMMM',
                        'year': 'MMMM',
                    }
                },
                scaleLabel: {
                    display: false,
                    labelString: 'เดือน'
                },
            }],
            yAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'ปริมาณน้ำในอ่างฯ - ล้าน ลบ.ม.'
                },
                ticks: {
                    beginAtZero: true,
                    //stepSize : y_step,
                    max: y_max,
                }
            }],
            chartArea: {
                backgroundColor: 'rgba(238, 247, 253, 0.4)'
            },
        },
        title: {
            display: false,
            text: "Report Title \n 5555",
            fontFamily: "thaisanslite",
            fontSize: 22,
            fontColor: "#000",
            fontStyle: "bold",
            padding: 10,
        },
        tooltips: {
            enabled: true,
            //mode: 'single',
            //-----
            titleFontSize: 0,
            //-----
            backgroundColor: "#333333",
            bodyFontColor: "#FFFFFF", //#000000
            bodyFontSize: 12,
            bodyFontStyle: "bold",
            bodyFontColor: '#FFFFFF',
            bodyFontFamily: "tahoma",
            //-----
            footerFontSize: 12,
            footerFontFamily: "tahoma",
            custom: function (tooltip) {
                // tooltip will be false if tooltip is not visible or should be hidden
                if (!tooltip) {
                    return;
                }
                //tooltip.body = ['TEST']

            },
            callbacks: {
                label: function (tooltipItem, data) {

                    //console.log( data );
                    //console.log( tooltipItem.index )  
                    var t_i = tooltipItem.datasetIndex;
                    var t_j = tooltipItem.index;
                    var t_arr_date = data.labels;
                    //console.log( arr_date[j] );
                    var t_year = data.datasets[t_i].label;
                    var t_date = chg_day_month(data.labels[t_j]);
                    var t_data = data.datasets[t_i].data[t_j];
                    if (t_year.length > 4) {
                        return " " + t_year;
                    } else {
                        //return "วันที่: "+t_date+" ปริมาณน้ำ: "+t_data+" ล้าน ลบ.ม.";
                        return " วันที่: " + t_date + " " + t_year;
                    }
                },
                footer: function (tooltipItem, data) {
                    //return ['new line', 'another line'];
                    var t_i = tooltipItem[0].datasetIndex;

                    var t_j = tooltipItem[0].index;
                    var t_arr_date = data.labels;
                    //console.log( arr_date[j] );
                    var t_year = data.datasets[t_i].label;
                    var t_date = chg_date_thai(data.labels[t_j]);
                    var t_data = data.datasets[t_i].data[t_j];
                    return "    ปริมาณน้ำ: " + t_data + " ล้าน ลบ.ม. ";

                }

            } //callbacks

        } //tooltips 

    } //options


    // Show/hide chart by click legend
    updateDataset = function (e, datasetIndex, obj) {

        if ($(obj).hasClass('line_hidden') == false) {
            $(obj).addClass('line_hidden');
        } else {
            $(obj).removeClass('line_hidden');
        }
        //console.log(e);
        //console.log(datasetIndex);
        var index = datasetIndex;
        var ci = e.view.chart;
        var meta = ci.getDatasetMeta(index);

        // See controller.isDatasetVisible comment
        //console.log(meta.hidden);
        if (meta.hidden == null) {
            meta.hidden = !ci.data.datasets[index].hidden; //Hidden
        } else {
            meta.hidden = ci.data.datasets[index].hidden; //Show
        }

        // We hid a dataset ... rerender the chart
        ci.update();
    };


    //------------------------------------------------------------------------------------------------
    var ctx = document.getElementById("chart1");
    Chart.plugins.register({
        beforeDraw: function (chart, easing) {
            if (chart.config.options.chartArea && chart.config.options.chartArea.backgroundColor) {
                var helpers = Chart.helpers;
                var ctx = chart.chart.ctx;
                var chartArea = chart.chartArea;

                ctx.save();
                ctx.fillStyle = chart.config.options.chartArea.backgroundColor;
                ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
                ctx.restore();
            }
        }
    });

    if (empty(chart) == false) {
        chart.destroy();
    }
    chart = new Chart(ctx, {
        type: 'line',
        data: data,
        options: option
    });
    document.getElementById('legend').innerHTML = chart.generateLegend();



}

