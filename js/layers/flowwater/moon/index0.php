﻿
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
    

    
    <meta name="theme-color" content="#ffffff">
    <meta name="description" content="">
    <meta name="author" content="">
    
    
    
    <title>IWOC : ศูนย์ปฏิบัติการน้ำอัจฉริยะ</title>

    <link rel="stylesheet" href="libraries/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="libraries/bootstrap/css/navbar.css">
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.css">
    <!--<link rel="stylesheet" href="libraries/leaflet1.0.3/leaflet.css">-->

	<link rel="stylesheet" href="asset/css/app.css">
    
   <!-- <link rel="stylesheet" href="asset/css/app.css">
    -->
    
    
    <!-- Stylesheets -->
		
        <link rel="stylesheet" type="text/css" href="map3d/css/style.css">
		<link rel="stylesheet" type="text/css" href="map3d/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="map3d/css/map.css">
		<link rel="stylesheet" type="text/css" href="map3d/mapplic/mapplic.css">
        
        
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

  <link rel='stylesheet prefetch' href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css'>

      
        
        <!--<link rel="stylesheet" type="text/css" href="map3d/css/style_breadcrumb.css">-->
    

    <!--<link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon-76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/img/favicon-120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/img/favicon-152.png">-->
    <link rel="icon" sizes="196x196" href="images/logo01.png">
    <link rel="icon" type="image/x-icon" href="images/irrigation.ico">

    <link href="fontsth/thsarabunnew/thsarabunnew.css" rel="stylesheet" type="text/css" media="screen" />

    <style>
       
        
     


#water2_test {
  width: 50px;
  height: 50px;
  position: absolute;
  background-color: red;
}



   #water1x {
	   
	
    
    stroke: #000;
    stroke-width: 0.5px;
    stroke-dasharray: 870;
    stroke-dashoffset: 870;
    animation: draw 10s infinite linear;
	
  }
  @keyframes draw {
  to {
	 
	 
	  
	  
    stroke-dashoffset: 0;
  }
  
 
}



 #water2,#water2-5{
  animation: td_130 1.5s infinite linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}

@keyframes td_130{



0% {
    
    
    opacity: 0;
    filter: alpha(opacity=0);
  }
  10% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
    90% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
  100% {
   
    transform:translate(0px,130px);
    opacity: 0;
    filter: alpha(opacity=0);
  }
  
  
  
  
  
  
  
  
  
 

  


}



@keyframes td_150{



0% {
    
    
    opacity: 0;
    filter: alpha(opacity=0);
  }
  10% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
    90% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
  100% {
   
    transform:translate(0px,150px);
    opacity: 0;
    filter: alpha(opacity=0);
  }
  
  
  
  
  
  
  
  
  
 

  


}



#water2-2,#water2-6,#water2-7{
  animation: td_30 1.2s infinite linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}

@keyframes td_30{



0% {
    
    
    opacity: 0;
    filter: alpha(opacity=0);
  }
  10% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
    90% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
  100% {
   
    transform:translate(0px,30px);
    opacity: 0;
    filter: alpha(opacity=0);
  }
  
  
  
  
  
  
  
  
  
 

  


}



#water2-3,#water2-4{
  animation: td_50 1.2s infinite linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}

@keyframes td_50{



0% {
    
    
    opacity: 0;
    filter: alpha(opacity=0);
  }
  10% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
    90% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
  100% {
   
    transform:translate(0px,50px);
    opacity: 0;
    filter: alpha(opacity=0);
  }
  
  
  
  
  
  
  
  
  
 

  


}



#w{
  animation: td_100 1.8s infinite linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}

@keyframes td_100{



0% {
    
    
    opacity: 0;
    filter: alpha(opacity=0);
  }
  10% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
    90% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
  100% {
   
    transform:translate(0px,100px);
    opacity: 0;
    filter: alpha(opacity=0);
  }
  
  
  
  
  
  
  
  
  
 

  


}




#water2-23,#water2-20,#water2-18,#water2-15,#water2-10{
  animation: dt_100 1.6s infinite linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}

@keyframes dt_100{



0% {
    
    
    opacity: 0;
    filter: alpha(opacity=0);
  }
  10% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
    90% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
  100% {
   
    transform:translate(0px,-100px);
    opacity: 0;
    filter: alpha(opacity=0);
  }
  
  
  
  
  
  
  
  
  
 

  


}



#water2-8,#water2-12,#water2-14,#water2-22{
  animation: dt_70 1.4s infinite linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}

@keyframes dt_70{



0% {
    
    
    opacity: 0;
    filter: alpha(opacity=0);
  }
  10% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
    90% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
  100% {
   
    transform:translate(0px,-70px);
    opacity: 0;
    filter: alpha(opacity=0);
  }
  
  
  
  
  
  
  
  
  
 

  


}




#water2-9,#water2-11,#water2-13,#water2-16,#water2-17{
  animation: dt_50 2.0s infinite linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}

@keyframes dt_50{



0% {
    
    
    opacity: 0;
    filter: alpha(opacity=0);
  }
  10% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
    90% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
  100% {
   
    transform:translate(0px,-50px);
    opacity: 0;
    filter: alpha(opacity=0);
  }
  
  
  
  
  
  
  
  
  
 

  


}


#water2-21,#water2-19{
  animation: dt_20 1.5s infinite linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}

@keyframes dt_20{



0% {
    
    
    opacity: 0;
    filter: alpha(opacity=0);
  }
  10% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
    90% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
  100% {
   
    transform:translate(0px,-20px);
    opacity: 0;
    filter: alpha(opacity=0);
  }
  
  
  
  
  
  
  
  
  
 

  


}




#water1_2,#water1_2-2,#water1_2-3{
  animation: lr_100 1.8s infinite linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}

@keyframes lr_100{

  
  
0% {
   
	transform:translate(0px);
	
	opacity: 0.1; 
	
	
  }
  
  10% {
   
	
	opacity: 1; 
	
	
	
  }
  
   90% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
  
  100% {
  
	transform: translateX(100px)  translateY(0px);
	
	
    opacity: 0;
    filter: alpha(opacity=0);
	
	
  }
  


}






 #water1_2-5,#water1_2-4{
  animation: lr_50 1.8s infinite linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}


@keyframes lr_50{

  
  
0% {
   
	transform:translate(0px);
	
	opacity: 0.1; 
	
	
  }
  
  10% {
   
	
	opacity: 1; 
	
	
	
  }
  
   90% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
  
  100% {
  
	transform: translateX(50px)  translateY(0px);
	
	
    opacity: 0;
    filter: alpha(opacity=0);
	
	
  }
  


}



 #w{
  animation: rl_100 1.8s infinite linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}


@keyframes rl_100{

  
  
0% {
   
	transform:translate(0px);
	
	opacity: 0.1; 
	
	
  }
  
  10% {
   
	
	opacity: 1; 
	
	
	
  }
  
   90% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
  
  100% {
  
	transform: translateX(-100px)  translateY(0px);
	
	
    opacity: 0;
    filter: alpha(opacity=0);
	
	
  }
  


}


 #w{
  animation: rl_70 1.8s infinite linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}


@keyframes rl_70{

  
  
0% {
   
	transform:translate(0px);
	
	opacity: 0.1; 
	
	
  }
  
  10% {
   
	
	opacity: 1; 
	
	
	
  }
  
   90% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
  
  100% {
  
	transform: translateX(-70px)  translateY(0px);
	
	
    opacity: 0;
    filter: alpha(opacity=0);
	
	
  }
  


}




 #water1_2-6,#water1_2-7{
  animation: rl_50 1.2s infinite linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}


@keyframes rl_50{

  
  
0% {
   
	transform:translate(0px);
	
	opacity: 0.1; 
	
	
  }
  
  10% {
   
	
	opacity: 1; 
	
	
	
  }
  
   90% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
  
  100% {
  
	transform: translateX(-50px)  translateY(0px);
	
	
    opacity: 0;
    filter: alpha(opacity=0);
	
	
  }
  


}








#water3_1{
  animation: animt_b_2 2.5s infinite linear  ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}

#water3_2{
  animation: animt_b_1 2.5s infinite linear  ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}



#water1_4{
  animation: animt_b_1 0.7s infinite linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}



 #water3{
  animation: animt_b2_1 1.8s infinite linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}


 #water4,#water5{
  animation: animt_b2_2 1.4s infinite linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}




#water9x{
  animation: animt_b2 0.8s infinite linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}

 #water2_1,#water2_2,#water2_3,#water2_4,#water2_5,#water2_6,#water2_7,#water2_8,#water2_9,#water2_10{
  animation: animt_b3 1s infinite linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}

/*#m2,#m4,#m6,#m8,#m10,#m12,#m14,#m16,#m18{
	
  animation: scale1  1.5s  linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}


#m1,#m3,#m5,#m6,#m7,#m9,#m11,#m13,#m15,#m17,#m19,#m4_2{
	
  animation: scale1  1.5s  linear ;
 animation-direction: normal;
  transform-origin: 50% 50%;
  
  
  
 
}*/





 #water2x{
  
  animation: animationFrames 3s ease infinite;
  transform-origin: 100% 100%;
  
  
 
}

@keyframes animt_b{



0% {
    
    
    opacity: 0;
    filter: alpha(opacity=0);
  }
  10% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
    90% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
  100% {
   
    transform:translate(0px,300px);
    opacity: 0;
    filter: alpha(opacity=0);
  }
  
  
  
  
  
  
  
  
  
 

  


}

@keyframes animt_b_1{



0% {
    
    
    opacity: 0;
    filter: alpha(opacity=0);
  }
  10% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
    90% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
  100% {
   
    transform:translate(0px,60px);
    opacity: 0;
    filter: alpha(opacity=0);
  }
  
  
  
  
  
  
  
  
  
 

  


}

@keyframes animt_b_2{



0% {
    
    
    opacity: 0;
    filter: alpha(opacity=0);
  }
  10% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
    90% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
  100% {
   
    transform:translate(0px,-60px);
    opacity: 0;
    filter: alpha(opacity=0);
  }
  
  
  
  
  
  
  
  
  
 

  


}

@keyframes animt_b_1_old{

  
  
0% {
   
	transform:translate(0px);
	opacity: 0.1; 
	
	
	
  }
  
  50% {
   
	
	opacity: 1; 
	
	
	
  }
  
  100% {
    
	transform:translate(0px,5px);
	
	
  }
  


}


@keyframes animt_b2{

  
  
0% {
   
	transform:translate(0px);
	
	opacity: 0.1; 
	
	
  }
  
  10% {
   
	
	opacity: 1; 
	
	
	
  }
  
   90% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
  
  100% {
  
	transform: translateX(170px)  translateY(0px);
	
	
    opacity: 0;
    filter: alpha(opacity=0);
	
	
  }
  


}


@keyframes animt_b2_1{

  
  
0% {
   
	transform:translate(0px);
	
	opacity: 0.1; 
	
	
  }
  
  10% {
   
	
	opacity: 1; 
	
	
	
  }
  
   90% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
  
  100% {
  
	transform: translateX(120px)  translateY(0px);
	
	
    opacity: 0;
    filter: alpha(opacity=0);
	
	
  }
  


}

@keyframes animt_b2_2{

  
  
0% {
   
	transform:translate(0px);
	
	opacity: 0.1; 
	
	
  }
  
  10% {
   
	
	opacity: 1; 
	
	
	
  }
  
   90% {
	  
    opacity: 1;
	
    -ms-filter: none;
    filter: none;
  }
  
  
  100% {
  
	transform: translateX(25px)  translateY(0px);
	
	
    opacity: 0;
    filter: alpha(opacity=0);
	
	
  }
  


}




@keyframes animt_b3{

  
  

  
  
  
  
  0% {
    
    
    opacity: 0;
    filter: alpha(opacity=0);
  }
  50% {
    opacity: 1;
    -ms-filter: none;
    filter: none;
  }
  100% {
   
   transform:translate(-1px,0px);
    opacity: 0;
    filter: alpha(opacity=0);
  }
  
  
  


}

@-moz-keyframes animationFrames{
  0% {
    -moz-transform:  translate(10px) ;
  }
  100% {
    -moz-transform:   translate(100px,100px) ;
	
  }
}

@-webkit-keyframes animationFrames {
  0% {
    -webkit-transform:   translate(10px);
  }
  100% {
    -webkit-transform:    translate(100px,100px);
  }
}

@-o-keyframes animationFrames {
  0% {
    -o-transform:   translate(10px);
  }
  100% {
    -o-transform:   translate(100px,100px) ;
  }
}

@-ms-keyframes animationFrames {
  0% {
    -ms-transform:    translate(10px) ;
  }
  100% {
    -ms-transform:    translate(100px,100px) ;
  }
}


@keyframes scale1{

  

  
  
  
  
 0% {
    
    transform: scale(0.1, 0.1);
    opacity: 0;
    filter: alpha(opacity=0);
  }
  50% {
    opacity: 1;
    -ms-filter: none;
    filter: none;
  }
  100% {
   
    transform: scale(1.2, 1.2);
    opacity: 0;
    filter: alpha(opacity=0);
  }


}




  

    </style>
    
    
  
    
   
</head>

<body>
    
    
    
    


  <div id="wrap">

			<!-- Site header -->
			<header id="header">
				
			</header>

			<!-- Site content -->
			<div id="content">
				
				<section id="map-section" class="inner over">
				
					<!-- Map -->
					<div class="map-container">
						
                        
                        
                        
                        
                        <div id="mapplic">   </div>
                        
                        
                       
                        
                        
						<div id="mapplic2"></div>
                        
                        
                        
                  

<br />



                        
                        
					</div>

					
				</section>

			
			</div>

			<!-- Site footer -->
			
		</div>

    


 <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
<script src='http://oss.maxcdn.com/libs/snapsvg/0.1.0/snap.svg-min.js'></script>



<!--<script type="text/javascript" src="map3d/js/jquery.min.js"></script>
-->
    <!--<script src="libraries/jquery/scripts/jquery-3.1.1.min.js"></script>-->
    <!--google api  -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA7JyQX3nde0OcFkgf6wEtz8JLtNmiWGw8" async defer></script>


    <script src="libraries/bootstrap/js/bootstrap.min.js"></script>




	
		<script type="text/javascript" src="map3d/js/hammer.min.js"></script>
		<script type="text/javascript" src="map3d/js/jquery.easing.js"></script>
		<script type="text/javascript" src="map3d/js/jquery.mousewheel.js"></script>
		<script type="text/javascript" src="map3d/js/smoothscroll.js"></script>
		<script type="text/javascript" src="map3d/mapplic/mapplic.js"></script>
        
        <script type="text/javascript" src="http://ricostacruz.com/jquery.transit/jquery.transit.min.js"></script>
        
        
        
        
       
        
        <script type="text/javascript">
			$(document).ready(function() {
				
				
				
				
				
	var mapplic = $('#mapplic').mapplic({
					source: 'map3d/overall.json',
					height: 900,
					sidebar: true,
					minimap: true,
					markers: true,
					fullscreen: true,
					smartip: false,
					hovertip: true,
					developer: false,
					maxscale: 2,
					zoom: false,
					
				});


	//$("#water2").hide();
				/* Landing Page */
			
	
	
	 $('#gohome').click(function() {  
       alert( "Handler for .click() called." );
    });	
		
		
	function autoupdate(para1) {
		var pa1 = para1;
  setTimeout(function(){
     $(pa1).css('display', 'none');
     setTimeout(function(){
        $(pa1).css('display', 'block');
        setTimeout(autoupdate(pa1), 50);
     }, 1000)
  }, 300)
}

$(function(){ //same as $(document).ready(function(){
  setTimeout(autoupdate('#water2_test'), 50);
   
   //$("#water1").transition({ x: 200 });
   
  // setInterval(autoupdate,20);
});
		




		
	//	 animateSVG();
		 
		 	
	//var animateSVG = animateSVG();
	 // ---------
  // SVG 
		
				
			});
			
			
			
	

		</script>
        
        
       
		
        


</body>
</html>
