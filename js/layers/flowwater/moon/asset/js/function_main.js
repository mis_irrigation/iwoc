


//*********************************************************************************************************************************//

function block(id, msg) {
    msg = (msg != undefined) ? msg : '';
    var z_index99 = 'z_index99';
    if (id == 'body' || id == '' || id == undefined) { //ถ้าไม่ได้ใส่ชื่อ ID ให้แสดง block คลุมทั้งหน้า
        id = 'body';
        z_index99 = '';
    }
    $(id).css('position', 'relative');
    $(id).children('#div_loading').remove();
    $(id).append(' <div id="div_loading" class="spinner_overlay  ' + z_index99 + ' "> <div class="spinner center"> <div class="spinner-blade"></div> <div class="spinner-blade"></div> <div class="spinner-blade"></div> <div class="spinner-blade"></div> <div class="spinner-blade"></div> <div class="spinner-blade"></div> <div class="spinner-blade"></div> <div class="spinner-blade"></div> <div class="spinner-blade"></div> <div class="spinner-blade"></div> <div class="spinner-blade"></div> <div class="spinner-blade"></div> <div class="spinner_text">' + msg + '</div> </div> </div> ');
}
function unblock(id){
	if(id=='body' || id=='' || id==undefined){
		id='body';
	}
	$(id).find('#div_loading').remove();	
}




function empty(e) {
  switch (e) {
    case "":
    case 0:
    case "0":
    case null:
    case false:	
	case undefined:
    case typeof this == "undefined":
      return true;
    default:
      return false;
  }
}



function number_format(number, decimals) {

	 dec_point  = '.';
	 thousands_sep = ',';
     n = number
	 prec = decimals;

		var toFixedFix = function (n,prec) {
        var k = Math.pow(10,prec);
        return (Math.round(n*k)/k).toString();
    };
 
    n = !isFinite(+n) ? 0 : +n;
    prec = !isFinite(+prec) ? 0 : Math.abs(prec);
    var sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep;
    var dec = (typeof dec_point === 'undefined') ? '.' : dec_point;
 
    var s = (prec > 0) ? toFixedFix(n, prec) : toFixedFix(Math.round(n), prec); //fix for IE parseFloat(0.55).toFixed(0) = 0;
 
    var abs = toFixedFix(Math.abs(n), prec);
    var _, i;
 
    if (abs >= 1000) {
        _ = abs.split(/\D/);
        i = _[0].length % 3 || 3;
 
        _[0] = s.slice(0,i + (n < 0)) +
              _[0].slice(i).replace(/(\d{3})/g, sep+'$1');
        s = _.join(dec);
    } else {
        s = s.replace('.', dec);
    }
 
    var decPos = s.indexOf(dec);
    if (prec >= 1 && decPos !== -1 && (s.length-decPos-1) < prec) {
        s += new Array(prec-(s.length-decPos-1)).join(0)+'0';
    }
    else if (prec >= 1 && decPos === -1) {
        s += dec+new Array(prec).join(0)+'0';
    }
	return s;
}



