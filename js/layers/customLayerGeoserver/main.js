var cst_onmaptop = {};
var objdate = new Date();
$(document).ready(function() {
    for (var sourcei_mncst in cst_config) {
        for (var sub_mncst in cst_config[sourcei_mncst]) {
            cst_event[cst_config[sourcei_mncst][sub_mncst].name] = sourcei_mncst + "@" + sub_mncst;
            source[sub_mncst]['layer'] = new L.LayerGroup();
        }
    }
    map.on('click', function(e) {
        if (getlastObject() != false) {
            var arg = getlastObject();
            var ex_arg = arg.split("@");
            var _group = ex_arg[0];
            var _node = ex_arg[1];
            var _point = 'POINT(' + e.latlng.lng + ' ' + e.latlng.lat + ')';
            var _cql = 'intersects(' + cst_config[_group][_node].geometric + ',' + _point + ')';
            var _popup = L.popup().setLatLng(e.latlng);
            $.ajax({
                url: cst_config[_group][_node].url,
                name: cst_config[_group][_node].name,
                jsonp: "callback",
                jsonpCallback: "parseResponse",
                dataType: "jsonp",
                fields: cst_config[_group][_node].fields,
                data: {
                    version: "1.0.0",
                    request: "GetFeature",
                    typeName: cst_config[_group][_node].layers,
                    CQL_FILTER: _cql,
                    service: "WFS",
                    outputFormat: "text/javascript",
                },
                // Work with the response
                success: function(data) {
                    if (data.totalFeatures > 0) {
                        var _vinfo = data.features[0].properties;
                        var content = '';
                        content += '<div><b>' + this.name + '</b></div>';
                        content += '<div>';
                        content += '<table>';
                        content += '<tbody>';
                        for (var cst_vinfo in this.fields) {
                            if (_vinfo[cst_vinfo] != undefined) {
                                content += "<tr><td>" + this.fields[cst_vinfo] + "&nbsp;</td><td>" + _vinfo[cst_vinfo] + "</td></tr>";
                            }
                        }
                        content += '</tbody>';
                        content += '</table>';
                        content += '</div>';
                        _popup.setContent(content).openOn(map);
                    }
                }
            });
        }
    });
    /*//even auto-click */
    //-- console.log( $(".menu-item-checkbox span").length );
    checkOnClickMenu();
});

function checkOnSubClickMenu() {
    location.search.substr(1).split("&").forEach(function(item) {
        stmp = item.split("=");
        if (stmp[0].substring(0, 5) == 'click' && stmp[0] != 'click') {
            var sprm = decodeURIComponent(stmp[1]);
            if (sprm != null) {
                $(".menu-item-checkbox span").each(function(index) {
                    if (sprm == index) { //sprm.trim() == $( this ).text().trim() ){
                        $(this).parent().find(".leaflet-control-layers-selector").click();
                    }
                });
            }
        }
    });
}

function checkOnClickMenu() {
    if ($(".menu-item-checkbox span").length > 0) {
        var prm = findGetParameter("click");
        if (prm != null) {
            $(".menu-item-checkbox span").each(function(index) {
                if (prm == index) { //prm.trim() == $( this ).text().trim() ){
                    //-- console.log(prm.trim());
                    $(this).parent().parent().parent().find(".menu").click();
                    $(this).parent().find(".leaflet-control-layers-selector").click();
                    checkOnSubClickMenu();
                }
            });
        }
    } else {
        setTimeout(function() {
            //-- console.log("call - [fnc]OnClickMenu; "+objdate.getMinutes()+":"+objdate.getSeconds());
            checkOnClickMenu();
        }, 500);
    }
}

function getInfoCustomLayerGeoData(lat, lng) {
    var strPOINT = 'POINT(' + lng + ' ' + lat + ')';
    var content_return = "";
    var vsourct = {};
    var nsourct = 0;
    $("#csinfo_body_dialog").html('');
    for (var sourcei_mncst in cst_config) {
        for (var sub_mncst in cst_config[sourcei_mncst]) {
            if (cst_config[sourcei_mncst][sub_mncst].public == true) {
                vsourct[nsourct] = sourcei_mncst + "@" + sub_mncst;
                nsourct++;
            }
        }
    }
    //Call start
    var randomid = "ID" + Date.now() + Math.random();
    randomid = randomid.replace(".", "_");
    $("#csinfo_body_dialog").html("<div id=" + randomid + "></div><div id='divloading' style='margin-top: 10px;'><i class='fa fa-circle-o-notch fa-spin' aria-hidden='true'></i> Loading..</div>");
    getInfoPoint(strPOINT, vsourct, 0, randomid);
}

function getInfoPoint(strPOINT, vsourct, nsourct, randomid) {
    if ($("#csinfo_body_dialog #" + randomid).html() == undefined) {
        return false;
    } else if (!vsourct[nsourct]) {
        $("#divloading").hide();
        return false;
    } else {
        var ex_split = vsourct[nsourct].split("@");
        var sourcei_mncst = ex_split[0];
        var sub_mncst = ex_split[1];

        var str_cql = 'intersects(' + cst_config[sourcei_mncst][sub_mncst].geometric + ',' + strPOINT + ')';
        var str_url = cst_config[sourcei_mncst][sub_mncst].url;
        var str_layers = cst_config[sourcei_mncst][sub_mncst].layers;
        var str_name = cst_config[sourcei_mncst][sub_mncst].name;
        var var_fields = cst_config[sourcei_mncst][sub_mncst].fields;
        if ($('.cst-model-geosever').html() != undefined && $("#mn-info-customlayer").html() == undefined) {
            var mncst_content = "";
            mncst_content += '<li id="mn-info-customlayer">';
            mncst_content += '<a href="#featureBigTeleAMR-WL" data-toggle="tab">';
            mncst_content += '<i class="fa fa-info-circle" aria-hidden="true"></i> ข้อมูลพื้นฐานชลประทาน';
            mncst_content += '</a>';
            mncst_content += '</li>';

            mncst_content += '<script>';
            mncst_content += '$(document).ready(function () {';
            mncst_content += '$( "#mn-info-customlayer" ).click(function() {';
            mncst_content += '$(\'.other-dialog\').hide();';
            mncst_content += '$(\'#csinfo_body_dialog\').show();';
            mncst_content += '});';
            mncst_content += '});';
            mncst_content += '</script>';
            $('.cst-model-geosever #aboutBigTeleAMRTabs').append(mncst_content);
            $("#csinfo_body_dialog").hide();
        }
        $.ajax({
            url: str_url,
            titlename: str_name,
            fields: var_fields,
            jsonpCallback: "parseResponse",
            dataType: "jsonp",
            data: {
                version: "1.0.0",
                request: "GetFeature",
                typeName: str_layers,
                CQL_FILTER: str_cql,
                service: "WFS",
                outputFormat: "text/javascript",
            },
            success: function(data) {
                if (data.totalFeatures > 0) {
                    if (!data.features == false && !data.features[0] == false && !data.features[0].properties == false) {
                        var _vinfo = data.features[0].properties;
                        var cst_content = "";
                        var isdata = false;
                        cst_content += '<table style="margin-top: 10px;" class="table table-striped table-bordered table-condensed">';
                        cst_content += '<tbody>';
                        cst_content += '<tr><th colspan="2" class="text-center">' + this.titlename + '</th></tr>';
                        for (var cst_vinfo in this.fields) {
                            if (_vinfo[cst_vinfo] != undefined) {
                                isdata = true;
                                cst_content += "<tr><td>" + this.fields[cst_vinfo] + "</td><td>" + _vinfo[cst_vinfo] + "</td></tr>";
                            }
                        }
                        cst_content += '</tbody>';
                        cst_content += '</table>';
                        if (isdata == true) {
                            $("#csinfo_body_dialog #" + randomid).append(cst_content);
                        }
                        getInfoPoint(strPOINT, vsourct, nsourct + 1, randomid);
                    }
                }
            }
        });
    }
}

function getCustomLayerGeoData(arg, callback) {
    var ex_arg = arg.split("@");
    var group = ex_arg[0];
    var node = ex_arg[1];
    var type = cst_config[group][node].type;
    cst_config[group][node].open = true;
    if (!cst_config[group][node].fields == false) {
        cst_onmaptop[node] = arg;
    }
    if (!cst_config[group][node].setzoom == false) {
        map.setView([cst_config[group][node].setzoom.lat, cst_config[group][node].setzoom.lon], cst_config[group][node].setzoom.zoom);
    }
    if (type == 'wms' && source[node].tileLayer == undefined) {
        block('#map');
        source[node].tileLayer = L.tileLayer.wms(cst_config[group][node].url, {
            layers: cst_config[group][node].layers,
            transparent: true,
            format: 'image/png'
        }).addTo(source[node].layer);
        unblock('#map');
    } else if (type == 'tileLayer' && source[node].tileLayer == undefined) {
        block('#map');
        source[node].tileLayer = L.tileLayer(cst_config[group][node].url + "&zoom={z}&x={x}&y={y}&format=image/png", {
            attribution: sub_mncst,
            maxZoom: 17,
            minZoom: 2
        }).addTo(source[node].layer);
        unblock('#map');
    } else if (type == 'json' && source[node].geoJson == undefined) {
        block('#map');
        $.ajax({
            url: cst_config[group][node].url,
            jsonp: "callback",
            jsonpCallback: "parseResponse",
            dataType: "jsonp",
            data: {
                version: "1.0.0",
                request: "GetFeature",
                typeName: cst_config[group][node].layers,
                styles: "pointTelemetry", //style_from_geoserver
                service: "WFS",
                outputFormat: "text/javascript",
            },
            // Work with the response
            success: function(data) {
                if (data !== null) {
                    source[node].geoJson = L.geoJSON(data, {
                        group: group,
                        node: node,
                        onEachFeature: onEachCustomLayer,
                        pointToLayer: function(feature, latlng) {
                            var violetMarker = L.marker(latlng, {
                                icon: blueIcon
                            });
                            return violetMarker;
                        }
                    }).addTo(source[node].layer);
                    unblock('#map');
                }
            }
        });
    }
    //legend
    if (!cst_config[group][node].legend == false) {
        var clegend = cst_config[group][node].legend;
        cst_legend[clegend].legend.addTo(map);
        if (!cst_legend[clegend].on) {
            cst_legend[clegend].on = 1;
        } else {
            cst_legend[clegend].on++;
        }
    }
}

function onEachCustomLayer(feature, layer) {
    var _group = this.group;
    var _node = this.node;
    if (!feature.properties[cst_config[_group][_node].key] != true) {
        var _id = feature.properties[cst_config[_group][_node].key];
        layer.on({
            click: function(e) {
                $.ajax({
                    url: cst_config[_group][_node].url,
                    jsonp: "callback",
                    jsonpCallback: "parseResponse",
                    dataType: "jsonp",
                    fields: cst_config[_group][_node].fields,
                    titlename: cst_config[_group][_node].titlename,
                    data: {
                        version: "1.0.0",
                        request: "GetFeature",
                        typeName: cst_config[_group][_node].layers,
                        CQL_FILTER: cst_config[_group][_node].key + "=" + _id,
                        service: "WFS",
                        outputFormat: "text/javascript",
                    },
                    // Work with the response
                    success: function(data) {
                        if (data.totalFeatures > 0) {
                            var _vinfo = data.features[0].properties;
                            var content = '';
                            content += '<div class="modal-dialog">';
                            content += '<div class="modal-content">';
                            content += '<div class="modal-header">';
                            content += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
                            content += '<h4 class="modal-title" id="featuretab-title">' + this.titlename + '</h4>';
                            content += '</div>';
                            content += '<div class="modal-body" id="featuretab-info ">';
                            content += '<table style="margin-top: 10px;" class="table table-striped table-bordered table-condensed">';
                            content += '<tbody>';
                            for (var cst_vinfo in this.fields) {
                                if (_vinfo[cst_vinfo] != undefined) {
                                    content += "<tr><td>" + this.fields[cst_vinfo] + "</td><td>" + _vinfo[cst_vinfo] + "</td></tr>";
                                }
                            }
                            content += '</tbody>';
                            content += '</table>';
                            content += '</div>';
                            content += '</div>';
                            $('#iwocModal').html(content).modal("show");
                        }
                    }
                });
            }
        });
    }
}

function overlayCustomLayerGeoData(arg, callback) {
    var ex_arg = arg.split("@");
    var group = ex_arg[0];
    var node = ex_arg[1];
    cst_config[group][node].open = false;
    if (!cst_onmaptop[node] == false) {
        delete cst_onmaptop[node];
    }
    if (!cst_config[group][node].legend == false) {
        var clegend = cst_config[group][node].legend;
        if (cst_legend[clegend].on == 1) {
            cst_legend[clegend].on = 0;
            map.removeControl(cst_legend[clegend].legend);
        } else if (cst_legend[clegend].on > 0) {
            cst_legend[clegend].on--;
        }
    }
}

function getlastObject() {
    if (Object.keys(cst_onmaptop).length > 0) {
        var last = "";
        for (var ino in cst_onmaptop) {
            last = cst_onmaptop[ino];
        }
        return last;
    } else {
        return false;
    }
}

function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search.substr(1).split("&").forEach(function(item) {
        tmp = item.split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    });
    return result;
}