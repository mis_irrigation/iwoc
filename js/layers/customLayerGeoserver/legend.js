$(document).ready(function () {
  //test
  cst_legend['test'] = {};
  cst_legend['test'].legend = L.control({ position: 'bottomright' });
  cst_legend['test'].legend.onAdd = function (map) {
      var div = L.DomUtil.create('div', 'info legend'),
          grades = [0, 30, 50, 80],
          labels = [],
          from, to;
      var statusBigTele = { "#00BD06":"ปกติ", "#FFDB43":"เตือนภัย", "#D90022":"วิกฤติ", "#006DF1":"การสื่อสารขัดข้อง"};
      $.each(statusBigTele, function( color, value ) {
          labels.push('<div style="margin-bottom: 2px;"><i style="background:' + color + '"></i> ' + value + '</div>');
      });
      div.innerHTML = labels.join('');
      return div;
  };
});