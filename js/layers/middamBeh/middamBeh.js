$(document).ready(function () {


    source.middamBeh.layer = new L.LayerGroup();


});//ready

function getLayermiddamBehData(callback) {
    if (source.middamBeh.geoJson == undefined) {
        block('#map');

        $.getJSON("http://gis.rid.go.th/damsafety/services/getGeoJson.php?projtype=2", function (data) {
            if (data != null) {
                source.middamBeh.geoJson = L.geoJSON(data, {

                    style: function (feature) {
                        return feature.properties && feature.properties.style;
                    },
                    onEachFeature: onEachmiddamBehFeature,
                    pointToLayer: function (feature, latlng) {
                        var smallIcon = L.icon({
                            iconSize: [30, 30],
                            iconAnchor: [13, 27],
                            popupAnchor: [1, -24],
                            iconUrl: 'images/middamBeh/middamBeh.png'
                        });
                        return L.marker(latlng, { icon: smallIcon });
                    }
                }).bindTooltip(function (e) {
                            return '' + e.feature.properties.projname;
                        }, {
                            direction: 'top',
                            offset: L.point(0, -20)
                        }).addTo(source.middamBeh.layer);
            }

        })
        .done(function () {
             callback($(source.middamBeh.geoJson).length);
        })
        .fail(function (res) {
            alert('middamBeh request failed! '+ JSON.stringify(res,null,'\t') );
        })
        .always(function () {
            unblock('#map');
        });

    } else {
        callback($(source.middamBeh.geoJson).length);
    }
}

function onEachmiddamBehFeature(feature, layer) {

    if (feature.properties && feature.properties.projname) {
        layer.on({
            //mouseover: highlightFeature,
            //mouseout: resetHighlight,
            click: function (e) {
                //console.warn(feature.properties.cresv);
                //$("#featuretab-info").html('');
                block('#map');
                $.get("js/layers/middamBeh/modal.html",
                    function (data) {
                        $('#iwocModal').html(data);
                      //  $('#modal').html(data);

                       // console.log(feature.properties.TMI_ID);
                        // $('#rsvmiddle,#cresv_s').val(feature.properties.cresv);
                        // $('#date_start_table').val('01' + chg_date_th(feature.properties.daily_date).substr(2, 8));
                        // $('#date_end_table').val(chg_date_th(feature.properties.daily_date));
                        // $('#date_start').val(chg_date_th(moment(feature.properties.daily_date).add(-3, 'years').format('YYYY-MM-DD')));
                        // $('#date_end').val(chg_date_th(feature.properties.daily_date));
                        //---------------------------------------------------------------------------
                        $("#featuretab-title").html('<strong>เขื่อนขนาดกลาง</strong><span class="glyphicon glyphicon-menu-right"></span> ' + ' <strong>' + feature.properties.projname + '</strong>');
                        var address="หมู่ที่ "+feature.properties.moo
                                    +" บ้าน "+feature.properties.village
                                    +" ตำบล "+feature.properties.tambon
                                    +" อำเภอ "+feature.properties.amphoe
                                    +" จังหวัด "+feature.properties.province
                        $("#address").html(address);
                        $("#basin").html(feature.properties.basin);
                        $("#ridcode").html("สำนักชลประทานที่"+feature.properties.ridcode);
                        $("#projcatego").html(feature.properties.projcatego);
                        $("#year_cons").html(feature.properties.year_cons);
                        $("#year_compl").html(feature.properties.year_compl);
						var bodybah="";
						if(feature.properties.owner_project!=null && feature.properties.owner_project.length>0){
							bodybah="<tr><td>โครงการที่รับผิดชอบ</td><td><span >"
										+feature.properties.owner_project+"</span></td></tr>"
										+"<tr><td>ปีที่ติดตั้ง</td><td><span >"
										+feature.properties.setup_year+"</span></td></tr>"
										+"<tr><td>บรษัทติดตั้ง</td><td><span >"
										+feature.properties.company+"</span></td></tr>"
										+"<tr><td>Link</td><td><span ><a target='_blank' href='"
										+feature.properties.link+"'>"
										+feature.properties.link+"</a></span></td></tr>";
						}else{
							bodybah="<tr><td><center>ไม่พบข้อมูล</center></td></tr>";
						}
						$("#body_bah").html(bodybah);
                        $("#iwocModal").modal("show");
                       // $("#modal").modal("show");
                        unblock('#map');
                    }
                );



            }
        });

    }//if 
}
//getLayerMiddleData();