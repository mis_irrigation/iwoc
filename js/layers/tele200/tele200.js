$(document).ready(function () {
    source.tele200.layer = new L.LayerGroup();


 

});//ready

var stationlist;


function getTele200Data(callback) {
    if (source.tele200.geoJson == undefined) {
        block('#map');
        $.getJSON("http://tele200.rid.go.th/webservice/wstele200geojson.ashx", function (data) {
            if (data !== null) {

               getStation();


                source.tele200.geoJson = L.geoJSON(data, {

                    style: function (feature) {
                        return feature.properties && feature.properties.style;
                    },

                    //style: function (feature) {
                    //    var QTotal = feature.properties.QTotal;
                    //    var fillColor = getColor(QTotal);


                    //    return { opacity: 1, radius: 8, color: "#000", weight: 1, fillColor: fillColor, fillOpacity: 0.8, text: feature.properties.DamName };
                    //},

                    onEachFeature: onEachTele200Feature,

                    //onEachFeature: function( feature, layer ){
                    //    layer.bindPopup( "<strong>" + feature.properties.Name + "</strong><br/>" + feature.properties.density + " rats per square mile" )
                    //}

                    pointToLayer: function (feature, latlng) {
                        //return L.circleMarker(latlng, {
                        //    radius: 8,
                        //    fillColor: getColorTele200(feature.properties.QTotal),
                        //    color: "#000",
                        //    weight: 1,
                        //    opacity: 1,
                        //    fillOpacity: 0.8
                        //className : 'bus_stops'

                        ////////////////
                        var lat;
                        var lng;
                        var latlngNew=null;


                        if (stationlist != null && stationlist.length > 0) {
                  

                            $.each(stationlist, function () {

                                var stationcode = this['StationCode'].replace(".", "");
                                if (stationcode== feature.properties.StationCode) {
                                    lat = this['Latitude'];
                                    lng = this['Longitude'];
                                    latlngNew = L.latLng(lat, lng);
                                    return false;

                                }
                            });
                        }



               

                   
                        
                        //  });//.bindTooltip("My Label", {permanent: true, className: "my-label", offset: [0, 0] });
                        /////////////////////////////////////////////////////////////////
                        if (latlngNew != null) {
                            var greenMarker = L.marker(latlngNew, {

                                icon: greenIcon
                                //  highlight: HLIconTempString//"permanent","temporary"
                            });





                            var markerUSE;

                            if (feature.properties.Last_DataList.length > 0) {

                                //  var detail = "";
                                var wpriority = 0;
                                var rpriority = 0;
                                var apriority = 0;
                                var priority = 0;
                                //var info = "";
                                //var winfo = "";
                                //var rinfo = "";
                                //var ainfo = "";
                                /////////////////////////////////////////
                                /////////////////////////////////////////

                                //   var id = feature.properties.StationCode;
                                var name = feature.properties.StationName;
                                var description = feature.properties.StationDetail;
                                var zerogage = feature.properties.ZeroGage;
                                //  var type = feature.properties.ZeroGage;
                                //  var vdata = markers[i].getAttribute("vdata");
                                //   var maxlv = markers[i].getAttribute("maxlv");
                                //   var minlv = markers[i].getAttribute("minlv");
                                var last_date = feature.properties.Last_DataDate;
                                //   var last_time = markers[i].getAttribute("last_time");
                                var ctime = feature.properties.CTime;
                                // var enabled = markers[i].getAttribute("enabled");
                                var sensor_type = feature.properties.TelemetryTypeString;
                                var wmaxlv = feature.properties.WMaxLevel;
                                var wminlv = feature.properties.WMinLevel;
                                var rmaxlv = feature.properties.RMaxLevel;
                                var rminlv = feature.properties.RMinLevel;
                                var amaxlv = feature.properties.AMaxLevel;
                                var aminlv = feature.properties.AMinLevel;
                                var briverlv = feature.properties.BRiverLevel;
                                var zoneid = feature.properties.UtokName;
                                var stationid = feature.properties.StationCode;
                                var mapid = feature.properties.StationCodeMapping;
                                var province = feature.properties.ProvinceName;
                                var basin = feature.properties.BasinName;
                                //  var point = new GLatLng(parseFloat(markers[i].getAttribute("latitude")), parseFloat(markers[i].getAttribute("longtitude")));

                                var wval = feature.properties.WVal;
                                var rval = feature.properties.RVal;
                                var aval = feature.properties.AVal;
                                // alert(stationid);
                                /**
                                 * Description: ตรวจสอบสิทธิ์ในการเข้าถึงข้อมูลเซนเซอร์
                                 * Modified by: Liang 31/10/2011
                                 */
                                //if ('anonymous' == permission) {
                                //    detail = "<b>รหัสเซนเซอร์ : " + id + "</b><br/>" + name + "<br/>";
                                //}
                                //else {
                                //    detail = "<a target='_blank' href='index.php?page=2&zid=" + zoneid + "&sid=" + stationid + "'><b>รหัสเซนเซอร์ : " + id + "</a></b><br/><a target='_blank' href='index.php?page=2&zid=" + zoneid + "&sid=" + stationid + "'>" + name + "</a><br/>";
                                //}
                                wval += zerogage;

                                if (wmaxlv != -1) {

                                    //   detail = "รหัสเซนเซอร์ : " + stationid + " " + name + "<br/> ระดับน้ำ : " + wval + " ม.(รทก)" + ", ระดับตลิ่ง : " + briverlv + " ม.(รทก)<br/>";
                                    if (wval > briverlv) {
                                        wpriority = 3;

                                    } else if (wval > wmaxlv) {
                                        wpriority = 2;

                                    } else if (wval < wminlv) {
                                        wpriority = 1;

                                    } else {
                                        wpriority = 0;

                                    }
                                }



                                //     alert("hi-5");
                                if (rmaxlv != -1) {

                                    if (rval > rmaxlv) {
                                        rpriority = 3;

                                    } else if (rval > rminlv) {
                                        rpriority = 2;

                                    } else {
                                        rpriority = 0;

                                    }
                                }

                                //   alert("hi-4");
                                if (amaxlv != -1) {

                                    if (aval > amaxlv) {
                                        apriority = 3;

                                    } else if (aval > aminlv) {
                                        apriority = 2;

                                    } else {
                                        apriority = 0;

                                    }
                                }



                                //    alert("hi-3");
                                if (wpriority > rpriority) {
                                    if (wpriority > apriority) {
                                        priority = wpriority;

                                    } else {
                                        priority = apriority;

                                    }
                                } else {
                                    if (rpriority > apriority) {
                                        priority = rpriority;

                                    } else {
                                        priority = apriority;

                                    }
                                }


                                //   alert("hi-1 p:" + priority);
                                if (priority == 0) {
                                    // detail = "<font style='color:green;'>" + detail + "<b>" + info + "</b></font>";
                                    markerUSE = greenMarker;
                                } else if (priority == 1) {
                                    //detail = "<font style='color:black;'>" + detail + "<b>" + info + "</b></font>";
                                    markerUSE = greenMarker;
                                } else if (priority == 2) {
                                    //detail = "<font style='color:blue;'>" + detail + "<b>" + info + "</b></font>";
                                    markerUSE = greenMarker;
                                } else if (priority == 3) {
                                    //  detail = "<font style='color:red;'>" + detail + "<b>" + info + "</b></font>";
                                    markerUSE = greenMarker;
                                } else {
                                    //detail = "<font style='color:red;'>" + detail + "<b>" + info + "</b></font>";
                                    markerUSE = greenMarker;
                                }

                                //    alert("hi");
                            } else {

                                markerUSE = greenMarker;
                                //    detail = "<font style='color:green;'>" + detail + "<b>" + info + "</b></font>";

                                //  alert("hi1");
                            }
                            /////////////////////////////////////////////////////////////////




                            //
                            //    alert("hi");

                            return markerUSE;

                        //return L.marker(latlng, {
                        //    //icon: L.ExtraMarkers.icon({
                        //    //    icon: '',
                        //    //    shape: 'circle',
                        //    //    markerColor: 'green'
                        //    //}),
                        //    icon:greenIcon,
                        //    highlight: "temporary"//"permanent","temporary"
                        //});

                        }
                   
                        //var greyMarker = L.marker(latlngNew, {

                        //    icon: greyIcon
                        //    //  highlight: HLIconTempString//"permanent","temporary"
                        //});

                        ////////////////////////////////////////////////////////////////



                    }

                    //    pointToLayer: function (feature, latlng) {
                    //    return L.marker(latlng, {icon: baseballIcon});
                    // marker.bindPopup(feature.properties.Location + '<br/>' + feature.properties.OPEN_DT);
                    //},

                    //    //filter: function (feature, layer) {
                    //    //    if (feature.properties) {
                    //    //        // If the property "underConstruction" exists and is true, return false (don't render features under construction)
                    //    //        return feature.properties.underConstruction !== undefined ? !feature.properties.underConstruction : true;
                    //    //    }
                    //    //    return false;
                    //    //},


                }).addTo(source.tele200.layer);
            }

        })
        .done(function () {
            callback($(source.tele200.geoJson).length);
        })
        .fail(function () {
            alert('Tele200 request failed! ');
        })
        .always(function () {
            unblock('#map');
        });

    } else {
        callback($(source.tele200.geoJson).length);
    }

}

function tableTele200Waterlevel(last_date, wmaxlv, wminlv, wval, zerogage, briverlv) {
    var html = "";
    var winfo = "";

    wval += zerogage;

    if (wmaxlv != -1) {

        //   detail = "รหัสเซนเซอร์ : " + stationid + " " + name + "<br/> ระดับน้ำ : " + wval + " ม.(รทก)" + ", ระดับตลิ่ง : " + briverlv + " ม.(รทก)<br/>";
        if (wval > briverlv) {

            winfo = "ระดับน้ำล้นตลิ่ง";
        } else if (wval > wmaxlv) {

            winfo = "ระดับน้ำอยู่ในเกณฑ์มาก";
        } else if (wval < wminlv) {

            winfo = "ระดับน้ำอยู่ในเกณฑ์น้อย";
        } else {

            winfo = "ระดับน้ำอยู่ในเกณฑ์ปกติ";
        }

        html = '<table class="table table-striped table-bordered table-condensed">' +
            '<tr><th colspan="2" class="text-center">วันที่ ' + last_date + '</th></tr>' +
            '<tr><td class="success">สถานการณ์</td><td class="success">' + winfo + '</td></tr>' +
            '<tr><td>ระดับน้ำ</td><td>' + commafy(wval, 2) + ' ม.(รทก)</td></tr>' +
            '<tr><td>ระดับตลิ่ง</td><td>' + commafy(briverlv, 2) + ' ม.(รทก)</td></tr>' +
            '<table>';


    }

    return html;

}


function tableTele200Rain(last_date, rmaxlv, rval, rminlv) {
    var html = "";

    var rinfo = "";
    if (rmaxlv != -1) {
        //detail = detail + "ปริมาณน้ำฝน : " + rval + " มม." + ", ระดับวิกฤต : " + rmaxlv + " มม.<br/>";
        if (rval > rmaxlv) {

            rinfo = "ปริมาณน้ำฝนอยู่ในเกณฑ์วิกฤต";
        } else if (rval > rminlv) {

            rinfo = "ปริมาณน้ำฝนอยู่ในเกณฑ์มาก";
        } else {

            rinfo = "ปริมาณน้ำฝนอยู่ในเกณฑ์ปกติ";
        }

        html = '<table class="table table-striped table-bordered table-condensed">' +
            '<tr><th colspan="2" class="text-center">วันที่ ' + last_date + '</th></tr>' +
            '<tr><td class="success">สถานการณ์</td><td class="success">' + rinfo + '</td></tr>' +
            '<tr><td>ปริมาณน้ำฝน</td><td>' + commafy(rval, 2) + ' มม.</td></tr>' +
            '<tr><td>ระดับวิกฤต</td><td>' + commafy(rmaxlv, 2) + ' มม.</td></tr>' +
            '<table>';
    }

    return html;

}



function tableTele200A(last_date, amaxlv, aval, aminlv) {

    var ainfo = "";
    var html = "";
    if (amaxlv != -1) {
        //   detail = detail + "คุณภาพอากาศ : " + aval + ", ระดับวิกฤต : " + amaxlv + "<br/>";
        if (aval > amaxlv) {

            ainfo = "คุณภาพอากาศอยู่ในเกณฑ์วิกฤต";
        } else if (aval > aminlv) {

            ainfo = "คุณภาพอากาศอยู่ในเกณฑ์มาก";
        } else {

            ainfo = "คุณภาพอากาศอยู่ในเกณฑ์ปกติ";
        }

        html = '<table class="table table-striped table-bordered table-condensed">' +
            '<tr><th colspan="2" class="text-center">วันที่ ' + last_date + '</th></tr>' +
            '<tr><td class="success">สถานการณ์</td><td class="success">' + ainfo + '</td></tr>' +
            '<tr><td>คุณภาพอากาศ</td><td>' + commafy(aval, 2) + '</td></tr>' +
            '<tr><td>ระดับวิกฤต</td><td>' + commafy(amaxlv, 2) + '</td></tr>' +
            '<table>';
    }

    return html;

}



function onEachTele200Feature(feature, layer) {
    if (feature.properties && feature.properties.StationCode) {
        var winfo = "";
        var rinfo = "";
        var ainfo = "";
        if (feature.properties.Last_DataList.length > 0) {
            //   var id = feature.properties.StationCode;
            var name = feature.properties.StationName;
            var description = feature.properties.StationDetail;
            var zerogage = feature.properties.ZeroGage;
            //  var type = feature.properties.ZeroGage;
            //  var vdata = markers[i].getAttribute("vdata");
            //   var maxlv = markers[i].getAttribute("maxlv");
            //   var minlv = markers[i].getAttribute("minlv");
            var last_date = feature.properties.Last_DataDate;
            //   var last_time = markers[i].getAttribute("last_time");
            var ctime = feature.properties.CTime;
            // var enabled = markers[i].getAttribute("enabled");
            var sensor_type = feature.properties.TelemetryTypeString;
            var wmaxlv = feature.properties.WMaxLevel;
            var wminlv = feature.properties.WMinLevel;
            var rmaxlv = feature.properties.RMaxLevel;
            var rminlv = feature.properties.RMinLevel;
            var amaxlv = feature.properties.AMaxLevel;
            var aminlv = feature.properties.AMinLevel;
            var briverlv = feature.properties.BRiverLevel;
            var zoneid = feature.properties.UtokName;
            var stationid = feature.properties.StationCode;
            var mapid = feature.properties.StationCodeMapping;
            var province = feature.properties.ProvinceName;
            var basin = feature.properties.BasinName;
            //  var point = new GLatLng(parseFloat(markers[i].getAttribute("latitude")), parseFloat(markers[i].getAttribute("longtitude")));
            var wval = feature.properties.WVal;
            var rval = feature.properties.RVal;
            var aval = feature.properties.AVal;
            // alert(stationid);
            /**
             * Description: ตรวจสอบสิทธิ์ในการเข้าถึงข้อมูลเซนเซอร์
             * Modified by: Liang 31/10/2011
             */
            winfo = tableTele200Waterlevel(last_date, wmaxlv, wminlv, wval, zerogage, briverlv);
            rinfo = tableTele200Rain(last_date, rmaxlv, rval, rminlv);
            ainfo = tableTele200A(last_date, amaxlv, aval, aminlv);
        } else {
            var tempText = "ยังไม่พบข้อมูลเข้ามาในระบบ";
            winfo = tempText;
            rinfo = tempText;
            ainfo = tempText;
        }
        layer.on({
            click: function (e) {
                $.get("js/layers/tele200/modal.html",
                    function (data) {
                        $('#iwocModal').html(data);
                        $("#featureTele200-title").html(stationid + " " + name);
                        $("#featureTele200-WL").html(winfo);
                        $("#featureTele200-R").html(rinfo);
                        $("#featureTele200-A").html(ainfo);
                        var tabID = "featureTele200-WL";
                        if (winfo != "")
                            tabID = "featureTele200-WL";
                        else {
                            if (rinfo != "") {
                                tabID = "featureTele200-R";
                            } else {

                                if (ainfo != "") {
                                    tabID = "featureTele200-A";

                                }
                            }
                        }
                        $('.nav-tabs a[href="#' + tabID + '"]').tab('show');
                        $("#iwocModal").modal("show");
                    }
                );

            }
        });
    }
}




function  getStation ()
{
    ////////////////////
    var HydroObj = new Object();
    HydroObj.hydroid = 0;
    HydroObj.provincecode = 0;


    var DTO = { 'hydro': HydroObj };


  //  var dataret = [];


    $.ajax({
        type: "POST",


        url: "http://hyd-app.rid.go.th/webservice/HDService.svc/getAllStationList",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(DTO),
        //beforeSend: function () {
        //    $('#loadingStation').show();
        //},
        crossDomain: true,
        async: false,
        success: function (msg) {
            ///////////////////////////////////////////


            if (msg.length > 0) {

                //$.each(msg, function () {

                //    //if (this['StationCode'] == "Y.40")
                //    //    alert(this['StationCode']);
                //    dataret.push(msg);

                //});
             
                stationlist = msg;
            }


        },
        complete: function () {

            //return datatret;
           
        },
        error: function (xhr, status, error) {
            alert("An error has occurred during processing: " + error);
        }

         
    });
   

}
    //getTele200Data();
