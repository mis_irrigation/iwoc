L.TileLayer.BetterWMS = L.TileLayer.WMS.extend({

    onAdd: function (map) {
        // Triggered when the layer is added to a map.
        //   Register a click listener, then do all the upstream WMS things        
        L.TileLayer.WMS.prototype.onAdd.call(this, map);
        map.on('click', this.getFeatureInfo, this);
    },

    onRemove: function (map) {
        // Triggered when the layer is removed from a map.
        //   Unregister a click listener, then do all the upstream WMS things
        L.TileLayer.WMS.prototype.onRemove.call(this, map);
        map.off('click', this.getFeatureInfo, this);
    },

    getFeatureInfo: function (evt) {
     
        // Make an AJAX request to the server and hope for the best
        var url = this.getFeatureInfoUrl(evt.latlng);
       var showResults = L.Util.bind(this.showGetFeatureInfo, this);
        $.ajax({
            jsonp: false,
            url: url,
            dataType: 'jsonp',
            jsonpCallback: 'getJson',
            success: function handleJson_featureRequest(data) {
                var err = typeof data === 'object' ? null : data;
                showResults(err, evt.latlng, data);
            }
        });

    },

    getFeatureInfoUrl: function (latlng) {
        // Construct a GetFeatureInfo request URL given a point
        var point = this._map.latLngToContainerPoint(latlng, this._map.getZoom()),
            size = this._map.getSize(),

            params = {
                request: 'GetFeatureInfo',
                service: 'WMS',
                srs: 'EPSG:4326',
                styles: this.wmsParams.styles,
                transparent: this.wmsParams.transparent,
                version: this.wmsParams.version,
                format: this.wmsParams.format,
                bbox: this._map.getBounds().toBBoxString(),
                height: size.y,
                width:  size.x,
                layers: this.wmsParams.layers,
                query_layers: this.wmsParams.layers,
                info_format: 'text/javascript',
                format_options: 'callback:getJson',
                propertyName: 'fieldCode,Owner'
            };

        params[params.version === '1.3.0' ? 'i' : 'x'] = Math.floor(point.x);
        params[params.version === '1.3.0' ? 'j' : 'y'] = Math.floor( point.y);

        return this._url + L.Util.getParamString(params, this._url, true);
    }
  
       
    ,

    showGetFeatureInfo: function (err, latlng, content) {
        if (err) { console.log(err); return; } // do nothing if there's an error

        // Otherwise show the content in a popup, or something.
        if (content.features.length > 0) {
            L.popup({ maxWidth: 800 })
              .setLatLng(latlng)
              .setContent(createPopup(content))
              .openOn(this._map);
        }
        //for (var i = 0; i < data.features.length; i++) {
        //    var feature = data.features[i];
        //    console.log(feature);
        //    txt = txt.concat(L.Util.template("<h2>{appcode}</h2><p>{url}</p>", feature.properties));
        //}
    
  
    }
});



L.tileLayer.betterWms = function (url, options) {
    return new L.TileLayer.BetterWMS(url, options);
};



function createPopup(content) {

    var record;

    var info = "<div >";

    for (var i = 0; i < content.features.length; i++) {

        record = content.features[i];

        info += "<p>"+ record.properties.Owner + "</p><p>" + record.properties.fieldCode + "</p>";
        


    }

    info += "</div>"


    return info;

}